<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Delivery Orders</title>
	<style type="text/css">
		@font-face {
			font-family: 'Helvetica';
		}

		@page {
	        margin-top: 15em;
	        margin-bottom: 5em;
	    }

		.clearfix:after {
			content: "";
			display: table;
			clear: both;
		}

		a {
			color: 000;
			text-decoration: none;
		}

		body {
			position: relative;
			width: 18cm;  
			height: 29.7cm; 
			margin: 0 auto; 
			color: #555555;
			background: #FFFFFF; 
			font-family: Helvetica, sans-serif;
			font-size: 14px;
		}

		header {
			position: fixed;
			float: left;
			left: 10px;
			top: -200px; 
			height: 70px;
			display: table;
		}
		main {
			position: relative;
			left: 10px;
			top: -50px; 
		}



		#company img {
			height: 70px;
		}

		#title {
			/*
			display: table-cell;
			text-align: center;
			width:30%;
			*/
			display: table-cell;
			float: left;
		}

		#qr {
			float: left;
			margin-left: -50px;
			margin-top: 4px;
			display: table-cell;
			width:50%;
		}

		h2.nametitle {
			font-size: 18px;
			font-weight: normal;
			margin: 0;
			text-align: center;
		}
		#company {
			text-align: left;
			display: table-cell;
			width:35%;
		}

		#title img {
			height: 100px;
		}

		#details {
			margin-bottom: 50px;
			margin-top: 0px;
			display: table;
			width: 100%;
		}

		#client {
			padding-left: 6px;
			border-left: 6px solid #0087C3;
			float: left;
			display: table-cell;
			width: 50%;
		}

		#client .to {
			color: #777777;
		}

		h2.name {
			font-size: 1em;
			font-weight: normal;
			margin: 0;
		}



		#invoice {
			text-align: right;
			display: table-cell;
			width: 50%;
		}

		#invoice h1 {
			color: #0087C3;
			font-size: 1em;
			font-weight: normal;
			margin: 0  0 10px 0;
		}

		#invoice .date {
			font-size: 1em;
			color: #777777;
		}

		#signature {
			width: 70%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
			padding: 10px;
		}

		#itemList {
			width: 70%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px;
		}

		#itemList th,
		#itemList td {
			padding: 0px;
			text-align: center;
			border-bottom: 1px solid #FFFFFF;
		}

		#itemList th {
			white-space: nowrap;        
			font-weight: normal;
			border-bottom: 1px solid;
		}

		#itemList td {
			text-align: left;
		}

		#itemList td h3{
			font-weight: normal;
			margin: 0 0 0.2em 0;
		}

		table .mid {
			text-align: center;
		}

		table .number {
			text-align: right;
		}

		table .qty {
			background: #DDDDDD;
		}

		table .total {
			/*background: #57B223;*/
			color: #FFFFFF;
		}

		table tbody tr:last-child td {
			border: none;
		}

		table tfoot td {
			padding: 10px 20px;
			/*background: #FFFFFF;*/
			border-bottom: none;
			/*font-size: 1.2em;*/
			white-space: nowrap;
			border-top: 1px solid #AAAAAA;
		}

		footer {
			color: #777777;
			width: 100%;
			height: 60px;
			position: absolute;
			bottom: 30;
			padding: 8px 0;
			text-align: left;
		}

	</style>
</head>
<body>
	<header>
		<div id="title">
            <img src="{{ public_path('assets/images/logost2.jpg') }}">
		</div>
	</header>
	<main>
		<div style="font-size:15pt;text-align:center;width:60%"><b><u>@if (($data->printed) == 0) TANDA TERIMA @else COPY TANDA TERIMA @endif</u></b></div>
		<div style="font-size:10pt;text-align:center;width:60%">No. : {{ $data->document_no }}</div>
		<br>
		<br>
		<div style="font-size:10pt;width:68%">Telah diterima dari PT. Sushitei Indonesia, pada tanggal {{ 
			Carbon\Carbon::parse($data->date)->format('d - m - Y') }} dengan rincian sebagai berikut : </div>
		<br>
		<table cellspacing="0" cellpadding="0" id='itemList'>
			<thead>
				<tr>
					<th>Item</th>
					<th>Qty.</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($line as $l)
				<tr>
					<td style="font-size:8pt;align:left">{{ $l->asset->code_asset}} -  {{ $l->asset->description_asset}} </td>
					<td style="font-size:8pt;align:left">1 Unit</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<table width="50%">
			<thead>
				<tr>
					<th style="font-size:10pt" border="0">Jumlah Deposit</th>
					<th style="font-size:10pt" border="0">Rp. </th>
				</tr>
			</thead>
		</table>
	</main>
	<footer>
    	<div>Jakarta, {{ Carbon\Carbon::now()->format('d F Y') }}<div>
		<br>
		<div>Yang menyerahkan, <div>
	</footer>
</body>
</html>