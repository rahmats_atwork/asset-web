@extends('template.master')
@section('header')
Barcode Print Configuration
@endsection
@section('content')
<div class="row">
    <fieldset>
        <div class="col-md-12">
            @if ($errors->has('upload_file'))
                <p class="alert alert-danger">{{ $errors->first('upload_file') }}</p>
            @endif
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {!!Session::get('success')!!}
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-warning alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {!!Session::get('error')!!}
                </div>
            @endif

            <form id="form" class="form-horizontal" method="POST" action="{{ url('config/barcode/input') }}">
                <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                <div class="form-group">
                    <div class="controls col-md-5">
                        <label for="measurement">Measurement<font size="3" color="red"> *</font></label>
                        <select id="measurement" name="measurement" required>
                            <option value="px" <?php if($config['measurement']==='px') echo 'selected';?>>pixel (px)</option>
                            <option value="mm" <?php if($config['measurement']==='mm') echo 'selected';?>>milimetre (mm)</option>
                            <option value="cm" <?php if($config['measurement']==='cm') echo 'selected';?>>centimetre (cm)</option>
                            <option value="in" <?php if($config['measurement']==='in') echo 'selected';?>>inch (in)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label for="height">Barcode Height<font size="3" color="red"> *</font></label>
                        <input type="text" id="height" name="height" value="{{ $config['height'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Barcode Width<font size="3" color="red"> *</font></label>
                        <input type="text" name="width" value="{{ $config['width'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Margin Top<font size="3" color="red"> *</font></label>
                        <input type="text" name="margin_top" value="{{ $config['margin-top'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Margin Bottom<font size="3" color="red"> *</font></label>
                        <input type="text" name="margin_bottom" value="{{ $config['margin-bottom'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Margin Left<font size="3" color="red"> *</font></label>
                        <input type="text" name="margin_left" value="{{ $config['margin-left'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Margin Right<font size="3" color="red"> *</font></label>
                        <input type="text" name="margin_right" value="{{ $config['margin-right'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Font Size<font size="3" color="red"> *</font></label>
                        <input type="text" name="font_size" value="{{ $config['font-size'] }}" required />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Top Text</label>
                        <textarea rows="4" name="text_over">{{ $config['text-over'] }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label>Bottom Text</label>
                        <textarea rows="4" name="text_below">{{ $config['text-below'] }}</textarea>
                    </div>
                </div>
                <input type="submit" class="btn btn-warning" name="submit" value="SAVE" />
            </form>
        </div>
    </fieldset>
</div>
@endsection
