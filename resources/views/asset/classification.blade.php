@extends('template.master')
@section('beforehead')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" />
@endsection
@section('header')
Asset Master
@endsection
@section('content')
<div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading icon-container">
                    <a href="{{ url('asset/'.$items->id_asset.'/edit') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                    Create New Asset<span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                        <li><a href="{{ url('asset/'.$items->id_asset.'/edit') }}">Asset Register</a></li>
                        <li class="active"><a href="#">Classification</a></li>
                        </ul>
                    </div>
                    <!-- form start -->
                        {!! csrf_field() !!}
                                <form id="form-create" class="form-horizontal" method="post" action="{{ url('asset/'.$items->id_asset.'/value') }}">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="code_asset" value="{{ $items->code_asset }}" />

                                    @foreach($parameter as $val)
                                    <div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
                                        <div class="col-sm-4">
                                        
                                            <label>{{ $val->name }}</label>
                                            @if ($val->type==2 )
                                            <div class="input-group">
                                            <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control datetime" >
                                            <div class="input-group-addon btn btn-primary">
                                                <i class="mdi mdi-calendar"></i>
                                            </div>
                                            </div>
                                            @elseif ($val->type==4 )
                                            <input type="number" min="1" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
                                            @elseif ($val->type==3 )
                                            <div class="input-group clockpicker">
                                                <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
                                                <div class="input-group-addon btn btn-primary">
                                                    <i class="mdi mdi-time"></i>
                                                </div>
                                            </div>
                                            @elseif ($val->type==5 )
                                            <select id="value" name="parameter[id-{{ $val->id }}]" class="form-control">
                                                <option value="">-- Choose Type Assets --</option>
                                                @foreach(explode(',',$val->value) as $key)
                                                <option {{ $val->existing ==  $key ? 'selected':'' }} value="{{ $key }}">{{ $key }}</option>
                                                @endforeach
                                            </select>
                                                    
                                            @else
                                            <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
                                            @endif
                                        </div>
                                    </div><!-- form group-->

                                    @endforeach		
                                    {{--  <button type="submit" class="btn btn-primary btn-submit">Save</button>  --}}
                                    {{--  <a href="{{ url('asset/'.$items->id_asset.'/edit') }}" class="btn btn-default">Back</a>  --}}
                                    <div class="row xs-pt-15">
                                        <div class="col-xs-12">
                                            <p class="text-right">
                                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                            </p>
                                        </div>
                                    </div>
                                </form>		
                    <!-- form end -->
                </div>
            </div>
        </div>
</div>  


@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $('.datetime').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('.clockpicker').clockpicker({autoclose:true});
	});
</script>
@endsection