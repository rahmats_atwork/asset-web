@extends('template.master')
@section('header')
Asset Master
@endsection
@section('content')
<style>
	.table-fit td, 
	.table-fit th {
		white-space: nowrap;
		width: 1%;
		/* max-width:350px; */
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-table">
			<div class="panel-heading  icon-container">
				<a href="{{ url('asset') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Validate Asset Upload<span class="panel-subtitle"></span>
			</div>

			<div class="panel-body">
					
				<div style="overflow-x:scroll">
					<table class="table table-striped table-fit">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>No</th>
								<th>Material Code</th>
								<th>Asset Number</th>
								<th>Description</th>
								<th>Is Asset</th>
								<th>Depreciation</th>
								<th>Serial Number</th>
								<th>Superior Asset Number</th>
								<th>Old Asset Number</th>
								<th>Vendor</th>
								<th>Plant</th>
								<th>Group Owner</th>
								<th>Asset Type</th>
								<th>Responsible Person</th>
								<th>Location</th>
								<th>Status</th>
								<th>Purchase Date</th>
								<th>Purchase Price</th>
								<th>Currency</th>
								<th>Warranty End Date</th>
								<th>Maintenance Cycle</th>
								<th>Maintenance Unit</th>
								<th>Last Maintenance Date</th>
								<th>Retired Date</th>
								<th>Retired Remarks</th>
							</tr>
						</thead>
						<tbody>
							@foreach($validate as $item)
							<tr>
								<td>
									@if($item['valid'])
									<p class='label label-success'>Data Valid</p>
									@else
									<p class='label label-warning'>Data Invalid</p>
									@endif
								</td>
								<td>
									<?php echo empty($item['message']) ? "" : "<small class='text-danger'>".$item['message']."</small>"; ?>
								</td>
								<td>{{ $item['no'] }}</td>
								<td>{{ $item['material_code'] }}</td>
								<td>{{ $item['code_asset'] }}</td>
								<td>{{ $item['description_asset'] }}</td>
								<td>{{ $item['is_asset_name'] }}</td>
								<td>{{ $item['depreciation'] }}</td>
								<td>{{ $item['serial_number'] }}</td>
								<td>{{ $item['superior_asset_number'] }}</td>
								<td>{{ $item['old_asset_number'] }}</td>
								<td>{{ $item['vendor'] }}</td>
								<td>{{ $item['plant'] }}</td>
								<td>{{ $item['valuation_group_name'] }}</td>
								<td>{{ $item['asset_type'] }}</td>
								<td>{{ $item['responsible_person_nik'] }}</td>
								<td>{{ $item['location'] }}</td>
								<td>{{ $item['status_name'] }}</td>
								<td>{{ !empty($item['purchase_date']) ? $item['purchase_date']->format('d/m/Y'):$item['purchase_date_str'] }}</td>
								<td>{{ $item['purchase_price'] }}</td>
								<td>
									@if ( $item['currency'] == 1)                               
									IDR 
									@elseif ( $item['currency'] == 2)
									USD
									@endif
								</td>
								<td>{{ $item['warranty_end_date'] }}</td>
								<td>{{ $item['maintenance_cycle'] }}</td>
								<td>{{ $item['mc_unit'] }}</td>
								<td>{{ $item['last_maintenance_date'] }}</td>
								<td>@php 
								if (!empty($item['retired_date'])){
									echo \Carbon\Carbon::parse($item['retired_date'])->format('d-m-Y');
								}
								 @endphp</td>
								<td>{{ $item['retired_ramarks'] }}</td>

							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
				{{ csrf_field() }}
				@if(!$dataOk)
				<div class="alert alert-warning">
					<strong>Warning</strong> To continue saving data, please fix all invalid data
				</div>
				@else
					<div class="row xs-pt-15">
						<div class="col-md-11">
							<p class="text-right">
								<button type="submit" id="storeUpload" class="btn btn-primary btn-lg">Save</button>
							</p>
						</div>
					</div>
				@endif
					
				
			</div>
					
		</div>	
					
	</div>
</div>
		
<script src="{{ asset('assets/js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> 

<script type="text/javascript">
    $(document).ready(function(){       
		$('[data-toggle="popover"]').popover();  
        $('#datetime').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

    });

	$('#storeUpload').click(function(e){
		e.preventDefault();
		window.location = "{{url('asset/upload/save')}}";
	})
</script>
@endsection
