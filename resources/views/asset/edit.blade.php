@extends('template.master')
@section('header')
Asset Master
@endsection
@section('beforehead')
<link rel="stylesheet" href="{{ asset('webix/webix.css') }}">
<style>
    .img-thumb {
        width:200px;
        height:200px;
        background-color:#f5f5f5; 
        margin-top: 10px;
        margin-bottom: 10px;
        margin-left:10px;
        border: 1px solid #bdbdbd;
        padding: 0 !important;
        overflow:hidden;
        display: flex; 
        justify-content: center;
        align-items: center;
    }

    .img-thumb:hover {
        border: 1.5px solid #bdbdbd;
    }

    #img-upload {
        border: 5px dashed #bdbdbd;
        background-color: #f5f5f5;
        display:flex;
        justify-content:center;
        align-items:center;
    }

    #img-upload i {
        font-size: 50px;
        color:#bdbdbd;
    }

    .delete-image{
        display:none;
        position: absolute;
        right: 5px;
        top: 2px;
    }
    .delete-image2{
        display:block;
        right: 0px;
        top: 0px;
    }

    .img-thumb:hover .delete-image {
        display:block;
    }


</style>
<!-- panorama 360 cdn -->
<link rel="stylesheet" href="https://cdn.pannellum.org/2.3/pannellum.css"/>
<script type="text/javascript" src="https://cdn.pannellum.org/2.3/pannellum.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" />
@endsection

@section('content')
<div class="row">
        <div class="col-sm-12">
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {!!Session::get('success')!!}
                </div>
            @elseif(Session::has('error'))
                <div class="alert alert-warning alert-dismissible">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {!!Session::get('error')!!}
                </div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading icon-container">
                    <a href="{{ url('asset') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                    Edit Asset<span class="panel-subtitle"></span>
                </div>
                <div class="panel-body">
                    <div class="row">
            
                        <div class="col-md-3">
                            <br/>
                            <strong>Asset Hierarchy</strong>
                            <br/>
                            <div id="box"></div>
                        </div>
                        <div class="col-md-9">
                            <div class="nav-tabs-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#tab_material" data-toggle="tab" aria-expanded="true">Edit Asset</a></li>
                                    {{-- <li class=""><a href="#tab_classification" data-toggle="tab" aria-expanded="false">Classification</a></li> --}}
                                    <li class=""><a href="#tab_images" data-toggle="tab" aria-expanded="false">Images</a></li> 
                                    {{--  <li class=""><a href="#tab_maintenance" data-toggle="tab" aria-expanded="false">Maintenance</a></li>  --}}
                                    <li class=""><a href="#tab_location" data-toggle="tab" aria-expanded="false">Location</a></li> 
                                    {{--<li class=""><a href="#tab_area" data-toggle="tab" aria-expanded="false">Area</a></li>--}}
                                    {{--  <li class=""><a href="#" data-toggle="tab" aria-expanded="false">Readings</a></li>  --}}
                                </ul>
                                <div class="tab-content">
                                    
                                    <div class="tab-pane active" id="tab_material">
                                        <!-- form start -->
                                        <form id="form-create" class="form-horizontal" method="post" action="{{ url('asset/'.$items->id_asset) }}">
                                            {!! csrf_field() !!}
                                            <input type="hidden" name="_method" value="put" />
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Material<font size="3" color="red"> *</font></sup></label>
                                                                        <select class="id_material form-control" id="id_material" name="id_material" required>
                                                                            <option value="{{ $items->id_material }}"selected="">[{{ $items->material->name }}] - {{ $items->material->description }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div> 

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                            <label>Description</label>
                                                                        <input type="text" class="form-control " name="desc_assets" id="desc_assets" value="{{ $items->description_asset }}"> 
                                                                    </div>
                                                                </div> 

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                            <label>Assets Number<font size="3" color="red"> *</font></sup></label>
                                                                        <input type="text" class="form-control " name="code_asset" id="code_asset" value="{{ $items->code_asset }}" > 
                                                                    </div>
                                                                </div> 
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                            <label>Serial Number</label>
                                                                        <input type="text" class="form-control " name="serial_num" id="serial_num"  value="{{ $items->serial_number }}" required='false'> 
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Superior Assets</label>
                                                                        <select class="id_superior_asset form-control" name="id_superior_asset" id="id_superior_asset">
                                                                            <option value="{{$items->id_superior_asset}}">{{ $items->parent_asset->code_asset ?? NULL }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Old Asset Number</label>
                                                                        <select id="old_asset" name="old_asset" class="form-control">
                                                                            <option value="{{$items->old_asset}}">{{ $items->old_asset->code_asset ?? NULL }}</option>
                                                                        </select> 
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Vendor</label>
                                                                        <select class="supplier form-control" name="supplier" id="supplier">
                                                                            <option value="{{$items->id_supplier}}">{{$items->supplier->name ?? NULL }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div> 

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                            <label>Plant<font size="3" color="red"> *</font></label>
                                                                        <select id="plant" name="plant" class="form-control " required>
                                                                            <option value="{{$items->id_plant}}">{{ $items->plant_asset->name ?? null }}</option>
                                                                        </select> 
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Group Owner</label>
                                                                        <select id="valuation_group" name="valuation_group" class="form-control ">
                                                                            <option value="{{$items->valuation_group}}">{{$items->valuation->name ?? NULL}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Type Assets</label>
                                                                        <select id="type_assets" name="type_assets" class="form-control">
                                                                            <option value="{{$items->id_type_asset}}">{{$items->type->name ?? NULL}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Status</label>
                                                                        <select id="status" name="status" class="form-control ">
                                                                            <option value="{{$items->status}}">{{ $items->asset_status->name ?? NULL }}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Depreciation Asset <font size="3" color="red"> *</font></label>
                                                                        <div class="input-group">
                                                                            <select id="depreciation" name="depreciation" class="form-control" disabled>
                                                                                <option value="{{$items->id_depreciation}}">{{ $items->depreciation->name ?? NULL }}</option>
                                                                            </select>
                                                                            
                                                                            <div class="input-group-addon btn btn-primary btn-lg" data-toggle="buttons" onclick="active_depreciation()" disabled>
                                                                                <i class="mdi mdi-check" id="is_asset"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Purchase Date</label>
                                                                        <div class="input-group">
                                                                        @php  
                                                                            $new_date =  !empty($items->purchase_date) ? date('Y-m-d', strtotime($items->purchase_date)) : "";
                                                                        @endphp 
                                                            
                                                                            <input type="text" class="form-control " id="purchase_date" name="purchase_date" value="{{ $new_date}}" disabled>
                                                                            <div class="input-group-addon btn btn-primary">
                                                                                <i class="mdi mdi-calendar"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div> 

                                                                <div class="form-group">
                                                                    <div class="col-sm-8">
                                                                        <label>Purchase Price</label>
                                                                        <input type="number" min="1" class="form-control " name="purchase_cost" id="purchase_cost" value="{{ $items->purchase_cost }}" disabled> 
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <label style="text-align: left; color:white">.</label>
                                                                        <select id="curency" name="curency" class="form-control" disabled>
                                                                            <option value="1" @if($items->curency==1) selected @endif >IDR</option>
                                                                            <option value="2" @if($items->curency==2) selected @endif >USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Warranty End</label>
                                                                        <div class="input-group">
                                                                        @php  
                                                                            $new_date =  !empty($items->waranty_finish) ? date('Y-m-d', strtotime($items->waranty_finish)) : "";
                                                                        @endphp
                                                                            <input type="text" class="form-control " id="warranty_finish" name="warranty_finish" value="{{ $new_date }}" >
                                                                            <div class="input-group-addon btn btn-primary">
                                                                                <i class="mdi mdi-calendar"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                                <div class="form-group">
                                                                    <div class="col-sm-8">
                                                                        <label>Maintenance Cycle</label>
                                                                        <div class="input-group">
                                                                            <input type="number" min="1" class="form-control " id="time_cycle" name="time_cycle" value="{{ $items->count_duration }}">
                                                                            <div class="input-group-addon btn btn-primary">
                                                                                <i class="mdi mdi-time"></i>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-4">
                                                                        <label style="text-align: left; color:white">.</label>
                                                                        <select id="unit_cycle" name="unit_cycle" class="form-control ">
                                                                        <!--  <option value="1" @if($items->unit_duration==1) selected @endif >Second</option>
                                                                            <option value="2" @if($items->unit_duration==2) selected @endif >Minutes</option>
                                                                            <option value="3" @if($items->unit_duration==3) selected @endif >Hour</option> -->
                                                                            <option value="4" selected >Day</option>
                                                                            <option value="5" >Week</option>
                                                                            <option value="6" >Month</option>
                                                                            <option value="7" >Year</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Last Maintenance</label>
                                                                    <div class="input-group">
                                                                        @php  
                                                                            $new_date =  !empty($items->last_maintenance) ? date('Y-m-d', strtotime($items->last_maintenance)) : "";
                                                                        @endphp
                                                                        <input type="text" class="form-control " name="last_maintenance" id="last_maintenance" value="{{ $new_date }}">
                                                                        <div class="input-group-addon btn btn-primary">
                                                                            <i class="mdi mdi-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Retired Date</label>
                                                                    <div class="input-group">
                                                                    @php  
                                                                        $new_date =  !empty( $items->retired_date) ? date('Y-m-d', strtotime( $items->retired_date)) : "";
                                                                    @endphp
                                                                        <input type="text" class="form-control " name="retired_date" id="retired_date" value="{{ $new_date }}">
                                                                        <div class="input-group-addon btn btn-primary">
                                                                            <i class="mdi mdi-calendar"></i>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </div> 

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Retired Reason</label>
                                                                        <select id="retired_reason" name="retired_reason" class="form-control ">
                                                                            <option value="{{$items->retired_reason}}">{{$items->retiredReason->name ?? NULL}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <label>Retired Remarks</label>
                                                                        <input type="text" class="form-control " name="retired_ramarks" id="retired_ramarks"value="{{ $items->retired_ramarks}}"> 
                                                                    </div>
                                                                </div>

                                                            </div>

                                                                
                                                        </div>

                                                        <div id="bar" class="collapse in">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <input type="hidden" class="form-control" style="width: 110px" id="us2-lon" name="longitude" value="{{ $items->longitude  }}"/>
                                                                            <input type="hidden" class="form-control" style="width: 110px" id="us2-lat" name="latitude" value="{{ $items->latitude  }}"/>
                                                                            <br>
                                                                            <div id="us2" style="width: 100%; height: 300px;"></div>
                                                                        </div>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                                    
                                                        <div class="row xs-pt-15">
                                                            <div class="col-xs-12">
                                                                <p class="text-right">
                                                                    <button type="submit" id="submit_update" class="btn btn-primary btn-lg">Save</button>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div> <!--end asset-->
                            
                                    <div class="tab-pane" id="tab_images">
                                            
                                            <!-- Images -->
                                            {{--<div class="row">
                                                <div class="col-md-12">
                                                    <div class="box-header">
                                                        <strong class="box-title"> Material Image</strong>
                                                    </div>
                                                    <div id="image-gallery"><!-- ajax loaded --></div>
                                                    <!--<label class="col-md-3 img-thumb btn" id="img-upload" style="position: relative; overflow:hidden">
                                                        <i id="btn-plus" class="fa fa-plus"></i> 
                                                        <i id="btn-upload" class="fa fa-refresh fa-spin" style="display:none"></i> 
                                                        <form method="POST" id="form-img-upload" action="{{ url('/material/'.$items->id_material.'/image') }}">
                                                            {{ csrf_field() }}
                                                            <input id="img-selector" name="images[]" type="file" style="display: none" accept="image/*" multiple="multiple">
                                                        </form>
                                                    </label>-->
                                                </div>
                                            </div>--}}
                                            <!-- Images asset -->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box-header">
                                                        <strong class="box-title"> Asset Image</strong>
                                                    </div>
                                                    <div id="image-gallery-asset"><!-- ajax loaded --></div>
                                                    <label class="col-md-3 img-thumb btn" id="img-upload" style="position: relative; overflow:hidden">
                                                        <i id="btn-plus" class="fa fa-plus"></i> 
                                                        <i id="btn-upload" class="fa fa-refresh fa-spin" style="display:none"></i> 
                                                        <form method="POST" id="form-img-upload" action="{{ url('/asset/'.$items->id_asset.'/image') }}">
                                                            {{ csrf_field() }}
                                                            <input id="img-selector" name="images[]" type="file" style="display: none" accept="image/*" multiple="multiple">
                                                        </form>
                                                    </label>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="box-header">
                                                        <strong class="box-title"> Asset Image 360</strong>
                                                    </div>
                                                    <div id="image360-gallery-asset"><!-- ajax loaded --></div>
                                                    <label class="col-md-3 img-thumb btn" id="img-upload" style="position: relative; overflow:hidden">
                                                        <i id="btn-plus360" class="fa fa-plus"></i> 
                                                        <i id="btn-upload360" class="fa fa-refresh fa-spin" style="display:none"></i> 
                                                        <form method="POST" id="form-img-upload" action="{{ url('/asset/'.$items->id_asset.'/image360') }}">
                                                            {{ csrf_field() }}
                                                            <input id="img360-selector" name="images[]" type="file" style="display: none" accept="image/*" multiple="multiple">
                                                        </form>
                                                    </label>
                                                </div>
                                            </div>

                                    </div> <!--end image-->
                                

                                    <div class="tab-pane " id="tab_location">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <table class="table table-striped table-hover">
                                                            <tr>
                                                                <td><strong>Responsible Person</strong></td>
                                                                <td>[ {{ $items->responsible_person_asset->nik ?? NULL }} ] - {{ $items->responsible_person_asset->name ?? NULL }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Location</strong></td>
                                                                <td>{{$items->location->name ?? NULL }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <table class="table table-striped table-hover">
                                                            <tr>
                                                                <td><strong>Province</strong></td>
                                                                <td>{{ $items->location->province ?? NULL }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>City</strong></td>
                                                                <td>{{ $items->location->city ?? NULL }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Building</strong></td>
                                                                <td>{{ $items->location->building ?? NULL }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>unit</strong></td>
                                                                <td>{{ $items->location->unit ?? NULL }}</td>
                                                            </tr>
                                                        </table>
                                                        
                                                        
                                                    </div>
                                                    
                                                </div>
                                                
                                                    
                                                <div class="row">
                                                    <div class="col-md-12">
                                                            <div class="col-md-4">
                                                                <button href="#bar1" data-toggle="collapse"class="btn show_btn btn-success btn-block"  aria-hidden="true" onclick="ShowHide(this);">Show Map</button>
                                                            </div>
                                                            <div class="col-sm-3">
                                                                <button class="btn btn-warning " data-toggle="modal" data-target="#myModal1"><i class="fa fa-arrow-right" aria-hidden="true"  ></i> Move Asset Location</button>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <button class="btn btn-primary " data-toggle="modal" data-target="#myModal2"><i class="fa fa-arrow-right" aria-hidden="true"  ></i> Move Responsible Person</button>
                                                            </div>
                                                    </div>
                                                </div>
                                                <br>
                                                <div id="bar1" class="collapse in">
                                                    <td><div id="us4" style="width: 100%; height: 300px;"></div></td>
                                                </div>
                                    </div><!--end location-->

                                    <div class="tab-pane " id="tab_area">
                                        {{-- <div class="row"> --}}
                                            <form id="form-create" class="form-horizontal" method="post" action="{{ url('asset/'.$items->id_asset.'/area') }}"> 
 
                                                {!! csrf_field() !!} 
                                                <input type="hidden" name="id_asset" value="{{ $items->id_asset }}" /> 
                                                <input type="hidden" name="area" id="poly" value="{{ isset($area) ? $area->area : "" }}"/>

                                                <div class="form-group">
                                                    <div class="col-sm-8">
                                                        <label>Name</label>
                                                        <input type="text" name="name" maxlength="30" class="form-control" value="{{ isset($area) ? $area->name : "" }}" required>
                                                        @if ($errors->has('description'))
                                                        <span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-8">
                                                        <label>Large</label>
                                                        <div class="input-group xs-mb-15">
                                                            <input type="text" name="large" id="large" maxlength="30" class="form-control" value="{{ isset($area) ? $area->large : ""}}" required><span class="input-group-addon">m<sup>2</sup></span>
                                                        </div>
                                                        @if ($errors->has('description'))
                                                        <span class="help-block" style="color:red">{{ $errors->first('large') }}</span>
                                                        @endif
                                                    </div>
                                                </div>                                                

                                                <div class="row">
                                                    <div class="col-md-12">
                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="Right click to remove the dot"> <i class="mdi mdi-info"></i></a>
                                                    <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <div id="map_area" style="width: 100%; height: 500px;"></div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>
                                                
                                                <div class="row xs-pt-15">
                                                    <div class="col-xs-12">
                                                        <p class="text-right">
                                                            <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                                        </p>
                                                    </div>
                                                </div>
                                            
                                            </form>
                                        {{-- </div> --}}
                                    </div><!--end area-->
                            
                                </div><!--end tab cotent-->
                            </div><!--end tab custom-->
                        </div>
                    </div>
                </div>                        
            </div>
        </div>
</div>

   <!-- Modal -->
  <div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Move Asset</h4>
        </div>
        <div class="modal-body">
            <form id="form-create2" class="form-horizontal" method="post" action="{{ url('/asset/'.$items->id_asset.'/location') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="id_asset" value="{{$items->id_asset}}" />
                <input type="hidden" name="longitude_old" value="{{ $items->longitude  }}"/>
                <input type="hidden" name="latitude_old" value="{{ $items->latitude  }}"/>
                <h4>Old Location</h4>
                <div class="form-group">
                    <label class="col-sm-4 control-label input-md">Location Name</sup></label>
                    <div class="col-sm-6">
                        <select name="id_location_old" class="form-control" readonly>
                            <option value="{{ $items->id_location2 }}">{{$items->location->name ?? NULL}}</option>
                        </select> 
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Address </label>
                    <div class="col-sm-10">
                        <input type="text" name="address_old" class="form-control" value="{{ $items->location->address ?? null }}" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Building </label>
                    <div class="col-sm-4">
                        <input type="text" name="building_old" class="form-control" value="{{ $items->location->building ?? null }}" readonly>
                    </div>

                    <label class="col-sm-2 control-label">unit </label>
                    <div class="col-sm-4">
                        <input type="text" name="unit_old" class="form-control" value="{{ $items->location->unit ?? null }}" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-4">
                        <input type="text" name="province_old" class="form-control" value="{{ $items->location->province ?? null }}" readonly>
                    </div>

                    <label class="col-sm-2 control-label">City </label>
                    <div class="col-sm-4">
                        <input type="text" name="city_old" class="form-control" value="{{ $items->location->city ?? null }}" readonly>
                    </div>
                </div>
                    <!-- input -->
                <br>
                <h4>New Location</h4>
                <br>
                <div class="form-group">
                        <label class="col-sm-4 control-label input-md">Location Name</sup></label>
                        <div class="col-sm-6">
                            <!-- <select name="new_location" id="new_location" class="form-control " onchange="getMap()">-->
                            <select name="new_location" id="new_location" class="form-control">
                            </select> 
                        </div>
                    </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Address </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="new_address" name="new_address" placeholder="address" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Building </label>
                    <div class="col-sm-4">
                        <input type="text" name="new_building" id="new_building" class="typeahead form-control" autocomplete="off" readonly>
                    </div>

                    <label class="col-sm-2 control-label">unit </label>
                    <div class="col-sm-4">
                        <input type="text" name="new_unit" id="new_unit" class="form-control" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Province</label>
                    <div class="col-sm-4">
                        <input type="text" name="new_province" id="new_province" class="form-control" readonly>
                    </div>

                    <label class="col-sm-2 control-label">City </label>
                    <div class="col-sm-4">
                        <input type="text" name="new_city" id="new_city" class="form-control" readonly>
                    </div>
                </div>
                
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" class="form-control" style="width: 110px" id="us3-lon" name="longitude" />
                                <input type="hidden" class="form-control" style="width: 110px" id="us3-lat" name="latitude" />
                                <br>
                            </div>
                        </div> 

                
                    <div id="us3" style="width: 100%; height: 300px; display: none;"></div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" id="form-create2-submit" name="form-create2-submit" class="btn btn-success" data-dismiss="modal"> Save</button> 
        </div>
      </div>
      
    </div>
  </div>

   <!-- Modal2 -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Move Responsible Person</h4>
        </div>
        <div class="modal-body">
            <form id="form-create3" class="form-horizontal" method="post" action="{{ url('/asset/'.$items->id_asset.'/person') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="id_asset" value="{{$items->id_asset}}" />

                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-4 control-label">Old Responsible Person </label>
                    <div class="col-sm-8">
                        <select class="form-control" name="responsible_person_old" id="responsible_person_old" readonly>
                            <option value="{{ $items->id_responsible_person }}"> {{$items->user_reponsible_nik}} - {{$items->user_reponsible_name}}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-4 control-label">New Responsible Person </label>
                    <div class="col-sm-8">
                        {{-- <input type="text" class="form-control clearme" name="responsible_person" > --}}
                        <select class="form-control" name="responsible_person" value="{{ ('responsible_person') }}" id="responsible_person"></select> 
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-create3').submit()"> Save</button> 
         <!--<button  class="btn btn-success"> Save</button>-->
        </div>
      </div>
      
    </div>
  </div>
 

@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
 <script src="https://maps.googleapis.com/maps/api/js?libraries=places,drawing&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs" ></script>



<script>
    $(function() {
        $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });         
    });
    $(function() {
        $('#table-data2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });         
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        if($('#depreciation').val() != '') {
            $('#depreciation').prop('disabled', false);
            $("#is_asset").removeClass("mdi mdi-check");
            $("#is_asset").addClass("mdi mdi-close");
            $("input").prop('required',true);
        }

        $('#form-create2-submit').click(function(){
            if($('#new_location').val() == null)
            {
                window.alert("Location harus diisi terlebih dahulu !!");
                return false;
            }
            else {
                $('#form-create2').submit();
            }
        })
        $('#id_material').select2({
            placeholder: 'Choose Material',
                ajax:  {
                            url: '{{route('material_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {
                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        text: "["+item.name+"] - "+item.description,
                                        id: item.id
                                    }
                                })
                            };
                        },

                    cache: true
                }
            });

            $('#responsible_person').select2({
            placeholder: 'Choose Responsible',
            width: '100%',
                ajax:  {
                            url: '{{route('user_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {
                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        text: "[" + item.nik + "] " + item.name,
                                        id: item.id
                                    }
                                })
                            };
                        },

                    cache: true
                }
            });
        
        $('.clockpicker').clockpicker({autoclose:true});
        $('#purchase_date').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#warranty_start').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#warranty_finish').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#last_maintenance').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#next_maintenance').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#retired_date').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('.datetime').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#type_assets').select2({
            placeholder: 'Asset Type',
            ajax:  {
                tags: "true",
                url: '{{route('assettype_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        }); 
        $('#valuation_group').select2({
            placeholder: 'Group Owner',
            ajax:  {
                tags: "true",
                url: '{{route('groupowner_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                //dropdownParent: parentElement,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#plant').select2({
            width: '100%',  
            placeholder: 'Choose Plant',
            ajax:  {
                url: '{{route('plant_active_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        $('#new_location').select2({
            width: '100%',  
            placeholder: 'Choose New Location',
            ajax:  {
                url: '{{route('location_active_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {

                            var plant =  item.plant;
                            if(plant == null) {
                                plant = 'unknown';
                            }
                            return {
                                text: "[ " + plant + " ] - " + item.name,
                                id: item.id,
                                address:item.address,
                                building:item.building,
                                unit:item.unit,
                                city:item.city
                            }
                        })
                    };
                },
                cache: true
            }
        });
        
        $('#new_location').on('select2:select', function (e) {
            var data = e.params.data;
            $("#new_address").val(data.address);
            if(data.unit != null)
            {
                $("#new_unit").val(data.unit);
            }
            if(data.city != null)
            {
                $("#new_city").val(data.city);
            }
            if(data.building != null)
            {
                $("#new_building").val(data.building);
            }
            //console.log(data.address);
        });
        
        $('#old_asset').select2({
            width: '100%',  
            placeholder: 'Choose Supperior Asset',
            ajax:  {
                url: '{{route('asset_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#supplier').select2({
        width: '100%',  
        placeholder: 'Choose Vendor',
            ajax:  {
                url: '{{route('supplier_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        }); 
        $('#id_superior_asset').select2({
            width: '100%',  
            placeholder: 'Choose Supperior Asset',
            ajax:  {
                url: '{{route('asset_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#status').select2({
            width: '100%',
            placeholder: 'Status',
            ajax:  {
                url: '{{route('status_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
/*
        $('#depreciation').select2({
            width: '100%',
            placeholder: 'Depreciation Asset',
            ajax:  {
                url: '{{route('depreciation_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: '[ '+ item.useful_life + ' Years ] - ' + item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
*/
        $('#retired_reason').select2({
            width: '100%',
            placeholder: 'Retired Reason',
            ajax:  {
                url: '{{route('retired_reason_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        loadImages(); 
        loadImagesAsset();     
        loadImages360Asset();    
        //autocomplite building
        var path = "{{ route('autocomplete.ajax') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    });
</script>
<script type="text/javascript">
    var countjson = 0;
    var polygon;
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map_area'), {
                center: {
                lat: {{ $items->latitude  }}, 
                lng: {{ $items->longitude }}
                },
                zoom: 13
            });

            var json = @php echo isset($area) ? $area->area : "[]" @endphp;
            countjson = json.length;
            polygon = new google.maps.Polygon({
                paths: json,
                // strokeColor: '#f4b942',
                strokeOpacity: 1,
                strokeWeight: 1,
                fillColor: '#ffff8c99',
                fillOpacity: 0.5,
                editable: true
            });
            polygon.setMap(map);
  
            var drawingManager = new google.maps.drawing.DrawingManager({
                drawingMode: json.length > 0 ? null : google.maps.drawing.OverlayType.POLYGON,
                drawingControl: true,
                drawingControlOptions: {
                    position: google.maps.ControlPosition.TOP_CENTER,
                    drawingModes: ['polygon', ]
                },
                polygonOptions: {
                    // strokeColor: '#f4b942',
                    fillColor: '#ffff8c99',
                    fillOpacity: 0.5,
                    strokeWeight: 1,
                    clickable: true,
                    editable: true,
                    zIndex: 1
                }
            });
            drawingManager.setMap(map);
            
            //polygon complete event
            google.maps.event.addListener(drawingManager, 'polygoncomplete', function(poly) {
                polygon.setMap(null);
                polygon = poly;
                updateArea();
                updateJson();
                drawingManager.setDrawingMode(null);

                var large = google.maps.geometry.spherical.computeArea(polygon.getPath());
                console.log(large);
                
                //event editting poly create asset
                google.maps.event.addListener(polygon.getPath(), 'insert_at', function(index){
                    updateJson();
                    updateArea();
                });
                
                google.maps.event.addListener(polygon.getPath(), 'remove_at', function(index){
                    updateArea();
                    updateJson();
                });
                
                google.maps.event.addListener(polygon.getPath(), 'set_at', function(index){
                    updateArea();
                    updateJson();
                });
                
                google.maps.event.addListener(polygon, 'rightclick', function(e) {
                    var path = polygon.getPath();
                    if (e.vertex == undefined) {
                        return;
                    } 
                    path.removeAt(e.vertex)
                }); 
                
            });

            //edit asset
            // console.log(countjson);
            if( countjson > 0){
                google.maps.event.addListener(polygon.getPath(), 'insert_at', function(index){
                    updateArea();
                    updateJson();
                });

                google.maps.event.addListener(polygon.getPath(), 'remove_at', function(index){
                    updateArea();
                    updateJson();
                });
                
                google.maps.event.addListener(polygon.getPath(), 'set_at', function(index){
                    updateArea();
                    updateJson();

                });
                
                google.maps.event.addListener(polygon, 'rightclick', function(e) {
                    var path = polygon.getPath();
                    if (e.vertex == undefined) {
                        return;
                    } 
                    path.removeAt(e.vertex)
                });
            }
            

        }


        function updateJson(){
            var path = polygon.getPath();
            var len = path.getLength();
            var segmentJson = [];
            
            for(var i=0;i<len;i++){
                var lat = path.getAt(i).lat();
                var lng = path.getAt(i).lng();
                segmentJson.push({lat:lat,lng:lng});
            }
            //update textarea
            document.getElementById('poly').value = JSON.stringify(segmentJson);
        }

        function updateArea(){
            var large = google.maps.geometry.spherical.computeArea(polygon.getPath());
            console.log(large);
            document.getElementById('large').value = Math.round(large * 10000) / 10000;
        }


    $(document).ready(function(){
        initMap();

        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
    });
    $(function() {
        $('#us2').locationpicker({
            location: {
                latitude: "{{ $items->latitude  }}",
                longitude: "{{ $items->longitude }}"
            },
            inputBinding: {
                latitudeInput: $('#us2-lat'),
            longitudeInput: $('#us2-lon'),
            locationNameInput: $('#us2-address')
            },
            radius:0,
            enableAutocomplete: true       
        });
    }); 
</script> 
 

<script type="text/javascript">
$(window).load(function () {
   $('#bar').removeClass('in')
   

});
</script> 
<script>
$('#submit_update').on('click', function() {
    $('input[name=serial_num').removeAttr("required");
    $('input[name=warranty_finish').removeAttr("required");
    $('input[name=time_cycle').removeAttr("required");
    $('input[name=unit_cycle').removeAttr("required");
    $('input[name=last_maintenance').removeAttr("required");
    $('input[name=retired_ramarks').removeAttr("required");
    //if($('#status').val() == '1')) {
    //    $('input[name=retired_reason').removeAttr("required");
    //    $('input[name=retired_ramarks').removeAttr("required");
    //}

});
</script>
<script>
function active_depreciation() {
    /*
            if($('#depreciation').prop('disabled')){
                $('#depreciation').prop('disabled', false);
                $("#is_asset").removeClass("mdi mdi-check");
                $("#is_asset").addClass("mdi mdi-close");
                $("input").prop('required',true);
            }
            else {
                $('#depreciation').prop('disabled', true);
                $("#is_asset").removeClass("mdi mdi-close");
                $("#is_asset").addClass("mdi mdi-check");
                $("input").prop('required',false);
                $("#depreciation").empty();
            }
*/
        }
$("#img-selector").change(function(){
	// $("#form-img-upload").submit();
	// STOP
	var files 	= this.files
	var form	= new FormData();
	var xhr		= new XMLHttpRequest()

	for(var i=0; i < files.length; i++){
		var file = files[i]
		form.append('images[]',file,file.name)
	}

	// CSRF
	form.append('_token','{{ csrf_token() }}')

	// Upload
	xhr.open('POST','{{ route('asset.images.upload',$items->id_asset) }}?_date='+ new Date().getTime(),true)
	
	// Handling
	xhr.onload = function () {
		if (xhr.status === 200) {
			// File(s) uploaded.
			console.log("Files uploaded")
			// Refresh Images
			$("#btn-upload").hide()
			$("#btn-plus").show()
			loadImagesAsset()
		} else {
			alert('An error occurred!');
		}
	};

	// Event listener
	xhr.upload.addEventListener("progress", function(event){
		if (event.lengthComputable) {
			var percentComplete = event.loaded / event.total;
			console.log('Upload:'+percentComplete)
		} else {
			// Unable to compute progress information since the total size is unknown
		}
	})

	// Send
	$("#btn-upload").show()
	$("#btn-plus").hide()
	xhr.send(form)

	console.log("Selected images",files)
})
</script>
<script>
function changeToUpperCase(t) {
   var eleVal = document.getElementById(t.id);
   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_]/g, "");
}

</script>

<script>
// Load image

function loadImages(){
	$("#image-gallery").load('{{ route('material.images.list',$items->id_material) }}');
}

function loadImagesAsset(){
	$("#image-gallery-asset").load('{{ route('asset.images.list',$items->id_asset) }}');
}

function loadImages360Asset(){
	$("#image360-gallery-asset").load('{{ route('asset.images360.list',$items->id_asset) }}');
}

// Delete image
$("#image-gallery-asset").on('click','.delete-image',function(){
	console.log("handle click",$(this).attr('id'))

	var imageId = $(this).attr('id')
	var csrf = $('meta[name="csrf-token"]').attr('content')

	$.post('{{ url('/asset/image/') }}/'+imageId,{
		'_token': csrf,
		'_method': 'delete'
	}).done(function(data){
		console.log('Delete image done',data)
	}).fail(function(){
		console.log('Delete image fail')
	}).always(function(){
		// console.log('Always call this log')
		loadImagesAsset();
	})
	
})

// Delete image 360
$("#image360-gallery-asset").on('click','.delete-image2',function(){
	console.log("handle click",$(this).attr('id'))

	var imageId = $(this).attr('id')
	var csrf = $('meta[name="csrf-token"]').attr('content')

	$.post('{{ url('/asset/image360/') }}/'+imageId,{
		'_token': csrf,
		'_method': 'delete'
	}).done(function(data){
		console.log('Delete image done',data)
	}).fail(function(){
		console.log('Delete image fail')
	}).always(function(){
		// console.log('Always call this log')
		loadImages360Asset();
	})
	
})


</script>

<script>
$("#img360-selector").change(function(){
	// $("#form-img-upload").submit();
	// STOP
	var files 	= this.files
	var form	= new FormData();
	var xhr		= new XMLHttpRequest()

	for(var i=0; i < files.length; i++){
		var file = files[i]

		form.append('images[]',file,file.name)
	}

	// CSRF
	form.append('_token','{{ csrf_token() }}')

	// Upload
	xhr.open('POST','{{ route('asset.images360.upload',$items->id_asset) }}?_date='+ new Date().getTime(),true)
	
	// Handling
	xhr.onload = function () {
		if (xhr.status === 200) {
			// File(s) uploaded.
			console.log("Files uploaded")

			// Refresh Images
			$("#btn-upload360").hide()
			$("#btn-plus360").show()
			loadImages360Asset()
		} else {
			alert('An error occurred!');
		}
	};

	// Event listener
	xhr.upload.addEventListener("progress", function(event){
		if (event.lengthComputable) {
			var percentComplete = event.loaded / event.total;
			console.log('Upload:'+percentComplete)
		} else {
			// Unable to compute progress information since the total size is unknown
		}
	})

	// Send
	$("#btn-upload360").show()
	$("#btn-plus360").hide()
	xhr.send(form)

	console.log("Selected images",files)

})
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });

        $('#us4').locationpicker({
            location: {
                latitude: "{{ $items->latitude  }}",
                longitude: "{{ $items->longitude  }}"
            },    
            radius:0,  
        });
        
         $('#us3').locationpicker({
            location: {
                latitude: "{{ $items->latitude  }}",
                longitude: "{{ $items->longitude  }}"
            },
            inputBinding: {
                latitudeInput: $('#us3-lat'),
            longitudeInput: $('#us3-lon'),
            locationNameInput: $('#us3-address')
            },
            radius:0,
            enableAutocomplete: true       
        });

        
        
    });
</script>

<script type="text/javascript">
$(window).load(function () {
   $('#bar1').removeClass('in')
 

});


function ShowHide(id) {
  if ($(id).html() == "Show Map") {
    $(id).html("Hide Map");
  } else {
    $(id).html ("Show Map");
  }

}
</script> 

<script src="{{ asset('webix/webix.js') }}"></script>
<script>
webix.ui({
    container:"box",
    view:"tree",
    height: 500,
    weigh:45,
    data: [
        {
            id:"{{$items->id_asset}}",
            value:"{{$items->code_asset}} - {{$items->description_asset}}",
            open: true,
            data:[
                @php
                $child1  = \App\Models\Transaction\Asset::where('id_superior_asset',$items->id_asset)->get();
                @endphp
                @foreach($child1 as $child)
                {
                    id:"{{$child->id_asset}}",
                    value:"{{$child->code_asset}} - {{$child->description_asset}}",
                    open: true,
                    data:[
                        @php
                        $child2  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                        @endphp
                        @foreach($child2 as $child)
                        {
                            id:"{{$child->id_asset}}",
                            value:"{{$child->code_asset}} - {{$child->description_asset}}",
                            open: true,
                            data:[
                                @php
                                $child3  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                                @endphp
                                @foreach($child3 as $child)
                                {
                                    id:"{{$child->id_asset}}",
                                    value:"{{$child->code_asset}} - {{$child->description_asset}}",
                                    open: true,
                                    data:[
                                        @php
                                        $child4  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                                        @endphp
                                        @foreach($child4 as $child)
                                        {
                                            id:"{{$child->id_asset}}",
                                            value:"{{$child->code_asset}} - {{$child->description_asset}}",
                                            open: true,
                                        }
                                        @endforeach
                                    ]
                                }
                                @endforeach
                            ]
                        }
                        @endforeach
                    ]
                },
                @endforeach
            ]
        }
    ],
    on: {'onItemClick': function (data) {
        //console.log(data);
        window.open("{{url('asset/') }}/"+data+"/edit");
    }},
    
    
});
</script>
<script type="text/javascript">
        function getMap() {

            var location = $("#location").val();

            $.ajax({
                url: '{{route('get_map')}}',
                type: 'post',
                data: {
                    location : location
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    $('#us3-lat').val(data.latitude);
                    $('#us3-lon').val(data.longitude);
                    $('#us3-address').val(data.address);
                    $('#city').val(data.city);
                    $('#province').val(data.province);
                    $('#building').val(data.building);
                    $('#unit').val(data.unit);
                    tes();
                },
                fail: function (data) {
                    alert('error occured');
                }
            });
        }
</script>
@endsection