{{-- <head>
    <link rel="stylesheet" type="text/css" href="{{asset('beagle/lib/datatables/css/dataTables.bootstrap.min.css')}}"/>
</head> --}}

<head>
    <title>IZORA Asset Barcode</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
@for ($i = 0; $i < count($assets); $i++)
    {{-- @if($i % 2 == 0)
    <br>
    <br>
    <br>
    @endif --}}
    {{-- <div class="col-xs-6"> --}}
        @php
            $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            echo $generator->getBarcode($assets[$i]->code_asset, $generator::TYPE_CODE_128);
        @endphp
        {{$assets[$i]->code_asset}}
        <br>
        <br>
        <br>
    {{-- </div> --}}
    
@endfor
    
 {{-- @foreach($assets as $asset)
<div class="row">
    <div class="col-xs-6">
        @php
            $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
            echo $generator->getBarcode($asset->code_asset, $generator::TYPE_CODE_128);
        @endphp
        {{$asset->code_asset}}
        <br>
    </div>
</div>
@endforeach  --}}

   