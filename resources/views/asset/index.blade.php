@extends('template.master')
@section('content')
{{--  <style>
.content-wrapper {display:none;}
.preload { width:100px;
    height: 100px;
    position: fixed;
    top: 40%;
    left: 50%;}
</style>  --}}
{{--  <div class="preload">
    <img src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif">
</div>  --}}
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Master
        <small>Asset</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Master</a></li>
            <li>Asset</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-lg-12">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                            <a href="{{ url('asset/create') }}" class="btn btn-primary" title="Create Asset"><i class="fa fa-plus"></i></a>
                            <a href="#upload" class="btn btn-success"  data-toggle="modal" data-target="#upload-modal"><i class="fa fa-upload"></i> Upload Asset</a>
                    </div>
                    <div class="box-body">
                        <table id="table-data" class="table table-bordered table-condensed table table-hover table-striped" width="100%" style="table-layout:fixed;">
                            <thead>
                                <tr>
                                    <th style="width: 30px">No</th>
                                    <th style="width: 100px;">Material</th>
                                    <th style="width: 300px;">Description</th>
                                    <th style="width: 80px;">Asset Number</th>             
                                    <th style="width: 80px;"> Serial Number</th>
                                   <!-- <th>Address Location</th> -->              
                                    <th style="width: 80px;">Superior Asset</th>
                                    <th style="width: 80px;">Old Asset Number</th>
                                    <th style="width: 50px;">Vendor</th>
                                    <th style="width: 50px;">Plant</th>
                                    <th style="width: 80px;">Group Owner</th>
                                    <th style="width: 80px;">Asset Type</th>
                                    <th style="width: 150px;">Responsible Person</th>
                                    <th style="width: 80px;">Status</th>
                                    <th style="width: 120px;">Purchase Date</th>
                                    <th style="width: 120px;">Purchase Price</th>
                                    <th style="width: 120px;">Warranty End</th>
                                    <th style="width: 80px;">Maintenance Cycle</th> 
                                    <th style="width: 120px;">Last Maintenance</th> 
                                    <th style="width: 120px;">Next Maintenance</th>      
                                    <th style="width: 120px;">Retired Date</th>    
                                    <th style="width: 50px;">Retired Reason</th>  
                                    <th style="width: 80px;">Retired Remarks</th>                       
                                    <th style="width: 90px;">Action</th>
                                    
                                </tr>
                            </thead>
                            @php $i = 1 @endphp
                            @foreach($list as $val)
                            <tr>
                                <td style="text-align: center">{{ $i++ }}</td>
                                <td>{{ $val->name_material}}</td>
                                <td>{{ $val->description_asset}}</td>
                                <td>{{ $val->code_asset }}</td>
                                <td>{{ $val->serial_number}}</td>
                               <!-- <td>{{ $val->address_location }}</td> --> 
                                <td>{{ $val->code_asset_superior }}</td>
                                <td>{{ $val->code_old_asset }}</td>
                                <td>{{ $val->name_supplier }}</td>

                                <td>{{ $val->name_valuation_group}}</td>
                                <td>{{ $val->name_asset_type}}</td>
                                <td>{{ $val->responsible_person}}</td>
                                <td>@if ( $val->status == 1)
                                Draft
                                @elseif ( $val->status == 2)
                                Active
                                @elseif ( $val->status == 3)
                                Retired
                                @elseif ( $val->status == 4)
                                Deleted
                                @endif
                                </td>
                                <td>
                                @php  
                                    $new_date =  !empty($val->purchase_date) ? date('d-m-Y', strtotime($val->purchase_date)) : "";
                                @endphp 
                                {{ $new_date}}
                                </td>
                                <td>@if ( $val->curency == 1)                               
                                IDR {{ $val->purchase_cost}}
                                @elseif ( $val->curency == 2)
                                USD {{ $val->purchase_cost}}
                                @endif
                                </td>
                                <td>
                                @php  
                                    $new_date =  !empty($val->waranty_finish) ? date('d-m-Y', strtotime($val->waranty_finish)) : "";
                                @endphp 
                                {{ $new_date}}
                                </td>
                                <td>
                                @if ( $val->count_duration != NULL)  
                                {{ $val->count_duration}} Day
                                @endif
                                </td>
                                <td>@php  
                                    $new_date =  !empty($val->last_maintenance) ? date('d-m-Y', strtotime($val->last_maintenance)) : "";
                                @endphp 
                                {{ $new_date}}
                                </td>
                                <td>@php  
                                    $new_date =  !empty($val->next_maintenance) ? date('d-m-Y', strtotime($val->next_maintenance)) : "";
                                @endphp 
                                {{ $new_date}}
                                </td>  
                                <td>@php  
                                    $new_date =  !empty($val->retired_date) ? date('d-m-Y', strtotime($val->retired_date)) : "";
                                @endphp 
                                {{ $new_date}}
                                </td>
                                <td>@if ( $val->retired_reason == 1)
                                Broken
                                @elseif ( $val->retired_reason == 2)
                                Warranty End
                                @elseif ( $val->retired_reason == 3)
                                Lost
                                @endif
                                </td>  
                                <td>{{ $val->retired_ramarks}}</td>                            
                                <td>
                                    <a href="{{ URL::to('asset/' . $val->id_asset) }}" class="btn btn-primary btn-sm" title="View Asset Data"><i class="fa fa-eye"></i></a>
                                    <a href="{{ URL::to('asset/' . $val->id_asset .'/edit') }}" class="btn btn-success btn-sm" title="Edit Asset Data"><i class="fa fa-pencil"></i></a>
                                    <form method="post" action="{{ url('asset/'. $val->id_asset) }}" onclick="return confirm('Are you sure delete this data?')" class="pull-right">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE" />
                                        <button type="submit" class="btn btn-danger btn-sm delete-list"><i class="fa fa-trash" title="Hapus Office Data"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
</div>

<!-- Upload Modal -->
<div id="upload-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Asset</h4>
      </div>
      <div class="modal-body">
		<form id="form-upload" class="form-horizontal" method="post" action="{{ url('asset/upload') }}" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="box-body text-center" >

				<div id="file_name"></div>
				<label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
					Browse <input name="upload_file" type="file" style="display: none" onChange="$('#file_name').html(this.files[0].name)">
				</label>

			</div>
			
		</form>
      </div>
      <div class="modal-footer">
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button> -->
				<a href="{{ url('asset/download/template') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>
				<button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload').submit()"><i class="fa fa-upload"></i> Upload</button>
      </div>
    </div>

  </div>
</div><!-- Upload modal -->


<script>
// $(document).ready(function() {
//     var table = $('#table-data').DataTable( {
//         scrollY:        "300px",
//         scrollX:        true,
//         scrollCollapse: true,
//         paging:         true,
//         ordering: true,
//         fixedColumns:   {
//             leftColumns: 4,
//             rightColumns: 1
//         }
//     } );
// } );
    $(function() {
        $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
        });         
    });

    // $(function() {
    // $(".preload").fadeOut(1000, function() {
    //     $(".content-wrapper").fadeIn(0);        
    // });
});
</script>
@endsection

