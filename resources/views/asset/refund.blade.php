@extends('template.master')
@section('header')
<meta name="csrf-token" content="{{ csrf_token() }}">
Asset Refund
@endsection
@section('content')
<style>
    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }
</style>
<form>
<div class="row">
    <div class="col-md-12">
        @if ($errors->has('upload_file'))
            <p class="alert alert-danger">{{ $errors->first('upload_file') }}</p>
        @endif
 
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('error')!!}
            </div>
        @endif        
    
        <div class="panel panel-default panel-table">
            <div class="panel-heading icon-container">
                Asset Refund
                <a class="btn btn-success pull-right" id="export" style="margin-right:5px" type="submit"> Export</a>
            </div>

            <div class="panel-body">
                <form> 
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="form-group">
                                    <select id="user" name="user">                                        
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-primary btn-xl pull-right" title="filter">
                                    <i class="mdi mdi-filter-list"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="col-md-12">
                    <h3 style="font-size: 18px;" id="nik"></h3>
                    <h3 style="font-size: 18px;" id="nama"></h3>
                    <h3 style="font-size: 18px;">Detail Pengembalian</h3>
                    <table id="table-data" class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                        <thead>
                            <tr>
                                <th style="width:10%">No</th>
                                <th style="width:40%">Asset Name</th>
                                <th style="width:25%">Asset Price</th>
                                <th style="width:25%">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($data != null)
                                @foreach($data as $d)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>      
                                        <td>[ {{ $d->assetAttr->code_asset }} ] - {{ $d->assetAttr->description_asset }}</td>
                                        <td>{{ number_format($d->assetAttr->purchase_cost) }}</td>      
                                        <td>{{ $d->assetAttr->retired_date }}</td>      
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            
                <div class="col-md-12">
                <br>
                <br>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-5">
                                <h3 style="font-size: 18px;">Detail Potongan</h3>
                            </div>
                        </div>
                    </div>
                    <table id="table-data" class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                        <thead>
                            <tr>
                                <th style="width:10%">No</th>
                                <th style="width:50%">Asset Name</th>
                                <th style="width:40%">Asset Price</th>
                                <th style="width:10%" class="text-center" ><input class="form-check-input" type="checkbox" id="selectedFunction"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>                                
                                @php
                                    $sum = 0;
                                @endphp
                                @if($data2 != null)
                                    @foreach($data2 as $d)
                                        @php

                                        @endphp
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>[ {{ $d->code_asset }} ] - {{ $d->description_asset }}</td>
                                            <td>{{ number_format($d->purchase_cost) }}</td>
                                            <td class="text-center">
                                                <input class="form-check-input" id="{{ $d->id_asset }}" type="checkbox" value='{{ $d->code_asset }};{{ $d->description_asset }};{{ $d->id_responsible_person }}' name="selectAsset">
                                            </td>
                                        </tr>
                                        @php
                                            $sum += $d->purchase_cost;
                                        @endphp
                                    @endforeach
                                @endif
                            </tr> 
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="2">Total Refund</td>
                                <td>{{ number_format($sum) }}</td>
                                <td>
                                    @if($data != null)
                                    <a class="show-used btn btn-warning" data-toggle="modal" data-id="contoh" style="cursor: pointer;margin-bottom: 10px;">Refund Asset</a>
                                    @endif
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">List Refund Asset</h4>
			</div>
			<div class="modal-body">		
				<div id="table-asset-popup" style="height: 200px !important; overflow: auto;">
					<table id="table_assetrefund" class="table table-bordered table-condensed">
						<thead>
							<tr>
								<th>Asset Code</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody id="data-asset">
						</tbody>
					</table>
                    
				</div>
                <br>
                <div id="table-asset-popup" style="height: 300px !important; overflow: auto;">
                <h4 class="modal-title">Process Refund</h4>
                <br>
					<table id="table_assetrefund" class="table table-borderless table-condensed">
						<tbody>
                            <tr>
								<th style="width: 40%;vertical-align: middle;">Status</th>
								<td><select id="asset_status" name="asset_status"></select></td>
							</tr>
                            <tr>
								<th style="width: 40%;vertical-align: middle;">Retired Reason</th>
								<td><select id="retired_reason" name="retired_reason"></select></td>
							</tr>
                            <tr>
								<th style="width: 40%;vertical-align: middle;">Retired Date</th>
								<td>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="retired_date" value="{{ old('retired_date') }}" id="retired_date">
                                        <div class="input-group-addon btn btn-primary">
                                            <i class="mdi mdi-calendar"></i>
                                        </div>
                                    </div>
                                </td>
							</tr>
                            <tr>
								<th style="width: 40%;vertical-align: middle;">Retired Remarks</th>
								<td>
                                    <select class="form-control" name="retired_remarks" id="retired_remarks">                                        
                                        <option value="Resign">Karyawan Resign</option>
                                        <option value="Masa Pakai">Masa Pakai Asset</option>
                                        <option value="Promosi / Demosi">Promosi / Demosi Jabatan</option>
                                    </select>
                                </td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
                <a class="process-refund btn btn-success" style="margin-bottom: 10px;">Process</a>
                <a class="btn btn-default" style="margin-bottom: 10px;" data-dismiss="modal">Close</a>
			</div>
			</div>

		</div>
	</div>
</div>
</form>

@endsection
@section('afterscript')
    <script type="text/javascript">
        $('#retired_date').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#export').hide();       
        $('#user').select2({
            width: '100%',
            placeholder: 'Choose user',
            ajax:  {
                url: '{{route('user_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                minimumInputLength: 2,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: "["+item.nik+"] "+item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

        @if($user != null)
            var user = <?= json_encode($user->id) ?>;
            $('#user').val(<?= json_encode($user->id) ?>).trigger('change');
            $('#export').show();
            $("#export").attr("href", "{{ url('export-refund') }}/"+user+"");
            $("#nik").text("NIK = {{$user->nik}}");
            $("#nama").text("Nama = {{$user->name}}");
        @endif

        $('.show-used').click(function() {

        $("#data-asset").empty();
        var selectedasset = new Array();
        
       
        $('input[name="selectAsset"]:checked').each(function() {
            var data = new Array();
            var value = this.value;
            var strarray = value.split(";");
            var markup = "<tr><td>" + strarray[0] + "</td>" + "<td>" + strarray[1] + "</td></tr>"
            $("#data-asset").append(markup);
        })

        var jumlah = $('input[name="selectAsset"]:checked').length;
        if (jumlah == 0)
        {

            window.alert("Pilih asset sebelum proses refund !!");
        }
        else
        {
            $('#myModal').modal('show');
        }

        $('.process-refund').click(function(){
            var status      = $('select[name="asset_status"]').val();
            var reason      = $('select[name="retired_reason"]').val();
            var date        = $('input[name="retired_date"]').val();
            var remarks     = $('select[name="retired_remarks"]').val();
            var arr_asset   = new Array();
            var i = 0;

            if (status === null||reason === null||date === null){
                window.alert("Status Asset / Retired Reason / Tanggal Retired wajib diisi !!");
                return false;
            }

            $('input[name="selectAsset"]:checked').each(function() {
                var value = this.value;           
                var strarray = value.split(";");
                var list = new Array();
                list['id'] = this.id;
                list['responsible'] = strarray[2];
                list['status_retired'] = status;
                list['status_reason'] = reason;
                list['retired_date'] = date;
                list['retired_remarks'] = remarks;
                var json = {id: this.id,responsible:strarray[2],status_retired:status,status_reason:reason,retired_date:date,retired_remarks:remarks};
                arr_asset[i] = json;
                i++;
            })
            
            var jsondata = JSON.stringify(Object.assign({}, arr_asset));
            /*console.log(jsondata);*/
            $.ajax({
                    dataType : 'JSON',
                    type:'POST',
                    url: '{{route('asset-refund-ajax')}}',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data : jsondata,
                    success : function (data){
                        $('#myModal').modal('hide');
                        window.alert("Asset berhasil Refund");
                        setInterval(function(){location.reload();},500);
                    },
                    error : function(e){
                        window.alert("Asset gagal untuk Refund. Message error :" + e );
                    }
            })
        })

        $('#asset_status').select2({
            width: '100%',
            placeholder: 'Status',
            ajax:  {
                url: '{{route('status_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#retired_reason').select2({
            width: '100%',
            placeholder: 'Retired Reason',
            ajax:  {
                url: '{{route('retired_reason_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
	})
    

    </script>
    <script> 
        /* Function Checkbox on Change */

        $('#selectedFunction').on('change',function() {
            var totalAsset = $('input[name="selectAsset"]').length;
            if($(this).is(':checked')) {
                $('input[name=selectAsset]').prop('checked',true);
            }
            else {
                $('input[name=selectAsset]').prop('checked',false);
            }
        })

        $('input[name=selectAsset]').on('change',function() {
            var totalAsset      = $('input[name="selectAsset"]').length;
            var totalChecked    = $('input[name="selectAsset"]:checked').length;
            if(totalChecked < totalAsset) {
                $('#selectedFunction').prop('checked',false);
            }
            else {
                $('#selectedFunction').prop('checked',true);
            } 
        })

    </script>

@endsection

