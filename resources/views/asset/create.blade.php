@extends('template.master')
@section('header')
Asset Master
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading icon-container">
                <a href="{{ url('asset') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Create New Asset<span class="panel-subtitle"></span>
            </div>
            <div class="panel-body">
            
                <!-- form start -->
                <form id="form-create" class="form-horizontal" method="post" action="{{ url('asset') }}">
                    {!! csrf_field() !!}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                            <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab_1" data-toggle="tab">Asset Master </a></li>
                                        <!--<li><a href="#tab_2" data-toggle="tab">Request Assets</a></li> -->
                                        <!--<li><a href="#" data-toggle="tab">Classification</a></li> -->
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab_1">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    
                                                </div>
                                            </div> 

                                            <div class="row">
                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <div class="col-sm-8">
                                                            <label>Material<font size="3" color="red"> *</font></label>
                                                            <select class="id_material form-control" id="id_material" name="id_material" required></select>
                                                        </div>
                                                        <div id="description">
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Description</label>
                                                            <input maxlength="50" type="text" class="form-control" name="desc_assets" id="desc_assets" value="{{ old('desc_assets') }}"> 
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Asset Number<font size="3" color="red"> *</font></sup></label>
                                                            <input maxlength="50" id="code_asset" type="text" class="form-control" name="code_asset" value="{{ old('code_asset') }}" id="code_asset" required autofocus onkeyup="changeToUpperCase(this)"> 
                                                            @if ($errors->has('code_asset'))
                                                                <span class="help-block" style="color:red">{{ $errors->first('code_asset') }}</span>
                                                            @endif
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Serial Number</label>
                                                            <input type="text" id="serial_num" class="form-control" name="serial_num" id="serial_num" value="{{ old('serial_num') }}" autofocus onkeyup="changeToUpperCase(this)" required="false"> 
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                            <div class="col-sm-12">
                                                                <label>Superior Assets</label>
                                                                <select class="form-control" name="id_superior_asset"  id="id_superior_asset">
                                                                </select>
                                                            </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Old Asset Number</label>
                                                            <select id="old_asset" name="old_asset" class="old_asset form-control"></select> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Vendor</label>
                                                            <select class="supplier form-control" name="supplier" id="supplier"></select>
                                                        </div>
                                                    </div>  

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Plant<font size="3" color="red"> *</font></sup></label>
                                                            <select id="plant" name="plant" class="form-control" required onchange="getLocation()">
                                                            </select> 
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Location Name</sup></label>
                                                            <select id="id_location" name="id_location" class="form-control">
                                                            </select> 
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Depreciation Asset <font size="3" color="red"> *</font></label>
                                                            <div class="input-group">
                                                                <select id="depreciation" name="depreciation" class="form-control " disabled>
                                                                </select>
                                                                <div class="input-group-addon btn btn-primary btn-lg" data-toggle="buttons" onclick="active_depreciation()">
                                                                    <i class="mdi mdi-check" id="is_asset"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Group Owner <font size="3" color="red"> *</font></label>
                                                            <select id="valuation_group" name="valuation_group" class="form-control" required>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Assets Type</label>
                                                            <select id="type_assets" name="type_assets" class="form-control">
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label style="text-align: left">Responsible Person <font size="3" color="red"> *</font></label>
                                                            <select class="form-control" name="responsible_person" value="{{ old('responsible_person') }}" id="responsible_person" required></select> 
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label style="text-align: left">Status</label>
                                                            <select id="status" name="status" class="form-control">
                                                                <option value="1">Active</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label style="text-align: left">Purchase Date</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="purchase_date" name="purchase_date" value="{{ old('purchase_date') }}">
                                                                <div class="input-group-addon btn btn-primary">
                                                                    <i class="mdi mdi-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div> 

                                                    <div class="form-group">
                                                        <div class="col-sm-8">
                                                            <label style="text-align: left">Purchase Price</label>
                                                            <input type="number" min="1" class="form-control" name="purchase_cost" value="{{ old('purchase_cost') }}" id="purchase_cost"> 
                                                        </div>

                                                        <div class="col-sm-4">
                                                            <label style="text-align: left; color:white">.</label>
                                                            <select id="curency" name="curency" class="form-control">
                                                                <option value="1">IDR</option>
                                                                <option value="2">USD</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <label>Warranty End</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="warranty_finish" name="warranty_finish" value="{{ old('warranty_finish') }}">
                                                                <div class="input-group-addon btn btn-primary">
                                                                    <i class="mdi mdi-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-8">
                                                            <label>Maintenance Cycle</label>
                                                            <div class="input-group">
                                                                <input type="number" min="1" class="form-control" id="time_cycle" name="time_cycle" value="{{ old('time_cycle') }}" autofocus onkeyup="changeToUpperCase(this)">
                                                                <div class="input-group-addon btn btn-primary">
                                                                    <i class="mdi mdi-time"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="text-align: left; color:white">.</label>
                                                            <select id="unit_cycle" name="unit_cycle" class="form-control">
                                                            <!-- <option value="1">Second</option>
                                                                <option value="2">Minutes</option>
                                                                <option value="3">Hour</option> -->
                                                                <option value="4">Day</option>
                                                                <option value="5">Week</option>
                                                                <option value="6">Month</option>
                                                                <option value="7">Year</option>
                                                            </select>
                                                        </div>
                                                    </div>


                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2">         
                                                </div>
                                            </div>
                                            
                                            <div id="bar" class="collapse in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <input type="hidden" class="form-control" style="width: 110px" id="us2-lon" name="longitude" />
                                                            <input type="hidden" class="form-control" style="width: 110px" id="us2-lat" name="latitude" />
                                                            <br>
                                                            <div id="us2" style="width: 100%; height: 300px;"></div>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    
                                        <!-- /.tab-pane -->
                                    <!-- <div class="tab-pane" id="tab_2">

                                        </div>-->
                                    <!-- /.tab-pane -->
                                    </div>
                                <!-- /.tab-content -->
                                </div>
                            <!-- nav-tabs-custom -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <div class="row xs-pt-15">
                            <div class="col-xs-12">
                                <p class="text-right">
                                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- form end -->
            </div>
        </div>
    </div>
</div>        

@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
 <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs"></script>
<script>
    $(function() {
        $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });         
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        @if(Auth::user()->id_role != 1)
            $('#valuation_group').val({{Auth::user()->id_role}}).trigger('change');
        @endif

        $('#id_material').select2({
            placeholder: 'Choose Material',
                ajax:  {
                            url: '{{route('material_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {

                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        text: "["+item.name+"] "+item.description,
                                        id: item.id
                                    }
                                })
                            };
                        },

                    cache: true
                }
            });


        $('#purchase_date').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        $('#warranty_start').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        $('#warranty_finish').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        $('#last_maintenance').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#next_maintenance').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
        $('#retired_date').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        $('#type_assets').select2({
            placeholder: 'Choose Asset Type',
            ajax:  {
                tags: "true",
                url: '{{route('assettype_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                //dropdownParent: parentElement,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#valuation_group').select2({
            placeholder: 'Group Owner',
            ajax:  {
                tags: "true",
                url: '{{route('groupowner_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                //dropdownParent: parentElement,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#plant').select2({
            width: '100%',  
            placeholder: 'Choose Plant',
            ajax:  {
                url: '{{route('plant_active_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#old_asset').select2({
            width: '100%',  
            placeholder: 'Choose Old Asset',
            ajax:  {
                url: '{{route('asset_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('#id_superior_asset').select2({
            width: '100%',  
            placeholder: 'Choose Superior Asset',
            ajax:  {
                url: '{{route('asset_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
        /*
        $('#status').select2({
            width: '100%',
            placeholder: 'Status',
            ajax:  {
                url: '{{route('status_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        */
        $('#retired_reason').select2({
            width: '100%',
            placeholder: 'Retired Reason',
            ajax:  {
                url: '{{route('retired_reason_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        var path = "{{ route('autocomplete.ajax') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
        

    });



        $(document).ready(function(){
            $('#supplier').select2({
            placeholder: 'Choose Vendor',
                ajax:  {
                            url: '{{route('supplier_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {

                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        },

                    cache: true
                }
            }); 

            $('#responsible_person').select2({
            placeholder: 'Choose Responsible',
                ajax:  {
                            url: '{{route('user_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {

                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        // text: item.name,
                                        text: "["+item.nik+"] "+item.name,
                                        id: item.id
                                    }
                                })
                            };
                        },

                    cache: true
                }
            });
        
        $('#depreciation').select2({
            width: '100%',
            placeholder: 'Depreciation Asset',
            ajax:  {
                url: '{{route('depreciation_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    //console.log(data);
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: '[ '+ item.useful_life + ' Years ] - ' + item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
    });

</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
    });
    function tes() {
        $('#us2').locationpicker({
            location: {
                latitude: $('#us2-lat').val(),
                longitude: $('#us2-lon').val()
            },
            inputBinding: {
                latitudeInput: $('#us2-lat'),
            longitudeInput: $('#us2-lon'),
            locationNameInput: $('#us2-address')
            },
            radius:0,
            enableAutocomplete: true       
        });
    } 
</script> 



<script type="text/javascript">
$(window).load(function () {
   $('#bar').removeClass('in')
   

});
</script> 

<script>
        function changeToUpperCase(t) {
           var eleVal = document.getElementById(t.id);
           eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_]/g, "");
        }

        function getLocation() {

            var plant = $("#plant").val();
            $.ajax({
                url: '{{route('get_location')}}',
                type: 'post',
                data: {
                    plant_id : plant
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    var first = $("<option selected disabled></option>").text('-- Choose Location --');
                    $('#id_location').empty();
                    $("#id_location").select2({
                        data: data,
                        width: '100%'
                    })
                    @if(old('id_location'))
                    $('#id_location').val({{old('id_location')}}).trigger('change');
                    @else
                    $('#id_location').append(first).trigger('change');
                    @endif
                },
                fail: function (data) {
                    alert('error occured');
                }
            });
        }

        $('#id_location').on('select2:select', function (e) {
            getMap();
        });

        function getMap() {

            var id = $("#id_location").val();
            $.ajax({
                url: '{{route('get_map')}}',
                type: 'post',
                data: {
                    id: id
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    $('#us2-lat').val(data.latitude);
                    $('#us2-lon').val(data.longitude);
                    tes();
                },
                fail: function (data) {
                    alert('error occured');
                }
            });
        }
        function active_depreciation() {
            if($('#depreciation').prop('disabled')){
                $('#depreciation').prop('disabled', false);
                $("#is_asset").removeClass("mdi mdi-check");
                $("#is_asset").addClass("mdi mdi-close");
                $("input").prop('required',true);
            }
            else {
                $('#depreciation').prop('disabled', true);
                $("#is_asset").removeClass("mdi mdi-close");
                $("#is_asset").addClass("mdi mdi-check");
                $("input").prop('required',false);
                $("#depreciation").empty();
            }

        }
        

</script>
<script>
$('#submit_update').on('click', function() {
    $('input[name=serial_num').removeAttr("required");
    $('input[name=warranty_finish').removeAttr("required");
    $('input[name=time_cycle').removeAttr("required");
    $('input[name=unit_cycle').removeAttr("required");
    $('input[name=last_maintenance').removeAttr("required");
    $('input[name=retired_ramarks').removeAttr("required");
});
</script>
@endsection