@extends('template.master')
@section('header')
Asset Master
@endsection
@section('beforehead')
<style>
    .img-thumb {
        width:200px;
        height:200px;
        background-color:#f5f5f5; 
        margin-top: 10px;
        margin-bottom: 10px;
        margin-left:10px;
        border: 1px solid #bdbdbd;
        padding: 0 !important;
        overflow:hidden;
        display: flex; 
        justify-content: center;
        align-items: center;
    }

    .img-thumb:hover {
        border: 1.5px solid #bdbdbd;
    }

    #img-upload {
        border: 5px dashed #bdbdbd;
        background-color: #f5f5f5;
        display:flex;
        justify-content:center;
        align-items:center;
    }

    #img-upload i {
        font-size: 50px;
        color:#bdbdbd;
    }

    .delete-image{
        display:none;
        position: absolute;
        right: 5px;
        top: 2px;
    }
    .delete-image2{
        display:block;
        right: 0px;
        top: 0px;
    }

    .img-thumb:hover .delete-image {
        display:none;
    }


</style>
<!-- panorama 360 cdn -->
    <link rel="stylesheet" href="https://cdn.pannellum.org/2.3/pannellum.css"/>
    <script type="text/javascript" src="https://cdn.pannellum.org/2.3/pannellum.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
    <link rel="stylesheet" href="{{ asset('webix/webix.css') }}">
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading icon-container">
                <a href="{{ url('asset') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Detail Asset<span class="panel-subtitle"></span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        </br>
                        <strong>Asset Hierarchy</strong>
                        </br>
                        <div id="box"></div>
                    </div>
                    <div class="col-md-9">
                    <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_asset" data-toggle="tab" aria-expanded="true">Detail Asset</a></li>
                                <li class=""><a href="#tab_depreciation" data-toggle="tab" aria-expanded="false">Depreciation</a></li> 
                                <li class=""><a href="#tab_images" data-toggle="tab" aria-expanded="false">Images</a></li> 
                                <li class=""><a href="#tab_location" data-toggle="tab" aria-expanded="false">Location</a></li>
                                <li class=""><a href="#tab_maps" data-toggle="tab" aria-expanded="false">Map Location</a></li>
                            </ul>
                            <div class="tab-content">

                                <div class="tab-pane active"  id="tab_asset">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table table-striped table-hover">
                                                <tr>
                                                    <td><strong>Material</strong></td>
                                                    <td>{{ $list->material->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Description</strong></td>
                                                    <td>{{ $list->description_asset ?? null }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Asset Number</strong></td>
                                                    <td>{{ $list->code_asset  }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Depreciation</strong></td>
                                                    <td>{{ $list->depreciation->name ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Serial Number</strong></td>
                                                    <td>{{ $list->serial_number ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Superior Asset</strong></td>
                                                    <td>{{ $list->parent_asset->code_asset ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Old Asset Number</strong></td>
                                                    <td>{{ $list->old_asset->code_asset ?? NULL  }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Vendor</strong></td>
                                                    <td>{{ $list->supplier->name ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Plant</strong></td>
                                                    <td>{{ $list->plant_asset->name ?? NULL}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Location Name</strong></td>
                                                    <td>{{ $list->location->name ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Group Owner</strong></td>
                                                    <td>{{ $list->valuation->name ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Asset Type</strong></td>
                                                    <td>{{ $list->type->name ?? NULL}}</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-md-6">
                                            <table class="table table-striped table-hover">
                                                <tr>
                                                    <td><strong>Status</strong></td>
                                                    <td>{{ $list->asset_status->name ?? null }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Purchase Date</strong></td>
                                                    <td>
                                                    @php  
                                                    $new_date =  !empty($list->purchase_date) ? date('d-m-Y', strtotime($list->purchase_date)) : "";
                                                    @endphp 
                                                    {{ $new_date}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Purchase Price</strong></td>
                                                    <td>@if ( $list->curency == 1)                               
                                                            IDR {{ $list->purchase_cost or '-'}}
                                                            @elseif ( $list->curency == 2 or '-')
                                                            USD {{ $list->purchase_cost}}
                                                        @endif</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Warranty End</strong></td>
                                                    <td>
                                                    @php  
                                                    $new_date =  !empty($list->waranty_finish) ? date('d-m-Y', strtotime($list->waranty_finish)) : "";
                                                    @endphp 
                                                    {{ $new_date}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Maintenance Cycle</strong></td>
                                                    <td>{{ $list->count_duration or '-' }} Day</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Last Maintenance</strong></td>
                                                    <td>
                                                    @php  
                                                    $new_date =  !empty($list->last_maintenance) ? date('d-m-Y', strtotime($list->last_maintenance)) : "";
                                                    @endphp 
                                                    {{ $new_date}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Retired Date</strong></td>
                                                    <td>
                                                    @php  
                                                    $new_date =  !empty($list->retired_date) ? date('d-m-Y', strtotime($list->retired_date)) : "";
                                                    @endphp 
                                                    {{ $new_date}}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Retired Reason</strong></td>
                                                    <td>@if ( $list->retired_reason == 1)
                                                            Broken
                                                            @elseif ( $list->retired_reason == 2)
                                                            Warranty End
                                                            @elseif ( $list->retired_reason == 3)
                                                            Lost
                                                        @endif
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Retired Remarks</strong></td>
                                                    <td>{{ $list->retired_ramarks }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Created By</strong></td>
                                                    <td>{{ $list->userCreated->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Created Time</strong></td>
                                                    <td>{{ $list->created_at}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Updated User</strong></td>
                                                    <td>{{ $list->userUpdated->name ?? NULL}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Updated Time</strong></td>
                                                    <td>{{ $list->updated_at}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <div class="tab-pane" id="tab_location">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <table class="table table-striped table-hover">
                                                <tr>
                                                    <td  width="150px"><strong>Responsible Person</strong></td>
                                                    <td>{{ $list->responsible_person_asset->name ?? NULL }} - [ {{ $list->responsible_person_asset->nik ?? NULL }} ] </td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Address</strong></td>
                                                    <td>{{ $list->location->address ?? NULL }}</td>
                                                </tr>
                                            </table>
                                        </div>

                                        <div class="col-md-4">
                                            <table class="table table-striped table-hover">
                                                <tr>
                                                    <td><strong>Province</strong></td>
                                                    <td>{{ $items->location->province ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>City</strong></td>
                                                    <td>{{ $items->location->city ?? NULL}}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Building</strong></td>
                                                    <td>{{ $items->location->building ?? NULL }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>unit</strong></td>
                                                    <td>{{ $items->location->unit ?? NULL }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Location History</h3>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Location Name</th>
                                                        <th>Address</th>
                                                        <th>Building</th>
                                                        <th>Unit</th>
                                                        <th style="width: 90px;">Updated By</th>
                                                        <th style="width: 150px;">Updated Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($location_history) != 0)
                                                    @php $i = 1 @endphp
                                                    @foreach($location_history as $val)
                                                        <tr>
                                                            <td style="text-align: center">{{ $i++ }}</td>
                                                            <td>{{ $val->location->name }}</td>
                                                            <td>{{ $val->location->address }}</td>
                                                            <td>{{ $val->location->building }}</td>
                                                            <td>{{ $val->location->unit }}</td>
                                                            <td>{{ $val->userCreated->name }}</td>
                                                            <td>{{ $val->created_at }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>                                            
                                            <h3>Responsible Person History</h3>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Responsible Person</th>
                                                        <th style="width: 90px;">Updated By</th>
                                                        <th>Updated Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($person_history) != 0)
                                                    @php $i = 1 @endphp
                                                    @foreach($person_history as $val)
                                                        <tr>
                                                            <td style="text-align: center">{{ $i++ }}</td>
                                                            <td>{{ $val->user->name }} - [ {{ $val->user->nik}} ]</td>
                                                            <td>{{ $val->userCreated->name }}</td>
                                                            <td>{{ $val->created_at }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.tab-pane -->
                                <!-- /.tab-classific -->
                                <div class="tab-pane" id="tab_depreciation">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h3>Depreciation History</h3>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                    <tr>
                                                        <th style="width: 20px;">#</th>
                                                        <th>Periode</th>
                                                        <th>Depreciation Amount</th>
                                                        <th>Salvage Value</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @if(count($depreciation_history) != 0)
                                                    @php $i = 1 @endphp
                                                    @foreach($depreciation_history as $h)
                                                        <tr>
                                                            <td style="text-align: center">{{ $i++ }}</td>
                                                            <td>{{ $h->depreciation_periode }}</td>
                                                            <td>Rp. {{ number_format($h->depreciation_amount,2,',','.') }}</td>
                                                            <td>Rp. {{ number_format($h->salvage_value,2,',','.') }}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table> 
                                            </div>                                      
                                    </div>
                                </div><!--end clasification-->

                                <div class="tab-pane" id="tab_images">
                                    <!-- Images -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box-header">
                                                <strong class="box-title"> Material Image</strong>
                                            </div>
                                            <div id="image-gallery"><!-- ajax loaded --></div>

                                        </div>
                                    </div>
                                    <!-- Images asset -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box-header">
                                                <strong class="box-title"> Asset Image</strong>
                                            </div>
                                            <div id="image-gallery-asset"><!-- ajax loaded --></div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="box-header">
                                                <strong class="box-title"> Asset Image 360</strong>
                                            </div>
                                            <div id="image360-gallery-asset"><!-- ajax loaded --></div>
                                        </div>
                                    </div>
                                </div> <!--end image-->
                                <div class="tab-pane" id="tab_maps">
                                        <div class="col-md-12">
                                            <div id="map_area" style="width: 100%; height: 500px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- /.tab-content -->
                        </div>
                    <!-- nav-tabs-custom -->
                    </div>
                </div>
            </div>                        
        </div>
    </div>
</div>
               


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Move Asset</h4>
            </div>
            <div class="modal-body">
                <form id="form-create" class="form-horizontal" method="post" action="{{ url('/asset/'.$list->id_asset.'/location') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id_asset" value="{{$list->id_asset}}" />
                    <input type="hidden" name="longitude_old" value="{{ $list->longitude  }}"/>
                    <input type="hidden" name="latitude_old" value="{{ $list->latitude  }}"/>
                    <h4>Old Location</h4>
                    </br>
                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Address </label>
                        <div class="col-sm-10">
                            <input type="text" name="address_old" class="form-control input-sm" value="{{$list->location_address}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Building </label>
                        <div class="col-sm-4">
                            <input type="text" name="building_old" class="form-control input-sm" value="{{$list->building}}" readonly>
                        </div>

                        <label class="col-sm-2 control-label">unit </label>
                        <div class="col-sm-4">
                            <input type="text" name="unit_old" class="form-control input-sm" value="{{$list->unit}}" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Province</label>
                        <div class="col-sm-4">
                            <input type="text" name="province_old" class="form-control input-sm" value="{{$list->province}}" readonly>
                        </div>

                        <label class="col-sm-2 control-label">City </label>
                        <div class="col-sm-4">
                            <input type="text" name="city_old" class="form-control input-sm" value="{{$list->city}}" readonly>
                        </div>
                    </div>
                        <!-- input -->
                    </br>
                    <h4>New Location</h4>
                    </br>

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-2 control-label">Address </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control clearme" id="us3-address" id="location" name="location" placeholder="Location ..." >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Building </label>
                        <div class="col-sm-4">
                            <input type="text" name="building" class="form-control input-sm" >
                        </div>

                        <label class="col-sm-2 control-label">unit </label>
                        <div class="col-sm-4">
                            <input type="text" name="unit" class="form-control input-sm" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Province</label>
                        <div class="col-sm-4">
                            <input type="text" name="province" class="form-control input-sm">
                        </div>

                        <label class="col-sm-2 control-label">City </label>
                        <div class="col-sm-4">
                            <input type="text" name="city" class="form-control input-sm">
                        </div>
                    </div>
                    
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <input type="hidden" class="form-control" style="width: 110px" id="us3-lon" name="longitude" />
                                    <input type="hidden" class="form-control" style="width: 110px" id="us3-lat" name="latitude" />
                                    <br>
                                    
                                </div>
                            </div> 

                    
                        <div id="us3" style="width: 100%; height: 300px;"></div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-create').submit()"> Save</button> 
            <!--<button  class="btn btn-success"> Save</button>-->
            </div>
        </div>
        
        </div>
    </div>

    <!-- Modal2 -->
    <div class="modal fade" id="myModal2" role="dialog">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Move Responsible Person</h4>
            </div>
            <div class="modal-body">
                <form id="form-create2" class="form-horizontal" method="post" action="{{ url('/asset/'.$list->id_asset.'/person') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id_asset" value="{{$list->id_asset}}" />

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-4 control-label">Old Responsible Person </label>
                        <div class="col-sm-8">
                            <input type="text" name="responsible_person_old" class="form-control input-sm" value="{{$list->responsible_person}}" readonly>
                        </div>
                    </div>

                        <!-- input -->


                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label class="col-sm-4 control-label">New Responsible Person </label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control clearme" name="responsible_person" >
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-create2').submit()"> Save</button> 
            <!--<button  class="btn btn-success"> Save</button>-->
            </div>
        </div>
        
        </div>
    </div>

@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs"></script>
<script src="{{ asset('webix/webix.js') }}"></script>
</script> 

<script type="text/javascript">

    function initMap() {
            var map = new google.maps.Map(document.getElementById('map_area'), {
            zoom: 13,
            center: {
                lat: {{ $list->latitude  }}, 
                lng: {{ $list->longitude  }}
            },
            });

            // Construct the polygon.
            var poly = new google.maps.Polygon({
                paths: json,
                strokeColor: '#f4b942',
                strokeOpacity: 1,
                strokeWeight: 1,
                fillColor: '#ffff8c99',
                fillOpacity: 0.5
            });
            poly.setMap(map);

            //set polygon to ceter of map
            // var bounds = new google.maps.LatLngBounds();
			// var points = poly.getPath().getArray();
			// for (var n = 0; n < points.length ; n++){
			// 	bounds.extend(points[n]);
			// }
			// map.fitBounds(bounds);

    }

    $(document).ready(function(){
        initMap();

        $('.clearme').focus(function() { 
            $(this).val(''); 
        });

        $('#us2').locationpicker({
            location: {
                latitude: "{{ $list->latitude  }}",
                longitude: "{{ $list->longitude  }}"
            },    
            radius:0,  
        });
        
         $('#us3').locationpicker({
            location: {
                latitude: "-6.189680",
                longitude: "106.823440"
            },
            inputBinding: {
                latitudeInput: $('#us3-lat'),
            longitudeInput: $('#us3-lon'),
            locationNameInput: $('#us3-address')
            },
            radius:0,
            enableAutocomplete: true       
        });

        loadImages(); 
        loadImagesAsset();     
        loadImages360Asset(); 
        
    });
</script>

<script type="text/javascript">

function loadImages(){
	$("#image-gallery").load('{{ route('material.images.list',$items->id_material) }}');
}

function loadImagesAsset(){
	$("#image-gallery-asset").load('{{ route('asset.images.list',$items->id_asset) }}');
}

function loadImages360Asset(){
	$("#image360-gallery-asset").load('{{ route('asset.images360.list',$items->id_asset) }}');
}

$(window).load(function () {
   $('#bar').removeClass('in')
 

});


function ShowHide(id) {
  if ($(id).html() == "Show Map") {
    $(id).html("Hide Map");
  } else {
    $(id).html ("Show Map");
  }

}


</script> 

<script>
        webix.ui({
            container:"box",
            view:"tree",
            height: 500,
            weigh:45,
            data: [
                {
                    id:"{{$list->id_asset}}",
                    value:"{{$list->code_asset}} - {{$list->description_asset}}",
                    open: true,
                    data:[
                        @php
                        $child1  = \App\Models\Transaction\Asset::where('id_superior_asset',$list->id_asset)->get();
                        @endphp
                        @foreach($child1 as $child)
                        {
                            id:"{{$child->id_asset}}",
                            value:"{{$child->description_asset}} - {{$child->code_asset}}",
                            open: true,
                            data:[
                                @php
                                $child2  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                                @endphp
                                @foreach($child2 as $child)
                                {
                                    id:"{{$child->id_asset}}",
                                    value:"{{$child->description_asset}} - {{$child->code_asset}}",
                                    open: true,
                                    data:[
                                        @php
                                        $child3  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                                        @endphp
                                        @foreach($child3 as $child)
                                        {
                                            id:"{{$child->id_asset}}",
                                            value:"{{$child->description_asset}} - {{$child->code_asset}}",
                                            open: true,
                                            data:[
                                                @php
                                                $child4  = \App\Models\Transaction\Asset::where('id_superior_asset',$child->id_asset)->get();
                                                @endphp
                                                @foreach($child4 as $child)
                                                {
                                                    id:"{{$child->id_asset}}",
                                                    value:"{{$child->description_asset}} - {{$child->code_asset}}",
                                                    open: true,
                                                }
                                                @endforeach
                                            ]
                                        }
                                        @endforeach
                                    ]
                                }
                                @endforeach
                            ]
                        },
                        @endforeach
                    ]
                }
            ],
            on: {'onItemClick': function (data) {
                //console.log(data);
                window.open("{{url('asset/') }}/"+data);
            }},
            
            
        });
</script>
@endsection