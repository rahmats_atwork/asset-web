@foreach($images as $val)
<div class="col-md-3 img-thumb">
    <div class="delete-image" id="{{ $val->id_image_asset }}">
        <i id="icon-upload" class="fa fa-close" style="color:red;font-size:20px;cursor:pointer" title="Delete image"></i> 
    </div>
    <div>
        <a href="{{ asset($val->image_asset) }}" data-lightbox="image-gallery">
            <img src="{{ asset($val->image_asset) }}" style="max-width:200px; max-height:200px"/>
        </a>
    </div>
</div>
@endforeach