@php $i = 1 @endphp
@foreach($images as $val)

<div class="col-md-3 img-thumb">
    <div class="delete-image2" id="{{ $val->id}}">
        <i id="icon-upload" class="fa fa-close" style="color:red;font-size:20px;cursor:pointer" title="Delete image"></i> 
    </div>
   <!-- <div>
        <a href="{{ asset($val->image360_asset) }}" data-lightbox="image-gallery">
            <img src="{{ asset($val->image360_asset) }}" style="max-width:200px; max-height:200px"/>
        </a>-->
                        <div id="panorama{{$i}}"></div>                                      
    <!--</div>-->
</div>
<script>
pannellum.viewer('panorama{{$i}}', {
            "type": "equirectangular",
            // "autoLoad": true,
            "panorama": "{{ asset($val->image360_asset) }}"
        });
</script>
@php $i++ @endphp
@endforeach
