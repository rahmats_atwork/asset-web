<head>
    <title>Print Barcode - Asset Management System </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}
</script>
<body style="font-size: <?php echo $config['font-size']?>px">
    <input type="button" onclick="printDiv('printableArea')" value="Print Barcode" />
    <div id="printableArea" style="border:1px solid black;">
        <div style="margin-left:<?php echo $config['margin-left'].$config['measurement'];?>;padding-top:<?php echo $config['margin-top'].$config['measurement'];?>;margin-right:<?php echo $config['margin-right'].$config['measurement'];?>;">
        <?php foreach($assets as $asset):?>
            <div style="padding-bottom:<?php echo $config['margin-bottom'].$config['measurement'];?>;">
                <?php
                // $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                $qrCode = new CodeItNow\BarcodeBundle\Utils\QrCode();
                $qrCode->setText($asset->code_asset)
                    ->setSize(1000)
                    ->setPadding(0)
                    ->setErrorCorrection('high')
                    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
                    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
                    ->setImageType(CodeItNow\BarcodeBundle\Utils\QrCode::IMAGE_TYPE_PNG)
                ;
                echo nl2br($config['text-over']).'<br/>';
                // echo '<img src="data:image/png;base64,'.base64_encode($generator->getBarcode($asset->code_asset, $generator::TYPE_CODE_39)).'" style="width:'.$config['width'].$config['measurement'].'; height:'.$config['height'].$config['measurement'].';"/><br/>';
                echo '<img src="data:'.$qrCode->getContentType().';base64,'.$qrCode->generate().'" style="width:'.$config['width'].$config['measurement'].'; height:'.$config['height'].$config['measurement'].';"/><br/>';
                echo $asset->code_asset.'<br/>';
                echo nl2br($config['text-below']).'<br/>';
                ?>
            </div>
        <?php endforeach;?>
        </div>
    </div>
</body>