@extends('template.master')
@section('header')
Asset Master
@endsection
@section('content')
<style>
    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>
<div class="row">
    <div class="col-md-12">
        @if ($errors->has('upload_file'))
            <p class="alert alert-danger">{{ $errors->first('upload_file') }}</p>
        @endif
 
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
                
            
                <div class="panel panel-default panel-table">
                    <div class="row">
                            <div class="col-sm-12">
                                <div id="accordion1" class="panel-group accordion" style="margin-bottom:0">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo" class="collapsed"><i class="icon mdi mdi-chevron-down"></i> Filter</a></h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form class="form-horizontal" method="get">
                                                        {{--  {!! csrf_field() !!}  --}}
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Status</br></label>
                                                                </div>
                                                                <select id="status" name="status[]" class="form-control" multiple="multiple">
                                                                    {{--  <option {{ Request::input('status') == '' ? "selected":"" }} value="">Status</option>  --}}
                                                                    @foreach($status as $st)                                                           
                                                                        <option {{ Request::input('status') == $st->code}} value="{{$st->code}}">{{$st->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Plant</br></label>
                                                                </div>
                                                                <select id="plant" name="plant[]" class="form-control" multiple="multiple">
                                                                    {{--  <option {{ Request::input('plant') == '' ? "selected":"" }} value="">plant</option>  --}}
                                                                    @foreach($plants as $r)                                                           
                                                                        <option {{ Request::input('plant') == $r->id}} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Material</br></label>
                                                                </div>
                                                                <select id="material" name="material[]" class="form-control" multiple="multiple">
                                                                    {{--  @foreach($bo_ar as $a)                                                           
                                                                        <option value="{{$a}}">{{$a}}</option>
                                                                    @endforeach  --}}
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Location Name</br></label>
                                                                </div>
                                                                <select id="location" name="location[]" class="form-control" multiple="multiple">
                                                                    {{--  <option {{ Request::input('plant') == '' ? "selected":"" }} value="">plant</option>  --}}
                                                                    @foreach($location as $r)                                                           
                                                                        <option {{ Request::input('plant') == $r->id}} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Responsible Person</br></label>
                                                                </div>
                                                                <select id="responsible" name="responsible" class="form-control" multiple="multiple">
                                                                    @foreach($responsible as $r)                                                           
                                                                        <option {{ Request::input('responsible') == $r->name}} value="{{$r->id}}">[ {{$r->nik}} ] - {{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Vendor</br></label>
                                                                </div>
                                                                <select id="vendor" name="vendor[]" class="form-control" multiple="multiple">
        
                                                                    @foreach($supplier as $r)                                                           
                                                                        <option {{ Request::input('vendor') == $r->id}} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Asset Type</br></label>
                                                                </div>
                                                                <select id="type" name="type[]" class="form-control" multiple="multiple">
                                                                    {{--  <option {{ Request::input('plant') == '' ? "selected":"" }} value="">plant</option>  --}}
                                                                    @foreach($type as $r)                                                           
                                                                        <option {{ Request::input('type') == $r->id}} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Group Owner</br></label>
                                                                </div>
                                                                <select id="valuation" name="valuation[]" class="form-control" multiple="multiple">
                                                                    {{--  <option {{ Request::input('plant') == '' ? "selected":"" }} value="">plant</option>  --}}
                                                                    @foreach($valuation as $r)                                                           
                                                                        <option {{ Request::input('valuation') == $r->id}} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label> Created Date From</label>
                                                                        <div class="input-group">
                                                                            <input type="text"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="start_date"
                                                                               id="start_date"
                                                                               class="form-control date datetimepicker">
                                                                            <span class="input-group-addon btn btn-primary">
                                                                            <i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">                                                                    
                                                                </div>
                                                                <div class="col-md-5 col-sm-5">
                                                                    <div class="form-group">
                                                                        <label>To</label>
                                                                        <div class="input-group">
                                                                            <input type="text"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="end_date"
                                                                               id="end_date"
                                                                               class="form-control date datetimepicker">
                                                                            <span class="input-group-addon btn btn-primary">
                                                                            <i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Superior Asset</br></label>
                                                                </div>
                                                                <select id="superior" name="superior[]" class="form-control" multiple="multiple">
                                                                    {{--  @foreach($bo_ar as $a)                                                           
                                                                        <option value="{{$a}}">{{$a}}</option>
                                                                    @endforeach  --}}
                                                                </select>
                                                                <button type="submit" class="btn btn-primary btn-xl" title="filter"><i class="mdi mdi-filter-list "></i></button>
                                                                 <!-- <button formaction="{{-- url('savefilter/asset') --}}" class="btn btn-success btn-xl" title="Save Filter"><i class="mdi mdi-bookmark"></i></button>  -->
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                    </div>
                    <div class="panel-heading icon-container">
                        <a href="{{ url('asset/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                        Asset List
                        <a href="#upload" class="btn btn-success pull-right"  data-toggle="modal" data-target="#upload-modal">Upload Asset</a>
                        {{--  <a href="#upload-update" class="btn btn-success pull-right"  data-toggle="modal" data-target="#upload-modal-update">Upload Update Asset</a>   --}}
                        <a onclick="myFunction()" id="hrefdownload" class="btn btn-success pull-right" style="margin-right:5px" type="submit"> Export</a>
                        {{-- <a href="{{ url('barcode/asset') }}" class="btn btn-success pull-right" title="Print Barcode" style="margin-right:5px">Print Barcode</a> --}}
                        <a onclick="myFunction2()" id="hrefbarcode" target="_blank" class="btn btn-success pull-right" title="Print Barcode" style="margin-right:5px">Print Barcode</a>
                    </div>
        
                    <div class="panel-body">
                        <div class="pull-left" style="margin-left: 10px;">
                            <form>
								<div>
								<select id="limit-row" name="limit" class="form-control">
									<option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
									<option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
									<option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
								</select>
								</div>
                            </form>
                        </div>
                        <div class="pull-right" style="margin-right: 10px;">
                            <input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
                        </div>
                        <table id="table-data" class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="width: 100px;">Action</th>
                                    <th style="width: 200px;">Asset Number</th> 
                                    <th style="width: 100px;">Description</th>            
                                    <th style="width: 90px;"> Serial Number</th>
                                    <th style="width: 90px;">Status</th>
                                    <th style="width: 90px;">Material</th>
                                    <th style="width: 90px;">Location</th>
                                    <th style="width: 90px;">Vendor</th>
                                    <th style="width: 90px;">Asset Type</th>
                                    <th style="width: 90px;">Group Owner</th>
                                    <th style="width: 90px;">Superior Asset</th>
                                    <th style="width: 90px;">Responsible Person</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($assets as $key => $asset)
                                
                                <tr>
                                    <td>{{ $assets->firstItem() + $key }}</td>
                                    <td class="actions">
                                        <div class="row">
                                        <div class="btn-group btn-space">
                                            <a href="{{ route('asset.show',$asset->id_asset) }}" type="button" class="btn btn-default">Detail</a>
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            <ul role="menu" class="dropdown-menu" style="min-width:100px" >
                                                <li><a href="{{ route('asset.edit',$asset->id_asset) }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
                                                <li><a href="{{ url('print_barcode/asset/'.$asset->id_asset) }}" target="_blank"><i class="mdi mdi-print"></i> Print Barcode</a></li>
                                                <li><a href="#delete-{{$asset->id_asset}}" onclick="if(confirm('Are you sure you want to delete this item?')) $('#delete-{{$asset->id_asset}}').submit()"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
                                                <form id="delete-{{$asset->id_asset}}" method="POST" action="{{ route('asset.destroy',$asset->id_asset) }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            </ul>
                                        </div>
                                    </div>
                                    </td>
                                    <td title="{{ $asset->code_asset }}">{{ $asset->code_asset }}
                                        {{-- @php
                                        $generator = new Picqer\Barcode\BarcodeGeneratorHTML();
                                        echo $generator->getBarcode($asset->code_asset, $generator::TYPE_CODE_128);
                                    @endphp --}}
                                    </td>
                                    <td title="{{ $asset->description_asset }}">{{ $asset->description_asset }}</td>
                                    <td title="{{ $asset->serial_number or '-' }}">{{ $asset->serial_number or '-' }}</td>
                                    <td>{{ !empty($asset->asset_status) ? $asset->asset_status->name : 'Draft'}}</td>
                                    {{-- <td title="">{{$asset->plant_asset->name}}</td> --}}
                                    <td title="{{ !empty($asset->material) ? $asset->material->description : '-' }}">{{ !empty($asset->material) ? $asset->material->description : '-' }}</td>
                                    <td title="">{{!empty($asset->location->name) ? $asset->location->name : "-"}}</td>
                                    <td title="">{{!empty($asset->supplier) ? $asset->supplier->name : "-"}}</td>
                                    <td title="{{ !empty($asset->type) ? $asset->type->name : '-' }}">{{ !empty($asset->type) ? $asset->type->name : '-' }}</td>
                                    <td title="">{{!empty($asset->valuation) ? $asset->valuation->name : "-"}}</td>
                                    <td title="">{{!empty($asset->parent_asset) ? $asset->parent_asset->code_asset : "-"}}</td>
                                    <td title="">{{!empty($asset->responsible_person_asset->name) ? $asset->responsible_person_asset->name  : "-"}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            <?php
                            $appends = [];
                            foreach(request()->all() as $k => $v)
                                $appends[$k] = $v;
							?>
                            {{ $assets->appends($appends)->links() }}
                        </div>
                    
                    </div>
				</div>
	</div>
</div>
<!-- Upload Modal -->
<div id="upload-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Asset</h4>
      </div>
      <div class="modal-body">
		<form id="form-upload" class="form-horizontal" method="post" action="{{ url('asset/upload') }}" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="box-body text-center" >

				<div id="file_name"></div>
				<label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
					Browse <input name="upload_file" type="file" style="display: none" onChange="$('#file_name').html(this.files[0].name)">
				</label>

                <!-- <div class="alert">
                <strong>Info!</strong> Per upload Max 1000 rows.
                </div> -->

			</div>
			
		</form>
      </div>
      <div class="modal-footer">
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button> -->
				<a href="{{ url('asset/download/template') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>
				<button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload').submit()"><i class="fa fa-upload"></i> Upload</button>
      </div>
    </div>

  </div>
</div><!-- Upload modal -->

<!-- Upload Modal Update -->
<div id="upload-modal-update" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Upload Update Asset</h4>
      </div>
      <div class="modal-body">
		<form id="form-upload2" class="form-horizontal" method="post" action="{{ url('asset/uploadupdate') }}" enctype="multipart/form-data">
			{!! csrf_field() !!}
			<div class="box-body text-center" >

				<div id="file_name2"></div>
				<label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
					Browse <input name="upload_file" type="file" style="display: none" onChange="$('#file_name2').html(this.files[0].name)">
				</label>

			</div>
			
		</form>
      </div>
      <div class="modal-footer">
				<!-- <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button> -->
				{{--  <a href="{{ url('asset/download/templateupdate') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>  --}}
				<button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload2').submit()"><i class="fa fa-upload"></i> Upload</button>
				<!-- <button type="button" class="btn btn-success" data-dismiss="modal" onclick="myFunction()"><i class="fa fa-upload"></i> Upload</button>                 -->
      </div>
    </div>

  </div>
</div><!-- Upload modal -->

@endsection
@section('afterscript')
<script>
    function delete_data(){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete').submit();
        }
    }

    function myFunction() {
    var search = "export/asset"+window.location.search;
    document.getElementById("hrefdownload").href = search;
    }

    function myFunction2() {
    var search2 = "barcode/asset"+window.location.search;
    document.getElementById("hrefbarcode").href = search2;
    }

    $(function() {
        $('#table-data').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });         
    });

    $(document).ready(function(){
        var init_material = <?php echo json_encode($input_material) ?>;
        $('#material').select2({
			width: '33%',
            placeholder: 'Choose Material',
			allowClear: true,
            tokenSeparators: [',', ' '],
			ajax:  {
						url: '{{route('material_ajax')}}',
						dataType: 'json',
						type : 'GET',
						delay: 250,
						processResults: function (data) {
						return {
							results:  $.map(data, function (item) {
								return {
									text: item.name+' - '+item.description,
									id: item.id
								}
							})
						};
					},

				cache: true
			}
		}).val(init_material).trigger("change");;
        // $("#material").select2('val', init_material);

        var init_status = <?php echo json_encode($input_status) ?>;
        $('#status').select2({ 
            width: '33%',
            placeholder: 'Choose Status',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_status).trigger("change");
        // $("#status").select2('val', init_status);

        // console.log(init_status);
        
        var init_plant = <?php echo json_encode($input_plant) ?>;
        $('#plant').select2({ 
            width: '33%',
            placeholder: 'Choose Plant',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_plant).trigger("change");
        // $("#plant").select2('val', init_plant);

        var init_vendor = <?php echo json_encode($input_vendor) ?>;
        $('#vendor').select2({ 
            width: '33%',
            placeholder: 'Choose Vendor',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_vendor).trigger("change");
        // $("#vendor").select2('val', init_vendor);

        var init_type = <?php echo json_encode($input_type) ?>;
        $('#type').select2({ 
            width: '33%',
            placeholder: 'Choose Type',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_type).trigger("change");
        // $("#type").select2('val', init_type);

        var init_valuation = <?php echo json_encode($input_valuation) ?>;
        $('#valuation').select2({ 
            width: '33%',
            placeholder: 'Choose Group',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_valuation).trigger("change");
        // $("#valuation").select2('val', init_valuation);

        var init_responsible = <?php echo json_encode($input_responsible) ?>;
        $('#responsible').select2({ 
            width: '33%',
            placeholder: 'Choose responsible',
            allowClear: true,
            tokenSeparators: [',', ' '],
            maximumSelectionLength: 1
        }).val(init_responsible).trigger("change");

            var init_superior = <?php echo json_encode($input_superior) ?>;
        $('#superior').select2({
            width: '33%',
            placeholder: 'Choose Superior Asset',
			allowClear: true,
            tokenSeparators: [',', ' '],
                ajax:  {
                            url: '{{route('superior_asset_ajax')}}',
                            dataType: 'json',
                            type : 'GET',
                            delay: 250,
                            processResults: function (data) {
                    
                            return {
                              results:  $.map(data, function (item) {
                                    return {
                                        text: item.code_asset,
                                        id: item.id_asset
                                    }
                                })
                            };
                        },

                    cache: true
                }
            }).val(init_superior).trigger("change");
            // $("#superior").select2('val', init_superior);

            var init_location = <?php echo json_encode($input_location) ?>;
            $('#location').select2({ 
                width: '33%',
                placeholder: 'Choose location',
                allowClear: true,
                tokenSeparators: [',', ' '],
            }).val(init_location).trigger("change");
            // $("#location").select2('val', init_location);
    });	

    var start_date = <?php echo json_encode($start_date) ?>;
    $('#start_date').val(start_date);

    var end_date = <?php echo json_encode($end_date) ?>;
    $('#end_date').val(end_date);
    
    $(".datetimepicker").datepicker({
        autoclose: true
    });

    $('#search-input').keypress(function (e) {
		if(e.which == 13) {
            <?php
            $query_array = request()->query();
            unset($query_array['q']);
            ?>
            var query = "<?php echo !empty($query_array)?"&".http_build_query($query_array):"";?>";
            window.location.replace("{{ url('asset') }}?q="+($("#search-input").val()?$("#search-input").val():'')+query);
		}
	});

    $('#limit-row').change(function(){
        <?php
        $query_array = request()->query();
        unset($query_array['limit']);
        ?>
        var query = "<?php echo !empty($query_array)?"&".http_build_query($query_array):"";?>";
        window.location.replace("{{ url('asset') }}?limit="+($("#limit-row").val()?$("#limit-row").val():'')+query);
	});
</script>
@endsection
