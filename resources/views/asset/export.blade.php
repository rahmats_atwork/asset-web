<table>
    <thead>
        <tr>
            <th>NIK</th>
            <td>:</td>
            <td>{{ $user->nik }}</td>
        </tr>
        <tr>
            <th>Nama</th>
            <td>:</td>
            <td>{{ $user->name }}</td>
        </tr>
        <tr>
            <th>Tanggal</th>
            <td>:</td>
            <td>{{ $user->date }}</td>
        </tr>
    </thead>
</table>
<table border="1">
    <thead>
        <tr>
            <th colspan="4">Detail Pengembalian</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Asset Name</th>
            <th>Asset Price</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>     
        @if($refund != null)
            @foreach($refund as $d)
                <tr>
                    <td>{{ $loop->iteration }}</td>      
                     <td>[ {{ $d->code_asset }} ] - {{ $d->description_asset }}</td>
                    <td>{{ number_format($d->purchase_cost) }}</td>      
                    <td>{{ $d->date }}</td>      
                </tr>
            @endforeach
        @endif       
    </tbody>
</table>


<table>
    <thead>
        <tr>
            <th colspan="3">Detail Potong</th>
        </tr>
        <tr>
            <th>No</th>
            <th>Asset Name</th>
            <th>Asset Price</th>
        </tr>
    </thead>
    <tbody>
        @php
            $sum = 0;
        @endphp
        @if($refund != null)
            @foreach($assets as $r)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>[ {{ $r->code_asset }} ] - {{ $r->description_asset }}</td>
                    <td>{{ number_format($r->purchase_cost) }}</td>
                </tr>
                @php
                    $sum += $r->purchase_cost;
                @endphp
            @endforeach
        @endif
        <tr>
            <td colspan="2">Total Refund</td>
            <td>{{ number_format($sum) }}</td>
        </tr>
    </tbody>
</table>                       