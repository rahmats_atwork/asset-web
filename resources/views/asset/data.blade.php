
        <table>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Material Code</th>
                    <th>Asset Number</th> 
                    <th>Description</th>            
                    <th>Serial Number</th>
                    <th>Superior Asset Number</th>
                    <th>Old Asset Number</th>
                    <th>Vendor</th>
                    <th>Plant</th>
                    <th>Group Owner</th>
                    <th>Asset Type</th>
                    <th>Responsible Person</th>
                    <th>Location Name</th>
                    <th>Location</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Building</th>
                    <th>Unit</th>
                    <th>Status</th>
                    <th>Purchase Date</th>
                    <th>Purchase Price</th>
                    <th>Currency</th>
                    <th>Warranty End Date</th>
                    <th>Maintenance Cycle</th>
                    <th>MC Unit</th>
                    <th>Last Maintenance Date</th>
                    <th>Retired Remarks</th>
                </tr>
            </thead>
            <tbody>
                @foreach($assets as $asset)
                
                <tr>
                    <td>{{ $loop->index + 1}}</td>
                    <td>{{ isset($asset->material) ? $asset->material->name : '' }}</td>
                    <td>{{ $asset->code_asset }}</td>
                    <td>{{ $asset->description_asset }}</td>
                    <td>{{ $asset->serial_number or '' }}</td>
                    <td></td>
                    <td></td>
                    <td>{{ $asset->supplier ? $asset->supplier->name_supplier : '' }}</td>
                    <td>{{ $asset->region }}</td>
                    <td>{{ $asset->valuation ? $asset->valuation->name : '' }}</td>
                    <td>{{ $asset->type ? $asset->type->name : '' }}</td>
                    <td>{{ $asset->nik }} - {{ $asset->responsible_person }}</td></td>
                    <td>{{ isset($asset->location) ? $asset->location->tim_serpo_name : '' }}</td>
                    <td>{{ $asset->address_location }}</td>
                    <td>{{ $asset->latitude }}</td>
                    <td>{{ $asset->longitude }}</td>
                    <td>{{ $asset->building }}</td>
                    <td>{{ $asset->unit }}</td>
                    <td>{{ $asset->asset_status ? $asset->asset_status->name : ''}}</td>
                    <td>@if($asset->purchase_date!=NULL){{ \Carbon\Carbon::parse($asset->purchase_date)->format('d/m/Y') }}@endif</td>
                    <td>{{ $asset->purchase_cost }}</td>
                    <td>{{ $asset->curency }}</td>
                    <td>@if($asset->waranty_finish!=NULL){{ \Carbon\Carbon::parse($asset->waranty_finish)->format('d/m/Y') }}@endif</td>
                    <td>{{ $asset->count_duration }}</td>
                    <td>{{ $asset->unit_duration }}</td>
                    <td>@if($asset->last_maintenance!=NULL){{ \Carbon\Carbon::parse($asset->last_maintenance)->format('d/m/Y') }}@endif</td>
                    <td>{{ $asset->retired_ramarks }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
                        