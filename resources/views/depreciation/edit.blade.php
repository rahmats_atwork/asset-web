@extends('template.master')
@section('header')
Depreciation Master
@endsection
@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-divider icon-container">
                <a href="{{ url('depreciation') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Edit Depreciation<span class="panel-subtitle"></span>
            </div>
            <div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" action="{{ url('depreciation/'.$list->id) }}">
						{!! csrf_field() !!}
						<input type="hidden" name="_method" value="put" />
						<div class="col-md-12">
							<div class="row">
								<div class="form-group">
									<div class="col-sm-4">
										<input type="hidden" name="id_depreciation" value="{{ $list->id }}" />
										<label>Depreciation Type</sup></label>
										<select id="id_depreciation_type" name="id_depreciation_type" class="form-control">
											<option value="{{ $list->id_depreciation_type }}" selected="">{{ $list->depreciationType->name }}</option>
										</select> 
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<label class="control-label">Name<font size="3" color="red"> *</font></label>
										<input type="text" name="name" maxlength="40" class="form-control" value="{{ $list->name }}" required>
										@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-4">
										<label>Useful Life Asset &nbsp( Year )<font size="3" color="red"> *</font></label>
										<input type="number" min="1" name="useful_life" maxlength="9" class="form-control" value="{{ $list->useful_life }}" required>
											@if ($errors->has('useful_life'))
										<span class="help-block" style="color:red">{{ $errors->first('useful_life') }}</span>
											@endif										
									</div>
									{{-- 
									<div class="col-sm-4">
										<label>Rate Depreciation ( % )<font size="3" color="red"> *</font></label>
										<input type="number" min="1" name="rate" maxlength="9" class="form-control" value="{{ old('rate') }}" required>
											@if ($errors->has('rate'))
										<span class="help-block" style="color:red">{{ $errors->first('rate') }}</span>
											@endif										
									</div>
									--}}
								</div>
							</div>
						</div>
						<div class="row xs-pt-15">
							<div class="col-xs-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
		                    {{--  <a href="{{ url('depreciation') }}" class="btn btn-default">Back</a>  --}}
								</p>
							</div>
						</div>
					</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('afterscript')

<script>
$(document).ready(function(){
	$('#id_depreciation_type').select2({
    	placeholder: 'Choose Depreciation Type',
        ajax:  {
			url: '{{route('depreciation_type_ajax')}}',
			dataType: 'json',
			type : 'GET',
			delay: 250,
			processResults: function (data) {
				return {
					results:  $.map(data, function (item) {
						return {
							text: item.name,
							id: item.id
						}
					})
				};
			},
			cache: true
		}
	});
});
</script>
@endsection
