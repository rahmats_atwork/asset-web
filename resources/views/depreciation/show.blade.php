@extends('template.master')
@section('header')
Depreciation Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('depreciation') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail Depreciation<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th width="200">Name</th>
								<td>{{ $list->name }}</td>
							</tr>
							<tr>
								<th>Useful Life</th>
								<td>{{ $list->useful_life }} Years</td>
							</tr>
							<tr>
								<th>Rate Depreciate</th>
								<td>{{ isset($list->rate) ? $list->rate : " - " }} %</td>
							</tr>							
						</table>
					</div>

					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th style="width:20%">Created By</th>
								<td>{{ isset($list->userCreated) ? $list->userCreated->name : ""}}</td>
							</tr>
							<tr>
								<th>Created Date</th>
								<td>{{ $list->created_at }}</td>
							</tr>
							<tr>
								<th>Updated By</th>
								<td>{{ isset($list->userUpdated) ? $list->userUpdated->name : ""}}</td>
							</tr>	
							<tr>
								<th>Updated Date</th>
								<td>{{$list->updated_at}}</td>
							</tr>						
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
@endsection