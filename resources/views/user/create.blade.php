@extends('template.master')
@section('beforehead')
<style>
		.img-thumb {
			width:200px;
			height:200px;
			background-color:#f5f5f5; 
			margin-top: 10px;
			margin-left:10px;
			border: 1px solid #bdbdbd;
			padding: 0 !important;
			overflow:hidden;
			display: flex; 
			justify-content: center;
			align-items: center;
		}
		
		.img-thumb:hover {
			border: 1.5px solid #bdbdbd;
		}
		
		.img-upload {
			width:200px;
			height:200px;
			border: 5px dashed #bdbdbd;
			background-color: #f5f5f5;
			display:flex;
			justify-content:center;
			align-items:center;
		}
		
		.img-upload i {
			font-size: 50px;
			color:#bdbdbd;
		}
		
		.delete-image{
			display:none;
			position: absolute;
			right: 5px;
			top: 2px;
		}
		
		.img-thumb:hover .delete-image {
			display:block;
		}
		
		</style>
@endsection
@section('header')
User Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('user') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Create User<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('user') }}">
						{!! csrf_field() !!}
						<div class="col-md-12">
							<div class="row">

								<div class="col-md-12">
									<div class="form-group">
										<div class="col-sm-12">
											<label>Image</label>
										</div>
										<div id="image-gallery"><!-- ajax loaded --></div>
										<label class="col-sm-3 btn"  style="position: relative; overflow:hidden">
											<i id="btn-plus" class="icon img-upload"><span class="mdi mdi-plus"></span></i> 
												<input id="img-selector" name="icon" value="{{old('icon')}}" type="file" style="display: none" accept="image/*">
												<img class="img-thumb" src="" id="profile-img-tag" style="display: none"/>
												@if ($errors->has('icon'))
													<span class="help-block col-sm-1" style="color:red">{{ $errors->first('icon') }}</span>
												@endif
										</label>		
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-sm-12">
										<label>Name<font size="3" color="red"> *</font></label>
											<input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
											@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
											@endif										
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>NIK<font size="3" color="red"> *</font></label>
											<input type="text" name="nik" class="form-control" value="{{ old('nik') }}" required>
											@if ($errors->has('nik'))
											<span class="help-block" style="color:red">{{ $errors->first('nik') }}</span>
											@endif										
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>Mobile<font size="3" color="red"> *</font></label>
											<input type="text" name="mobile" class="form-control" value="{{ old('mobile') }}" required>
											@if ($errors->has('nik'))
											<span class="help-block" style="color:red">{{ $errors->first('mobile') }}</span>
											@endif										
										</div>
									</div>
								</div>

								<div class="col-md-6">								
									<div class="form-group">
										<div class="col-sm-12">
											<label>Email<font size="3" color="red"> *</font></label>
											<input type="email" name="email" class="form-control" value="{{ old('email') }}" required>
											@if ($errors->has('email'))
											<span class="help-block" style="color:red">{{ $errors->first('email') }}</span>
											@endif
										</div>
									</div>
									{{--
									<div class="form-group">
										<div class="col-sm-12">
											<label>Plant<font size="3" color="red"> *</font></sup></label>
											<select id="region" name="region" class="form-control" required>
												<option value="">--Choose Plant--</option>
												@foreach($regions as $r)
													<option value="{{$r->region_bismo}}">{{$r->region_bismo}}</option>
												@endforeach
											</select> 
										</div>
									</div>
									--}}
									<div class="form-group">
										<div class="col-sm-12">
											<label>Role<font size="3" color="red"> *</font></sup></label>
											<select id="role" name="role" class="form-control" required>
												<option value="">--Choose Role--</option>
												@foreach($roles as $r)
													<option value="{{$r->id}}">{{$r->name}}</option>
												@endforeach
											</select> 
										</div>
									</div>
								</div>

							</div>
						</div>

						{{--  <div class="row xs-pt-15">  --}}
							<div class="col-md-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
									{{--  <a href="{{ url('statusasset') }}" class="btn btn-default">Back</a>  --}}
								</p>
							{{--  </div>  --}}
						</div>

					</form>
				</div>
			</div>
		</div>
</div>				
 
@endsection
@section('afterscript')
<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img-selector").change(function(){
        readURL(this);
        $("#btn-plus").hide()
		$("#profile-img-tag").show()
    });
    $(function() {
	    $('#location_type').on('keypress', function(e) {
	        if (e.which == 32)
	            return false;
	    });
	});

	function changeToUpperCase(t) {
	   var eleVal = document.getElementById(t.id);
	   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_;:",]/g, "");
	}

    
	$('#region').select2();
	$('#role').select2();
	
</script>
@endsection