@extends('template.master')
@section('header')
User Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ URL::previous() }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail User<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					@if($items->image != null)
						<div class="xs-mt-15 xs-mb-15">					
							<div class="text-center"><img src="{{asset('/storage/images/user/'.$items->image)}}" alt="Placeholder" class="img-circle xs-mr-10" width="200px"></div>
						</div>	
					@else
						<div class="xs-mt-15 xs-mb-15">					
							<div class="text-center"><img src="{{ asset('beagle/img/avatar.png') }}" alt="Placeholder" class="img-circle xs-mr-10" width="200px"></div>
						</div>
					@endif
					<div class="col-md-6">					
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th>Name</th>
								<td>{{ $items->name }}</td>
							</tr>
							<tr>
								<th>NIK</th>
								<td>{{ $items->nik}}</td>
							</tr>
							<tr>
								<th>Mobile</th>
								<td>{{ $items->mobile}}</td>
							</tr>
							<tr>
								<th>Email</th>
								<td>{{ $items->email}}</td>
							</tr>	
							<tr>
								<th>Plant</th>
								<td>{{ $items->region}}</td>
							</tr>	
							<tr>
								<th>Role</th>
								<td>{{ isset($items->role) ? $items->role->name : ""}}</td>								
							</tr>					
						</table>
					</div>
					@if(Auth::user()->id==1)
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th style="width:20%">Created By</th>
								<td>{{ isset($items->createdBy) ? $items->createdBy->name : ""}}</td>
							</tr>
							<tr>
								<th>Created Date</th>
								<td>{{ $items->created_at }}</td>
							</tr>
							<tr>
								<th>Updated By</th>
								<td>{{ isset($items->updatedBy) ? $items->updatedBy->name : ""}}</td>
							</tr>	
							<tr>
								<th>Updated Date</th>
								<td>{{$items->updated_at}}</td>
							</tr>						
						</table>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div> 
@endsection