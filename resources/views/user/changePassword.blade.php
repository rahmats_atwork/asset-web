@extends('template.master')
@section('beforehead')
<style>
		.img-thumb {
			width:200px;
			height:200px;
			background-color:#f5f5f5; 
			margin-top: 10px;
			margin-left:10px;
			border: 1px solid #bdbdbd;
			padding: 0 !important;
			overflow:hidden;
			display: flex; 
			justify-content: center;
			align-items: center;
		}
	
		.img-thumb:hover {
			border: 1.5px solid #bdbdbd;
		}
	
		#img-upload {
			border: 5px dashed #bdbdbd;
			background-color: #f5f5f5;
			display:flex;
			justify-content:center;
			align-items:center;
		}
	
		#img-upload i {
			font-size: 50px;
			color:#bdbdbd;
		}
	
		.delete-image{
			display:none;
			position: absolute;
			right: 5px;
			top: 2px;
		}
	
		.img-thumb:hover .delete-image {
			display:block;
		}
	
	</style>
@endsection
@section('header')
User Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ URL::previous() }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Change Password<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{  url('changepassword/store/'.$items->id) }}">
						{!! csrf_field() !!}
						<input type="hidden" name="_method" value="put" />
						<div class="col-md-12">
							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-sm-12">
										<label>Old Password</label>
											<input type="password" name="oldPassword" class="form-control" value="" placeholder="old password" autofocus required>
											@if ($errors->has('oldPassword'))
											<span class="help-block" style="color:red">{{ $errors->first('oldPassword') }}</span>
											@endif
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>New Password</label>
											<input type="password" name="newPassword" class="form-control" value="" placeholder="new password" required>
                                            @if ($errors->has('newPassword'))
											<span class="help-block" style="color:red">{{ $errors->first('newPassword') }}</span>
											@endif
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>Confirm new Password</label>
											<input type="password" name="confirmNewPassword" class="form-control" value="" placeholder="confrim new password" required>
                                            @if ($errors->has('confirmNewPassword'))
											<span class="help-block" style="color:red">{{ $errors->first('confirmNewPassword') }}</span>
											@endif
										</div>
									</div>

								</div>

							</div>
						</div>

						{{--  <div class="row xs-pt-15">  --}}
							<div class="col-md-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
									{{--  <a href="{{ url('statusasset') }}" class="btn btn-default">Back</a>  --}}
								</p>
							{{--  </div>  --}}
						</div>

					</form>
				</div>
			</div>
		</div>
</div>

@endsection
@section('afterscript')
<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img-selector").change(function(){
        readURL(this);
        $("#btn-plus").hide()
		$("#profile-img-tag").show()
    });
    $("#profile-img-tag").on('click','.delete-image',function(){
	console.log("handle click",$(this).attr('id'))
	
	

})
	$('#region').select2();
	$('#role').select2();

</script>
@endsection
