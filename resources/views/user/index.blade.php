@extends('template.master')
@section('header')
User Master
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('error')!!}
            </div>
        @endif
        <div class="panel panel-default panel-table">

            <div class="panel-heading icon-container">
                <a href="{{ url('user/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                User List
                <a href="#upload" class="btn btn-success pull-right"  data-toggle="modal" data-target="#upload-modal">Upload User</a>
            </div>

            <div class="panel-body">
                <table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
                    <thead>
                        <tr>
                            {{--  <th style="width: 5%">No</th>  --}}
                            <th>Name</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th style="width: 115px;">Action</th>
                        </tr>
                    </thead>
                    @php $i = 1 @endphp
                    @foreach($list as $val)
                    <tr>
                        {{--  <td style="text-align: center">{{ $i++ }}</td>  --}}
                        @if($val->image == null)
                            <td class="user-avatar"> <img src="{{ asset('beagle/img/avatar.png') }}" alt="Avatar">{{ $val->name }}</td>
                        @else
                            <td class="user-avatar"> <img src="{{asset('/storage/images/user/'.$val->image)}}" alt="Avatar">{{ $val->name }}</td>
                        @endif
                        <td>{{ $val->email}}</td>
                        <td> <span class="badge badge-success">Active</span></td>
                        <td class="actions">
                            <form id="form-delete{{$val->id}}" method="post" action="{{ url('user/'. $val->id) }}" class="pull-right">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE" />
                            </form>

                            <div class="btn-group btn-space">
                                <a href="{{ url('user/' . $val->id) }}" type="button" class="btn btn-default">Detail</a>
                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                <ul role="menu" class="dropdown-menu" style="min-width:100px" >
                                    <li><a href="{{ url('user/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
                                    @if($val->id!=1)
                                    <li><a href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
                                    @endif
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Upload Modal -->
<div id="upload-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Upload User</h4>
            </div>
            <div class="modal-body">
                <form id="form-upload" class="form-horizontal" method="post" action="{{ url('user/upload') }}" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                    <div class="box-body text-center">
                        <div id="file_name"></div>
                        <label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
                            Browse <input name="upload_file" type="file" style="display: none" onChange="$('#file_name').html(this.files[0].name)">
                        </label>
                    </div>            
                </form>
            </div>
            <div class="modal-footer">
                <a href="{{ url('user/download/template') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>
                <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload').submit()"><i class="fa fa-upload"></i> Upload</button>
            </div>
        </div>
    </div>
</div><!-- Upload modal -->

@endsection
@section('afterscript')
<script>
    function delete_data(val){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete'+val).submit();
        }
    }

    $(function() {
        $('#table12').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
        });         
    });
</script>
@endsection
