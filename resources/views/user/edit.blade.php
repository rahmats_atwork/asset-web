@extends('template.master')
@section('beforehead')
<style>
		.img-thumb {
			width:200px;
			height:200px;
			background-color:#f5f5f5; 
			margin-top: 10px;
			margin-left:10px;
			border: 1px solid #bdbdbd;
			padding: 0 !important;
			overflow:hidden;
			display: flex; 
			justify-content: center;
			align-items: center;
		}
	
		.img-thumb:hover {
			border: 1.5px solid #bdbdbd;
		}
	
		#img-upload {
			border: 5px dashed #bdbdbd;
			background-color: #f5f5f5;
			display:flex;
			justify-content:center;
			align-items:center;
		}
	
		#img-upload i {
			font-size: 50px;
			color:#bdbdbd;
		}
	
		.delete-image{
			display:none;
			position: absolute;
			right: 5px;
			top: 2px;
		}
	
		.img-thumb:hover .delete-image {
			display:block;
		}
	
	</style>
@endsection
@section('header')
User Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
			<a href="{{url('/changepassword/'.$items->id)}}" id="hrefdownload" class="btn btn-success pull-right" style="margin-right:20px;margin-top:30px"type="submit"> Change Password</a>
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ URL::previous() }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Edit User<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" enctype="multipart/form-data" method="post" action="{{  url('user/'.$items->id) }}">
						{!! csrf_field() !!}
						<input type="hidden" name="_method" value="put" />
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-sm-12">
											<label>Image</label>
										</div>
										<div id="image-gallery"><!-- ajax loaded --></div>
										<label class="col-md-3 btn" style="position: relative; overflow:hidden">
											<i id="btn-plus" class="fa fa-plus img-upload" style="display:none"></i>
											@if($items->image != null)
											<input id="img-selector" value="{{$items->image}}" name="icon" type="file" style="display: none" accept="image/*" >
											<img class="img-thumb" id="profile-img-tag" src="{{URL::asset('/storage/images/user/'.$items->image)}}"/>
											@else
											<input id="img-selector" name="icon" value="{{old('icon')}}" type="file" style="display: none" accept="image/*">
											<img class="img-thumb" id="profile-img-tag" src="{{ asset('beagle/img/avatar.png') }}"/>
											@endif
										</label>		
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-sm-12">
										<label>Name<font size="3" color="red"> *</font></label>
											<input type="text" name="name" class="form-control" value="{{ $items->name }}" required>
											@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
											@endif										
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>NIK<font size="3" color="red"> *</font></label>
											<input type="text" name="nik" class="form-control" value="{{ $items->nik }}" required>
											@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('nik') }}</span>
											@endif										
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
										<label>Mobile<font size="3" color="red"> *</font></label>
											<input type="text" name="mobile" class="form-control" value="{{ $items->mobile }}" required>
											@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('mobile') }}</span>
											@endif										
										</div>
									</div>

									

								</div>

									

								<div class="col-md-6">

									<div class="form-group">
										<div class="col-sm-12">
											<label>Email<font size="3" color="red"> *</font></label>
											<input type="text" name="email" class="form-control" value="{{ $items->email }}" required>
											@if ($errors->has('email'))
											<span class="help-block" style="color:red">{{ $errors->first('email') }}</span>
											@endif
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
												<label>Plant<font size="3" color="red"> *</font></sup></label>
											<select id="region" name="region" class="form-control " required>
												<option value="">Choose Plant</option>
												@foreach($regions as $r)
													<option {{$items->region == $r->region_bismo ? 'selected' : ''}} value="{{$r->region_bismo}}">{{$r->region_bismo}}</option>
												@endforeach
											</select> 
										</div>
									</div>

									<div class="form-group">
										<div class="col-sm-12">
												<label>Role<font size="3" color="red"> *</font></sup></label>
											<select id="role" name="role" class="form-control " required>
												<option value="">Choose Role</option>
												@foreach($roles as $r)
													<option {{$items->id_role == $r->id ? 'selected' : ''}} value="{{$r->id}}">{{$r->name}}</option>
												@endforeach
											</select> 
										</div>
									</div>
								</div>
							</div>
						</div>

						{{--  <div class="row xs-pt-15">  --}}
							<div class="col-md-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
									{{--  <a href="{{ url('statusasset') }}" class="btn btn-default">Back</a>  --}}
								</p>
							{{--  </div>  --}}
						</div>

					</form>
				</div>
			</div>
		</div>
</div>

@endsection
@section('afterscript')
<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img-selector").change(function(){
        readURL(this);
        $("#btn-plus").hide()
		$("#profile-img-tag").show()
    });
    $("#profile-img-tag").on('click','.delete-image',function(){
	console.log("handle click",$(this).attr('id'))
	
	

})
	$('#region').select2();
	$('#role').select2();

</script>
@endsection
