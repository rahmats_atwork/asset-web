
@foreach($parameter as $val)
<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
    <div class="col-sm-12">    
        <label>{{ $val->name }}</label>
        @if ($val->type==2 )
        <div class="input-group">
        <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control datetime" >
        <div class="input-group-addon btn btn-primary">
            <i class="mdi mdi-calendar"></i>
        </div>
        </div>
        @elseif ($val->type==4 )
        <input type="number" min="1" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
        @elseif ($val->type==3 )
        <div class="input-group clockpicker">
            <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control clockpicker" >
            <div class="input-group-addon btn btn-primary">
                <i class="mdi mdi-time"></i>
            </div>
        </div>
        @elseif ($val->type==5 )
        <select id="value" name="parameter[id-{{ $val->id }}]" class="form-control">
            <option value="">-- Choose Value --</option>
            @foreach(explode(',',$val->value) as $key)
            <option {{ $val->existing ==  $key ? 'selected':'' }} value="{{ $key }}">{{ $key }}</option>
            @endforeach
        </select>
                
        @else
        <input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
        @endif
    </div>
</div><!-- form group-->

@endforeach	

<script>
//Init datepicker
$('.datetime').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
$('.clockpicker').clockpicker({autoclose:true});
</script>
