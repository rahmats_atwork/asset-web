@extends('template.master')
@section('header')
Material Master
@endsection
@section('beforehead')
<style>
.img-thumb {
	width:200px;
	height:200px;
	background-color:#f5f5f5; 
	margin-top: 10px;
	margin-left:10px;
	border: 1px solid #bdbdbd;
	padding: 0 !important;
	overflow:hidden;
	display: flex; 
	justify-content: center;
	align-items: center;
}

.img-thumb:hover {
	border: 1.5px solid #bdbdbd;
}

#img-upload {
	border: 5px dashed #bdbdbd;
	background-color: #f5f5f5;
	display:flex;
	justify-content:center;
	align-items:center;
}

#img-upload i {
	font-size: 50px;
	color:#bdbdbd;
}

.delete-image{
	display:none;
	position: absolute;
	right: 5px;
	top: 2px;
}

.img-thumb:hover .delete-image {
	display:block;
}

</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" />
@endsection

@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading icon-container">
					<a href="{{ url('material') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Edit Material<span class="panel-subtitle"></span>
				</div>
					<div class="tab-container">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab_material" data-toggle="tab" aria-expanded="true">Material</a></li>
							<li class=""><a href="#tab_parameter" data-toggle="tab" aria-expanded="false">Parameter</a></li> 
							<li class=""><a href="#tab_images" data-toggle="tab" aria-expanded="false">Images</a></li> 
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab_material">
								<form id="form-create" class="form-horizontal" method="post" action="{{ url('material/'.$items->id) }}">
									{!! csrf_field() !!}
									<input type="hidden" name="_method" value="put" />

									<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
										<div class="col-sm-4">
											<label>Code Material<font size="3" color="red"> *</font></sup></label>
											<input type="text" id="name" name="name" maxlength="30" class="form-control" value="{{ $items->name }}" autofocus onkeyup="changeToUpperCase(this)" required>
											@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
											@endif
										</div>
									</div>

									<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
									<div class="col-sm-12">
											<label>Description</label>
											<input type="text" name="description" class="form-control" value="{{ $items->description }}" maxlength="255">
										</div>
									</div>

									<div class="form-group {{ $errors->has('id_material_group') ? ' has-error' : '' }}">
										<div class="col-sm-4">
											<label>Material group</label>
											<select class="form-control input-sm" name="id_material_group" id="id_material_group">
												@foreach($material_group as $m)
													<option {{$items->id_material_group == $m->id ? 'selected' : ''}} value="{{$m->id}}">{{$m->name}}</option>
												@endforeach
											</select>
										</div>
									</div> 
								

									<div class="form-group {{ $errors->has('id_clasification') ? ' has-error' : '' }}">
										<div class="col-sm-4">
											<label>Classification</label>
											<select class="form-control input-sm" name="id_classification" id="id_classification">
												<option value="">-- Choose Spesification--</option>
												@foreach($classification as $val)
													<option {{$items->id_classification == $val->id? 'selected' : ''}} value="{{$val->id}}">{{$val->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="row xs-pt-15">
										<div class="col-xs-12">
											<p class="text-right">
												<button type="submit" class="btn btn-primary btn-lg">Save</button>
										{{--  <a href="{{ url('materialgroup') }}" class="btn btn-default">Back</a>  --}}
											</p>
										</div>
									</div>
								</form>	
							</div> 
							<!-- tab material -->
							<div class="tab-pane" id="tab_parameter">
								<form id="form-create" class="form-horizontal" method="post" action="{{ url('material/'.$items->id.'/parameter') }}">

									{!! csrf_field() !!}
									<input type="hidden" name="id_material" value="{{ $items->id }}" />

									@foreach($parameter as $val)
									<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
										<div class="col-sm-4">										
											<label>{{ $val->name }}</label>
											@if ($val->type==2 )
											<div class="input-group">
											<input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control datetime" >
											<div class="input-group-addon btn btn-primary">
												<i class="mdi mdi-calendar"></i>
											</div>
											</div>
											@elseif ($val->type==4 )
											<input type="number" min="1" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
											@elseif ($val->type==3 )
											<div class="input-group clockpicker">
												<input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
												<div class="input-group-addon btn btn-primary">
													<i class="mdi mdi-time"></i>
												</div>
											</div>
											@elseif ($val->type==5 )
											<select id="value" name="parameter[id-{{ $val->id }}]" class="form-control">
												<option value="">-- Choose Type Assets --</option>
												@foreach(explode(',',$val->value) as $key)
												<option {{ $val->existing ==  $key ? 'selected':'' }} value="{{ $key }}">{{ $key }}</option>
												@endforeach
											</select>
													
											@else
											<input type="text" name="parameter[id-{{ $val->id }}]" value="{{ $val->existing }}" class="form-control" >
											@endif
											</div>
										</div><!-- form group-->

									@endforeach	
									@if(count($parameter) > 0)	
										<div class="row xs-pt-15">
											<div class="col-xs-12">
												<p class="text-right">
													<button type="submit" class="btn btn-primary btn-lg">Save</button>
											{{--  <a href="{{ url('materialgroup') }}" class="btn btn-default">Back</a>  --}}
												</p>
											</div>
										</div>
									@endif
							</div>
							<div class="tab-pane" id="tab_images">
								
								<!-- Images -->
								<div class="row">
									<div class="col-md-12">
										<div id="image-gallery"><!-- ajax loaded --></div>
										<label class="col-md-3 img-thumb btn" id="img-upload" style="position: relative; overflow:hidden">
											<i id="btn-plus" class="mdi mdi-plus"></i> 
											<i id="btn-upload" class="fa fa-refresh fa-spin" style="display:none"></i> 
											<form method="POST" id="form-img-upload" action="{{ url('/material/'.$items->id.'/image') }}">
												{{ csrf_field() }}
												<input id="img-selector" name="images[]" type="file" style="display: none" accept="image/*" multiple="multiple">
											</form>
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
</div>		

</div>

@endsection

@section('afterscript')

<script>
	$("#img-selector").change(function(){
		// $("#form-img-upload").submit();
		// STOP
		var files 	= this.files
		var form	= new FormData();
		var xhr		= new XMLHttpRequest()
	
		for(var i=0; i < files.length; i++){
			var file = files[i]
	
			form.append('images[]',file,file.name)
		}
	
		// CSRF
		form.append('_token','{{ csrf_token() }}')
	
		// Upload
		xhr.open('POST','{{ route('material.images.upload',$items->id) }}?_date='+ new Date().getTime(),true)
		
		// Handling
		xhr.onload = function () {
			if (xhr.status === 200) {
				// File(s) uploaded.
				console.log("Files uploaded")
	
				// Refresh Images
				$("#btn-upload").hide()
				$("#btn-plus").show()
				loadImages()
			} else {
				alert('An error occurred!');
			}
		};
	
		// Event listener
		xhr.upload.addEventListener("progress", function(event){
			if (event.lengthComputable) {
				var percentComplete = event.loaded / event.total;
				console.log('Upload:'+percentComplete)
			} else {
				// Unable to compute progress information since the total size is unknown
			}
		})
	
		// Send
		$("#btn-upload").show()
		$("#btn-plus").hide()
		xhr.send(form)
	
		console.log("Selected images",files)
	})

	$(document).ready(function(){
		$('#id_material_group').select2(); 

		//Init datepicker
		$('.datetime').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});
		$('.clockpicker').clockpicker({autoclose:true});
		// Load images on startup
		loadImages();
	});
		

		function changeToUpperCase(t) {
		   var eleVal = document.getElementById(t.id);
		   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_]/g, "");
		}
		

			$(document).ready(function(){
				loadImages()				
				$('#id_classification').select2({
				placeholder: 'Choose Classification',
					ajax:  {
								url: '{{route('classification_ajax')}}',
								dataType: 'json',
								type : 'GET',
								delay: 250,
								processResults: function (data) {
					
								return {
									results:  $.map(data, function (item) {
										return {
											text: item.name,
											id: item.id
										}
									})
								};
							},
		
						cache: true
					}
				}); 
			});
		

		// Load image
		function loadImages(){
			$("#image-gallery").load('{{ route('material.images.list',$items->id) }}');
		}
		
		// Delete image
		$("#image-gallery").on('click','.delete-image',function(){
			console.log("handle click",$(this).attr('id'))
		
			var imageId = $(this).attr('id')
			var csrf = $('meta[name="csrf-token"]').attr('content')
		
			$.post('{{ url('/material/image/') }}/'+imageId,{
				'_token': csrf,
				'_method': 'delete'
			}).done(function(data){
				console.log('Delete image done',data)
			}).fail(function(){
				console.log('Delete image fail')
			}).always(function(){
				// console.log('Always call this log')
				loadImages();
			})
			
		})
		
		
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.js"></script>

@endsection