

@extends('template.master')
@section('header')
Material Master
@endsection
@section('content')
<div class="row">
		<div class="col-md-12">
				@if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
				<div class="panel panel-default panel-table">
					<div class="row">
                            <div class="col-sm-12">
                                <div id="accordion1" class="panel-group accordion" style="margin-bottom:0">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo" class="collapsed"><i class="icon mdi mdi-chevron-down"></i> Filter</a></h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form class="form-horizontal" method="get">
                                                        {{--  {!! csrf_field() !!}  --}}
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Material Group<br/></label>
                                                                </div>
                                                                <select id="mat_group" name="mg" class="form-control" multiple="multiple">
                                                                    @foreach($mat_group as $r)                                                           
                                                                        <option {{ in_array($r->id, explode(',', request()->mg))?'selected':'' }} value="{{$r->id}}">{{'['.$r->name.'] '.$r->description}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Classification<br/></label>
                                                                </div>
                                                                <select id="mat_class" name="clsf" class="form-control" multiple="multiple">
                                                                    @foreach($classification as $r)                                                           
                                                                        <option {{ in_array($r->id, explode(',', request()->clsf))?'selected':'' }} value="{{$r->id}}">{{$r->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
        
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <button type="button" class="btn btn-primary btn-xl" id="btn_filter" title="filter"><i class="mdi mdi-filter-list "></i></button>
                                                                <!-- <button formaction="{{ url('savefilter/asset') }}" class="btn btn-success btn-xl" title="Save Filter"><i class="mdi mdi-bookmark"></i></button>  -->
                                                            </div>
                                                        </div>
                                                    </form>
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                    </div>
					<div class="panel-heading icon-container">
						<a href="{{ url('material/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
						Material List
						<a href="#upload" class="btn btn-success pull-right"  data-toggle="modal" data-target="#upload-modal">Upload Material</a>
					</div>
					{{--  <div class="panel-heading icon-container">
						<a href="#" data-toggle="modal" data-target="#form-bp1" type="button" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
						Master Material Group
					</div>  --}}
		
					<div class="panel-body">
						<div class="pull-left" style="margin-left: 10px;">
                            <form>
								<div>
								<select id="limit-row" name="limit" class="form-control">
									<option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
									<option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
									<option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
								</select>
								</div>
                            </form>
                        </div>
						<div class="pull-right" style="margin-right: 10px;">
							<input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
                        </div>
						<table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
							<thead>
								<tr>
									<th style="width: 5%">No</th>
									<th>Material Code</th>
									<th>Description</th>
									<th>Material Group</th>
									<th>Classification</th>
									<th>Used By Asset</th>
									<th style="width: 80px;">Action</th>
								</tr>
							</thead>
							@foreach($list as $key => $val)
							<tr>
								<td style="text-align: center">{{ $list->firstItem() + $key }}</td>
								<td>{{ $val->name }}</td>
								<td>{{ $val->description }}</td>
								<td>{{ !empty($val->materialGroup) ? $val->materialGroup->name : "-" }}</td>
								<td>{{ !empty($val->classification) ? $val->classification->name : "-" }}</td>							
								<td>
									<?php
									$total_used = 0;
									foreach($val->asset as $asset){
										if($asset->status == 1)
											$total_used++;
									}
									?>
									<a class="show-used" data-toggle="modal" data-id="{{$val->id}}" style="cursor: pointer;">{{$total_used}}</a>
								</td>
								<td class="actions">
								<form id="form-delete{{$val->id}}" method="post" action="{{ url('material/'. $val->id) }}" class="pull-right">
										{!! csrf_field() !!}
										<input type="hidden" name="_method" value="DELETE" />
									</form>
		
									<div class="btn-group btn-space">
										<a href="{{ url('material/'. $val->id) }}" type="button" class="btn btn-default">Detail</a>
										<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
										<ul role="menu" class="dropdown-menu" style="min-width:100px" >
											<li><a href="{{ url('material/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
											<li><a style="display: <?php echo $total_used >= 1 ? 'none':'block' ?>" href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
										</ul>
									</div>
								</td>
							</tr>
							@endforeach
						</table>
                        <div class="text-center">
							<?php
							$appends = [];
							if(request()->has('q')) $appends['q'] = request()->q;
							if(request()->has('limit')) $appends['limit'] = request()->limit;
							if(request()->has('mg')) $appends['mg'] = request()->mg;
							if(request()->has('clsf')) $appends['clsf'] = request()->clsf;
							?>
                            {{ $list->appends($appends)->links() }}
                        </div>
					</div>
				</div>
		</div>
</div>

<!-- Upload Modal -->
<div id="upload-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
  
	  <!-- Modal content-->
	  <div class="modal-content">
		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal">&times;</button>
		  <h4 class="modal-title">Upload Material</h4>
		</div>
		<div class="modal-body">
		  <form id="form-upload" class="form-horizontal" method="post" action="{{ url('material/upload') }}" enctype="multipart/form-data">
			  {!! csrf_field() !!}
			  <div class="box-body text-center" >
  
				  <div id="file_name"></div>
				  <label class="btn btn-default btn-lg" style="position: relative; overflow:hidden">
					  Browse <input name="upload_file" type="file" style="display: none" onChange="$('#file_name').html(this.files[0].name)">
				  </label>
  
			  </div>
			  
		  </form>
		</div>
		<div class="modal-footer">
				  <!-- <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Close</button> -->
				  <a href="{{ url('material/download/template') }}" class="btn btn-warning" ><i class="fa fa-download"></i> Download Template</a>
				  <button type="button" class="btn btn-success" data-dismiss="modal" onclick="$('#form-upload').submit()"><i class="fa fa-upload"></i> Upload</button>
		</div>
	  </div>
  
	</div>
</div><!-- Upload modal -->

	<!-- Modal used by -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Used By</h4>
			</div>
			<div class="modal-body">
				{{--<a href="" id="pdfMaterial">Download</a><br/>--}}
				<a href="" id="pdfMaterial" class="btn btn-success" style="margin-bottom: 10px;">Export PDF</a>
				<div id="table-asset-popup" style="height: 300px !important; overflow: auto;">
					<table id="table5" class="table table-bordered table-condensed">
						<thead>
							<tr>
								<th style="width: 5%">No</th>
								<th>Asset Code</th>
								<th>Description</th>
							</tr>
						</thead>
						<tbody id="data-asset">

						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</div>

		</div>
	</div>
@endsection
@section('afterscript')
<script>
    function delete_data(val){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete'+val).submit();
        }
    }

    $(function() {
        $('#table12').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });
    });

    $('.show-used').click(function(){
 		$('#data-asset').empty()
		var id = $(this).attr('data-id');
		document.getElementById("pdfMaterial").href = "{{ url('material/assetusedpdf') }}/"+id;
		url = "{{ url('material/assetused/') }}/"+id;
		$.get(url, function(dataasset) {
			data = JSON.parse(dataasset);
			var html;
			var i=0;
			data.forEach(function(item){
				i++;
				html += '<tr><td style="text-align: center">'+i+'</td><td>'+item.code_asset+'</td><td>'+item.description_asset+'</td></tr>'
			})
 			$('#data-asset').html(html)
		});
		$('#myModal').modal('show');
	})

	$(document).ready(function(){
		var init_mgroup = <?php echo json_encode($input_mgroup) ?>;
		$('#mat_group').select2({ 
			width: '33%',
			placeholder: 'Choose Material Group',
			allowClear: true,
			tokenSeparators: [',', ' '],
		}).val(init_mgroup).trigger("change");

		var init_classification = <?php echo json_encode($input_classification) ?>;
		$('#mat_class').select2({ 
			width: '33%',
			placeholder: 'Choose Classification',
			allowClear: true,
			tokenSeparators: [',', ' '],
		}).val(init_classification).trigger("change");
	});

	$('#limit-row').change(function(){
		window.location.replace("{{ url('material') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val()+"&mg="+$("#mat_group").val()+"&clsf="+$("#mat_class").val());
	});

	$('#search-input').keypress(function (e) {
		if(e.which == 13) {
			window.location.replace("{{ url('material') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val()+"&mg="+$("#mat_group").val()+"&clsf="+$("#mat_class").val());
		}
	});

	$('#btn_filter').click(function() {
		window.location.replace("{{ url('material') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val()+"&mg="+$("#mat_group").val()+"&clsf="+$("#mat_class").val());
	});

</script>
@endsection
