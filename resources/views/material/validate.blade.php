@extends('template.master')
@section('header')
Material Master
@endsection
@section('content')
	<style>
		.table-fit td, 
		.table-fit th {
			white-space: nowrap;
			width: 1%;
			/* max-width:350px; */
		}
	</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-table">
			<div class="panel-heading  icon-container">
				<a href="{{ url('material') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Validate Material Upload<span class="panel-subtitle"></span>
			</div>
			
			<div class="panel-body">
				
				<div style="overflow-x:scroll">
					<table class="table table-striped table-fit">
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>No</th>
								<th>Material Code</th>
								<th>Description</th>
								<th>Material Group</th>
								<th>Classification</th>

							</tr>
						</thead>
						<tbody>
							@foreach($validate as $item)
							<tr>
								<td>@if($item['valid'])
									<p class='label label-success'>Data Valid</p>
									@else
									<p class='label label-warning'>Data Invalid</p>
									@endif
								</td>
								<td>
									<?php echo empty($item['message']) ? "" : "<small class='text-danger'>".$item['message']."</small>"; ?>
								</td>
								<td>{{ $item['no'] }}</td>
								<td>{{ $item['name'] }}</td>
								<td>{{ $item['description'] }}</td>
								<td>{{ $item['material_group'] }}</td>
								<td>{{ $item['classification'] }}</td>
								
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				
			</div>
			
			{{ csrf_field() }}
			@if(!$dataOk)
			<div class="alert alert-warning">
				<strong>Warning</strong> To continue saving data, please fix all invalid data
			</div>
			@else
				<div class="row xs-pt-15">
					<div class="col-md-11">
						<p class="text-right">
							<button type="submit" id="storeUpload" class="btn btn-primary btn-lg">Save</button>
						</p>
					</div>
				</div>
			@endif
		</div>	
							
	</div>
</div>

<script src="{{ asset('assets/js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>

<script type="text/javascript">


	$('#storeUpload').click(function(e){
		e.preventDefault();
		window.location = "{{url('material/upload/save')}}";
	})
</script>
@endsection
