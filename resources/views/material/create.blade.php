@extends('template.master')
@section('header')
Material Master
@endsection
@section('beforehead')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.css" />
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('material') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Create New Material<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" action="{{ url('material') }}">
						{!! csrf_field() !!}
						<div class="row">

							<div class="col-md-6">

								<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<div class="col-sm-6">
										<label>Material Code<font size="3" color="red"> *</font></sup></label>
										<input type="text" id="name" name="name" maxlength="30" class="form-control" value="{{ old('name') }}" autofocus onkeyup="changeToUpperCase(this)" required>
										@if ($errors->has('name'))
										<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
										@endif
									</div>
								</div>

								<div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
									<div class="col-sm-12">
										<label>Description</label>
										<input type="text" id="description" name="description" maxlength="255" class="form-control" value="{{ old('description') }}">
									</div>
								</div>

								<div class="form-group {{ $errors->has('id_material_group') ? ' has-error' : '' }}">
									<div class="col-sm-6">
										<label>Material group</label>
										<select id="id_material_group" name="id_material_group" class="form-control select2">
                                            <option value="">Choose Type</option>
                                                @foreach($material_group as $t)
                                                    <option value="{{$t->id}}">{{$t->name}}</option>
                                                @endforeach
                                        </select>
									</div>
								</div>

								<div class="form-group ">
									<div class="col-sm-6">
										<label>Classification</label>
										<select class="select2 id_classification form-control" name="id_classification" id="id_classification"></select>
									</div>
								</div>
							</div>
							<div id="parameter-form" class="col-md-6">
							<!-- parameter -->
							</div>

						</div>
						<div class="row xs-pt-15">
							<div class="col-xs-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
							{{--  <a href="{{ url('materialgroup') }}" class="btn btn-default">Back</a>  --}}
								</p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
</div>

@endsection

@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.js"></script>
<script type="text/javascript">
	$('#name').change(function() {
		name = $('#name').val();
		$('#description').val(name);
	});
	$(document).ready(function(){
		$('#id_classification').select2({
		placeholder: 'Choose Classification',
			ajax:  {
						url: '{{route('classification_ajax')}}',
						dataType: 'json',
						type : 'GET',
						delay: 250,
						processResults: function (data) {
						return {
							results:  $.map(data, function (item) {
								return {
									text: item.name,
									id: item.id
								}
							})
						};
					},

				cache: true
			}
		}); 

		// On change callback
		$('#id_classification').change(function(){
			var choosenClassification = $(this).val()
			console.log("Change",$(this).val())

			$("#parameter-form").load('{{ url('/') }}/material/classification/'+choosenClassification+'.ajax');
		});

		$('#id_material_group').select2();
    });

function changeToUpperCase(t) {
   var eleVal = document.getElementById(t.id);
   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_]/g, "");
}

</script>
@endsection