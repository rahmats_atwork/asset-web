@extends('template.master')
@section('header')
Role Master
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('error')!!}
            </div>
        @endif
        <div class="panel panel-default">

            <div class="panel-heading icon-container">
                <a href="{{ url('role/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                Role List
            </div>

            <div class="panel-body">
                <table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
                    <thead>
                        <tr>
                            <th style="width: 5%">No</th>
                            <th>ID</th>
                            <th>Role</th>
                            <th>Description</th>
                            <th style="width: 115px;">Action</th>
                        </tr>
                    </thead>
                    @php $i = 1 @endphp
                    @foreach($list as $val)
                    <tr>
                        <td style="text-align: center">{{ $i++ }}</td>
                        <td>{{ $val->id }}</td>
                        <td>{{ $val->name }}</td>
                        <td>{{ $val->description}}</td>
                        <td class="actions">
                            <form id="form-delete{{$val->id}}" method="post" action="{{ url('role/'. $val->id) }}" class="pull-right">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE" />
                            </form>

                            <div class="btn-group btn-space">
                                <a href="{{ url('role/' . $val->id) }}" type="button" class="btn btn-default">Detail</a>
                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                <ul role="menu" class="dropdown-menu" style="min-width:100px" >
                                    <li><a href="{{ url('role/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
                                    <li><a href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('afterscript')
<script>
    function delete_data(val){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete'+val).submit();
        }
    }

    $(function() {
        $('#table12').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            // "scrollX": true,
        });         
    });
</script>
@endsection
