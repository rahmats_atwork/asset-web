@extends('template.master')
@section('header')
Role Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('role') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail Role<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th>Role</th>
								<td>{{ $items->name }}</td>
							</tr>
							<tr>
								<th>Description</th>
								<td>{{ $items->description}}</td>
							</tr>							
						</table>
					</div>
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th style="width:20%">Created By</th>
								<td>{{ isset($items->createdBy) ? $items->createdBy->name : ""}}</td>
							</tr>
							<tr>
								<th>Created Date</th>
								<td>{{ $items->created_at }}</td>
							</tr>
							<tr>
								<th>Updated By</th>
								<td>{{ isset($items->updatedBy) ? $items->updatedBy->name : ""}}</td>
							</tr>	
							<tr>
								<th>Updated Date</th>
								<td>{{$items->updated_at}}</td>
							</tr>						
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 
@endsection