@extends('template.master')
@section('header')
Asset Handover
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading icon-container">
                <a href="{{ url('asset-handover') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Create New Document<span class="panel-subtitle"></span>
            </div>
            <div class="panel-body">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <ul class="nav nav-tabs" id='tab_menu'>
                                    <li class="active"><a href="#tabs-1" data-toggle="tab" id='tabs_1'> Pre-Transaction </a></li>
                                    <li><a href="#tabs-2" data-toggle="tab" id='tabs_2'>Transaction</a></li>
                                </ul>
                            <!-- Custom Tabs -->
                                <div class="nav-tabs-custom">
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tabs-1">
                                            <div class="row form-horizontal">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <label>Date</label>
                                                            <div class="input-group">
                                                                <input type="text" class="form-control" id="date_handover" name="date_handover" value="{{ old('date_handover') ?? date('Y-m-d') }}">
                                                                <div class="input-group-addon btn btn-primary">
                                                                    <i class="mdi mdi-calendar"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <label style="text-align: left">Responsible Person <font size="3" color="red"> *</font></label>
                                                            <select class="form-control" name="responsible_person" value="{{ old('responsible_person') }}" id="responsible_person" required></select> 
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-6">
                                                            <label>Notes</label>
                                                            <input maxlength="50" id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" id="description"> 
                                                            @if ($errors->has('description'))
                                                                <span class="help-block" style="color:red">{{ $errors->first('description') }}</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="hidden" class="form-control" name="document_no" id="document_no"> 
                                                    </div>
                                                </div><!-- /.row col-md-12 -->    
                                            </div><!-- /.row-->     
                                            <div class="row xs-pt-15">
                                                <div class="col-xs-12">
                                                    <p class="text-right">
                                                        <button class="btn btn-primary btn-lg" onClick="saveOrupdateHeader()"><i class="mdi mdi-save"></i>  &nbsp Document</button>
                                                    </p>
                                                </div><!-- /.col-xs-12 -->
                                            </div><!-- /.row xs-pt-15 -->
                                            </form><!-- form end -->                         
                                        </div><!-- /.tab-pane -->
                                        <div class="tab-pane" id="tabs-2">
                                            <div class="col-md-12">
                                                    <div class="row">
                                                    <table id='header_document'>
                                                    </table>
                                                    <hr>
                                                        <div class="input-group">
                                                            <select id="asset" name="asset"></select>
                                                                <div class="input-group-addon btn btn-primary"  onClick="storeLine()">
                                                                    <button type="submit" class="btn btn-primary btn-md" title="filter">
                                                                        <i class="mdi mdi-filter-list"></i>
                                                                    </button>
                                                                </div>
                                                        </div>
                                                    </div>
                                                <br>
                                                <div class="row">
                                                    <h3 style="font-size: 18px;">List Asset Handover</h3>
                                                    <br>
                                                    <table class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:30%">Asset Code - Description</th>
                                                                <th style="width:10%">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="data-asset">
                                                        </tbody>
                                                    </table>
                                                </div> <!-- /.Row -->
                                                <hr>
                                                <br>
                                                <div class="row xs-pt-15">
                                                    <div class="col-xs-12">
                                                        <p class="text-right">
                                                                {{ csrf_field() }}
                                                                <button onClick="printDocument()" class="btn btn-success btn-lg"><i class="mdi mdi-print"></i> &nbsp Document</button>
                                                                <input type="hidden" name="id"  value="">                                                          
                                                        </p>
                                                    </div><!-- /.col-xs-12 -->
                                                </div><!-- /.row xs-pt-15 -->
                                            </div><!-- /.col-md-12 -->
                                        </div><!-- /.tab-pane -->
                                    </div><!-- /.tab-Content -->
                                </div><!-- /.Custom Tabs -->  
                            </div><!-- /.Custom Tabs -->  
                        </div><!-- box-body row col-md-12 -->
                    </div><!-- box-body -->
            </div><!-- Panel Body -->
        </div><!-- panel panel-default -->
    </div><!-- row col-sm-12 -->
</div><!-- row -->

@endsection
@section('afterscript')
<script src="{{ asset('assets/js/locals/handover/app.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
 <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs"></script>
<script>
    $(function() {
        $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });         
    });
</script>

<script type="text/javascript">

    $(document).ready(function(){
        @if(Auth::user()->id_role != 1)
            $('#valuation_group').val({{Auth::user()->id_role}}).trigger('change');
        @endif

        $('#date_handover').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        var path = "{{ route('autocomplete.ajax') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
        $('#tab_menu').tab();
    });

    $(document).ready(function(){
            //Search Responsible Person
            $('#responsible_person').select2({
            placeholder: 'Choose Responsible',
                ajax:  {
                    url: '{{route('user_ajax')}}',
                    dataType: 'json',
                    type : 'GET',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                      // text: item.name,
                                    text: "["+item.nik+"] - "+ item.name,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            //Search Asset
            $('#asset').select2({
            width: '100%',  
            placeholder: 'Choose Asset',
            ajax:  {
                url: '{{route('asset-stock')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
    });
</script>
@endsection