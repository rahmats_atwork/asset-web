@extends('template.master')
@section('header')
<meta name="csrf-token" content="{{ csrf_token() }}">
Asset Handover 
@endsection
@section('content')
<style>
    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }
</style>
<div class="row">
    <div class="col-md-12">
        @if ($errors->has('upload_file'))
            <p class="alert alert-danger">{{ $errors->first('upload_file') }}</p>
        @endif
 
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('error')!!}
            </div>
        @endif        
    
        <div class="panel panel-default panel-table">
            <div class="panel-heading icon-container">
                <a href="{{ url('asset-handover') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Edit Document<span class="panel-subtitle"></span>
            </div>
            
            <div class="panel-body">
                    <div class="col-sm-12">
                    <h3 style="font-size: 18px;" id="nik">Document No. &nbsp; : &nbsp;{{ $header->document_no }}</h3>
                    <h3 style="font-size: 18px;" id="nik">NIK &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; : &nbsp;{{ $header->responsible->nik }}</h3>
                    <h3 style="font-size: 18px;" id="nama">Nama &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;{{ $header->responsible->name }}</h3>
                    <h3 style="font-size: 18px;" id="nama">Notes &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : &nbsp;{{ $header->responsible->notes }}</h3>
                    <hr>
                    <br>
                    </div>
                    <div class="col-sm-6">
  
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="form-group">
                                    <div class="input-group">
                                        <select id="asset" name="asset"></select>
                                            <div class="input-group-addon btn btn-primary"  onClick="storeLine()">
                                                <button class="btn btn-primary btn-md" title="filter">
                                                    <i class="mdi mdi-filter-list"></i>
                                                </button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>            
                <div class="col-md-12">
                <br>
                <br>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-sm-5">
                                <h3 style="font-size: 18px;">List Asset Handover</h3>
                            </div>
                        </div>
                    </div>
                    <table class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                        <thead>
                            <tr>
                                <th style="width:30%">Asset Code - Description</th>
                                <th style="width:10%">Action</th>
                            </tr>
                        </thead>
                        <tbody id="data-asset">
                            @foreach ($line as $l)
                                <tr id='{{ $l->id }}'>
                                    <td>[ {{ $l->asset->code_asset }} ] - {{ $l->asset->description_asset }}</td>
                                    <td><button type='button' class='btn btn-danger' onclick='deleteLine("{{ $l->id }}")'><i class='mdi mdi-delete'></i></button></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <hr>
                    <br>
                    <div class="row xs-pt-15">
                        <div class="col-xs-12">
                            <p class="text-right">
                                    {{ csrf_field() }}
                                    <button onClick="printDocument()" class="btn btn-success btn-lg"><i class="mdi mdi-print"></i> &nbsp Document</button>
                                    <input type="hidden" name="id"  value="{{ $header->id }}">                                                          
                            </p>
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row xs-pt-15 -->
                </div>
            </div>
        </div>
    </div>
   
</div>
@endsection
@section('afterscript')
<script src="{{ asset('assets/js/locals/handover/app.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
 <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
 <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs"></script>
<script>
    $(function() {
        $('#table-data').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });         
    });
</script>

<script type="text/javascript">

    $(document).ready(function(){
    
        @if(Auth::user()->id_role != 1)
            $('#valuation_group').val({{Auth::user()->id_role}}).trigger('change');
        @endif

        $('#date_handover').datepicker({todayHighlight:true,format : "yyyy-mm-dd",autoclose:true});

        var path = "{{ route('autocomplete.ajax') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
        $('#tab_menu').tab();
    });

    $(document).ready(function(){
            //Search Responsible Person
            $('#responsible_person').select2({
            placeholder: 'Choose Responsible',
                ajax:  {
                    url: '{{route('user_ajax')}}',
                    dataType: 'json',
                    type : 'GET',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results:  $.map(data, function (item) {
                                return {
                                      // text: item.name,
                                    text: "["+item.nik+"] - "+ item.name,
                                    id: item.id,
                                }
                            })
                        };
                    },
                    cache: true
                }
            });

            //Search Asset
            $('#asset').select2({
            width: '100%',  
            placeholder: 'Choose Asset',
            ajax:  {
                url: '{{route('asset-stock')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: '[' + item.code_asset + '] - ' + item.description_asset,
                                id: item.id_asset
                            }
                        })
                    };
                },
                cache: true
            }
        });
    });
</script>
@endsection

