@extends('template.master')
@section('header')
Asset Handover
@endsection
@section('content')
<style>
    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>
<div class="row">
    <div class="col-md-12">
        @if ($errors->has('upload_file'))
            <p class="alert alert-danger">{{ $errors->first('upload_file') }}</p>
        @endif
 
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
                
            
                <div class="panel panel-default panel-table">
                    <div class="row">
                            <div class="col-sm-12">
                                <div id="accordion1" class="panel-group accordion" style="margin-bottom:0">
                                    <div class="panel panel-default">
                                        <div class="panel-heading ">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo" class="collapsed"><i class="icon mdi mdi-chevron-down"></i> Filter</a></h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <form class="form-horizontal" method="get">
                                                        {{--  {!! csrf_field() !!}  --}}
                                                        <div class="col-sm-12">                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Responsible Person</br></label>
                                                                </div>
                                                                <select id="responsible" name="responsible[]" class="form-control" multiple="multiple">
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label> Created Date From</label>
                                                                        <div class="input-group">
                                                                            <input type="text"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="start_date"
                                                                               id="start_date"
                                                                               class="form-control date datetimepicker">
                                                                            <span class="input-group-addon btn btn-primary">
                                                                            <i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2">                                                                    
                                                                </div>
                                                                <div class="col-md-5 col-sm-5">
                                                                    <div class="form-group">
                                                                        <label>To</label>
                                                                        <div class="input-group">
                                                                            <input type="text"
                                                                               data-date-format="yyyy-mm-dd"
                                                                               name="end_date"
                                                                               id="end_date"
                                                                               class="form-control date datetimepicker">
                                                                            <span class="input-group-addon btn btn-primary">
                                                                            <i class="icon-th mdi mdi-calendar"></i></span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Document No.</br></label>
                                                                </div>
                                                                <select id="documents" name="documents[]" class="form-control" multiple="multiple"></select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">                                                                
                                                            <div class="form-group">
                                                                <div> 
                                                                    <label>Print Status</br></label>
                                                                </div>
                                                                <select id="status" name="status[]" class="form-control" multiple="multiple"> 
                                                                    <option value="0">Not Printed </option>
                                                                    <option value="1">Printed </option>
                                                                </select>
                                                                <button type="submit" class="btn btn-primary btn-xl" title="filter"><i class="mdi mdi-filter-list "></i></button>
                                                            </div>
                                                        </div>

                                                    </form>
                                            </div>
                                        </div>
                                    </div>        
                                </div>
                            </div>
                    </div>
                    <div class="panel-heading icon-container">
                        <a href="{{ url('asset-handover/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                        Asset Handover
                    </div>
        
                    <div class="panel-body">
                        
                        <div class="pull-left" style="margin-left: 10px;">
                            <form>
								<div>
								<select id="limit-row" name="limit" class="form-control">
									<option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
									<option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
									<option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
								</select>
								</div>
                            </form>
                        </div>
                        <div class="pull-right" style="margin-right: 10px;">
                            <input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
                        </div>
                        <table id="table-data" class="table table-fit table-striped table-hover table-fw-widget table-responsive">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th style="width: 100px;">Action</th>
                                    <th style="width: 100px;">Document No.</th> 
                                    <th style="width: 200px;">Handover Date</th> 
                                    <th style="width: 100px;">Responsible Person</th>            
                                    <th style="width: 90px;">Printed Status</th>
                                    <th style="width: 90px;">Asset Quantity</th>
                                    <th style="width: 90px;">Created Date</th>
                                    <th style="width: 90px;">Created By</th>
                                    <th style="width: 90px;">Last Update</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($documents as $key => $document_no)
                                
                                <tr>
                                    <td>{{ $documents->firstItem() + $key }}</td>
                                    <td class="actions">
                                        <div class="row">
                                        <div class="btn-group btn-space">
                                            <a href="{{ route('asset-handover.show',$document_no->id) }}" type="button" class="btn btn-default">Detail</a>
                                            <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                            @if (($document_no->printed) == 0) 
                                            <ul role="menu" class="dropdown-menu" style="min-width:100px" >
                                                <li><a href="{{ route('asset-handover.edit',$document_no->id) }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
                                                <li><a href="{{ url('asset-handover-pdf/'.$document_no->id) }}" target="_blank"><i class="mdi mdi-print"></i>Print Copy</a></li>
                                                <li><a href="#delete-{{$document_no->id}}" onclick="if(confirm('Are you sure you want to delete this item?')) $('#delete-{{$document_no->id}}').submit()"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
                                                <form id="delete-{{$document_no->id}}" method="POST" action="{{ route('asset-handover.destroy',$document_no->id) }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                </form>
                                            </ul>
                                            @endif
                                        </div>
                                    </div>
                                    </td>
                                    <td title="{{ $document_no->document_no }}">{{ $document_no->document_no }}</td>
                                    <td title="{{ $document_no->date }}">{{ $document_no->date }}</td>
                                    <td title="{{ $document_no['responsible']['name'] }} - [ {{ $document_no['responsible']['nik'] }} ]">{{ $document_no['responsible']['name'] }} - [ {{ $document_no['responsible']['nik'] }} ]</td>
                                    <td title="@if (($document_no->printed) == 1) Printed @else Not-Printed @endif">@if (($document_no->printed) == 1) Printed @else Not-Printed @endif</td>
                                    <td title="{{ $document_no->handoverLine->count() }}">{{ $document_no->handoverLine->count() }}</td>
                                    <td>{{ $document_no->created_at }}</td>
                                    <td title="{{ $document_no->userCreated->name }}">{{ $document_no->userCreated->name ?? null }}</td>
                                    <td>{{ $document_no->updated_at }}</td>
                            
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            <?php
                            $appends = [];
                            foreach(request()->all() as $k => $v)
                                $appends[$k] = $v;
							?>
                            {{ $documents->appends($appends)->links() }}
                        </div>
                    
                    </div>
				</div>
	</div>
</div>
@endsection
@section('afterscript')
<script>
        $('#responsible').select2({
            placeholder: 'Choose Responsible',
            width: '33%',
            ajax:  {
                url: '{{route('user_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                             return {
                                text: "["+item.nik+"] "+item.name,
                                id: item.id
                             }
                        })
                    };
                },
            cache: true
            }
        });
        $('#documents').select2({
            placeholder: 'Choose Document',
            width: '33%',
            ajax:  {
                url: '{{route('asset-handover-document')}}',
                dataType: 'json',
                type : 'GET',
                delay: 50,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                             return {
                                text: item.document_no,
                                id: item.document_no
                             }
                        })
                    };
                },
            cache: true
            }
        });
    function delete_data(){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete').submit();
        }
    }

    function myFunction() {
    var search = "export/asset"+window.location.search;
    document.getElementById("hrefdownload").href = search;
    }

    function myFunction2() {
    var search2 = "barcode/asset"+window.location.search;
    document.getElementById("hrefbarcode").href = search2;
    }

    $(function() {
        $('#table-data').DataTable({
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": false,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });         
    });

    $(document).ready(function(){

        var init_status     = <?php echo json_encode($input_status) ?>;
        var init_document   = <?php echo json_encode($input_document) ?>;
        $('#status').select2({ 
            width: '33%',
            placeholder: 'Choose Status',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_status).trigger("change");

        $('#document').select2({ 
            width: '33%',
            placeholder: 'Choose Document No.',
            allowClear: true,
            tokenSeparators: [',', ' '],
        }).val(init_status).trigger("change");

    });

    

    var start_date = <?php echo json_encode($start_date) ?>;
    $('#start_date').val(start_date);

    var end_date = <?php echo json_encode($end_date) ?>;
    $('#end_date').val(end_date);
    
    $(".datetimepicker").datepicker({
        autoclose: true
    });

    $('#search-input').keypress(function (e) {
		if(e.which == 13) {
            <?php
            $query_array = request()->query();
            unset($query_array['q']);
            ?>
            var query = "<?php echo !empty($query_array)?"&".http_build_query($query_array):"";?>";
            window.location.replace("{{ url('asset-handover') }}?q="+($("#search-input").val()?$("#search-input").val():'')+query);
		}
	});

    $('#limit-row').change(function(){
        <?php
        $query_array = request()->query();
        unset($query_array['limit']);
        ?>
        var query = "<?php echo !empty($query_array)?"&".http_build_query($query_array):"";?>";
        window.location.replace("{{ url('asset') }}?limit="+($("#limit-row").val()?$("#limit-row").val():'')+query);
	});
</script>
@endsection
