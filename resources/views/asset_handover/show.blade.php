@extends('template.master')
@section('header')
Asset Handover
@endsection
@section('beforehead')

@endsection

@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading icon-container">
					<a href="{{ url('asset-handover') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Detail Asset Handover<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					
					<div class="row">
						<div class="col-md-6">
							<table class="table table-striped table-hover table-fw-widget">
								<tr>
									<th style="width:30%">Document No.</th>
									<td>{{ $header->document_no }}</td>
								</tr>
								<tr>
									<th>Description</th>
									<td>{{ Carbon\Carbon::parse($header->date)->format('d F Y') }}</td>
								</tr>	
								<tr>
									<th>Responsible User</th>
									<td>{{ $header->responsible->name}} - [ {{ $header->responsible->nik }} ]</td>
								</tr>	
								<tr>
									<th>Count Printed</th>
									<td>{{ $header->count_printed}}</td>
								</tr>					
							</table>
						</div>
						<div class="col-md-6">
							<table class="table table-striped table-hover table-fw-widget">
								<tr>
									<th style="width:30%">Created By</th>
									<td>{{ !empty($header->userCreated) ? $header->userCreated->name : ""}}</td>
								</tr>
								<tr>
									<th>Created Date</th>
									<td>{{ $header->created_at }}</td>
								</tr>
								<tr>
									<th>Updated By</th>
									<td>{{ !empty($header->userUpdated) ? $header->userUpdated->name : ""}}</td>
								</tr>
								<tr>
									<th>Updated Date</th>
									<td>{{$header->updated_at}}</td>
								</tr>
							</table>
						</div>
					</div> <!-- row -->
					<br>
					<div class="panel">
					<h4>&nbsp; Asset List</h4>
					<br>
						<table class="table table-striped table-fw-widget">
							<tr>
								<th style="width:10%">No. </th>
								<th style="width:30%">Asset Code</th>
								<th style="width:30%">Asset Description</th>
								<th style="width:30%">Responsible Name</th>
							</tr>
							</tr>
							@php
								$i = 1;
							@endphp
							@if (!empty($line[0]))
								@foreach ($line as $l)
									<tr>
										<td>{{ $i }} </td>
										<td>{{ $l->asset->code_asset }}</td>
										<td>{{ $l->asset->description_asset }}</td>
										<td>{{ $header->responsible->name}} - [ {{ $header->responsible->nik }} ] </td>
									</tr>
									@php
										$i++;
									@endphp
								@endforeach
							@else
								<tr>
									<td colspan="4" style="text-align:center">No Result </td>
								</tr>
							@endif
						</table>
					</div>
				</div><!-- Panel Body -->
			</div><!-- panel -->
		</div><!-- sm--12 -->
</div><!-- row -->

@endsection

@section('afterscript')


@endsection