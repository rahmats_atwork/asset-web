@extends('template.master')
@section('content')
@section('header')
Dashboard
@endsection
<style>
body {
    color: #1d2b36;
}
</style>
<div class="main-content container-fluid">

    <!-- MasterData -->
    <span class="icon mdi mdi-dns" style="font-size:30px; color:1d2b36;"> </span>
    <strong style="font-size:30px; color:1d2b36;">Master Data</strong>
    <br/><br/>
    <div class="row">

        <!-- Location -->
        <a href = "{{url('location')}}">
            <div class="col-12 col-lg-4 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <span class="icon mdi mdi-map" style="font-size:50px"></span>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            Location
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countTimSerpo}}" class="number">
                                {{$countTimSerpo}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <!-- Vendor -->
        <a href = "{{url('supplier')}}">
            <div class="col-12 col-lg-4 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <span class="icon mdi mdi-store" style="font-size:50px"></span>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            Vendor
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countMasterSupplier}}" class="number">
                                {{$countMasterSupplier}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>
    
    <!-- MasterDataSetting -->
    <span class="icon mdi mdi-settings-square" style="font-size:30px; color:1d2b36;"> </span>
    <strong style="font-size:30px; color:1d2b36;">Master Data Setting</strong>
    <br/><br/>
    <div class="row">

        <!-- Plant -->
        <a href = "{{url('system/plant')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <Strong >Plant<br/><br/></Strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">

                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countRegion}}" class="number">
                                {{$countRegion}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <!-- LocationType -->
        <a href = "{{url('locationtype')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <Strong>Location<br/>Type</Strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                        
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countLocationType}}" class="number">
                                {{$countLocationType}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>

    <!-- Asset -->
    <span class="icon mdi mdi-balance" style="font-size:30px; color:1d2b36;"> </span>
    <strong style="font-size:30px; color:1d2b36;"> Asset</strong>
    <br/><br/>
    <div class="row">
        <a href = "{{url('classification')}}">
            <div class="col-12 col-lg-4 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <span class="icon mdi mdi-chart" style="font-size:50px"></span>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            Classification
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countClassification}}" class="number">
                                {{$countClassification}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href = "{{url('material')}}">
            <div class="col-12 col-lg-4 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <span class="icon mdi mdi-shape" style="font-size:50px"></span>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            Material
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countMasterMaterial}}" class="number">
                                {{$countMasterMaterial}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href = "{{url('asset')}}">
            <div class="col-12 col-lg-4 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <span class="icon mdi mdi-assignment" style="font-size:50px"></span>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            Asset
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countMasterAsset}}" class="number">
                                {{$countMasterAsset}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>

    <!-- AssetSetting -->
    <span class="icon mdi mdi-settings-square" style="font-size:30px; color:1d2b36;"> </span>
    <strong style="font-size:30px; color:1d2b36;">Asset Setting</strong>
    <br/><br/>
    <div class="row">

        <a href = "{{url('materialgroup')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <strong>Material<br/>Group</strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">

                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countMaterialGroup}}" class="number">
                                {{$countMaterialGroup}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href = "{{url('assettype')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                    <strong>Asset<br/>Type</strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countAssetType}}" class="number">
                                {{$countAssetType}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        {{--<a href = "{{url('valuationgroup')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <strong>Valuation<br/>Group</strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">

                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countValuationGroup}}" class="number">
                                {{$countValuationGroup}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>--}}

        <a href = "{{url('statusasset')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <strong>Status<br/>Asset</strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countStatusAsset}}" class="number">
                                {{$countStatusAsset}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

        <a href = "{{url('retiredreason')}}">
            <div class="col-12 col-lg-2 col-xl-3">
                <div class="widget widget-tile">
                    <div id="spark1" class="chart sparkline">
                        <strong>Retired<br/>Reason</strong>
                    </div>
                    <div class="data-info">
                        <div class="desc">
                            
                        </div>
                        <div class="value">
                            <span data-toggle="counter" data-end="{{$countRetiredReason}}" class="number">
                                {{$countRetiredReason}}
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </a>

    </div>

</div>
@endsection

