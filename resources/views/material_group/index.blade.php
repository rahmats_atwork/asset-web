@extends('template.master')
@section('header')
Material Group Master
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
            </div>
        @elseif(Session::has('error'))
            <div class="alert alert-warning alert-dismissible">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('error')!!}
            </div>
        @endif
        <div class="panel panel-default panel-table">

            <div class="panel-heading icon-container">
                <a href="{{ url('materialgroup/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                Material Group List
            </div>
            {{--  <div class="panel-heading icon-container">
                <a href="#" data-toggle="modal" data-target="#form-bp1" type="button" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                Master Material Group
            </div>  --}}

            <div class="panel-body">
                <div class="pull-left" style="margin-left: 10px;">
                    <form>
                        <div>
                        <select id="limit-row" name="limit" class="form-control">
                            <option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
                            <option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
                            <option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
                        </select>
                        </div>
                    </form>
                </div>
                <div class="pull-right" style="margin-right: 10px;">
                    <input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
                </div>
                <table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
                    <thead>
                        <tr>
                            <th style="width: 5%">No</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th style="width: 11%;">Action</th>
                        </tr>
                    </thead>
                    @php $i = 1 @endphp
                    @foreach($list as $val)
                    <tr>
                        <td style="text-align: center">{{ $i++ }}</td>
                        <td>{{ $val->code }}</td>
                        <td>{{ $val->name }}</td>
                        <td>{{ $val->description}}</td>
                        <td class="actions">
                        <form id="form-delete{{$val->id}}" method="post" action="{{ url('materialgroup/'. $val->id) }}" class="pull-right">
                                {!! csrf_field() !!}
                                <input type="hidden" name="_method" value="DELETE" />
                            </form>

                            <div class="btn-group btn-space">
                                <a href="{{ url('materialgroup/' . $val->id) }}" type="button" class="btn btn-default">Detail</a>
                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                <ul role="menu" class="dropdown-menu" style="min-width:100px" >
                                    <li><a href="{{ url('materialgroup/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
                                    <li><a href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
                <div class="text-center">
                    <?php
                    $appends = [];
                    if(request()->has('q')) $appends['q'] = request()->q;
                    if(request()->has('limit')) $appends['limit'] = request()->limit;
                    ?>
                    {{ $list->appends($appends)->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

<!--Form Modals-->
{{--  <div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
    <div class="modal-dialog custom-width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="mdi mdi-close"></span></button>
                <h3 class="modal-title">Create Material Group</h3>
            </div>
            <div class="modal-body">
                <form id="form-create" class="form-horizontal group-border-dashed" method="post" action="{{ url('materialgroup') }}">
                    {!! csrf_field() !!}
                    <div class="col-md-12">

                        <div class="form-group">
                            <label>Code</label>
                            <input type="text" name="code" maxlength="9" class="form-control" value="{{ old('code') }}" required>
                            @if ($errors->has('code'))
                            <span class="help-block" style="color:red">{{ $errors->first('code') }}</span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Name</label>                           
                            <input type="text" name="name" maxlength="9" class="form-control" value="{{ old('name') }}" required>
                            @if ($errors->has('name'))
                            <span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
                            @endif                            
                        </div>

                        <div class="form-group">
                            <label>Description </label>                            
                            <input type="text" name="description" maxlength="30" class="form-control" value="{{ old('description') }}">
                            @if ($errors->has('description'))
                            <span class="help-block" style="color:red">{{ $errors->first('description') }}</span>
                            @endif                           
                        </div>

                    </div>   
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                <button type="button" data-dismiss="modal" onclick="$('#form-create').submit()" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>  --}}
@endsection
@section('afterscript')
<script>
    function delete_data(val){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete'+val).submit();
        }
    }

    $(function() {
        $('#table12').DataTable({
            // "paging": true,
            // "lengthChange": true,
            // "searching": true,
            // "ordering": false,
            // "info": true,
            // "autoWidth": true,
            // // "scrollX": true,
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });         
    });

    $('#limit-row').change(function(){
        window.location.replace("{{ url('materialgroup') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
    });

    $('#search-input').keypress(function (e) {
        if(e.which == 13) {
            window.location.replace("{{ url('materialgroup') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
        }
    });

</script>


@endsection