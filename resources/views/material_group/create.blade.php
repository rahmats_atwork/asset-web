@extends('template.master')
@section('header')
Material Group Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('materialgroup') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Create New Material Group<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<form id="form-create" class="form-horizontal" method="post" action="{{ url('materialgroup') }}">
					{!! csrf_field() !!}
					<div class="col-md-12">
						<div class="row">

							<div class="form-group">
								<div class="col-sm-4">
									<label class="control-label">Code<font size="3" color="red"> *</font></label>
									<input type="text" name="code" maxlength="9" class="form-control" value="" required>
									@if ($errors->has('code'))
										<span class="help-block" style="color:red">{{ $errors->first('code') }}</span>
									@endif
								</div>
							</div>


							<div class="form-group">
								<div class="col-sm-4">									
									<label class="control-label">Name<font size="3" color="red"> *</font></label>
									<input type="text" name="name" maxlength="30" class="form-control" value="" required>
									@if ($errors->has('name'))
										<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12">
									<label class="control-label">Description </label>
									<input type="text" name="description" maxlength="255" class="form-control" value="">
									@if ($errors->has('description'))
									<span class="help-block" style="color:red">{{ $errors->first('description') }}</span>
									@endif
								</div>
							</div>

						</div>
					</div>
					<div class="row xs-pt-15">
						<div class="col-xs-12">
							<p class="text-right">
								<button type="submit" class="btn btn-primary btn-lg">Save</button>
						{{--  <a href="{{ url('materialgroup') }}" class="btn btn-default">Back</a>  --}}
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
 
@endsection
