@extends('template.master')
@section('header')
Plant Master
@endsection
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading panel-heading-divider icon-container">
                <a href="{{ url('system/region') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
                Create New Plant<span class="panel-subtitle"></span>
            </div>
            <div class="panel-body">
                @if(session()->has('message.level'))
                    <div class="alert alert-{{ session('message.level') }}">
                        {!! session('message.content') !!}
                    </div>
                @endif
                <form class="form-horizontal group-border-dashed" role="form" method="POST" action="{{ url('system/region') }}">
                    {{ csrf_field() }}
                    <div class="row">   
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('region_bismo') ? ' has-error' : '' }}">
                                <div class="col-sm-4">
                                <label class="control-label">Plant<font size="3" color="red"> *</font></label>
                                <input type="text" class="form-control field-reg-bismo field-name" name="region_bismo" id="region_bismo" autofocus value="{{ old('region_bismo') }}" maxlength="30" required>
                                    @if ($errors->has('region_bismo'))
                                        <p class="text-bold text-danger">{{ $errors->first('region_bismo') }}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                    <div class="col-sm-12">
                                    <label class="control-label">Address<font size="3" color="red"> *</font></label>
                                    <input type="text" class="form-control clearme" id="us2-address" name="wil_reg" id="wil_reg" placeholder="Address" value="{{ old('wil_reg') }}">
                                        <input type="hidden" class="form-control" style="width: 110px" id="us2-lon" name="longitude" />
                                        <input type="hidden" class="form-control" style="width: 110px" id="us2-lat" name="latitude" />
                                        <br>
                                        <div id="us2" style="width: 100%; height: 300px;"></div>
                                    </div> 
                            </div>
                            <div class="form-group {{ $errors->has('keterangan') ? ' has-error' : '' }}">
                                <div class="col-sm-12">
                                <label class="control-label">Description</label>
                                <textarea rows="4" class="form-control field-ket input-sm field-name" name="keterangan" id="keterangan" maxlength="100">{{ old('keterangan') }}</textarea>
                                    @if ($errors->has('keterangan'))
                                        <p class="text-bold text-danger">{{ $errors->first('keterangan') }}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row xs-pt-15">
                        <div class="col-xs-12">
                            <p class="text-right">
                                <button type="submit" class="btn btn-primary btn-lg">Save</button>
                                {{--  <a href="{{ url('system/region') }}" class="btn btn-default">Back</a>  --}}
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    


@endsection
@section('afterscript')
<!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script>  -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBat0J0W6eArCkCSjkEX3FN9Tt8gSKmcRc&libraries=places"></script>
<script type="text/javascript">
$(document).ready(function(){  
    $('.clearme').focus(function() { 
                $(this).val(''); 
            });
    $(function() {
        $('#us2').locationpicker({
            location: {
                latitude: "-6.189680",
                longitude: "106.823440"
            },
            inputBinding: {
                latitudeInput: $('#us2-lat'),
                longitudeInput: $('#us2-lon'),
                locationNameInput: $('#us2-address'),                
            },
            radius:0,
            enableAutocomplete: true       
        });
    }); 
});
</script> 
@endsection