@extends('template.master')
@section('header')
Location Type Master
@endsection
@section('beforehead')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
@endsection
@section('content')
<div class="row">
		<div class="col-md-12">
				@if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
				<div class="panel panel-default panel-table">

						<div class="panel-heading icon-container">
							<a href="{{ url('locationtype/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
							Location Type List
						</div>
			
						<div class="panel-body">				
							<div class="pull-left" style="margin-left: 10px;">
	                            <form>
									<div>
									<select id="limit-row" name="limit" class="form-control">
										<option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
										<option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
										<option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
									</select>
									</div>
	                            </form>
	                        </div>
							<div class="pull-right" style="margin-right: 10px;">
								<input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
	                        </div>
	                        <table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
								<thead>
									<tr>
										<th style="width: 5%">No</th>
										<th>Location Type</th>
										<th>Icon</th>
										{{--  <th>Zoom Level</th>  --}}
										
										
										<th style="width: 90px;">Action</th>
									</tr>
								</thead>
								@php $i = 1 @endphp
								@foreach($list as $val)
								<tr>
									<td style="text-align: center">{{ $i++ }}</td>
									<td>{{ $val->name }}</td>
									<td>
										@if(($val->icon)!=null)
										<a href="{{URL::asset('/storage/images/'.$val->icon)}}" data-lightbox="image-gallery">
											<img class="myImg" src="{{URL::asset('/storage/images/'.$val->icon)}}" height="40" width="40" id="image-gallery">
										</a>
										@endif
									</td>
									{{--  <td>{{ $val->zoom_level }} - {{ $val->zoom_level_end }} </td>  --}}
									<td class="actions">
										<form id="form-delete{{$val->id}}" method="post" action="{{ url('locationtype/'. $val->id) }}" class="pull-right">
											{!! csrf_field() !!}
											<input type="hidden" name="_method" value="DELETE" />
										</form>
			
										<div class="btn-group btn-space">
											<a href="{{ url('locationtype/' . $val->id) }}" type="button" class="btn btn-default">Detail</a>
											<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
											<ul role="menu" class="dropdown-menu" style="min-width:100px" >
												<li><a href="{{ url('locationtype/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
												<li><a  href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
											</ul>
										</div>
									</td>
									
									{{--  <td>
										<a href="{{ URL::to('locationtype/show/' . $val->id) }}" class="btn btn-primary btn-sm" title="View Location Type"><i class="fa fa-eye"></i></a>
										<a href="{{ URL::to('locationtype/' . $val->id .'/edit') }}"class="btn btn-success btn-sm" title="Edit Location Type"><i class="fa fa-pencil"></i></a>
										<form method="post" action="{{ url('locationtype/'. $val->id) }}" onclick="return confirm('Are you sure delete this data?')" class="pull-right">
											{!! csrf_field() !!}
											<input type="hidden" name="_method" value="DELETE" />
											<button type="submit" class="btn btn-danger btn-sm delete-list" title="Hapus Location Type"><i class="fa fa-trash"></i></button>
										</form>
									</td>  --}}
								</tr>
								@endforeach
							</table>
	                        <div class="text-center">
								<?php
								$appends = [];
								if(request()->has('q')) $appends['q'] = request()->q;
								if(request()->has('limit')) $appends['limit'] = request()->limit;
								if(request()->has('mg')) $appends['mg'] = request()->mg;
								if(request()->has('clsf')) $appends['clsf'] = request()->clsf;
								?>
	                            {{ $list->appends($appends)->links() }}
	                        </div>
						</div>
					</div>
			</div>
</div>					

@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<script>
    function delete_data(val){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete'+val).submit();
        }
    }

    $(function() {
        $('#table12').DataTable({
            // "paging": true,
            // "lengthChange": true,
            // "searching": true,
            // "ordering": false,
            // "info": true,
            // "autoWidth": true,
            // // "scrollX": true,
            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });         
    });

    $('#limit-row').change(function(){
		window.location.replace("{{ url('locationtype') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
	});

	$('#search-input').keypress(function (e) {
		if(e.which == 13) {
			window.location.replace("{{ url('locationtype') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
		}
	});
</script>
@endsection