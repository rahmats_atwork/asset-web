@extends('template.master')
@section('header')
Location Type Master
@endsection
@section('beforehead')
<style>
		.img-thumb {
			width:200px;
			height:200px;
			background-color:#f5f5f5; 
			margin-top: 10px;
			margin-left:10px;
			border: 1px solid #bdbdbd;
			padding: 0 !important;
			overflow:hidden;
			display: flex; 
			justify-content: center;
			align-items: center;
		}
		
		.img-thumb:hover {
			border: 1.5px solid #bdbdbd;
		}
		
		.img-upload {
			width:200px;
			height:200px;
			border: 5px dashed #bdbdbd;
			background-color: #f5f5f5;
			display:flex;
			justify-content:center;
			align-items:center;
		}
		
		.img-upload i {
			font-size: 50px;
			color:#bdbdbd;
		}
		
		.delete-image{
			display:none;
			position: absolute;
			right: 5px;
			top: 2px;
		}
		
		.img-thumb:hover .delete-image {
			display:block;
		}
		
		</style>
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
				@if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
				<div class="panel panel-default">
						<div class="panel-heading panel-heading-divider icon-container">
							<a href="{{ url('locationtype') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
							Create New Location Type<span class="panel-subtitle"></span>
						</div>
						<div class="panel-body">
							<form id="form-create" class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('locationtype') }}">
								{!! csrf_field() !!}
								<div class="box-body">

									<div class="form-group">
										<div class="col-sm-4">
											<label>Location Type<font size="3" color="red"> *</font></label>
											<input type="text" name="location_type" id="location_type" class="form-control" autofocus onkeyup="changeToUpperCase(this)" value="{{ old('location_type') }}">
											@if ($errors->has('location_type'))
												<span class="help-block" style="color:red">{{ $errors->first('location_type') }}</span>
											@endif
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-12">
											<label>Icon</label>
										</div>
										<div id="image-gallery"><!-- ajax loaded --></div>
										<label class="col-sm-3 btn"  style="position: relative; overflow:hidden">
											<i id="btn-plus" class="icon img-upload"><span class="mdi mdi-plus"></span></i> 
											
												<input id="img-selector" name="icon" value="{{old('icon')}}" type="file" style="display: none" accept="image/*">
												<img class="img-thumb" src="" id="profile-img-tag" style="display: none"/>
												@if ($errors->has('icon'))
													<span class="help-block col-sm-1" style="color:red">{{ $errors->first('icon') }}</span>
												@endif
										</label>		
									</div>
									
									{{--  <div class="form-group">
										<div class="col-sm-12">
											<label>Zoom Level  <a href="#" data-toggle="tooltip" data-placement="right" title="Range 1-20 to show/hide route on map"> <i class="mdi mdi-info"></i></a></label>
										</div>
										<div class="col-sm-1">
											<input type="number" name="zoom_level" class="form-control" min="1" max="20" value="{{ old('zoom_level') }}" required>	
											@if ($errors->has('zoom_level'))
											<span class="help-block" style="color:red">{{ $errors->first('zoom_level') }}</span>
											@endif
										</div>
										<div class="col-sm-1">
											<input type="number" name="zoom_level_end" class="form-control" min="1" max="20" value="{{ old('zoom_level_end')}}" required>											
											@if ($errors->has('zoom_level_end'))
											<span class="help-block" style="color:red">{{ $errors->first('zoom_level_end') }}</span>
											@endif
										</div>
			
									</div>  --}}
									

								</div>
								{{--  <div class="box-footer">
									<button type="submit" class="btn btn-primary btn-submit">Save</button>
									<a href="{{ url('locationtype') }}" class="btn btn-default">Back</a>
								</div>  --}}
								<div class="row xs-pt-15">
									<div class="col-xs-12">
										<p class="text-right">
											<button type="submit" class="btn btn-primary btn-lg">Save</button>
										</p>
									</div>
								</div>
							</form>
						</div>
				</div>
		</div>
</div>				


@endsection
@section('afterscript')
<script type="text/javascript">
	
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#img-selector").change(function(){
        readURL(this);
        $("#btn-plus").hide()
		$("#profile-img-tag").show()
    });
    $(function() {
	    $('#location_type').on('keypress', function(e) {
	        if (e.which == 32)
	            return false;
	    });
	});

	function changeToUpperCase(t) {
	   var eleVal = document.getElementById(t.id);
	   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_;:",]/g, "");
	}

</script>
@endsection