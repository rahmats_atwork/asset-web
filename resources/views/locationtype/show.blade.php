@extends('template.master')
@section('header')
Location Type Master
@endsection
@section('beforehead')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css" />
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('locationtype') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Detail Location Type<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
							<table class="table table-striped table-hover table-fw-widget">
								<tr>
									<th>Location Type</th>
									<td>{{ $loc->name }}</td>
								</tr>
								{{--  <tr>
									<th>Zoom Level</th>
									<td>{{ $loc->zoom_level }} - {{$loc->zoom_level_end}}</td>
								</tr>							  --}}
								<tr>
									<th>Icon</th>
									<td>
										@if(!$loc->icon)
										@else
										<a href="{{URL::asset('/storage/images/'.$loc->icon)}}" data-lightbox="image-gallery">
											<img class="myImg" src="{{URL::asset('/storage/images/'.$loc->icon)}}" width="200px" id="image-gallery">
										</a>
										@endif
										{{--  <img class="myImg" src="{{URL::asset('/storage/images/'.$loc->icon)}}" width="200px" id="profile-img-tag">  --}}
									</td>
								</tr>							
								
							</table>
						</div>
						<div class="col-md-6">
								<table class="table table-striped table-hover table-fw-widget">
									<tr>
										<th style="width:20%">Created By</th>
										<td>{{ isset($loc->createdBy) ? $loc->createdBy->name : ""}}</td>
									</tr>
									<tr>
										<th>Created Date</th>
										<td>{{ $loc->created_at }}</td>
									</tr>
									<tr>
										<th>Updated By</th>
										<td>{{ isset($loc->updatedBy) ? $loc->updatedBy->name : ""}}</td>
									</tr>	
									<tr>
										<th>Updated Date</th>
										<td>{{$loc->updated_at}}</td>
									</tr>																						
								</table>
							</div>
					</div>
				</div>
			</div>
		</div>
</div>
@endsection
@section('afterscript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
</script>
@endsection