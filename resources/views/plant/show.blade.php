@extends('template.master')
@section('header')
Plant Master
@endsection
@section('content')

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('system/plant') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail Plant<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th>Plant</th>
								<td>{{ $plants->name }}</td>
							</tr>
							<tr>
								<th width="150">Description</th>
								<td>{{ $plants->description }}</td>
							</tr>
							<tr>
								<th>Address</th>
								<td id="td-address">
									<textarea class="form-control" id="us2-address" rows="2" disabled="">
										
									</textarea>
								</td>
                                <input type="hidden" class="form-control" style="width: 110px" id="us2-lon" name="longitude" />
                                <input type="hidden" class="form-control" style="width: 110px" id="us2-lat" name="latitude" />
							</tr>
						</table>						
					</div>
				</div>
				<div class="row xs-pt-15">
					<div class="col-xs-12">
						<btn href="#bar" data-toggle="collapse" class="btn btn-success" onclick="ShowHide(this);">Show Map</btn>
					</div>
				</div>
					
				<div class="row xs-pt-15">
					<div class="col-xs-12">
						<div id="bar" class="collapse">
							<td><div id="show-map" style="width: 100%; height: 300px;"></div></td>
						</div>
					</div>	
				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection

@section('afterscript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBat0J0W6eArCkCSjkEX3FN9Tt8gSKmcRc&libraries=places"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
    });
    $(function() {
        $('#show-map').locationpicker({
            location: {
                latitude: "{{ $plants->latitude }}",
                longitude: "{{ $plants->longitude }}"
            },
            inputBinding: {
               latitudeInput: $('#us2-lat'),
               longitudeInput: $('#us2-lon'),
               locationNameInput: $('#us2-address')
           	},
            radius:0,
            draggable: false
            // zoom: 20
        });
    }); 
</script>

<script type="text/javascript">
// $(window).load(function () {
//    $('#bar').removeClass('in')
   

// });

function ShowHide(id) {
  if ($(id).html() == "Show Map") {
    $(id).html("Hide Map");
  } else {
    $(id).html ("Show Map");
  }

}
</script> 
@endsection