@extends('template.master')

@section('head')
<link rel="stylesheet" href="{{ asset('/webix/webix.css') }}" />
@endsection

@section('content')
<div id="app" style="height:100%"><!-- content goes here--></div>
@endsection

@section('after_script')
<script src="{{ asset('/webix/webix.js') }}"></script>

<script>
var header = {
    view:"toolbar",
    elements:[
        { view:"label",label:"Role List",fillspace:true},
        { view:"button",width:100,label:"Add" }
    ]
}
var datatable =  {
    id:"dtable",
    view:"datatable",
    select:true,
    columns:[
        { id:"name", header:"Role", width:200, sort:"string" },
        { id:"slug", header:"Slug", width:200, sort:"string" },
        { id:"description", header:"Description", fillspace:true, sort:"string" },
        { header:"Action", width: 100}
    ]
}


webix.ui({
    type:"line",
    container:"app",
    rows:[
        header,
        datatable
    ]
})

function refreshTable(){
    // Refresh webix datatable
    $$('dtable').showOverlay("Loading...")
    webix.ajax()
        .headers({
            "Accept":"application/json"
        })
        .get("{{ route('role.list') }}").then(function(data){
            console.log('role',data.json())
            $$('dtable').hideOverlay()
            $$('dtable').parse(data.json(),"json")
        })
}

// Init Webix
webix.ready(function(){
    refreshTable()
})
</script>
@endsection