<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title','Asset Management System')</title>
  <link rel="shortcut icon" href="{{ asset('assets/images/izora.png')}}" type="image/png" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  @yield('beforehead')

    <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/><!--[if lt IE 9]>
      
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" type="text/css" href="{{asset('beagle/lib/datatables/css/dataTables.bootstrap.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{asset('beagle/lib/select2/css/select2.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('assets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/plugins/datepicker/datepicker3.css')}}">
    <link rel="stylesheet" href="{{asset('assets/js/plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <link rel="stylesheet" href="{{ asset('beagle/css/style.css') }}" type="text/css"/>
    @yield('afterhead')
  </head>
   <style>
        .be-left-sidebar {
          background-color: #1D2B36;}

        .be-top-header {
            background-color: #1D2B36;
            border-bottom-color: #141F27; 
            -webkit-box-shadow:0px 2px 2px #141F27;
            -moz-box-shadow:0px 2px 2px #141F27;
            box-shadow:0px 1px 1px #141F27;
        }

        .be-top-header .navbar-header .be-toggle-left-sidebar .icon {
          color: white;}

        .be-left-sidebar .sidebar-elements > li ul li > a:hover {
            background-color: #1D2B36; }

        .be-left-sidebar .sidebar-elements > li ul {
          background-color: #2C3A45;}

        .be-left-sidebar .sidebar-elements > li > ul {
          border-top: 1px solid #141F27;
          border-bottom: 1px solid #141F27; }

        .be-left-sidebar .sidebar-elements > li > a:hover {
              background-color: #141F27; }

        .be-left-sidebar .sidebar-elements > li.active > a {
            color: white;
            /* background-color: #141F27 */
            background: linear-gradient(to right,  #F7C915 0%,#F7C915 5%,#141F27 0%,#141F27 100%);
            
        }

        .be-left-sidebar .sidebar-elements > li.active > a > span {
          color: white;}

        .be-left-sidebar .sidebar-elements > li ul li.active > a {
          color: #F7C915;
        }

      .be-left-sidebar .sidebar-elements > li ul li > a {
        color: white;}

      .be-left-sidebar .sidebar-elements > li > a {
        color: white;}

      .be-left-sidebar .sidebar-elements > li > a:hover > span {
          color: white; }

      .icon-container:hover .icon {
        background-color: #F7C915; }

      .navbar2 {
        position: relative;
        min-height: 50px;
        /* margin-bottom: 18px; */
        border: 1px solid transparent;
      }

      .be-user-nav > li.dropdown .dropdown-menu .user-info {
        background-color: #1D2B36;}

      .be-top-header .be-user-nav > li.dropdown .dropdown-menu:after {
        border-bottom-color: #1D2B36;}

      /* .main-content {
        padding: 0px;} */
  </style> 
  <body>
    <div id="collabs" class="be-wrapper be-collapsible-sidebar">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header">
        <div class="container-fluid">
          <div class="navbar-header"><a href="{{ route('home') }}" class="navbar-brand"></a><a id="menu" href="#" class="be-toggle-left-sidebar"><span style="margin-right:3px" class="icon mdi mdi-caret-left"></span><span style="margin-right:3px" class="icon mdi mdi-menu"></span><span style="margin-right:3px; display: none;" class="icon mdi mdi-caret-right"></span></a>
          </div>
          {{-- <div class="navbar-header"><a href="{{ route('home') }}" class="navbar-brand"></a>
          </div> --}}
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              @if(\Auth::user()->image != null)
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span>cvxvxvxcvxcv</span><img src="{{asset('/storage/images/user/'.Auth::user()->image)}}" alt="Avatar" width="300px" hight="300px"><span class="user-name">{{ Auth::user()->name }}</span></a>
              @else
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{ asset('beagle/img/avatar.png') }}" alt="Avatar"><span class="user-name">{{ Auth::user()->name }}</span></a>
              @endif
                <ul role="menu" class="dropdown-menu">
                  @guest
                  <li><a href="{{ route('login') }}"><span class="icon mdi mdi-face"></span>Sign Up</a></li>
                  @else
                  <li>
                    <div class="user-info">
                      <div class="user-name">{{ Auth::user()->name }}</div>
                      <!-- <div class="user-position online">Available</div> -->
                    </div>
                  </li>
                  <li><a href="{{ url('user/'.Auth::user()->id) }}"><span class="icon mdi mdi-face"></span> Account</a></li>
                  <li><a href="{{ url('user/'.Auth::user()->id).'/edit' }}"><span class="icon mdi mdi-settings"></span> Settings</a></li>
                  <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> Logout</a></li>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                  @endguest
                </ul>
              </li>
            </ul>
            {{--  <div class="page-title"><span>CLP</span></div>  --}}
            <ul class="nav navbar-nav navbar-right be-icons-nav">
              <li class="dropdown"><a href="#" role="button" aria-expanded="false" class="be-toggle-right-sidebar"><span style="color:white">Hallo, {{Auth::user()->name}}</span></a></li>
               {{-- <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span Style="color:white" class="icon mdi mdi-notifications-none"></span><span class="indicator"></span></a> --}}
                <ul class="dropdown-menu be-notifications">
                  <li>
                    <div class="title">Notifications<span class="badge">3</span></div>
                    <div class="list">
                      <div class="be-scroller">
                        <div class="content">
                          <ul>
                            <li class="notification notification-unread"><a href="#">
                                <div class="image"><img src="{{ asset('beagle/img/avatar2.png') }}" alt="Avatar"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Jessica Caruso</span> accepted your invitation to join the team.</div><span class="date">2 min ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="{{ asset('beagle/img/avatar3.png') }}" alt="Avatar"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">Joel King</span> is now following you</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="{{ asset('beagle/img/avatar4.png') }}" alt="Avatar"></div>
                                <div class="notification-info">
                                  <div class="text"><span class="user-name">John Doe</span> is watching your main repository</div><span class="date">2 days ago</span>
                                </div></a></li>
                            <li class="notification"><a href="#">
                                <div class="image"><img src="{{ asset('beagle/img/avatar5.png') }}" alt="Avatar"></div>
                                <div class="notification-info"><span class="text"><span class="user-name">Emily Carter</span> is now following you</span><span class="date">5 days ago</span></div></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">View all notifications</a></div>
                  </li>
                </ul>
              </li>
              {{-- <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><span class="icon mdi mdi-apps"></span></a>
                <ul class="dropdown-menu be-connections">
                  <li>
                    <div class="list">
                      <div class="content">
                        <div class="row">
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/github.png') }}" alt="Github"><span>GitHub</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/bitbucket.png') }}" alt="Bitbucket"><span>Bitbucket</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/slack.png') }}" alt="Slack"><span>Slack</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/dribbble.png') }}" alt="Dribbble"><span>Dribbble</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/mail_chimp.png') }}" alt="Mail Chimp"><span>Mail Chimp</span></a></div>
                          <div class="col-xs-4"><a href="#" class="connection-item"><img src="{{ asset('beagle/img/dropbox.png') }}" alt="Dropbox"><span>Dropbox</span></a></div>
                        </div>
                      </div>
                    </div>
                    <div class="footer"> <a href="#">More</a></div>
                  </li>
                </ul>
              </li>  --}}
            </ul>
          </div>
        </div>
      </nav>
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="{{ route('home') }}" class="left-sidebar-toggle">Dashboard</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
              <div class="left-sidebar-content">
                <ul class="sidebar-elements">
                  <li class="divider">Menu</li>
                  <li class="{{ Request::is(['home']) ? 'active':'' }}">
                      <a href="{{ url('/home') }}"><i class="icon mdi mdi-home"></i> <span>Dashboard</span></a>
                  </li>
                  
                  <li class="parent">
                        <a href="#"><i class="icon mdi mdi-view-list-alt"></i><span>Master Data</span></a>
                        <ul class="sub-menu">    
                          <li class="{{ Request::is(['location/*', 'location']) ? 'active':'' }}"><a href="{{ url('location') }}">Location</a></li>                          
                          <li class="{{ Request::is(['supplier/*', 'supplier']) ? 'active':'' }}"><a href="{{ url('supplier') }}">Vendor</a></li>
                          <li class="parent">
                            <a href="#"><span>Setting</span></a>
                            <ul class="sub-menu">
                              <li class="{{ Request::is(['system/plant/*', 'system/plant']) ? 'active':'' }}"><a href="{{ url('system/plant') }}">Plant</a></li>
                              <li class="{{ Request::is(['locationtype/*', 'locationtype']) ? 'active':'' }}"><a href="{{ url('locationtype') }}">Location Type</a></li>
                            </ul>
                          </li>
                        </ul>
                  </li>
                  <li class="parent">
                      <a href="#"><i class="icon mdi mdi-folder"></i><span>Asset</span></a>
                      <ul class="sub-menu">
                        <li class="{{ Request::is(['asset/*', 'asset']) ? 'active':'' }}"><a href="{{ url('asset') }}">Asset</a></li>
                        <li class="{{ Request::is(['asset-refund', 'asset-refund']) ? 'active':'' }}"><a href="{{ url('asset-refund') }}">Asset Refund</a></li>
                        <li class="{{ Request::is(['asset-handover', 'asset-handover']) ? 'active':'' }}"><a href="{{ url('asset-handover') }}">Asset Handover</a></li>
                        <li class="parent">
                        <a href="#"><span>Material</span></a>
                          <ul class="sub-menu">
                            <li class="{{ Request::is(['material/*', 'material']) ? 'active':'' }}"><a href="{{ url('material') }}">Material</a></li>
                            <li class="{{ Request::is(['materialgroup/*', 'materialgroup']) ? 'active':'' }}"><a href="{{ url('materialgroup') }}">Material Group</a></li>
                            <li class="{{ Request::is(['classification/*', 'classification']) ? 'active':'' }}"><a href="{{ url('classification') }}">Classification</a></li>           
                          </ul>
                        </li>
                        <li class="parent">
                        <a href="#"><span>Depreciation</span></a>
                          <ul class="sub-menu">
                            <li class="{{ Request::is(['depreciation/*', 'depreciation']) ? 'active':'' }}"><a href="{{ url('depreciation') }}">Depreciation</a></li>
                            <li class="{{ Request::is(['depreciationtype/*', 'depreciationtype']) ? 'active':'' }}"><a href="{{ url('depreciationtype') }}">Depreciation Type</a></li>
                          </ul>
                        </li>
                        <li class="parent">
                        <a href="#"><span>Setting</span></a>
                          <ul class="sub-menu">
                            <li class="{{ Request::is(['assettype/*', 'assettype']) ? 'active':'' }}"><a href="{{ url('assettype') }}">Asset Type</a></li>
                            <li class="{{ Request::is(['statusasset/*', 'statusasset']) ? 'active':'' }}"><a href="{{ url('statusasset') }}">Asset Status</a></li>
                            <li class="{{ Request::is(['retiredreason/*', 'retiredreason']) ? 'active':'' }}"><a href="{{ url('retiredreason') }}"></i>Retired Reason</a></li>
                            <li class="{{ Request::is(['config/barcode/*', 'config/barcode']) ? 'active':'' }}"><a href="{{ url('config/barcode') }}">Barcode Print Config</a></li>
                            {{--<li class="{{ Request::is(['valuationgroup/*', 'valuationgroup']) ? 'active':'' }}"><a href="{{ url('valuationgroup') }}">Valuation Group</a></li>--}}
                          </ul>
                        </li>
                      </ul>
                  </li>
                  <li class="parent">
                      <a href="#"><i class="icon mdi mdi-file"></i><span>Report</span></a>
                      <ul class="sub-menu">
                        <li class="{{ Request::is(['report/periodic-movement*', 'report/periodic-movement']) ? 'active':'' }}"><a href="{{ url('report/periodic-movement') }}">Periodic Movement</a></li>
                        <li class="{{ Request::is(['report/asset-handover*', 'report/asset-handover']) ? 'active':'' }}"><a href="{{ url('report/asset-handover') }}">Asset Handover</a></li>
                        <li class="{{ Request::is(['report/stock-card*', 'report/stock-card']) ? 'active':'' }}"><a href="{{ url('report/stock-card') }}">Stock Card</a></li>
                        <li class="{{ Request::is(['report/expiry-usage*', 'report/expiry-usage']) ? 'active':'' }}"><a href="{{ url('report/expiry-usage') }}">Expiry Usage</a></li>
                      </ul>
                  </li>
                  @if(Auth::user()->id_role==1)
                  <li class="divider">User</li>
                  <li class="parent">
                      <a href="#"><i class="icon mdi mdi-settings"></i><span>System</span></a>
                      <ul class="sub-menu">
                        <li class="{{ Request::is(['user/*', 'user']) ? 'active':'' }}"><a href="{{ url('user') }}"></i> User</a></li>
                        <li class="{{ Request::is(['role/*', 'role']) ? 'active':'' }}"><a href="{{ url('role') }}"></i> Role</a></li>
                      </ul>
                  </li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="be-content">
        <!-- <div class="main-content container-fluid"></div> -->
        <nav class="navbar2 navbar-default" style="background-color: #F7C915">
            <div class="container-fluid">
                <h4><strong style="color:white"> @yield('header') </strong></h4>
            </div>
        </nav>
        <div class="main-content">
        @yield('content')
        </div>
        
      </div>
      
    </div>
    @yield('beforescript')
    <script src="{{ asset('beagle/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    {{-- <script src="https://cdn.ravenjs.com/3.22.1/raven.min.js" crossorigin="anonymous"></script> --}}
    <script src="{{asset('beagle/lib/datatables/js/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/js/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/dataTables.buttons.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/buttons.html5.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/buttons.flash.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/buttons.print.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/buttons.colVis.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/datatables/plugins/buttons/js/buttons.bootstrap.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/js/app-tables-datatables.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/gmaps/locationpicker.jquery.js',false) }}"></script>
    <!-- <script src="{{asset('assets/gmaps/locationpicker.jquery.js', config('app.env') == 'production') }}"></script>-->
    <script src="{{asset('beagle/lib/select2/js/select2.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('beagle/lib/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/plugins/daterangepicker/moment.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
    <script>
      // configure the SDK as you normally would
      // Raven.config('https://4cb738e8215349ec81c9df925af272ec:4e24b4072d48434cbd122a2247befc61@sentry.mindaperdana.com/12').install();
      // $('.be-toggle-left-sidebar').click(function(){
      //   $('.mdi-caret-left').hide();
      // })
      var status_menu = localStorage.getItem('menu');
      
      $(document).ready(function(){
        $(".be-toggle-left-sidebar").click(function(){
          $(".mdi-caret-left").toggle(200);
          $(".mdi-caret-right").toggle(200);
        });
        // console.log(status_menu);        
        if(status_menu == 1){
          $("#collabs").addClass("be-collapsible-sidebar-collapsed");
          $(".mdi-caret-left").toggle(200);
          $(".mdi-caret-right").toggle(200);
        }
        
      });

      $('#menu').click( function() {
        status_menu = localStorage.getItem('menu');        
        if(status_menu == 1){
          // window.localStorage.clear();
          localStorage.setItem('menu', 0);
        }else{
          localStorage.setItem('menu', 1);
        }
      });
    
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
      	//initialize the javascript
      	App.init();
        // App.formElements();
        App.dataTables();
      });
    </script>
    
    @yield('afterscript')
  </body>
</html>
