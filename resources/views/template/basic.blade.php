<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>@yield('title',"ITS'IZORA")</title>
  {{--  <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('/css/perfect-scrollbar.min.css') }}" />  --}}
  <!-- <link rel="stylesheet" href="node_modules/flag-icon-css{{ asset('/css/flag-icon.min.css') }}" />  -->
  <link rel="stylesheet" href="{{ asset('/css/style.css') }}" />
  <link rel="shortcut icon" href="{{ asset('assets/images/izora.png')}}" type="image/png" />
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
  <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/><!--[if lt IE 9]>
      
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
  <link rel="stylesheet" href="{{ asset('beagle/css/style.css') }}" type="text/css"/>
  
  @yield('head')
</head>
<style>
body:before {
  content: "";
  position: fixed;
  left: 0;
  right: 0;
  z-index: -1;
  
  display: block;
  background-image: linear-gradient(
      rgba(0, 0, 0, 0.7), 
      rgba(0, 0, 0, 0.7)
    ),
    url("{{ asset('assets/images/back.jpg')}}");
    min-width: 100%;
    min-height: 100%;

  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
<body>
  @yield('body')

  @yield('before_script')
  {{-- 
<script src="{{ asset('assets/js/bootstrap.min.js', config('app.env') == 'production') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/js/plugins/slimScroll/jquery.slimscroll.min.js', config('app.env') == 'production') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/js/plugins/fastclick/fastclick.js', config('app.env') == 'production') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js', config('app.env') == 'production') }}"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="../../dist/js/demo.js"></script>-->
<!-- DataTables -->
<script src="{{ asset('assets/js/webix.js')}}"></script> 
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.js')}}"></script>
<script src="{{ asset('assets/gmaps/locationpicker.jquery.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.js', config('app.env') == 'production')}}"></script>
<script src="{{ asset('assets/gmaps/locationpicker.jquery.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js', config('app.env') == 'production') }}"></script>
--}}
<script src="{{ asset('beagle/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('beagle/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('beagle/js/main.js') }}" type="text/javascript"></script>
<script src="{{ asset('beagle/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //initialize the javascript
    App.init();
  });
      
    </script>
  @yield('after_script')
</body>

</html>