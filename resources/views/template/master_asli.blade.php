<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@yield('title','Asset Management')</title>
  <link rel="Shortcut Icon" href="{{ asset('assets/images/IZORALOGOkecil.png'}}" type="image/x-icon" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- Bootstrap 3.3.7 -->
  
  <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css', config('app.env') == 'production') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.css', config('app.env') == 'production') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('assets/css/AdminLTE.min.css', config('app.env') == 'production') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('assets/css/skins/_all-skins.min.css', config('app.env') == 'production') }}">

  <link rel="stylesheet" href="{{ asset('assets/css/custom.css', config('app.env') == 'production') }}">

  <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2.css')}}">
  {{-- <link rel="stylesheet" href="{{asset('assets/js/webix.css')}}"> --}}
  <link rel="stylesheet" href="{{asset('assets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}">
  <link rel="stylesheet" href="{{asset('assets/js/plugins/datepicker/datepicker3.css')}}">
  <link rel="stylesheet" href="{{asset('assets/js/plugins/daterangepicker/daterangepicker-bs3.css')}}">
  <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2.min.css', config('app.env') == 'production')}}">
  <link rel="stylesheet" href="{{asset('assets/js/plugins/select2/select2.css', config('app.env') == 'production')}}">
 
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- jQuery 3.1.1 -->
  <!-- <script src="{{ asset('assets/js/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script> -->
  <script src="{{ asset('assets/js/plugins/jQuery/jQuery-2.1.4.min.js', config('app.env') == 'production') }}"></script>
  <script src="{{ asset('assets/js/plugins/daterangepicker/moment.min.js')}}"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <link rel="stylesheet" type="text/css" href="https://res.cloudinary.com/qiscus/raw/upload/DbSLsqjXn5/qiscus-sdk.2.5.8.css">
  <!-- add this CDN for emojione if you intend to support emoji -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/emojione/2.2.7/lib/js/emojione.min.js"></script>
  <script src="https://res.cloudinary.com/qiscus/raw/upload/LwJUbnqW0r/qiscus-sdk.2.5.8.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/webrtc-adapter/5.0.3/adapter.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/simple-peer/8.1.1/simplepeer.min.js"></script>
  <script src="https://sangkil.id/assets/js/hubV2.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
 

  @yield('headerscript')

</head>
<body class="hold-transition skin-blue sidebar-mini {{ $sidebar or '' }}">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>SM</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Asset</b>Management</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        AdminLTE Design Team
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user3-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user4-128x128.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="notifications-menu">
            <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();" class="dropdown-toggle" data-toggle="dropdown" title="Sign Out">{{ Auth::user()->email }}
              <span class="hidden-xs"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
            </a>
          </li>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          @if (Auth::user()->image != null)
            <img src="{{URL::to('storage/'.Auth::user()->image.'')}}" alt="User Image">
          @else
            <img src="{{URL::to('storage/images/user.png')}}" alt="User Image">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGATION</li>
        <li>
            <a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        
        <li class="treeview">
              <a href="#"><i class="fa fa-list"></i><span>Master Data</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url('office') }}"><i class="fa fa-circle-o"></i> Office</a></li>
                <li><a href="{{ url('supplier') }}"><i class="fa fa-circle-o"></i> Vendor</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-wrench"></i><span>Setting</span><i class="fa fa-angle-left pull-right"></i></a>
                  <ul class="treeview-menu">
                    <li><a href="{{ url('system/region') }}"><i class="fa fa-circle-o"></i> Region</a></li>
                  </ul>
                </li>
              </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-folder"></i><span>Asset</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            
            <li><a href="{{ url('classification') }}"><i class="fa fa-circle-o"></i> Classification</a></li>           
            <li><a href="{{ url('material') }}"><i class="fa fa-circle-o"></i> Material</a></li>
            <li><a href="{{ url('asset') }}"><i class="fa fa-circle-o"></i> Asset</a></li>
            <li class="treeview">
            <a href="#"><i class="fa fa-wrench"></i><span>Setting</span><i class="fa fa-angle-left pull-right"></i></a>
              <ul class="treeview-menu">
                <li><a href="{{ url('materialgroup') }}"><i class="fa fa-circle-o"></i> Material Group</a></li>
                <li><a href="{{ url('assettype') }}"><i class="fa fa-circle-o"></i> Asset Type</a></li>
                <li><a href="{{ url('valuationgroup') }}"><i class="fa fa-circle-o"></i> Valuation Group</a></li>
                <li><a href="{{ url('statusasset') }}"><i class="fa fa-circle-o"></i> Asset Status</a></li>
                <li><a href="{{ url('retiredreason') }}"><i class="fa fa-circle-o"></i> Retired Reason</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#"><i class="fa fa-gear"></i><span>System</span><i class="fa fa-angle-left pull-right"></i></a>
          <ul class="treeview-menu">
            <li><a href="{{ route('permissions.index') }}"><i class="fa fa-user"></i> User</a></li>
            <li><a href="{{ route('permissions.index') }}"><i class="fa fa-user-plus"></i> Role</a></li>
          </ul>
        </li> 
        <style type="text/css">
img.icon{
    width:20px;
    height:20px;
    margin-right: 10px;
    margin-left: -5px;
}
</style>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  

    @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2017 <a href="http://mindaperdana.com">AMS</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>





<script src="{{ asset('assets/js/bootstrap.min.js', config('app.env') == 'production') }}"></script>
<!-- SlimScroll -->
<script src="{{ asset('assets/js/plugins/slimScroll/jquery.slimscroll.min.js', config('app.env') == 'production') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/js/plugins/fastclick/fastclick.js', config('app.env') == 'production') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/js/adminlte.min.js', config('app.env') == 'production') }}"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="../../dist/js/demo.js"></script>-->
<!-- DataTables -->
{{-- <script src="{{ asset('assets/js/webix.js')}}"></script> --}}
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.js')}}"></script>
<script src="{{ asset('assets/gmaps/locationpicker.jquery.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/plugins/select2/select2.js', config('app.env') == 'production')}}"></script>
<script src="{{ asset('assets/gmaps/locationpicker.jquery.js', config('app.env') == 'production') }}"></script>
<script src="{{ asset('assets/js/bootstrap-notify.min.js', config('app.env') == 'production') }}"></script>

@yield('footerscript')
</body>
</html>
