@extends('template.master')
@section('header')
Vendor Master
@endsection
@section('content')

<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('supplier') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail Vendor<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							<tr>
								<th>Vendor</th>
								<td>{{ $items->name }}</td>
							</tr>
							<tr>
								<th width="150">Contact Person</th>
								<td>{{ $items->contact_person }}</td>
							</tr>							
							<tr>
								<th>Phone</th>
								<td>{{ $items->phone }}</td>
							</tr>							
							<tr>
								<th>Email </th>
								<td>{{ $items->email }}</td>
							</tr>							
							<tr>
								<th>Website </th>
								<td><a href="http://{{ $items->website }}" target="_blank">http://{{ $items->website }}</a></td>
							</tr>	
							<tr>
								<th>Address</th>
								<td>{{ $items->address_location }}</td>
							</tr>								
						
						</table>
						
					</div>
					<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							
							<tr>
								<th>Building</th>
								<td>{{ $items->building }}</td>
							</tr>
							<tr>
								<th>Unit</th>
								<td>{{ $items->unit }}</td>
							</tr>
							<tr>
								<th>Created By</th>
								<td>{{ $items->userCreated->name ?? NULL }}</td>
							</tr>
							<tr>
								<th style="width: 150px;">Created Date</th>
								<td>{{ $items->created_at }}</td>
							</tr>
							<tr>
								<th>Updated By</th>
								<td>{{ $items->userUpdated->name ?? NULL }}</td>
							</tr>
							<tr>
								<th>Updated Date</th>
								<td>{{ $items->updated_at }}</td>
							</tr>
						</table>
					</div>
				</div>
				<div class="row xs-pt-15">
					<div class="col-xs-12">
						<btn href="#bar" data-toggle="collapse" class="btn btn-success" onclick="ShowHide(this);">Show Map</btn>
					</div>
				</div>
					
				<div class="row xs-pt-15">
					<div class="col-xs-12">
						<div id="bar" class="collapse">
							<td><div id="show-map" style="width: 100%; height: 300px;"></div></td>
						</div>
					</div>	
				</div>

				<div class="row xs-pt-15">
					<div class="col-xs-12">
						<p class="text-right">
							{{--  <a href="{{ url('supplier') }}" class="btn btn-default btn-lg">Back</a>  --}}
						</p>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

@endsection

@section('afterscript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBat0J0W6eArCkCSjkEX3FN9Tt8gSKmcRc&libraries=places"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
    });
    $(function() {
        $('#show-map').locationpicker({
            location: {
                latitude: "{{ $items->latitude }}",
                longitude: "{{ $items->longitude }}"
            },
            radius:0,
            draggable: false

            // zoom: 20
        });
    }); 
</script>

<script type="text/javascript">
// $(window).load(function () {
//    $('#bar').removeClass('in')
   

// });

function ShowHide(id) {
  if ($(id).html() == "Show Map") {
    $(id).html("Hide Map");
  } else {
    $(id).html ("Show Map");
  }

}
</script> 
@endsection