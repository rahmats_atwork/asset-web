@extends('template.master')
@section('header')
Vendor Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
		  	<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('supplier') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				  Create New Vendor<span class="panel-subtitle"></span>
			</div>
		  	<div class="panel-body">
				<form id="form-create" class="form-horizontal group-border-dashed" method="post" action="{{ url('supplier') }}">
					{!! csrf_field() !!}
					<div class="row">

						<div class="col-md-6">

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Vendor<font size="3" color="red"> *</font></label>
									<input type="text" name="name" class="form-control" value="{{ old('name') }}" required>
									@if ($errors->has('name'))
									<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">									
									<label class="control-label">Contact Person<font size="3" color="red"> *</font></sup></label>
									<input type="text" name="contact_person" class="form-control" value="{{ old('contact_person') }}" required>
									@if ($errors->has('contact_person'))
									<span class="help-block" style="color:red">{{ $errors->first('contact_person') }}</span>
									@endif
								</div>										
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Phone<font size="3" color="red"> *</font></sup></label>
									<input type="text" maxlength="12" minlength="10" pattern="\d*" name="phone" class="form-control" value="{{ old('phone') }}">
									@if ($errors->has('phone'))
									<span class="help-block" style="color:red">{{ $errors->first('phone') }}</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Email</label>
									<input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="e.g admin@gmail.com">
									@if ($errors->has('email'))
									<span class="help-block" style="color:red">{{ $errors->first('email') }}</span>
									@endif
								</div>
							</div>

						</div>

						<div class="col-md-6">

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Website </label>
									<div class="input-group"><span class="input-group-addon">http://</span>
										<input type="text" name="website" class="form-control">
									</div>
									
									@if ($errors->has('website'))
									<span class="help-block" style="color:red">{{ $errors->first('website') }}</span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-12">
									<label class="control-label">Address</label>
									<input class="form-control clearme" id="us2-address" name="supplier_address" id="supplier_address" placeholder="Posisi" value="{{ old('supplier_address') }}">
									<input type="hidden" class="form-control" style="width: 110px" id="longitude" name="longitude" />
									<input type="hidden" class="form-control" style="width: 110px" id="latitude" name="latitude" />
								</div>
							</div>

							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Province</sup></label>
											<input type="text" id="province" name="province" class="form-control" value="{{ old('province') }}">
											@if ($errors->has('province'))
											<span class="help-block" style="color:red">{{ $errors->first('province') }}</span>
											@endif
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">City</sup></label>
											<input type="text" id= "city" name="city" class="form-control" value="{{ old('city') }}">
											@if ($errors->has('city'))
											<span class="help-block" style="color:red">{{ $errors->first('city') }}</span>
											@endif
										</div>
									</div>
								</div>

							</div>

							<div class="row">

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Building</sup></label>
											<input type="text" id="building" name="building" class="typeahead form-control" value="{{ old('building') }}" autocomplete="off" >
											@if ($errors->has('building'))
											<span class="help-block" style="color:red">{{ $errors->first('building') }}</span>
											@endif
										</div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
										<div class="col-md-12">
											<label class="control-label">Unit </sup></label>
											<input type="text" id="unit" name="unit" class="form-control" value="{{ old('unit') }}" autocomplete="off" >
											@if ($errors->has('unit'))
											<span class="help-block" style="color:red">{{ $errors->first('unit') }}</span>
											@endif
										</div>
									</div>
								</div>

							</div>
							
						</div>
						
					</div>

					<div class="row">
						<div class="col-md-12">
							<div id="us2" style="width: 100%; height: 300px;"></div>
						</div>
					</div>

					<hr>

					<div class="row xs-pt-15">
						<div class="col-xs-12">
							<p class="text-right">
								<button type="submit" class="btn btn-primary btn-lg">Save</button>
								{{--  <a href="{{ url('supplier') }}" class="btn btn-default btn-lg">Back</a>  --}}
							</p>
						</div>
					</div>		
				</form>
		  	</div>
		</div>
	</div>
</div>


@endsection
@section('afterscript')

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBat0J0W6eArCkCSjkEX3FN9Tt8gSKmcRc&libraries=places"></script>


<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });

		var path = "{{ route('autocomplete.supplier.building.ajax') }}";
        $('#building').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });

		var path2 = "{{ route('autocomplete.supplier.unit.ajax') }}";
        $('#unit').typeahead({
            source:  function (query, process) {
            return $.get(path2, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
    });
    $(function() {
        $('#us2').locationpicker({
            location: {
                // latitude: "-6.189680",
                latitude: null,
                // longitude: "106.823440"
                longitude: null
            },
            inputBinding: {
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#us2-address'),				
            },
            radius:0,
			enableAutocomplete: true       
        });
    }); 
</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
@endsection
