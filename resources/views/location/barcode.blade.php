<head>
    <title>IZORA Location Barcode</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript">
function printDiv(divName) {
    var printContents = document.getElementById(divName).innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}
</script>
<body style="">
    <input type="button" onclick="printDiv('printableArea')" value="PRINT" />
    <div id="printableArea" style="border:1px solid black;">
        <div style="margin-left:5px;margin-top: 15px;">
        <?php foreach($location as $l):?>
            <div style="padding-bottom: 10px">
                <?php
                $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                
                echo '<img src="data:image/png;base64,'.base64_encode($generator->getBarcode($l->code, $generator::TYPE_CODE_39)).'"/><br/>';
                echo $l->code.'<br/>';
                
                ?>
            </div>
        <?php endforeach;?>
        </div>
    </div>
</body>