@extends('template.master')
@section('header')
Location Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('location') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Create New Location<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" action="{{ url('location') }}">
						{!! csrf_field() !!}
						
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group {{ $errors->has('type') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Type</label>
										<select class="form-control" name="type" id="type">
											<option value="">-- Choose Type --</option>
											@foreach($loctype as $val)
												<option value="{{$val->id}}">{{$val->name}}</option>
											@endforeach
										</select>
										@if ($errors->has('type'))
											<span class="help-block" style="color:red">{{ $errors->first('type') }}</span>
										@endif
									</div>
								</div>

								
								<div class="form-group">
									<div class="col-md-12">
										<label>Plant<font size="3" color="red"> *</font></label>
										<select class="form-control" name="plant_id" id="plant_id" required>
										</select>
									</div>
								</div>
															
								<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<div class="col-md-12">
										<label>Location<font size="3" color="red"> *</font></label>
										<input type="text" name="name" style="text-transform: capitalize;" class="form-control" required>
										@if ($errors->has('name'))
											<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
										@endif
									</div>
								</div>

								<div class="form-group">
									
										<div class="col-md-12">
											<label>Address</label>
											<div class="input-group">
												<textarea type="text" rows="4" class="form-control clearme" id="us2-address" name="address" id="address" placeholder="Address">{{ old('address') }}</textarea>
												<div class="input-group-addon btn btn-primary" href="#bar" data-toggle="collapse">
													<i  class="mdi mdi-map" ></i>
												</div>
											</div>
											<input type="hidden" class="form-control" style="width: 110px" value="{{ old('longitude') }}" id="longitude" name="longitude" />
											<input type="hidden" class="form-control" style="width: 110px" value="{{ old('latitude') }}" id="latitude" name="latitude"/>
											<br>
										</div> 
								</div>
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group {{ $errors->has('building') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Building</label>
									<input type="text" autocomplete="off" id="building" name="building" value="{{ old('building') }}" data-provide="typeahead" class="form-control">
										@if ($errors->has('building'))
											<span class="help-block" style="color:red">{{ $errors->first('building') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('unit') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Unit</label>
									<input type="text" name="unit" class="form-control" value="{{ old('unit') }}">
										@if ($errors->has('unit'))
											<span class="help-block" style="color:red">{{ $errors->first('unit') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Responsible Person</label>
									<input type="text" name="contact" class="form-control" value="{{ old('contact') }}">
										@if ($errors->has('contact'))
											<span class="help-block" style="color:red">{{ $errors->first('contact') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Phone</label>
									<input type="text" name="phone" class="form-control" value="{{ old('phone') }}">
										@if ($errors->has('phone'))
											<span class="help-block" style="color:red">{{ $errors->first('phone') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
									<div class="col-md-12">
										<label>Email</label>
									<input type="email" name="email" class="form-control" value="{{ old('email') }}">
										@if ($errors->has('email'))
											<span class="help-block" style="color:red">{{ $errors->first('email') }}</span>
										@endif
									</div>
								</div>
							</div>
							
							<div class="box-body collapse" id="bar">
								<div id="us2" style="width: 100%; height: 300px;"></div>
							</div>	
						</div>
						<div class="row xs-pt-15">
							<div class="col-xs-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
								</p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
</div> 				

@endsection
@section('afterscript')
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAr2g3IUvJLISp0QowAAN5fhQnf3oQA354"></script>
<script type="text/javascript">

    $(document).ready(function(){
		$('#plant_id').select2({
            width: '100%',  
            placeholder: 'Choose Plant',
            ajax:  {
                url: '{{route('plant_active_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.description,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });

		$('#type').select2({width :'100%'});
    });
   
</script> 
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });

        var path = "{{ route('completebuilding.ajax') }}";
        $('#building').typeahead({
        	
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        }); 
    });
    $(function() {
        $('#us2').locationpicker({
            location: {
                latitude: "-6.189680",
                longitude: "106.823440"
            },
            inputBinding: {
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#us2-address')
            },
            radius:0,
            enableAutocomplete: true       
        });
    });


	function changeToUpperCase(t) {
	   var eleVal = document.getElementById(t.id);
	   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_;:",]/g, "");
	}
    
</script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
@endsection