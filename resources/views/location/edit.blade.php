@extends('template.master')
@section('header')
Location Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('location') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Edit Location<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" action="{{ url('location/'.$items->id) }}">
						{!! csrf_field() !!}
						<input type="hidden" name="_method" value="put" />
						<div class="box-body">
							<div class="col-md-6">
								<div class="form-group">
									
									<div class="col-md-12">
										<label>Type<font size="3" color="red"> *</font></label>
										<select class="form-control input-sm" name="type" id="type" required>
											<option value="{{$items->type}}">{{$items->locationType->name}}</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									
									<div class="col-md-12">
										<label>Plant<font size="3" color="red"> *</font></label>
										<select class="form-control input-sm" name="plant" id="plant" required>
											<option value="{{$items->id_plant}}">{{$items->plantLocation->name}}</option>
										</select>
									</div>
								</div>					
								<div class="form-group">
									<div class="col-md-12">
										<label>Location<font size="3" color="red"> *</font></label>
									<input type="text" name="location_name" style="text-transform: capitalize;" class="form-control input-md" value="{{ $items->name }}" required>
									</div>
								</div>
								<div class="form-group">
									
										<div class="col-md-12">
											<label>Address</label>
											<div class="input-group">
												<textarea type="text" rows="4" class="form-control clearme" id="us2-address" name="address" id="address" placeholder="Address" value="{{ $items->address }}"></textarea>
												<div class="input-group-addon btn btn-primary" href="#bar" data-toggle="collapse">
													<i  class="mdi mdi-map" ></i>
												</div>
											</div>
											<input type="hidden" class="form-control" style="width: 110px" id="longitude" name="longitude" value="{{ $items->longitude }}"/>
											<input type="hidden" class="form-control" style="width: 110px" id="latitude" name="latitude"  value="{{ $items->latitude}}"/>
											<br>
											@if ($errors->has('address'))
											<span class="help-block" style="color:red">{{ $errors->first('address') }}</span>
										@endif
											<!-- <div id="us2" style="width: 100%; height: 300px;"></div> -->
										</div> 
								</div>
								{{--  <div class="box-body">
										<div class="col-md-12">
											<btn href="#bar" data-toggle="collapse" class="btn btn-success pull-right" onclick="ShowHide(this);">Show Map</btn>
										</div>
								</div>  --}}
		                    </div>
		                    <div class="col-md-6">
								<div class="form-group">
									<div class="col-md-12">
										<label>Building</label>
									<input type="text" autocomplete="off" id="building" name="building" class="typeahead form-control input-md" value="{{ $items->building }}">
										@if ($errors->has('building'))
											<span class="help-block" style="color:red">{{ $errors->first('building') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('unit') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Unit</label>
									<input type="text" name="unit" class="form-control input-md" value="{{ $items->unit }}">
										@if ($errors->has('unit'))
											<span class="help-block" style="color:red">{{ $errors->first('unit') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('contact') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Responsible Person</label>
									<input type="text" name="contact" class="form-control input-md" value="{{ $items->contact}}">
										@if ($errors->has('contact'))
											<span class="help-block" style="color:red">{{ $errors->first('contact') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
									
									<div class="col-md-12">
										<label>Phone</label>
									<input type="text" name="phone" class="form-control input-md" value="{{ $items->phone }}">
										@if ($errors->has('phone'))
											<span class="help-block" style="color:red">{{ $errors->first('phone') }}</span>
										@endif
									</div>
								</div>
								<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
									<div class="col-md-12">
										<label>Email</label>
										<input type="email" name="email" class="form-control input-md" value="{{ $items->email }}">
										@if ($errors->has('email'))
											<span class="help-block" style="color:red">{{ $errors->first('email') }}</span>
										@endif
									</div>
		                    	</div>
		                	</div>
							<br>
								
						</div>
							<div class="box-body collapse" id="bar">
								<div id="us2" style="width: 100%; height: 300px;"></div>
							</div>
						<div class="row xs-pt-15">
							<div class="col-xs-12">
								<p class="text-right">
									<button type="submit" class="btn btn-primary btn-lg">Save</button>
								</p>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	
</div>


@endsection
@section('afterscript')
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAr2g3IUvJLISp0QowAAN5fhQnf3oQA354"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $('#region_bismo').select2({width :'100%'});
    });
    
    $(document).ready(function(){
        $('#type').select2({width :'100%'});
    });

</script> 
<script type="text/javascript">
    $(document).ready(function(){

		$('#plant').select2({
            width: '100%',  
            placeholder: 'Choose Plant',
            ajax:  {
                url: '{{route('plant_active_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
		$('#type').select2({
            width: '100%',  
            placeholder: 'Choose Location Type',
            ajax:  {
                url: '{{route('location_type_ajax')}}',
                dataType: 'json',
                type : 'GET',
                delay: 250,
                processResults: function (data) {
                    return {
                        results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
         var path = "{{ route('completebuilding.ajax') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
            return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        }); 
    });
    $(function() {
        $('#us2').locationpicker({
            location: {
                latitude: "{{ $items->latitude }}",
                longitude: "{{ $items->longitude }}"
            },
            inputBinding: {
                latitudeInput: $('#latitude'),
                longitudeInput: $('#longitude'),
                locationNameInput: $('#us2-address')
            },
            radius:0,
            enableAutocomplete: true       
        });
    }); 

	function changeToUpperCase(t) {
	   var eleVal = document.getElementById(t.id);
	   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_;:",]/g, "");
	}
    
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
@endsection
