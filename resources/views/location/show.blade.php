@extends('template.master')
@section('header')
Location Master
@endsection
@section('content')
<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-divider icon-container">
					<a href="{{ url('location') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
					Detail Location<span class="panel-subtitle"></span>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-6">
						<table class="table table-striped table-hover table-fw-widget">
							
							<tr>
								<th>Type</th>
								<td>{{ $items->locationType->name }}</td>
							</tr>
							{{--<tr>
								<th>Code</th>
								<td>{{ $items->code }}</td>
							</tr>--}}
							<tr>
								<th>Location</th>
								<td>{{ $items->name }}</td>
							</tr>							
							<tr>
								<th>Region</th>
								<td>{{ $items->plantLocation->name }}</td>
							</tr>							
							
						</table>
							{{--  <div class="box-body">
                                        <div class="col-md-2">
                                            <button href="#bar" data-toggle="collapse" class="btn btn-success pull-right" onclick="ShowHide(this);">Show Map</btn>
                                        </div>
                        	</div>  --}}
						</div>
						<div class="col-md-6">
							<table class="table table-striped table-hover table-fw-widget">
								<tr>
									<th>Address</th>
									<td>{{ $items->address }}</td>
									<td>
										<div class="input-group-addon btn btn-primary" href="#bar" data-toggle="collapse">
											<i  class="mdi mdi-map" ></i>
										</div>
									</td>
								</tr>
								<tr>
									<th>Building</th>
									<td>{{ $items->building }}</td>
									<td></td>
								</tr>
								<tr>
									<th>Unit</th>
									<td>{{ $items->unit }}</td>
									<td></td>
								</tr>
								
								<tr>
									<th>Contact Person</th>
									<td>{{ $items->contact }}</td>
									<td></td>
								</tr>
								<tr>
									<th>Phone</th>
									<td>{{ $items->phone }}</td>
									<td></td>
								</tr>
								<tr>
									<th>Email</th>
									<td>{{ $items->email }}</td>
									<td></td>
								</tr>
								<tr>
									<th>Updated By</th>
									<td>{{ $items->userUpdated->name }}</td>
									<td></td>
								</tr>
								<tr>
									<th>Updated At</th>
									<td>{{ $items->updated_at }}</td>
									<td></td>
								</tr>
							</table>
						</div>
						
					</div>
						<div class="box-body collapse" id="bar">
							<div id="show-map" style="width: 100%; height: 300px;"></div></td>
						</div>
					
				</div>
			</div>
		</div>
	</div> 						



@endsection
@section('afterscript')
<!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }} &libraries=places"></script> -->
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBlB4HeG1M_xIzlECsRNiKQsFYRNrTujMs"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.clearme').focus(function() { 
            $(this).val(''); 
        });
    });
    $(function() {
        $('#show-map').locationpicker({
            location: {
                latitude: "{{ $items->latitude }}",
                longitude: "{{ $items->longitude }}"
            },
            radius:0   
        });
    }); 

</script>
@endsection