@extends('template.master')
@section('beforehead')
<style type="text/css">

    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>
@endsection
@section('header')
Location Master
@endsection
@section('content')
<div class="row">
        <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('success')!!}
                    </div>
                @elseif(Session::has('error'))
                    <div class="alert alert-warning alert-dismissible">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        {!!Session::get('error')!!}
                    </div>
                @endif
                    <div class="panel panel-default panel-table">

                            <div class="panel-heading icon-container">
                                <a href="{{ url('location/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
                                Location List
                                <a onclick="myFunction()" id="hrefbarcode" target="_blank" class="btn btn-success pull-right" title="Print Barcode">Print Barcode</a>
                            </div>
                
                            <div class="panel-body">
                                <table id="table-data" class="table-fit table table-striped table-hover table-fw-widget">
                                    <thead>
                                        <tr>
                                            <th style="width: 200px;">Action</th>
                                            <th>Type</th>
                                            <th>plant</th>
                                            <th style="width: 150px;">Location</th>
                                            <th style="width: 200px;">Address</th>
                                            <th>Building</th>
                                            <th>Unit</th>
                                            <th style="width: 120px;">Responsible Person</th>
                                            <th>Phone</th>
                                            <th style="width: 150px;">Email</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                    </div>
        </div>
</div>
@endsection
@section('afterscript')
<script>
        function myFunction() {
            var link = "barcode/location";
            document.getElementById("hrefbarcode").href = link;
        }
        $(function() {
            $('#table-data').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true,
                "scrollCollapse": true,
                "scrollX": "1800px",
                "iDisplayLength": 50,
                "ajax":{
                         "url": "{{ url('location_ajax') }}",
                         "dataType": "json",
                         "type": "POST",
                         "data":{ 
                             _token: "{{csrf_token()}}",
                            //  plant: <?php echo json_encode($search_plant) ?>,
                            // loctype: <?php echo json_encode($search_loctype) ?>,
                             }
    
                       },
    
                "columns": [
                    { "data": "options" },
                    { "data": "type" },
                    { "data": "plant_name" },
                    { "data": "location_name" },
                    { "data": "address" },
                    { "data": "building" },
                    { "data": "unit" },
                    { "data": "contact" },
                    { "data": "phone" },
                    { "data": "email" },
                ]	 
            });			
        });
        
        $(document).ready(function() {
          
            $('#plant').select2({ 
                width: '33%',
                placeholder: 'Choose plant',
                allowClear: true,
                tokenSeparators: [',', ' '],
            });
    
             $('#loctype').select2({ 
                width: '33%',
                placeholder: 'Choose Type',
                allowClear: true,
                tokenSeparators: [',', ' '],
            });//.select2('data', initdata);
            
            //  $("#loctype").select2('val', init_loctype);
            $('.collapse').on('shown.bs.collapse', function(){
                $(this).parent().find(".fa-chevron-right").removeClass("fa-chevron-right").addClass("fa-chevron-down");
                }).on('hidden.bs.collapse', function(){
                $(this).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-right");
            }); 
    
    
        });
    function delete_data(){
        if (confirm('Are you sure delete this data?')){
            $('#form-delete').submit();
        }
    }
    </script>
@endsection
