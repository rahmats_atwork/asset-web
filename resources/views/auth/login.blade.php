@extends('template.basic')

@section('body')
<body class="be-splash-screen">
  <div class="be-wrapper be-login">
    <div class="be-content">
      <div class="main-content container-fluid">
        <div class="splash-container">
          <div class="panel panel-default panel-border-color panel-border-color-warning" style="background-color: #1D2B36;">
            <div class="panel-heading"><img src="assets/images/IZORALOGOTEXT.png" alt="logo" style="max-width:325px" max-height="27px" class="logo-img"><span class="splash-description" style="color:white">Please enter your user information.</span></div>
            <div class="panel-body">
              <form method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" name="email" value="{{ old('email') }}" required autofocus class="form-control p_input" placeholder="Email">
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group d-flex align-items-center justify-content-between">
                    <div class="form-check">
                        <label>
                          {{--  <input type="checkbox" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me  --}}
                        </label>
                    </div>
                  {{--  <a href="#" class="forgot-pass">Forgot password</a>  --}}
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-warning btn-block btn-xl enter-btn">LOG IN</button>
                </div>
              </form>
              <span class="splash-description" style="color:white">Wanna get izora in your mobile ? <a href="http://devsrv.mindaperdana.com/asset-management/public/storage/apk/Izora.apk">Download now. </a></span>
            </div>
          </div>
          {{--  <div class="splash-footer"><span>Don't have an account? <a href="#">Sign Up</a></span></div>  --}}
        </div>
      </div>
    </div>
  </div>
</body>
@endsection