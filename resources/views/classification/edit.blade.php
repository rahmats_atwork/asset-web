@extends('template.master')
@section('header')
Classification Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('classification') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Add Parameter<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
			
				<form id="form-create" class="form-horizontal" method="post" action="{{ url('classification/'.$items->id) }}"> 
					{!! csrf_field() !!} 
					<input type="hidden" name="_method" value="put" /> 
					<div class="box-body"> 
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}"> 
							<label class="col-sm-4 control-label">Classification Name<font size="3" color="red"> *</font></label> 
							<div class="col-sm-8"> 
								<input type="text" maxlength="15" id="name" name="name" class="form-control" value="{{ $items->name }}" autofocus onkeyup="changeToUpperCase(this)" required> 
								@if ($errors->has('name_clasification')) 
								<span class="help-block" style="color:red">{{ $errors->first('name') }}</span> 
								@endif 
							</div> 
						</div> 
					</div> 
					<div class="row xs-pt-15">
						<div class="col-xs-12">
							<p class="text-right">
								{{--  <a href="{{ url('classification') }}" class="btn btn-default">Back</a>  --}}
								<button type="submit" class="btn btn-primary btn-lg">Save</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>

		<div class="panel panel-default">
			<div class="panel-body">
				<form class="form-horizontal" method="post" action="{{ route('save.paramclassific',$items->id)}}">
					{!! csrf_field() !!}
					<input type="hidden" name="id_classification" value="{{ $items->id }}" />
					<div class="box-body">

						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							<label class="col-sm-4 control-label">Parameter Name<font size="3" color="red"> *</font></label>
							<div class="col-sm-8">
								<input type="text" maxlength="15" name="name" class="form-control" value="{{ old('name') }}"required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label input-md">Type<font size="3" color="red"> *</font></label>
							<div class="col-sm-4">
								<select id="type" name="type" class="form-control" onchange="yesnoCheck(this);"required>
									<option value="">-- Choose Type  --</option>
									<option value="1">Char</option>
									<option value="2">Date</option>
									<option value="3">Time</option>
									<option value="4">Numeric</option>
									<option value="5">List</option>
								</select>
							</div>
						</div>

									
						<div id="ifYes2" style="display: none;">
							<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
								<label class="col-sm-4 control-label">Values</label>
								<div class="col-sm-4">
									<input type="text" name="value" class="form-control" placeholder="Separate With ( , )" value="{{ old('value') }}">
								</div>
							</div>
						</div>
						
						<div id="ifYes" style="display: none;">
							<div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
								<label class="col-sm-4 control-label">Length</label>
								<div class="col-sm-4">
									<input type="number" name="length" class="form-control" value="{{ old('length') }}">
								</div>
							</div>
						</div>
						<div id="ifYes3" style="display: none;">
							<div class="form-group {{ $errors->has('decimal') ? ' has-error' : '' }}">
								<label class="col-sm-4 control-label">Decimal</label>
								<div class="col-sm-4">
									<input type="number" name="decimal" class="form-control"  value="{{ old('decimall') }}">
								</div>
							</div>
						</div>

						{{--  <div class="form-group {{ $errors->has('key_parameter') ? ' has-error' : '' }}">
							<label class="col-sm-4 control-label">Reading Indicator</label>
							<div class="col-sm-8">
								<input type="checkbox" name="reading" value="1"><br>
							</div>
						</div>  --}}

					</div>
					<div class="row xs-pt-15">
						<div class="col-xs-12">
							<p class="text-right">
								{{--  <a href="{{ url('classification') }}" class="btn btn-default">Back</a>  --}}
								<button type="submit" class="btn btn-primary btn-lg">Add</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div> 
	</div>

	<div class="col-sm-6">
		<div class="panel panel-default panel-table panel-border-color panel-border-color-primary">
            <div class="panel-body">
				<table id="table" class="table table-striped table-hover table-fw-widget" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Parameter</th>
							<th>Type</th>
							<th>Length</th>
							<th>Values</th>
							{{--  <th>Reading</th>  --}}
							<th width="110px">Action</th>									
						</tr>
					</thead>			
					@foreach($paramclassific as $val)	
					<tr>
						<td>{{ $val->id }}</td>
						<td>{{ $val->name }}</td>
						<td>
						@if ($val->type==1)
						Char
						@elseif ($val->type==2)
						Date
						@elseif ($val->type==3)
						Time
						@elseif ($val->type==4)
						Numeric
						@else
						List
						@endif</td>
						<td>@if ($val->type==4)
						{{ $val->length or 0 }}, {{ $val->decimal or 0}}
						@elseif ($val->type==1)
						{{ $val->length or 0 }}
						@else
						{{ $val->length}}
						@endif
						</td>
						<td>
						@foreach(explode(',',$val->value) as $key)
						{{ $key }}<br>
						@endforeach
						</td>
						</td>
						{{--  <td>@if ($val->reading==1)
						yes
						@else
						No
						@endif
						</td>  --}}
						{{--  <td>
							<a href="{{ URL::to('classification/editparam/' . $val->id) }}" class="btn btn-success btn-sm" title="Edit Office Data"><i class="fa fa-pencil"></i></a>
							<form method="post" action="{{ URL::to('classification/paramclassificdestroy/' . $val->id) }}" onclick="return confirm('Are you sure delete this data?')" class="pull-right">
								{!! csrf_field() !!}
								<input type="hidden" name="_method" value="DELETE" />
								<button type="submit" class="btn btn-danger btn-sm delete-list"><i class="fa fa-trash" title="Hapus Parameter Data"></i></button>
							</form>
						</td>  --}}
						<td class="actions">
							<form id="form-delete{{$val->id}}" method="post" action="{{ url('classification/paramclassificdestroy/' . $val->id) }}" class="pull-right">
								{!! csrf_field() !!}
								<input type="hidden" name="_method" value="DELETE" />
							</form>

							<div class="btn-group btn-space">
								<a href="{{ url('classification/editparam/' . $val->id) }}" type="button" class="btn btn-default">Edit</a>
								<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
								<ul role="menu" class="dropdown-menu" style="min-width:100px" >
									{{--  <li><a href="{{ url('assettype/' . $val->id .'/edit') }}" ><i class="mdi mdi-edit"> </i> Edit</a></li>  --}}
									<li><a href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
								</ul>
							</div>
						</td>
					</tr>
					@endforeach				
				</table>
			</div>
		</div> 
	</div>


</div>


						


@endsection
@section('afterscript')
<script>

function delete_data(val){
	if (confirm('Are you sure delete this data?')){
		$('#form-delete'+val).submit();
	}
}

function yesnoCheck(that) {
        if (that.value == "1") {
            document.getElementById("ifYes").style.display = "block";
			document.getElementById("ifYes2").style.display = "none";
			document.getElementById("ifYes3").style.display = "none";
		}
		else if (that.value == "5") {
            document.getElementById("ifYes2").style.display = "block";
			document.getElementById("ifYes").style.display = "none";
			document.getElementById("ifYes3").style.display = "none";
			}
		else if (that.value == "4") {
            document.getElementById("ifYes3").style.display = "block";
			document.getElementById("ifYes").style.display = "block";
			document.getElementById("ifYes2").style.display = "none";
        } else {
            document.getElementById("ifYes").style.display = "none";
			document.getElementById("ifYes2").style.display = "none";
			document.getElementById("ifYes3").style.display = "none";
        }
    }

	function changeToUpperCase(t) {
   var eleVal = document.getElementById(t.id);
   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/-_]/g, " ");
}
</script>

@endsection