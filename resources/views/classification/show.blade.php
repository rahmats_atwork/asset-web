@extends('template.master')
@section('header')
Classification Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-6">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('classification') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Detail Classification<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<form class="form-horizontal" method="post" action="{{ route('save.paramclassific',$items->id)}}">
					{!! csrf_field() !!}
					<input type="hidden" name="id_classification" value="" />
					<div class="box-body">
						<table class="table table-striped table-hover table-fw-widget">
								<tr>
									<th style="width:20%">Name</th>
									<td>{{ $items->name }}</td>
								</tr>
								<tr>
									<th style="width:30%">Created By</th>
									<td>{{ isset($items->userCreated) ? $items->userCreated->name : ""}}</td>
								</tr>
								<tr>
									<th>Created Date</th>
									<td>{{ $items->created_at }}</td>
								</tr>
								<tr>
									<th>Updated By</th>
									<td>{{ isset($items->userUpdated) ? $items->userUpdated->name : ""}}</td>
								</tr>	
								<tr>
									<th>Updated Date</th>
									<td>{{$items->updated_at}}</td>
								</tr>						
						</table>
					</div>
				</form>
			</div>
		</div> 
	</div>

	<div class="col-sm-6">
		<div class="panel panel-default panel-table">
            <div class="panel-body">
				<table id="table" class="table table-striped table-hover table-fw-widget" width="100%">
					<thead>
						<tr>
							<th>ID</th>
							<th>Parameter</th>
							<th>Type</th>
							<th>Length</th>
							<th>Values</th>
							{{--  <th>Reading</th>									  --}}
						</tr>
					</thead>			
					@foreach($paramclassific as $val)	
					<tr>
						<td>{{ $val->id }}</td>
						<td>{{ $val->name }}</td>
						<td>
						@if ($val->type==1)
						Char
						@elseif ($val->type==2)
						Date
						@elseif ($val->type==3)
						Time
						@elseif ($val->type==4)
						Numeric
						@else
						List
						@endif</td>
						<td>@if ($val->type==4)
						{{ $val->length or 0 }}, {{ $val->decimal or 0}}
						@elseif ($val->type==1)
						{{ $val->length or 0 }}
						@else
						{{ $val->length}}
						@endif
						</td>
						<td>
						@foreach(explode(',',$val->value) as $key)
						{{ $key }}<br>
						@endforeach
						</td>
						</td>
						{{--  <td>@if ($val->reading==1)
						yes
						@else
						No
						@endif
						</td>  --}}
					</tr>
					@endforeach				
				</table>
			</div>
		</div> 
	</div>


</div>


						


@endsection
@section('afterscript')

@endsection