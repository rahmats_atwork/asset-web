@extends('template.master')
@section('header')
Classification Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ URL::previous() }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Edit Parameter<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
				<form id="form-create" class="form-horizontal" method="post" action="{{ url('editparam/'.$items->id_classification.'/'.$items->id) }}">
					{!! csrf_field() !!}
					<input type="hidden" name="_method" value="put" />
					<div class="col-md-12">
						<div class="row">				
							<div class="form-group {{ $errors->has('key_parameter') ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Parameter Name<font size="3" color="red"> *</font></label>
								<div class="col-sm-8">
									<input type="text" maxlength="15" name="name" class="form-control" value="{{  $items->name }}" required >
								</div>
							</div>

							<div class="form-group">
								<label class="col-sm-2 control-label input-md">Type<font size="3" color="red"> *</font></label>
									<div class="col-sm-4">
										<select id="type" name="type" class="form-control" onchange="yesnoCheck();" required>
											<option value="">-- Choose Type  --</option>
											<option value="1" @if($items->type==1) selected @endif >Char</option>
											<option value="2" @if($items->type==2) selected @endif >Date</option>
											<option value="3" @if($items->type==3) selected @endif >Time</option>
											<option value="4" @if($items->type==4) selected @endif >Numeric</option>
											<option value="5" @if($items->type==5) selected @endif >List</option>
										</select>
									</div>
							</div>

											
							<div id="ifYes2" style="display: none;">

								<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Values</label>
									<div class="col-sm-4">
										<input type="text" name="value" class="form-control" placeholder="Separate With ( , )" value="{{ $items->value }}">
									</div>
								</div>
							</div>
							
							<div id="ifYes" style="display: none;">
								<div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Length</label>
									<div class="col-sm-4">
										<input type="number" name="length" class="form-control" value="{{ $items->length }}">
									</div>
								</div>
							</div>

							<div id="ifYes3" style="display: none;">
								<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Decimal</label>
									<div class="col-sm-4">
										<input type="number" name="decimal" class="form-control"  value="{{ $items->decimal }}">
									</div>
								</div>
							</div>

							{{--  @if($items->type==5)
								<div class="form-group {{ $errors->has('value') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Values</label>
									<div class="col-sm-4">
										<input type="text" name="value" class="form-control" placeholder="Separate With ( , )" value="{{ $items->value }}">
									</div>
								</div>
							@elseif($items->type==4)
								<div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Length</label>
									<div class="col-sm-4">
										<input type="number" name="length" class="form-control" value="{{ $items->length }}">
									</div>
								</div>
							
								<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Decimal</label>
									<div class="col-sm-4">
										<input type="number" name="decimal" class="form-control"  value="{{ $items->decimal }}">
									</div>
								</div>
								
							@elseif($items->type==1)
								<div class="form-group {{ $errors->has('length') ? ' has-error' : '' }}">
									<label class="col-sm-2 control-label">Length</label>
									<div class="col-sm-4">
										<input type="number" name="length" class="form-control" value="{{ $items->length }}">
									</div>
								</div>
							@endif  --}}


							{{--  <div for="checkreading" class="form-group {{ $errors->has('key_parameter') ? ' has-error' : '' }}">
								<label class="col-sm-2 control-label">Reading Indicator</label>
								<div class="col-sm-8">
									@if ($items->reading == 1)
										<input id="checkreading" onclick="ShowHideClass();" type="checkbox" name="reading" value="1" checked="checked"></input><br>
									@else
										<input id="checkreading" onclick="ShowHideClass();" type="checkbox" name="reading" value="1"></input><br>
									@endif
								</div>
							</div>  --}}
						</div>
					</div>
					<div class="row xs-pt-15">
						<div class="col-xs-12">
							<p class="text-right">
								{{--  <a href="{{ url('classification') }}" class="btn btn-default">Back</a>  --}}
								<button  type="submit" class="btn btn-primary btn-lg">Save</button>
							</p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<script>
	
	//$(document).ready(function(){
		var that = document.getElementById("type");
		//console.log(that.options[that.selectedIndex].value)
		yesnoCheck()

		function yesnoCheck() {
			var selected = that.options[that.selectedIndex].value
			if (selected == "1") {
				document.getElementById("ifYes").style.display = "block";
				document.getElementById("ifYes2").style.display = "none";
				document.getElementById("ifYes3").style.display = "none";
			}
			else if (selected == "5") {
				document.getElementById("ifYes2").style.display = "block";
				document.getElementById("ifYes").style.display = "none";
				document.getElementById("ifYes3").style.display = "none";
				}
			else if (selected == "4") {
				document.getElementById("ifYes3").style.display = "block";
				document.getElementById("ifYes").style.display = "block";
				document.getElementById("ifYes2").style.display = "none";
			} else {
				document.getElementById("ifYes").style.display = "none";
				document.getElementById("ifYes2").style.display = "none";
				document.getElementById("ifYes3").style.display = "none";
			}
		}
	//})

</script>  
@endsection
