@extends('template.master')
@section('header')
Classification Master
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		@if(Session::has('success'))
			<div class="alert alert-success alert-dismissible">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				{!!Session::get('success')!!}
			</div>
		@elseif(Session::has('error'))
			<div class="alert alert-warning alert-dismissible">
				<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
				{!!Session::get('error')!!}
			</div>
		@endif
		<div class="panel panel-default panel-table">

				{{--  <div class="panel-heading icon-container">
					<a href="#" data-toggle="modal" data-target="#form-bp1" type="button" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
					Master Asset Type
				</div>  --}}
				<div class="panel-heading icon-container">
					<a href="{{ url('classification/create') }}" class="icon" title="Add Data"><span class="mdi mdi-plus"></span></a><span class="icon-class"></span>
					Classification List
				</div>
	
				<div class="panel-body">
					<div class="pull-left" style="margin-left: 10px;">
                        <form>
							<div>
							<select id="limit-row" name="limit" class="form-control">
								<option {{ request()->has('limit') && request()->limit==10?'selected':'' }} value="10">10</option>
								<option {{ request()->has('limit') && request()->limit==25?'selected':'' }} value="25">25</option>
								<option {{ request()->has('limit') && request()->limit==100?'selected':'' }} value="100">100</option>
							</select>
							</div>
                        </form>
                    </div>
					<div class="pull-right" style="margin-right: 10px;">
						<input id="search-input" class="form-control" placeholder="Search" type="text" name="q" value="@if(request()->has('q')){{ request()->q}}@endif" />
                    </div>
					<table id="table12" class="table table-striped table-hover table-fw-widget" width="100%">
						<thead>
							<tr>
								<th style="width: 5%">No</th>
								<th>Classification Name</th>
								<th style="width: 150px;">Total Parameter</th>
								<th style="width: 90px;">Used By</th>
								<th style="width: 90px;">Action</th>
							</tr>
						</thead>
						@php $i = 1 @endphp
						@foreach($master_classification as $val)
						<tr id="{{$val->id}}">
							<td style="text-align: center">{{ $i++ }}</td>
							<td>{{ $val->name }}</td>
							<td>@php $total_parameter = DB::table('classification_parameter')
									->where('id_classification', $val->id)
									->count('name'); 
									@endphp
									{{$total_parameter}}</td>
							<td>
								
								@php 
									$m = \App\Models\Masters\Material::where('id_classification',$val->id)->get();
									$total_used = \App\Models\Masters\Material::where('id_classification', $val->id)
									->count('id'); 
								@endphp
									<a class="show-used" data-toggle="modal" data-id="{{$val->id}}">{{$total_used}}</a>
							
							</td>

							<td class="actions">
								<form id="form-delete{{$val->id}}" method="post" action="{{ url('classification/'. $val->id) }}" class="pull-right">
									{!! csrf_field() !!}
									<input type="hidden" name="_method" value="DELETE" />
								</form>
	
								<div class="btn-group btn-space">
									<a href="{{ url('classification/' . $val->id) }}" type="button" class="btn btn-default">Detail</a>
									<button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="mdi mdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
									<ul role="menu" class="dropdown-menu" style="min-width:100px" >
										<li><a href="{{ url('classification/' . $val->id .'/edit')}}" ><i class="mdi mdi-edit"> </i> Edit</a></li>
										<li><a style="display: <?php echo $total_used >= 1 ? 'none':'block' ?>" href="#" onclick="delete_data({{$val->id}})" type="submit"><i class=" text-danger mdi mdi-delete"> </i> Delete</a></li>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
                    <div class="text-center">
						<?php
						$appends = [];
						if(request()->has('q')) $appends['q'] = request()->q;
						if(request()->has('limit')) $appends['limit'] = request()->limit;
						?>
                        {{ $list->appends($appends)->links() }}
                    </div>
				</div>
		</div>
	</div>
</div>

		
	</section>
</div>
<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Used By</h4>
			</div>
			<div class="modal-body">
				<table id="table5" class="table table-bordered table-condensed">
									<thead>
										<tr>
											<th style="width: 5%">No</th>
											<th>Material Code</th>
											<th>Description</th>
										</tr>
									</thead>
									<tbody id="data-material">
										@php $i = 1 @endphp
										@foreach($master_material as $val)
										{{--  <tr>
											<td style="text-align: center">{{ $i++ }}</td>
											<td>{{ $val->name }}</td>
											<td>{{ $val->description }}</td>
											
										</tr>  --}}
									@endforeach
									</tbody>
								</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			</div>

		</div>
</div>
@endsection
@section('afterscript')
<script>

	function delete_data(val){
		if (confirm('Are you sure delete this data?')){
			$('#form-delete'+val).submit();
		}
	}

	$('.show-used').click(function(){
 		$('#data-material').empty()
		var idClass = $(this).attr('data-id');
		// console.log($(this).attr('data-id'));
		url = "{{ url('classification/materialused/') }}/"+idClass;
		$.get(url, function(datamaterial) {
			data = JSON.parse(datamaterial);
			var html;
			var i=0;
			data.forEach(function(item){ 
				i++;
				html += '<tr><td style="text-align: center">'+i+'</td><td>'+item.name+'</td><td>'+item.description+'</td></tr>'
			}) 
			// console.log(html);
 			$('#data-material').html(html)
		});

		$('#myModal').modal('show');
		
	})


	$(function() {
        $('#table12').DataTable({
            // "paging": true,
            // "lengthChange": true,
            // "searching": true,
            // "ordering": false,
            // "info": true,
            // "autoWidth": true,
            // // "scrollX": true,

            "paging": false,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true,
            "scrollX": true,
            // "responsive": true,
            "scrollY": "400px",
        });         
    });

	$('#limit-row').change(function(){
		window.location.replace("{{ url('classification') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
	});

	$('#search-input').keypress(function (e) {
		if(e.which == 13) {
			window.location.replace("{{ url('classification') }}?q="+$("#search-input").val()+"&limit="+$("#limit-row").val());
		}
	});
</script>  
@endsection