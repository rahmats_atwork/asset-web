@extends('template.master')
@section('header')
Classification Master
@endsection
@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-default">
			<div class="panel-heading panel-heading-divider icon-container">
				<a href="{{ url('classification') }}" class="icon" title="Back"><span class="mdi mdi-arrow-left"></span></a><span class="icon-class"></span>
				Create New Classification<span class="panel-subtitle"></span>
			</div>
			<div class="panel-body">
					<form id="form-create" class="form-horizontal" method="post" action="{{ url('classification') }}">
						{!! csrf_field() !!}
						<div class="col-md-12">
							<div class="row">

								<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
									<div class="col-sm-8">
										<label>Classification Name<font size="3" color="red"> *</font></label>
										<input id="name" type="text" maxlength="15" name="name" class="form-control" autofocus onkeyup="changeToUpperCase(this)" required>
										@if ($errors->has('name'))
										<span class="help-block" style="color:red">{{ $errors->first('name') }}</span>
										@endif
									</div>
								</div>

							</div>
						</div>
						<div class="row xs-pt-15">
							<div class="col-xs-12">
								<p class="text-right">
									{{--  <a href="{{ url('classification') }}" class="btn btn-default">Back</a>  --}}
									<button  type="submit" class="btn btn-primary btn-lg">Next</button>
								</p>
							</div>
						</div>
					</form>
			</div>
		</div>
	</div>
</div>

@endsection
@section('afterscript')

<script>
	function changeToUpperCase(t) {
	   var eleVal = document.getElementById(t.id);
	   eleVal.value= eleVal.value.toUpperCase().replace(/[^-a-zA-Z0-9/_;:",]/g, "");
	}
	
</script>

@endsection
