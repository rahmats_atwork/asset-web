@extends('template.master')
@section('beforehead')
<script>

</script>
<script src="{{ asset('assets/js/locals/report/app.js')}}"></script>
<style type="text/css">

    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>

<link rel="stylesheet" href="{{asset('assets/css/plugins/chartjs/Chart.min.css')}}">


@endsection
@section('header')
Report Analyst Asset Movement
@endsection
@section('content')
<div class="row">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-warning alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {!!Session::get('error')!!}
        </div>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <br>
            <div id="accordion" class="panel-group accordion" style="margin-bottom:0">
                <div class="panel-body">
                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" id='collapsedbutton' name='collapsedbutton'><i class="icon mdi mdi-chevron-down"></i> &nbsp; Dimension Parameter</a></h4>
                
                <div id="collapseTwo" class="panel-collapse collapse">
                        <br>
                        <br>
                        {{--<form  method="get" action="{{ url('contoh') }}"> --}}
                        {{--  {!! csrf_field() !!}  --}}
                        <div class="col-sm-12">
                            <div class="form-group">
                                    <label>Status Transaction</br></label>                                                
                            <select id="transaction" name="transaction" class="form-control">
                                <option value="1">Penyerahan Asset</option>
                                <option value="2">Pengembalian Asset</option>
                                {{-- <option value="3">Penyerahan - Pengembalian Asset</option> --}}
                            </select>
                            </div>
                        </div>
                        <div class="col-sm-12">                                
                            <div class="form-group">
                                <div> 
                                    <label>Asset Type</br></label>
                                </div>
                                <select id="assetType" name="assetType[]" class="form-control" multiple="multiple">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12"> 
                            <div class="form-group">
                                <div><label>Periode</br></label></div>
                                <div class="input-group">
                                    <input class="form-control" type="text" data-date-format="yyyy-mm" name="start_period" id="start_period" class="form-control date datetimepicker" value='@php echo date("Y-m") @endphp'>
                                    <div class="input-group-addon"> - </div>
                                    <input class="form-control" type="text" data-date-format="yyyy-mm" name="end_period" id="end_period" class="form-control date datetimepicker" value='@php echo date("Y-m",strtotime("+3 Months", strtotime(date("Y-m-d")))) @endphp'>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12"> 
                            <p class="text-right">
                                <button type="submit" class="btn btn-primary btn-lg" id="showGraph" onClick="showGraphAsset()"> Show </button>
                            </p>
                        </div>
                        {{--</form>--}}
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <br>
            <div id="accordionGraph" class="panel-group accordion" style="margin-bottom:0">
                <div class="panel-body">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionGraph" href="#collapseGraph" class="collapsed" id='collapsedGraphbutton' name='collapsedbutton'><i class="icon mdi mdi-chevron-down"></i> &nbsp; Data Graph</a></h4> 
                        <div id="collapseGraph" class="panel-collapse collapse">
                            <br>
                            <br>
                            <div id="chartDiv"> 
                                <table class="table table-borderless">
                                    <tr>
                                        <td style="text-align:center;">No Result</td>
                                    </tr>
                                </table>
                                <br>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="panel panel-default">
            <br>
            <div id="accordionResume" class="panel-group accordion" style="margin-bottom:0">
                <div class="panel-body">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionResume" href="#collapseData" class="collapsed" id='collapsedDatabutton' name='collapsedDatabutton'>
                    <i class="icon mdi mdi-chevron-down"></i> &nbsp; Data Resume Asset</a></h4> 
                        <div id="collapseData" class="panel-collapse collapse">
                            <br>
                            <div id="columnDiv" style="overflow:scroll; height:400px;"> 
                                <table class="table table-borderless fixed_header" >
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User - NIK</th>
                                            <th>Qty</th>
                                            <th>Date Transaction</th>
                                        </tr>
                                    </thead>
                                    <tbody id='tableasset' name='tableasset'>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>User - NIK</th>
                                            <th>Qty</th>
                                            <th>Date Transaction</th>
                                        </tr>
                                    </tfoot>

                                </table>
                                <br>
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection
@section('afterscript')

<script>
    $(document).ready(function(){ // on document ready
        $('#collapseTwo').collapse("show");
        //$('#assetColumn2').hide();
    })

    $('#start_period').datepicker({
        changeMonth: true,
        changeYear: true,
        todayHighlight:true,
        viewMode: "months", 
        minViewMode: "months",
        autoclose:true
    });

    $('#end_period').datepicker({
        changeMonth: true,
        changeYear: true,
        todayHighlight:true,
        viewMode: "months", 
        minViewMode: "months",
        autoclose:true
    });

    $('#assetType').select2({
            tags:false,
            width: '100%',
            placeholder: 'Choose Asset Type',
            allowClear: true,
            tokenSeparators: [',', ' '],
            ajax:  {
                tags: "true",
                url: '{{route('assettype_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                //dropdownParent: parentElement,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
</script>
<script>
 function showGraphAsset() {

    var urlGraph    = '{{route('data-graph-periodic')}}';
    var urlData     = '{{route('data-excel-periodic')}}';
    var start       = $('input[name="start_period"]').val();
    var end         = $('input[name="end_period"]').val();
    var transaction = $('select[name="transaction"]').val();
    var assettype   = $('select[name="assetType[]"]').val();
    $('#collapseGraph').collapse("show");
    $('#collapseData').collapse("hide");
    $("#assetColumn1").show();
    $("#chartDiv").empty();
    $("#chartDiv").append("<div id='chartAsset' style='height: 300px; width: 100%;'></div> <br>");

    showGraph(urlGraph,start,end,transaction,assettype);
    $("#tableasset").empty();
    $('#collapsedbutton').click();

    showData(urlData,start,end,transaction,assettype);

 }
</script>

<script src="{{ asset('assets/js/plugins/chartjs/Chart.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/chartjs/Chart.bundle.min.js')}}"></script>
<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
@endsection
