@extends('template.master')
@section('beforehead')
<script>

</script>
<style type="text/css">

    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>

<link rel="stylesheet" href="{{asset('assets/css/plugins/chartjs/Chart.min.css')}}">


@endsection
@section('header')
Stock Card Report
@endsection
@section('content')
<div class="row">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-warning alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {!!Session::get('error')!!}
        </div>
    @endif
<div class="col-sm-12">
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-sm-12">
                <div class="col-sm-12">
                <br>
                    <table class="table table-striped table-hover table-sm">
                        <thead class="thead-dark">
                            <tr>
                                <th style="text-align:center;"></th>
                                <th style="text-align:center;"></th>
                                <th style="text-align:center;"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($stock as $sa)
                            <tr>
                                <td width="5%"> #</td>
                                <td colspan="2"><b> Total Stock : {{$sa['material_type']}}  --- {{$sa['total_stock']}} Unit </b>  </td>
                            </tr>
                            @foreach($sa['stock_card'] as $key => $material)
                                @for ($i=0 ; $i < count($material) ; $i++)
                                    <tr>
                                        <td width="5%">##</td>
                                        <td colspan="2"> Material Name : <b> <i>{{$key}}</i> </b></td>
                                    </tr>
                                    <tr>
                                        <td width="5%"></td>
                                        <th style="text-align:left;">Purchase Date</th>
                                        <th style="text-align:left;">Quantity Stock</th>
                                    </tr>
                                    @foreach($material as $b) 
                                    <tr>
                                        <td width="5%"></td>
                                        <td style="text-align:left;">{{ date('d - M - Y',strtotime($b->purchase_date)) }}</td>
                                        <td style="text-align:left;">{{$b->count }} Unit</td>
                                    </tr>
                                    @endforeach 
                                @endfor
                            @endforeach
                            <tr>
                                <td colspan="3" style="text-align:center;"></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>  
</div>
@endsection
@section('afterscript')



@endsection
