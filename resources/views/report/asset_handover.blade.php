@extends('template.master')
@section('beforehead')
<script>

</script>
<script src="{{ asset('assets/js/locals/report/app.js')}}"></script>
<style type="text/css">

    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>

<link rel="stylesheet" href="{{asset('assets/css/plugins/chartjs/Chart.min.css')}}">


@endsection
@section('header')
Report Asset Handover
@endsection
@section('content')
<div class="row">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-warning alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {!!Session::get('error')!!}
        </div>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <br>
            <div id="accordion" class="panel-group accordion" style="margin-bottom:0">
                <div class="panel-body">
                <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="collapsed" id='collapsedbutton' name='collapsedbutton'>
                    <i class="icon mdi mdi-chevron-down"></i> &nbsp; Filter Data</a>
                </h4>
                
                <div id="collapseTwo" class="panel-collapse collapse">
                        <br>
                        <br>
                        <div class="col-sm-12">                                
                            <div class="form-group">
                                <div> 
                                    <label>Asset Type</br></label>
                                </div>
                                <select id="assetType" name="assetType[]" class="form-control" multiple="multiple">
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12"> 
                            <div class="form-group">
                                <div><label>Periode</br></label></div>
                                <div class="input-group">
                                    <input class="form-control" type="text" data-date-format="yyyy-mm" name="start_period" id="start_period" class="form-control date datetimepicker" value='@php echo date("Y-m") @endphp'>
                                    <div class="input-group-addon"> - </div>
                                    <input class="form-control" type="text" data-date-format="yyyy-mm" name="end_period" id="end_period" class="form-control date datetimepicker" value='@php echo date("Y-m",strtotime("+3 Months", strtotime(date("Y-m-d")))) @endphp'>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12"> 
                            <p class="text-right">
                                <button type="submit" class="btn btn-primary btn-lg" id="showGraph" onClick="showDataAssetHandover()"> Show </button>
                            </p>
                        </div>
                        {{--</form>--}}
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <br>
            <h4 class="panel-title">&nbsp; Data Asset Handover</a></h4> 
            <br>
                <div id="columnDiv" style="overflow:scroll; height:400px;"> 
                    <table class="table table-sm fixed_header" >
                        <thead>
                            <tr>
                                <th style='text-align:center;'>User - NIK</th>
                                <th>Material</th>
                                <th>Date</th>
                                <th>Qty</th>
                            </tr>
                        </thead>
                        <tbody id='tableasset' name='tableasset'>
                            <tr>
                                <td colspan = '4' style='text-align:center;'> Click button filter to find your data. </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>User - NIK</th>
                                <th>Material</th>
                                <th>Date</th>
                                <th>Qty</th>
                            </tr>
                        </tfoot>
                    </table> 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('afterscript')

<script>
    $(document).ready(function(){ // on document ready
        $('#collapseTwo').collapse("show");
    })

    $('#start_period').datepicker({
        changeMonth: true,
        changeYear: true,
        todayHighlight:true,
        viewMode: "months", 
        minViewMode: "months",
        autoclose:true
    });

    $('#end_period').datepicker({
        changeMonth: true,
        changeYear: true,
        todayHighlight:true,
        viewMode: "months", 
        minViewMode: "months",
        autoclose:true
    });

    $('#assetType').select2({
            tags:false,
            width: '100%',
            placeholder: 'Choose Asset Type',
            allowClear: true,
            tokenSeparators: [',', ' '],
            ajax:  {
                tags: "true",
                url: '{{route('assettype_ajax')}}',
                dataType: 'json',
                type : 'GET',
                minimumInputLength: 3,
                allowClear: true,
                delay: 250,
                //dropdownParent: parentElement,
                processResults: function (data) {
                    return {
                      results:  $.map(data, function (item) {
                            return {
                                text: item.name,
                                id: item.id
                            }
                        })
                    };
                },
                cache: true
            }
        });
</script>
<script>
 function showDataAssetHandover() {

    var urlData     = "{{route('data-asset-handover')}}";
    var start       = $('input[name="start_period"]').val();
    var end         = $('input[name="end_period"]').val();
    var assettype   = $('select[name="assetType[]"]').val();

    $("#tableasset").empty();
    var rows = "<tr><td colspan = '4' style='text-align:center;'> Searching.... </td></tr>";
    $("#tableasset").append(rows);
    showDataHandover(urlData,start,end,assettype);
 }
</script>
@endsection
