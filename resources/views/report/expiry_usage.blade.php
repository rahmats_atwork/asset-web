@extends('template.master')
@section('beforehead')
<script src="{{ asset('assets/js/locals/report/app.js')}}"></script>
<style type="text/css">

    .table-fit td, 
    .table-fit th {
        white-space: nowrap;
        width: 1%;
        /* max-width:350px; */
    }

</style>

<link rel="stylesheet" href="{{asset('assets/css/plugins/chartjs/Chart.min.css')}}">


@endsection
@section('header')
Expiry Usage Report
@endsection
@section('content')
<div class="row">
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                {!!Session::get('success')!!}
        </div>
    @elseif(Session::has('error'))
        <div class="alert alert-warning alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            {!!Session::get('error')!!}
        </div>
    @endif
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
            <div class="col-sm-12">
                <br>
                <div class="col-sm-6">
                <h3 style="font-size: 18px;">Filter Data</h3>
                <br>
                    <div class="col-sm-10">    
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <input class="form-control" type="text" data-date-format="yyyy-mm-dd" name="from_date" id="from_date" class="form-control date datetimepicker">
                                <div class="input-group-addon"> - </div>
                                <input class="form-control" type="text" data-date-format="yyyy-mm-dd" name="to_date" id="to_date" class="form-control date datetimepicker">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2"  style="margin-left:-30px">
                        <button type="submit" class="btn btn-primary btn-xl" title="filter" onClick="showDataExp()">
                            <i class="mdi mdi-filter-list"></i>
                        </button>
                    </div>
                </div>
                <div class="col-sm-12">
                <h3 style="font-size: 18px;">Data Expiry Usage</h3>
                <table class="table table-sm">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align:center;"> Responsible </th>
                            <th style="text-align:center;"> Date Estimation </th>
                            <th style="text-align:center;"> Material </th>
                            <th style="text-align:center;"> Quantity </th>
                        </tr>
                    </thead>
                    <tbody id='expiryTable' name='expiryTable'>
                        <tr><td colspan = '3' style='text-align:center;'> Click button filter to find your data. </td></tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('afterscript')
<script>
    var today = new Date();
    var curmont = (today.getMonth()+1);
    if (curmont < 10) { curmont = '0' + curmont; }
    var date = today.getFullYear() +'-'+ curmont +'-'+ today.getDate();
    $('#to_date').val(date);
    $('#from_date').val(date);
    $('#from_date').datepicker({
        autoclose:true,
        todayHighlight:true,
        showOn: "button",
        showButtonPanel: true,
        buttonImageOnly: true,
        buttonText: ""
    });
    $("#to_date").datepicker({ 
        autoclose:true,
        todayHighlight:true,
        showOn: "button",
        showButtonPanel: true,
        buttonImageOnly: true,
        buttonText: ""
    });
    
</script>
<script>
    function showDataExp() {
        var urlData     = "{{route('data-expiry-usage')}}";
        var start       = $('input[name="from_date"]').val();
        var end         = $('input[name="to_date"]').val();
        if (start.length === 0 || end.length === 0 ) {
            window.alert("Start / End date value is empty. Please fill filter date !!");
            return false;
        }

        if (Date.parse(start) > Date.parse(end)) {
            window.alert("Start date greater than end date. Fix your date filter !!");
            return false;
        }

        $("#expiryTable").empty();
        var rows = "<tr><td colspan = '4' style='text-align:center;'> Searching.... </td></tr>";
        $("#expiryTable").append(rows);
        showDataExpiry(urlData,start,end);

    }
</script>
@endsection
