<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Masters\ReasonCodes;

class ReasonCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ReasonCodes::create([
            'name'            => 'MASA PAKAI',
            'description'     => 'Asset dikembalikan karena asset sudah melewati masa pakai.',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        ReasonCodes::create([
            'name'            => 'LULUS PROBABITION',
            'description'     => 'Asset dikembalikan karena employee telah lulus masa probabition.',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        ReasonCodes::create([
            'name'            => 'PROMOSI/DEMOSI',
            'description'     => 'Asset dikembalikan karena employee promosi / demosi.',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        ReasonCodes::create([
            'name'            => 'RESIGN',
            'description'     => 'Asset dikembalikan karena employee resign.',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
