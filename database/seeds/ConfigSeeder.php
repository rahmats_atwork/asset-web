<?php

use Illuminate\Database\Seeder;
use App\ConfigBarcode;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ConfigBarcode::firstOrCreate(
            ['key' => 'margin-right'],
            ['value' => 5, 'id' => 9]
        );

        ConfigBarcode::firstOrCreate(
            ['key' => 'font-size'],
            ['value' => 17, 'id' => 10]
        );
    }
}
