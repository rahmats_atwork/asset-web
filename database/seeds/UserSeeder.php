<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            [
                'email'     => 'superadmin@localhost.com',
            ],
            [
                'password'  => bcrypt('admin'),
                'name'      => 'Super Admin',
				'id_role' => 1
            ]
        );
    }
}
