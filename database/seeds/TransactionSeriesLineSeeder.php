<?php

use Illuminate\Database\Seeder;
use App\Models\Masters\TransactionSeriesLine;
use Carbon\Carbon;

class TransactionSeriesLineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionSeriesLine::create([
            'id_transaction_series' => 1,
            'starting_no'     => 'HO1900000000',
            'ending_no'       => 'HO1999999999',
            'starting_date'   => '2019-01-01',
            'ending_date'     => '2019-12-31',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        TransactionSeriesLine::create([
            'id_transaction_series' => 1,
            'starting_no'     => 'HO2000000000',
            'ending_no'       => 'HO2099999999',
            'starting_date'   => '2020-01-01',
            'ending_date'     => '2020-12-31',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);

        TransactionSeriesLine::create([
            'id_transaction_series' => 1,
            'starting_no'     => 'HO2100000000',
            'ending_no'       => 'HO2199999999',
            'starting_date'   => '2021-01-01',
            'ending_date'     => '2021-12-31',
            'created_by'      => 1,
            'updated_by'      => 1,
            self::ATTRIBUTE_UPDATED_AT      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
