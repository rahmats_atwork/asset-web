<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset Permission
        DB::table('permissions')->delete();

        // Seed Permission
        Permission::create([
            'id'          => 1,
            'name'        => 'Access Dashboard',
            'slug'        => 'access.dashboard',
            'description' => 'User can access dashboard'
        ]);

        Permission::create([
            'id'          => 2,
            'name'        => 'Access ACL Menu',
            'slug'        => 'access.acl',
            'description' => 'User can access ACL Menu'
        ]);
        
        Permission::create([
            'id'          => 3,
            'name'        => 'Create ACL Menu',
            'slug'        => 'create.acl',
            'description' => 'User can create new Role/Permission'
        ]);
    }
}
