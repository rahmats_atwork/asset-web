<?php

use Illuminate\Database\Seeder;
use App\Models\Config\Roles;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset Role
        DB::table('roles')->delete();

        // Seed New Role
        Roles::create([
            'id'          => 1,
            'name'        => 'Super Administrator',
            'slug'        => 'superadmin',
            'description' => 'Super Administrator User',
            'special'     => 'all-access'
        ]);

        Roles::create([
            'id'          => 2,
            'name'        => 'Administrator',
            'slug'        => 'admin',
            'description' => 'Administrator User'
        ]);

        Roles::create([
            'id'          => 3,
            'name'        => 'Auditor',
            'slug'        => 'auditor',
            'description' => 'Auditor User'
        ]);

        Roles::create([
            'id'          => 4,
            'name'        => 'General',
            'slug'        => 'general',
            'description' => 'General User'
        ]);
    }
}
