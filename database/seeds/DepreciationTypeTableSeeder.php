<?php

use Illuminate\Database\Seeder;
//use App\Models\Masters\DepreciationType as DTModels;
use Carbon\Carbon;

class DepreciationTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('depreciation_type')->insert([
            'name'       => 'Straight Line Depreciation',
            'created_by' => '1',
            'updated_by' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            self::ATTRIBUTE_UPDATED_AT => Carbon::now()->format('Y-m-d H:i:s'),
            'deleted_at' => null
        ]);

        DB::table('depreciation_type')->insert([
            'name'       => 'Double Declining Balance',
            'created_by' => '1',
            'updated_by' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            self::ATTRIBUTE_UPDATED_AT => Carbon::now()->format('Y-m-d H:i:s'),
            'deleted_at' => null
        ]);

        DB::table('depreciation_type')->insert([
            'name'       => 'Sum of Years Digits',
            'created_by' => '1',
            'updated_by' => '1',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            self::ATTRIBUTE_UPDATED_AT => Carbon::now()->format('Y-m-d H:i:s'),
            'deleted_at' => null
        ]);
    }
}
