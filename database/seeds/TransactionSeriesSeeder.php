<?php

use Illuminate\Database\Seeder;
use App\Models\Masters\TransactionSeries;
use Carbon\Carbon;

class TransactionSeriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionSeries::create([
            'module'          => 'asset-handover',
            'code'            => 'HO',
            'created_by'      => 1,
            'updated_by'      => 1,
            'updated_at'      => Carbon::now()->format('Y-m-d H:i:s'),
            'created_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
