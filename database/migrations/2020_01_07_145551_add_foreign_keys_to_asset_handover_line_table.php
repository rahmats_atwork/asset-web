<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAssetHandoverLineTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asset_handover_line', function(Blueprint $table)
		{
			$table->foreign('id_asset_handover')->references('id')->on('asset_handover_header')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_asset')->references('id_asset')->on('asset')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asset_handover_line', function(Blueprint $table)
		{
			$table->dropForeign('asset_handover_line_id_asset_handover_foreign');
			$table->dropForeign('asset_handover_line_id_asset_foreign');
		});
	}

}
