<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepreciationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('depreciation', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 30);
			$table->smallInteger('useful_life');
			$table->smallInteger('rate')->nullable();
			$table->integer('id_depreciation_type');
			$table->timestamps();
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('depreciation');
	}

}
