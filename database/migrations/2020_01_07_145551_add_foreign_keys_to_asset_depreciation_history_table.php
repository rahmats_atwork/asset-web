<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAssetDepreciationHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asset_depreciation_history', function(Blueprint $table)
		{
			$table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_asset')->references('id_asset')->on('asset')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_depreciation')->references('id')->on('depreciation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asset_depreciation_history', function(Blueprint $table)
		{
			$table->dropForeign('asset_depreciation_history_updated_by_foreign');
			$table->dropForeign('asset_depreciation_history_created_by_foreign');
			$table->dropForeign('asset_depreciation_history_id_asset_foreign');
			$table->dropForeign('asset_depreciation_history_id_depreciation_foreign');
		});
	}

}
