<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssetHandoverHeaderTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asset_handover_header', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->date('date');
			$table->bigInteger('id_responsible_person')->nullable();
			$table->string('description', 50)->nullable();
			$table->integer('status')->nullable();
			$table->integer('created_by');
			$table->integer('updated_by');
			$table->timestamps();
			$table->softDeletes();
			$table->smallInteger('printed')->default(0);
			$table->smallInteger('count_printed')->default(0);
			$table->string('document_no', 30)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asset_handover_header');
	}

}
