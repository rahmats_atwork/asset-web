<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMaterialsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('materials', function(Blueprint $table)
		{
			$table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_material_group')->references('id')->on('material_group')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('id_classification')->references('id')->on('classification')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('materials', function(Blueprint $table)
		{
			$table->dropForeign('materials_updated_by_foreign');
			$table->dropForeign('materials_created_by_foreign');
			$table->dropForeign('materials_id_material_group_foreign');
			$table->dropForeign('materials_id_classification_foreign');
		});
	}

}
