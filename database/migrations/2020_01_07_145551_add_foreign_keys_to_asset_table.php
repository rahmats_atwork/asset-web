<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToAssetTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('asset', function(Blueprint $table)
		{
			$table->foreign('id_depreciation')->references('id')->on('depreciation')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('asset', function(Blueprint $table)
		{
			$table->dropForeign('asset_id_depreciation_foreign');
			$table->dropForeign('asset_updated_by_foreign');
			$table->dropForeign('asset_created_by_foreign');
		});
	}

}
