<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToMaterialImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('material_image', function(Blueprint $table)
		{
			$table->foreign('id_material')->references('id')->on('materials')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('updated_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('created_by')->references('id')->on('users')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('material_image', function(Blueprint $table)
		{
			$table->dropForeign('material_image_id_material_foreign');
			$table->dropForeign('material_image_updated_by_foreign');
			$table->dropForeign('material_image_created_by_foreign');
		});
	}

}
