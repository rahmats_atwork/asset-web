<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAssetLocationHistoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('asset_location_history', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('id_asset')->nullable();
			$table->string('address')->nullable();
			$table->string('latitude')->nullable();
			$table->string('longitude')->nullable();
			$table->string('province')->nullable();
			$table->string('city')->nullable();
			$table->string('building')->nullable();
			$table->string('unit')->nullable();
			$table->timestamps();
			$table->integer('id_location')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('asset_location_history');
	}

}
