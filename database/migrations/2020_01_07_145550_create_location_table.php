<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name')->nullable();
			$table->string('address')->nullable();
			$table->string('latitude', 50)->nullable();
			$table->string('longitude', 50)->nullable();
			$table->string('code')->nullable();
			$table->integer('type')->nullable();
			$table->string('building')->nullable();
			$table->string('unit')->nullable();
			$table->string('contact')->nullable();
			$table->string('phone')->nullable();
			$table->string('email')->nullable();
			$table->timestamps();
			$table->integer('id_plant')->nullable();
			$table->integer('created_by')->nullable();
			$table->integer('updated_by')->nullable();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location');
	}

}
