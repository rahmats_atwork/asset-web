<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Transaction\Asset;
use App\Models\Masters\LocationType;
use App\Models\Masters\Plant;
use App\Models\Config\Users;

class Location extends BaseModel
{
    /* Local Const */

    const ATTRIBUTE_BUILDING_ADDRESS    = 'building';
    const ATTRIBUTE_BUILDING_UNIT       = 'unit';
    const ATTRIBUTE_CONTACT_PERSON      = 'contact';
    const ATTRIBUTE_PHONE_NO            = 'phone';
    
    /* Table - Primary Key */
    protected $table      = self::TABLE_LOCATION;
    protected $primarykey = self::ATTRIBUTE_ID;

    protected $fillable = [
        self::ATTRIBUTE_ADDRESS,
        self::ATTRIBUTE_BUILDING_ADDRESS,
        self::ATTRIBUTE_BUILDING_UNIT,
        self::ATTRIBUTE_CODE,
        self::ATTRIBUTE_CONTACT_PERSON,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_EMAIL,
        self::ATTRIBUTE_LATITUDE,
        self::ATTRIBUTE_LONGITUDE,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_PHONE_NO,
        self::ATTRIBUTE_PLANT_ID,
        self::ATTRIBUTE_TYPE,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,        
    ];
    
    public function plantAsset() {
        return $this->hasMany(Asset::class, self::ATTRIBUTE_LOCATION_ID ,self::ATTRIBUTE_ID);
    }

    public function locationType() {
        return $this->belongsTo(LocationType::class, self::ATTRIBUTE_TYPE);
    }

    public function plantLocation() {
        return $this->belongsTo(Plant::class, self::ATTRIBUTE_PLANT_ID, self::ATTRIBUTE_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
