<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;

class MaterialGroup extends BaseModel
{
    protected   $table        = self::TABLE_MATERIAL_GROUP;
    public      $primaryKey   = self::ATTRIBUTE_ID;
    protected   $fillable     = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_CODE,
        self::ATTRIBUTE_NAME, 
        self::ATTRIBUTE_DATA_DESCRIPTION,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_DELETED_AT,
    ];

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
    
}
