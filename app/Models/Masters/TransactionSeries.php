<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;
use App\Models\Masters\TransactionSeriesLine;

class TransactionSeries extends BaseModel
{
    /* Local Const */
    const ATTRIBUTE_LAST_NO_USED   = 'last_no_used';
    const ATTRIBUTE_MODULE_CODE    = 'module';

    /* Table - Primary key */
    protected $table      = self::TABLE_TRANSACTION_SERIES;
    protected $primaryKey = self::ATTRIBUTE_ID;
    
    /* Starting Defining table */
    protected $fillable = [
        self::ATTRIBUTE_CODE, 
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_LAST_NO_USED,
        self::ATTRIBUTE_MODULE_CODE,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,        
    ];

    /* Foreign Key Table */
    public function userCreated()
    {
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_UPDATED, self::ATTRIBUTE_ID);
    }

    public function seriesLine()
    {
        return $this->hasMany(TransactionSeriesLine::class, self::ATTRIBUTE_SERIES_TRANSACTION_ID);
    }

}
