<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;

class MaterialImage extends BaseModel
{
    const       ATTRIBUTE_IMAGE = 'image_material';

    protected   $table      = self::TABLE_MATERIAL_IMAGE;
    public      $primaryKey = self::ATTRIBUTE_ID;
    protected   $fillable   = [
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_MATERIAL_ID,
        self::ATTRIBUTE_IMAGE,
    ];

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }

    public function material()
    {
        return $this->belongsTo(Material::class, seLf::ATTRIBUTE_MATERIAL_ID, self::ATTRIBUTE_ID);
    }
    
}