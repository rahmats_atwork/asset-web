<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;

class Plant extends BaseModel
{
    protected $table        = self::TABLE_PLANT;
    protected $fillable     = [
            self::ATTRIBUTE_ID,
            self::ATTRIBUTE_NAME,
            self::ATTRIBUTE_DATA_DESCRIPTION,
            self::ATTRIBUTE_CREATED_AT,
            self::ATTRIBUTE_DELETED_AT,
            self::ATTRIBUTE_UPDATED_AT,
            self::ATTRIBUTE_USER_CREATED,
            self::ATTRIBUTE_USER_UPDATED,
            self::ATTRIBUTE_LONGITUDE,
            self::ATTRIBUTE_LATITUDE
    ];

    public function plantAsset()
    {
        return $this->hasMany(Asset::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_PLANT_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(User::class,self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(User::class,self::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
