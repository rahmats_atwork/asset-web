<?php

namespace App\Models\Masters;


use Illuminate\Database\Eloquent\Model;
use App\Models\Masters\materialGroup;
use App\Models\Masters\classification;
use App\Models\BaseModel;
use App\Models\Config\Users;
use App\Models\Transaction\Asset;

class Material extends BaseModel
{
    protected   $table      = self::TABLE_MATERIAL;
    public      $primaryKey = self::ATTRIBUTE_ID;
    protected   $fillable   = [
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_CLASSIFICATION_ID,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_DATA_DESCRIPTION,
        self::ATTRIBUTE_MATERIAL_GROUP_ID,
        self::ATTRIBUTE_UPDATED_AT, 
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_DELETED_AT,
    ];

    function classification(){
        return $this->hasOne(classification::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_CLASSIFICATION_ID);
    }

    function materialGroup(){
        return $this->hasOne(materialGroup::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_MATERIAL_GROUP_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
    
    public function asset() {
        return $this->hasMany(Asset::class, self::ATTRIBUTE_MATERIAL_ID, self::ATTRIBUTE_ID);    
    }
}