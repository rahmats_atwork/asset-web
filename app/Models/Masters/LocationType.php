<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masters\Location;
use App\Models\BaseModel;
use App\Models\Config\Users;

class LocationType extends BaseModel
{
    /* Local Const */
    const   ATTRIBUTE_ICON_IMAGE        = 'icon';
    const   ATTRIBUTE_ZOOM_LEVEL_END    = 'zoom_level_end';
    const   ATTRIBUTE_ZOOM_LEVEL_START  = 'zoom_level';
    
    /* Table & Primary Key */
    protected   $table     = self::TABLE_LOCATION_TYPE;
    protected   $fillable  = [
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_ICON_IMAGE,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_ZOOM_LEVEL_END,
        self::ATTRIBUTE_ZOOM_LEVEL_START,
    ];
    
    public function location()
    {
        return $this->hasOne(Location::class, self::ATTRIBUTE_TYPE, self::ATTRIBUTE_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}