<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;

class ClassificationParameter extends BaseModel
{

    /* Local Const */
    const ATTRIBUTE_LENGTH   = 'length';
    const ATTRIBUTE_DECIMAL  = 'decimal';
    const ATTRIBUTE_VALUE    = 'value';
    const ATTRIBUTE_READING  = 'reading';


    protected   $table       = self::TABLE_CLASSIFICATION_PARAMETER;
    public      $primaryKey  = self::ATTRIBUTE_ID;

    protected $fillable = [
        self::ATTRIBUTE_CLASSIFICATION_ID,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_TYPE,
        self::ATTRIBUTE_LENGTH,
        self::ATTRIBUTE_DECIMAL,
        self::ATTRIBUTE_VALUE,
        self::ATTRIBUTE_READING,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_DELETED_AT
    ];

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }

    public function classificationParameter()
    {
        return $this->belongsTo(self::class,seLf:: ATTRIBUTE_CLASSIFICATION_ID, self::ATTRIBUTE_ID);
    }
}