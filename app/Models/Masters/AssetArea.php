<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;


class AssetArea extends BaseModel
{

    /* Local Const */
    const ATTRIBUTE_LARGE_AREA   = 'large';
    const ATTRIBUTE_AREA         = 'area';

    protected   $table      = self::TABLE_AREA;  
    public      $primaryKey = self::ATTRIBUTE_ID;
      
    protected   $fillable   = [
        self::ATTRIBUTE_AREA,
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_LARGE_AREA,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,  
           
    ];

    public function createdBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_CREATED);
    }

    public function updatedBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_UPDATED);
    }
}