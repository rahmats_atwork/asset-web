<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;
use App\Models\Masters\DepreciationType;

class Depreciation extends BaseModel
{

    const ATTRIBUTE_USEFUL_LIFE         = 'useful_life';
    const ATTRIBUTE_DEPRECIATION_RATE   = 'rate';
    
    protected $table        = self::TABLE_DEPRECIATION;
    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $fillable     = [
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_USEFUL_LIFE,
        self::ATTRIBUTE_DEPRECIATION_RATE,
        self::ATTRIBUTE_DEPRECIATION_TYPE_ID,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_DELETED_AT
    ];

    public function depreciationType() {
        return $this->belongsTo(DepreciationType::class, self::ATTRIBUTE_DEPRECIATION_TYPE_ID, self::ATTRIBUTE_ID);
    }
    
    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
