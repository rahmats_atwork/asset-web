<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;

class ReasonCodes extends BaseModel
{
        protected   $primaryKey   = self::ATTRIBUTE_ID ;
        protected   $table        = self::TABLE_REASON_CODE;
        protected   $fillable     = [
                self::ATTRIBUTE_NAME,
                self::ATTRIBUTE_DATA_DESCRIPTION,
                self::ATTRIBUTE_DELETED_AT,
                self::ATTRIBUTE_USER_CREATED,
                self::ATTRIBUTE_CREATED_AT,
                self::ATTRIBUTE_USER_UPDATED,
                self::ATTRIBUTE_UPDATED_AT
        ];
    
        public function userCreated()
        {
            return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED, self::ATTRIBUTE_ID);
        }
    
        public function userUpdated()
        {
            return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_UPDATED ,self::ATTRIBUTE_ID);
        }
}
