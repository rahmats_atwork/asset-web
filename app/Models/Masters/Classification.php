<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Masters\ClassificationParameter;
use App\Models\Config\Users;

class Classification extends BaseModel
{
    protected $table        = self::TABLE_CLASSIFICATION;
    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $fillable     = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,        
    ];

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}