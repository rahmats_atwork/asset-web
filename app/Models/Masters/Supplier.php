<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;
use App\Models\Transaction\Asset;


class Supplier extends BaseModel
{
    /* Local Const */
    const ATTRIBUTE_SUPPLIER_ADDRESS    = 'address_location';
    const ATTRIBUTE_BUILDING_ADDRESS    = 'building';
    const ATTRIBUTE_BUILDING_UNIT       = 'unit';
    const ATTRIBUTE_CITY                = 'city';
    const ATTRIBUTE_CONTACT_PERSON      = 'contact_person';
    const ATTRIBUTE_PHONE_NO            = 'phone';
    const ATTRIBUTE_PROVINCE            = 'province';
    const ATTRIBUTE_COMPANY_WEBSITE     = 'website';

    protected $table        = self::TABLE_SUPPLIER;
    protected $primaryKey   = self::ATTRIBUTE_ID;

    protected $fillable     = [
        self::ATTRIBUTE_BUILDING_ADDRESS,
        self::ATTRIBUTE_BUILDING_UNIT,
        self::ATTRIBUTE_CITY,
        self::ATTRIBUTE_COMPANY_WEBSITE,
        self::ATTRIBUTE_CONTACT_PERSON,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_EMAIL,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_LATITUDE, 
        self::ATTRIBUTE_LONGITUDE,
        self::ATTRIBUTE_NAME, 
        self::ATTRIBUTE_PHONE_NO,
        self::ATTRIBUTE_PROVINCE,
        self::ATTRIBUTE_SUPPLIER_ADDRESS,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
    ];
    
    public function assets() {
        return $this->hasMany(Asset::class, self::ATTRIBUTE_SUPPLIER_ID, self::ATTRIBUTE_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
