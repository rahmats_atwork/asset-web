<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;

class AssetType extends BaseModel
{
    protected   $table      = self::TABLE_ASSET_TYPE;
    public      $primaryKey = self::ATTRIBUTE_ID;

    protected $fillable = [
        self::ATTRIBUTE_CODE,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
    ];

    public function createdBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_CREATED);
    }

    public function updatedBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_UPDATED);
    }
}