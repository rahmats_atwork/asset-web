<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;

class DepreciationType extends BaseModel
{

    protected $table        = self::TABLE_DEPRECIATION_TYPE;
    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $fillable     = [
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_DELETED_AT,
    ];


    public function userCreated()
    {
        return $this->belongsTo(User::class,self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(User::class,self::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
