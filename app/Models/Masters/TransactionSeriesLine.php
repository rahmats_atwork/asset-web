<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Masters\TransactionSeries;
use App\Models\Config\Users;

class TransactionSeriesLine extends BaseModel
{
    /* Local Const */
    const   ATTRIBUTE_START_NO      = 'starting_no';
    const   ATTRIBUTE_END_NO        = 'ending_no';
    const   ATTRIBUTE_LAST_NO_USED  = 'last_no_used';

    /* Table without primary key */
    protected   $primaryKey   = null;
    public      $incrementing = false;

    /* Starting Defining table */
    protected   $table        = self::TABLE_TRANSACTION_SERIES_LINE;
    protected   $fillable     = [
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_END_NO,
        self::ATTRIBUTE_ENDING_DATE,
        self::ATTRIBUTE_LAST_NO_USED,
        self::ATTRIBUTE_SERIES_TRANSACTION_ID,
        self::ATTRIBUTE_START_NO,
        self::ATTRIBUTE_STARTING_DATE,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,        
    ];

    public function seriesHeader()
    {
        return $this->belongsTo(TransactionSeries::class,seLf::ATTRIBUTE_SERIES_TRANSACTION_ID,self::ATTRIBUTE_ID);
    }

    public function userCreated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated()
    {
        return $this->belongsTo(Users::class,seLf::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }
}
