<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;

class AssetStatus extends BaseModel
{
    protected $table    = self::TABLE_ASSET_STATUS;
    public $primaryKey  = self::ATTRIBUTE_ID;
    protected $fillable = [
        self::ATTRIBUTE_CODE,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
    ];

    public function createdBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_CREATED);
    }

    public function updatedBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_UPDATED);
    }
}