<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;
use App\Models\Transaction\Asset;
use App\Models\Masters\location;

class AssetLocationHistory extends BaseModel
{
    /* Local Const */
    const ATTRIBUTE_BUILDING_NAME       = 'building';
    const ATTRIBUTE_BUILDING_UNIT       = 'unit';
    const ATTRIBUTE_CITY                = 'city';
    const ATTRIBUTE_PROVINCE            = 'province';

	protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $table        = self::TABLE_ASSET_LOCATION_HISTORY;
    protected $fillable     = [
        self::ATTRIBUTE_ADDRESS,
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_BUILDING_NAME,
        self::ATTRIBUTE_BUILDING_UNIT,
        self::ATTRIBUTE_CITY,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_LATITUDE,
        self::ATTRIBUTE_LOCATION_ID,
        self::ATTRIBUTE_LONGITUDE,
        self::ATTRIBUTE_PROVINCE,
        self::ATTRIBUTE_UPDATED_AT, 
        self::ATTRIBUTE_USER_CREATED,        
    ];

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED ,self::ATTRIBUTE_ID);
    }

    public function location(){
        return $this->belongsTo(location::class, self::ATTRIBUTE_LOCATION_ID ,self::ATTRIBUTE_ID);
    }

    public function asset(){
        return $this->belongsTo(Asset::class, self::ATTRIBUTE_ASSET_ID,self::ATTRIBUTE_ASSET_ID);
    }
}
