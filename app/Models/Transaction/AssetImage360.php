<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;

class AssetImage360 extends BaseModel
{
    const ATTRIBUTE_IMAGE_360   = 'image360_asset';

    protected   $table      = self::TABLE_ASSET_IMAGE_360;
    public      $primaryKey = self::ATTRIBUTE_ID;
    
    protected   $fillable   = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_IMAGE_360,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_DELETED_AT
    ];

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }

    public function asset(){
        return $this->hasOne(Asset::class, self::ATTRIBUTE_ASSET_ID, self::ATTRIBUTE_ASSET_ID);
    }
}