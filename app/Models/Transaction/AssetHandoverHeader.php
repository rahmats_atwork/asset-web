<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masters\Material;
use App\Models\Config\Users;
use App\Models\Masters\Plant;
use App\Models\Masters\Supplier;
use App\Models\Masters\AssetStatus;
use App\Models\Masters\AssetType;
use App\Models\Masters\Location;
use App\Models\Config\Roles;
use App\Models\Masters\RetiredReason;
use App\Models\Masters\Depreciation;
use App\Models\Transaction\AssetHandoverLine;
use App\Models\BaseModel;


class AssetHandoverHeader extends BaseModel
{
    
    const   ATTRIBUTE_DOCUMENT_NO = 'document_no';

    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $table        = self::TABLE_ASSET_HANDOVER_HEADER;

    protected $fillable     = [
        self::ATTRIBUTE_DOCUMENT_NO,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_DATA_DESCRIPTION,
        self::ATTRIBUTE_RESPONSIBLE_ID ,
        self::ATTRIBUTE_PRINTED_COUNT,
        self::ATTRIBUTE_PRINTED_STATUS,
        self::ATTRIBUTE_STATUS,
        self::ATTRIBUTE_TRANSACTION_DATE,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
    ];

    public function responsible(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_RESPONSIBLE_ID ,self::ATTRIBUTE_ID);
    }

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated(){
        return $this->belongsTo(Users::class,self::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }

    public function handoverLine(){
        return $this->hasMany(AssetHandoverLine::class, self::ATTRIBUTE_HANDOVER_H_ID, self::ATTRIBUTE_ID);
    }   
   
}
