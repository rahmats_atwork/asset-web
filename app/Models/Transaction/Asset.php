<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masters\Material;
use App\Models\Config\Users;
use App\Models\Masters\Plant;
use App\Models\Masters\Supplier;
use App\Models\Masters\AssetStatus;
use App\Models\Masters\AssetType;
use App\Models\Masters\Location;
use App\Models\Config\Roles;
use App\Models\Masters\RetiredReason;
use App\Models\Masters\Depreciation;
use App\Models\Transaction\AssetResponsiblePersonHistory;
use App\Models\BaseModel;

class Asset extends BaseModel
{
    protected $primaryKey   = self::ATTRIBUTE_ASSET_ID;
    protected $table        = self::TABLE_ASSET;

    protected $fillable     = [
            self::ATTRIBUTE_ASSET_ID,
            'code_asset',
            'count_duration',
            'count_schedule',
            self::ATTRIBUTE_CREATED_AT,
            self::ATTRIBUTE_USER_CREATED,
            'curency',
            'cycle_schedule',
            self::ATTRIBUTE_DELETED_AT,
            'description_asset',
            self::ATTRIBUTE_DEPRECIATION_ID,
            self::ATTRIBUTE_LOCATION_ID ,
            self::ATTRIBUTE_MATERIAL_ID,
            'id_owner',
            self::ATTRIBUTE_PLANT_ID,
            self::ATTRIBUTE_RESPONSIBLE_ID,
            'id_superior_asset',
            self::ATTRIBUTE_SUPPLIER_ID,
            self::ATTRIBUTE_ASSET_TYPE_ID,
            'is_asset',
            'last_maintenance',
            'level_asset',
            'next_maintenance',
            'old_asset',
            'purchase_cost',
            'purchase_date',
            'retired_date',
            'retired_ramarks',
            'retired_reason',
            'serial_number',
            self::ATTRIBUTE_STATUS,
            'unit',
            'unit_duration',
            'unit_schedule',
            self::ATTRIBUTE_USER_UPDATED,
            self::ATTRIBUTE_UPDATED_AT,
            'valuation_group',
            'waranty_finish',
            'waranty_start',
    ];

    /*
    protected $appends = [
        'nik'
    ];
    */
    
    public function material()
    {
        return $this->belongsTo(Material::class, self::ATTRIBUTE_MATERIAL_ID, self::ATTRIBUTE_ID);        
    }

    public function retiredReason()
    {
        return $this->belongsTo(RetiredReason::class, 'retired_reason', self::ATTRIBUTE_ID);        
    }

    public function depreciation()
    {
        return $this->belongsTo(Depreciation::class, self::ATTRIBUTE_DEPRECIATION_ID, self::ATTRIBUTE_ID);        
    }

    public function supplier()
    {
        return $this->hasOne(supplier::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_SUPPLIER_ID );        
    }

    public function asset_status()
    {
        return $this->hasOne(AssetStatus::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_STATUS);        
    }

    public function childrens(){
        return $this->hasMany(self::class,'id_superior_asset');
    }

    public function parent_asset(){
        return $this->hasOne(self::class, self::ATTRIBUTE_ASSET_ID, 'id_superior_asset');
    }

    public function old_asset(){
        return $this->belongsTo(self::class, self::ATTRIBUTE_ASSET_ID, 'old_asset');
    }

    public function type(){
        return $this->belongsTo(AssetType::class,'id_type_asset',self::ATTRIBUTE_ID);
    }

    public function valuation(){
        return $this->belongsTo(Roles::class,'valuation_group',self::ATTRIBUTE_ID);
    }

    public function location(){
        return $this->belongsTo(location::class, self::ATTRIBUTE_LOCATION_ID ,self::ATTRIBUTE_ID);
    }

    public function plant_asset(){
        return $this->belongsTo(Plant::class, self::ATTRIBUTE_PLANT_ID, self::ATTRIBUTE_ID);
    }

    public function responsible_person_asset(){

        return $this->belongsTo(Users::class, self::ATTRIBUTE_RESPONSIBLE_ID,self::ATTRIBUTE_ID);

    }

    public function userCreated() {

        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED, self::ATTRIBUTE_ID);

    }

    public function userUpdated() {

        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_UPDATED, self::ATTRIBUTE_ID);

    }

    public function historyPersonal() {

        return $this->hasMany(AssetResponsiblePersonHistory::class, self::ATTRIBUTE_ASSET_ID, self::ATTRIBUTE_ASSET_ID);

    }

    public function getNikAttribute()
    {
        if ($this->responsible_person != null) {
            $user = Users::where('name', $this->responsible_person)->first();
            if ($user) {
                return $user->nik;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

}
