<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;

class AssetClassification extends BaseModel
{
    const ATTRIBUTE_VALUE_CLASSIFICATION = 'value_classification';
    
    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $table        = self::TABLE_ASSET_CLASSIFICATION;
    protected $fillable = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_PARAMETER_CLASSIFICATION_ID,
        self::ATTRIBUTE_VALUE_CLASSIFICATION, 
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_DELETED_AT,
    ];

}
