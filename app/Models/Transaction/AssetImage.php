<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\BaseModel;
use App\Models\Transaction\Asset;

class AssetImage extends BaseModel
{
    const ATTRIBUTE_ASSET_CODE      = 'code_asset';
    const ATTRIBUTE_ASSET_IMAGE     = 'image_asset';
    const ATTRIBUTE_ASSET_IMAGE_ID  = 'id_image_asset';

    protected $table                = 'asset_image';
    public $primaryKey              = self::ATTRIBUTE_ASSET_IMAGE_ID;

    protected $fillable             = [
        self::ATTRIBUTE_ASSET_CODE,
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_ASSET_IMAGE,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_USER_UPDATED,
    ];

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED,self::ATTRIBUTE_ID);
    }

    public function userUpdated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_UPDATED,self::ATTRIBUTE_ID);
    }

    public function asset(){
        return $this->hasOne(Asset::class, self::ATTRIBUTE_ASSET_ID, self::ATTRIBUTE_ASSET_ID);
    }

}