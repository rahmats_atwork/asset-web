<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;
use App\Models\Config\Users;
use App\Models\Transaction\Asset;

class AssetResponsiblePersonHistory extends BaseModel
{
	protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $table        = self::TABLE_ASSET_RESPONSIBLE_HISTORY;
    
    protected $fillable = [
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_RESPONSIBLE_ID,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT
    ];

    public function assetAttr()
    {
        return $this->belongsTo(Asset::class, self::ATTRIBUTE_ASSET_ID, self::ATTRIBUTE_ASSET_ID);
    }

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED , self::ATTRIBUTE_ID);
    }

    public function user(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_RESPONSIBLE_ID, self::ATTRIBUTE_ID);
    }
}
