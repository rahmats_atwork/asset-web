<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Masters\Material;
use App\Models\Transaction\Asset;
use App\Models\Transaction\AssetHandoverHeader;
use App\Models\Config\Users;
use App\Models\BaseModel;

class AssetHandoverLine extends BaseModel
{
    
    protected $primaryKey   = self::ATTRIBUTE_ID;
    protected $table        = self::TABLE_ASSET_HANDOVER_LINE;
    protected $fillable     = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_DELETED_AT,
        self::ATTRIBUTE_HANDOVER_H_ID,
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_UPDATED_AT,
    ];

    public function handoverHeader(){
        return $this->belongsTo(
            AssetHandoverHeader::class, 
            self::ATTRIBUTE_HANDOVER_H_ID, 
            self::ATTRIBUTE_ID
        );
    }

    public function asset(){
        return $this->belongsTo(
            Asset::class, 
            self::ATTRIBUTE_ASSET_ID, 
            self::ATTRIBUTE_ASSET_ID
        );
    }
}
