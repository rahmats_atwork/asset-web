<?php

namespace App\Models\Transaction;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Users;
use App\Models\Masters\Asset;
use App\Models\Masters\Depreciation;
use App\Models\BaseModel;

class AssetDepreciationHistory extends BaseModel
{
    const ATTRIBUTE_DEPRECIATION_PERIODE    = 'depreciation_periode';
    const ATTRIBUTE_DEPRECIATION_AMOUNT     = 'depreciation_amount';
    const ATTRIBUTE_SEQUENCE                = 'sequence';
    const ATTRIBUTE_SALVAGE_VALUE           = 'salvage_value';
    const ATTRIBUTE_BLOCK                   = 'is_active';

    public      $incrementing   = false;
    protected   $primaryKey     = NULL;
    protected   $table          = self::TABLE_ASSET_DEPRECIATION_HISTORY;
    
    protected   $fillable       = [
        self::ATTRIBUTE_ASSET_ID,
        self::ATTRIBUTE_DEPRECIATION_ID,
        self::ATTRIBUTE_SEQUENCE,
        self::ATTRIBUTE_DEPRECIATION_AMOUNT,
        self::ATTRIBUTE_DEPRECIATION_PERIODE,
        self::ATTRIBUTE_SALVAGE_VALUE,
        self::ATTRIBUTE_BLOCK,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_DELETED_AT,
    ];

    public function assetAttr()
    {
        return $this->belongsTo(Asset::class, self::ATTRIBUTE_ASSET_ID,self::ATTRIBUTE_ASSET_ID);
    }

    public function depreciation()
    {
        return $this->belongsTo(Depreciation::class, self::ATTRIBUTE_DEPRECIATION_ID,self::ATTRIBUTE_ID);
    }

    public function userCreated(){
        return $this->belongsTo(Users::class, self::ATTRIBUTE_USER_CREATED ,self::ATTRIBUTE_ID);
    }
}
