<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\BaseModel;
use App\Models\Config\Roles;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    
    public function createdBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_CREATED);
    }

    public function updatedBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID,self::ATTRIBUTE_USER_UPDATED);
    }

    public function role()
    {
        return $this->hasOne(Roles::class,self::ATTRIBUTE_ID,'id_role');
    }

    public function assets()
    {
        return $this->hasOne(Asset::class,self::ATTRIBUTE_ID,'id_responsible_person');
    }
}
