<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class ConfigBarcode extends BaseModel
{

    /* Local Const */
    const ATTRIBUTE_KEY_PARAMETER    = 'key';
    const ATTRIBUTE_VALUE_PARAMETER  = 'value';
    
    protected $table    = self::TABLE_CONFIG_BARCODE;

    protected $fillable = [
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_KEY_PARAMETER, 
        self::ATTRIBUTE_VALUE_PARAMETER, 
    ];
}