<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use App\Models\BaseModel;

class Roles extends BaseModel
{
    /* Local Const */
    const ATTRIBUTE_SLUG          = 'slug';
    const ATTRIBUTE_SPECIAL       = 'special';

    protected $table    = self::TABLE_ROLE;
    public $primaryKey  = self::ATTRIBUTE_ID;
    protected $fillable = [ 
        self::ATTRIBUTE_DATA_DESCRIPTION,
        self::ATTRIBUTE_ID,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_SLUG,
        self::ATTRIBUTE_SPECIAL
    ];
	

}
