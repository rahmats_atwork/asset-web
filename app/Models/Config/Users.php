<?php

namespace App\Models\Config;

use Illuminate\Database\Eloquent\Model;
use App\Models\Config\Roles;
use App\Models\BaseModel;

class Users extends BaseModel
{
    /* Local Const */
    const ATTRIBUTE_ACTIVE          = 'isactive';
    const ATTRIBUTE_NIK             = 'nik';
    const ATTRIBUTE_PASSWORD        = 'password';
    const ATTRIBUTE_PHONE_NO        = 'mobile';
    const ATTRIBUTE_PHOTO_IMAGE     = 'image';
    const ATTRIBUTE_REMEMBER_TOKEN  = 'remember_token';


    protected $fillable = [
        self::ATTRIBUTE_ID ,
        self::ATTRIBUTE_NAME,
        self::ATTRIBUTE_PASSWORD,
        self::ATTRIBUTE_EMAIL,
        self::ATTRIBUTE_CREATED_AT,
        self::ATTRIBUTE_UPDATED_AT,
        self::ATTRIBUTE_REMEMBER_TOKEN,
        self::ATTRIBUTE_PHOTO_IMAGE,
        self::ATTRIBUTE_ACTIVE,
        self::ATTRIBUTE_USER_CREATED,
        self::ATTRIBUTE_USER_UPDATED,
        self::ATTRIBUTE_ROLE_ID,
        self::ATTRIBUTE_PHONE_NO,
        self::ATTRIBUTE_PLANT_ID,
        self::ATTRIBUTE_NIK
    ];
    protected $hidden = [
        self::ATTRIBUTE_PASSWORD, 
        self::ATTRIBUTE_REMEMBER_TOKEN,
    ];

    public function createdBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID ,self::ATTRIBUTE_USER_CREATED);
    }

    public function updatedBy()
    {
        return $this->hasOne(Users::class,self::ATTRIBUTE_ID ,self::ATTRIBUTE_USER_UPDATED);
    }

    public function role()
    {
        return $this->hasOne(Roles::class, self::ATTRIBUTE_ID, self::ATTRIBUTE_ROLE_ID);
    }

    public function assets()
    {
        return $this->hasOne(Asset::class,self::ATTRIBUTE_ID , self::ATTRIBUTE_ID_RESPONSIBLE);
    }
}
