<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    const TABLE_ASSET_HANDOVER_LINE         = 'asset_handover_line';
    const TABLE_ASSET_HANDOVER_HEADER       = 'asset_handover_header';
    const TABLE_ASSET                       = 'asset';
    const TABLE_ASSET_TYPE                  = 'asset_type';
    const TABLE_AREA                        = 'asset_area';
    const TABLE_REASON_CODE                 = 'reason_codes';
    const TABLE_ROLE                        = 'roles';
    const TABLE_MATERIAL_GROUP              = 'material_group';
    const TABLE_MATERIAL_PARAMETER          = 'material_parameter';
    const TABLE_MATERIAL_IMAGE              = 'material_image';
    const TABLE_DEPRECIATION                = 'depreciation';
    const TABLE_PLANT                       = 'plant';
    const TABLE_TRANSACTION_SERIES          = 'transaction_series';
    const TABLE_TRANSACTION_SERIES_LINE     = 'transaction_series_line';
    const TABLE_ASSET_DEPRECIATION_HISTORY  = 'asset_depreciation_history';
    const TABLE_ASSET_RESPONSIBLE_HISTORY   = 'asset_responsible_person_history';
    const TABLE_ASSET_LOCATION_HISTORY      = 'asset_location_history';
    const TABLE_ASSET_CLASSIFICATION        = 'asset_classification';
    const TABLE_ASSET_IMAGE_360             = 'asset_image360';
    const TABLE_ASSET_STATUS                = 'asset_status';
    const TABLE_SUPPLIER                    = 'master_supplier';
    const TABLE_VALUATION_GROUP             = 'valuation_group';
    const TABLE_CLASSIFICATION              = 'classification';
    const TABLE_CLASSIFICATION_PARAMETER    = 'classification_parameter';
    const TABLE_RETIRED_REASON              = 'retired_reason';
    const TABLE_MATERIAL                    = 'materials';
    const TABLE_LOCATION_TYPE               = 'location_type';
    const TABLE_LOCATION                    = 'location';
    const TABLE_DEPRECIATION_TYPE           = 'depreciation_type';
    const TABLE_CONFIG_BARCODE              = 'config_barcode';
   

    /*
    ** Global Variable
    **
    */

    const ATTRIBUTE_ID                  = 'id';
    const ATTRIBUTE_CODE                = 'code';
    const ATTRIBUTE_NAME                = 'name';
    const ATTRIBUTE_TYPE                = 'type';
    const ATTRIBUTE_ADDRESS             = 'address';
    const ATTRIBUTE_CREATED_AT          = 'created_at';
    const ATTRIBUTE_DELETED_AT          = 'deleted_at';
    const ATTRIBUTE_UPDATED_AT          = 'updated_at';
    const ATTRIBUTE_USER_CREATED        = 'created_by';
    const ATTRIBUTE_USER_UPDATED        = 'updated_by';
    const ATTRIBUTE_STATUS              = 'status';
    const ATTRIBUTE_DATA_DESCRIPTION    = 'description';
    const ATTRIBUTE_EMAIL               = 'email';
    const ATTRIBUTE_STARTING_DATE       = 'starting_date';
    const ATTRIBUTE_ENDING_DATE         = 'ending_date';
    const ATTRIBUTE_LONGITUDE           = 'longitude';
    const ATTRIBUTE_LATITUDE            = 'latitude';

    /*
    ** Asset Transaction Variable
    */

    const ATTRIBUTE_ASSET_ID                                = 'id_asset';
    const ATTRIBUTE_ASSET_TYPE_ID                           = 'id_type_asset';
    const ATTRIBUTE_RESPONSIBLE_ID                          = 'id_responsible_person';
    const ATTRIBUTE_CLASSIFICATION_ID                       = 'id_classification';
    const ATTRIBUTE_MATERIAL_ID                             = 'id_material';
    const ATTRIBUTE_SUPPLIER_ID                             = 'id_supplier';
    const ATTRIBUTE_MATERIAL_GROUP_ID                       = 'id_material_group';
    const ATTRIBUTE_LOCATION_ID                             = 'id_location';
    const ATTRIBUTE_PLANT_ID                                = 'id_plant';
    const ATTRIBUTE_DEPRECIATION_ID                         = 'id_depreciation';
    const ATTRIBUTE_DEPRECIATION_TYPE_ID                    = 'id_depreciation_type';
    const ATTRIBUTE_SERIES_TRANSACTION_ID                   = 'id_transaction_series';
    const ATTRIBUTE_HANDOVER_H_ID                           = 'id_asset_handover';
    const ATTRIBUTE_PARAMETER_CLASSIFICATION_ID             = 'id_parameter';
    const ATTRIBUTE_ROLE_ID                                 = 'id_role';


    /*
    ** Transaction Series
    */

    

    /*
    ** Asset Handover Variable
    */
    const ATTRIBUTE_PRINTED_STATUS      = 'printed';
    const ATTRIBUTE_PRINTED_COUNT       = 'count_printed';
    
    const ATTRIBUTE_TRANSACTION_DATE    = 'date';
}
