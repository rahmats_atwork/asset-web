<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\Masters\Location;
use App\Models\Masters\LocationType;
use App\Models\Masters\Supplier;
use App\Models\Masters\Plant;
use Illuminate\Http\Request;
use Session;
use Validator;
use PDF;
use Auth;
use DB;
use Carbon\Carbon;

class LocationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $list = Location::select('plant.id as plant_id',
                                      'plant.name as plant_name',
                                      'plant.description as plant_description',
                                      'location_type.name as loctype_name',
                                      'location.id',
                                      'location.name',
                                      'location.address',
                                      'location.building',
                                      'location.unit',
                                      'location.contact',
                                      'location.phone',
                                      'location.email',
                                      'location.type')
            ->leftjoin('plant', 'plant.id', '=', 'location.id_plant')
            ->leftjoin('location_type','location_type.id', '=', 'location.type')
            ->whereNull('location.deleted_at')
            ->orderBy('location.name', 'asc')
            ->get();
           
        $loctype = LocationType::whereNull('deleted_at')
                ->orderBy('name', 'asc')
                ->get();
        $plant = Plant::whereNull('deleted_at')->orderBy('id', 'asc')->get();       
        $input_plant = $request->plant;
        $input_loctype = $request->loctype;
        $data = [
            'list' => $list,
            'loctype' => $loctype,
            'plant' => $plant,
            'search_plant' => $input_plant,
            'search_loctype' => $input_loctype,
        ];
        //return $data;
        return view('location.index', $data);
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $plant = Plant::whereNull('deleted_at')->orderBy('id', 'asc')->get();
        $loctype = LocationType::whereNull('deleted_at')
            ->orderBy('name', 'asc')
            ->get();
          
        $build = Supplier::whereNotNull('building')->distinct()->get(['building']);
        return view('location.create', compact('plant', 'loctype', 'build'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'plant_id'      => 'required',
            #'address'       => 'required',
            #'latitude'      => 'required',
            #'longitude'     => 'required',
        );
        $validator = Validator::make($request->all(), $rules);
        $cek = Location::find(location::max('id'));

        $app_number = (!empty($cek->id) ? $cek->id+1 : 1);

        $app = 'L';
        $app .= sprintf("%05d", $app_number);

        if ($validator->fails()) {
            #echo 'fail';
            return redirect('location/create')->withInput()->withErrors($validator);
        } else {
            #var_dump($request->all());
            $insert = Location::create([
                'name'                  => ucfirst($request->name),
                'id_plant'              => intval($request->plant_id),
                'type'                  => $request->type,
                'building'              => $request->building,
                'unit'                  => $request->unit,
                'contact'               => $request->contact,
                'phone'                 => $request->phone,
                'email'                 => $request->email,
                'address'               => $request->location_address,
                'latitude'              => $request->latitude,
                'longitude'             => $request->longitude,
                'created_by'            => Auth::user()->id,
                'updated_by'            => Auth::user()->id,
            ]);
            Session::flash('success', 'Data Saved');

            return redirect('location');
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $items  = Location::with(['userCreated','userUpdated','plantLocation','locationType'])
                ->where('id','=',$id)
                ->first();
        return view('location.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = Location::with(['userCreated','userUpdated','plantLocation','locationType'])
                ->where('id','=',$id)
                ->first();
        return view('location.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $rules = array(
            'type'          => 'required',
            'location_name' => 'required',
            'plant'         => 'required',
            #'address'       => 'required',
            #'latitude'      => 'required',
            #'longitude'     => 'required',

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) 
        {
            return back()->withInput()->withErrors($validator);
        } 
        else {
            $update             = Location::find($id);
            $update->name       = ucfirst($request->location_name);
            $update->id_plant   = $request->plant;
            $update->address    = $request->address;
            $update->latitude   = $request->latitude;
            $update->longitude  = $request->longitude;
            $update->type       = $request->type;
            $update->building   = $request->building;
            $update->unit       = $request->unit;
            $update->contact    = $request->contact;
            $update->phone      = $request->phone;
            $update->email      = $request->email;
            $update->updated_by  = Auth::user()->id;
            $update->save();
            if ($update) {
                Session::flash('success', 'Data Updated');
            } else {
               Session::flash('error', 'Gagal rubah data');
            }
            return redirect('location');
        }
    }
    public function show_ajax(Request $request)
    {
        $columns = array( 
                            0 =>'type', 
                            1 =>'code',
                            2=> 'name',
                            3=> 'plant_id',
                            4=> 'address',
                            5=> 'building',
                            6=> 'unit',  
                            7=> 'contact',
                            8=> 'phone',
                            9=> 'email', 
                            10=> 'timserpo_id', 
                        );

        $totalData     = Location::count();
        $totalFiltered = $totalData;

        $posts = Location::select('plant.name as plant_name','location_type.name as loc_type_name','location.name','location.address','location.building','location.unit','location.contact','location.phone','location.email','location.id','location.code','location.type')
        ->leftjoin('plant', 'plant.id', '=', 'location.id_plant')
        ->leftjoin('location_type','location_type.id', '=', 'location.type')
        ->whereNull('location.deleted_at')
        ->orderBy('location.id', 'desc');

        $draw = $request->get('draw');
        $start = $request->input('start', 1);
        $limit = $request->input('length', 50);

        if(!empty($request->input('search.value'))){
            $search = $request->input('search.value'); 
            $posts->where('location.id','LIKE',"%{$search}%")
                            ->where('type', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit);
        }

        if($request->loctype){
            $posts->where(function($query) use ($request){
                            for($i=0; $i<count($request->loctype); $i++ ){
                                $query->orWhere('location.type', $request->loctype[$i]); 
                            }
                        });
                       
            
        }

        if($request->plant){
            $posts->where(function($query) use ($request){
                            for($i=0; $i<count($request->plant); $i++ ){
                                $query->orWhere('plant.plant_bismo', $request->plant[$i]); 
                            }
                        });
                       
            
        }

        $totalFiltered = $posts->count();
        $posts = $posts->get();        

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  url('location/' . $post->id);
                $delete = url('location/delete/'. $post->id);
                $edit = url('location/' . $post->id .'/edit');

                $nestedData['type'] = $post->loc_type_name;
                $nestedData['code'] = $post->code;
                $nestedData['location_name'] = $post->name;
                $nestedData['plant_name'] = $post->plant_name;
                $nestedData['address'] = $post->address;
                $nestedData['building'] = $post->building;
                $nestedData['unit'] = $post->unit;
                $nestedData['contact'] = $post->contact;
                $nestedData['phone'] = $post->phone;
                $nestedData['email'] = $post->email;
                $nestedData['options'] = "
                <div class='btn-group btn-space'>
                    <a type='button' class='btn btn-default' href='{$show}' >Detail</a>
                    <button type='button' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'><span class='mdi mdi-chevron-down'></span><span class='sr-only'>Toggle Dropdown</span></button>
                    <ul role='menu' class='dropdown-menu' style='min-width:100px' >
                        <li><a href='{$edit}'><i class='mdi mdi-edit'> </i> Edit</a></li>
                        <li><a href='#' data-toggle='modal' title='Delete Location Data' data-target=\"#delete".$post->id."\"><i class=' text-danger mdi mdi-delete'> </i> Delete</a></li>
                    </ul>
                </div>
                <div id=\"delete".$post->id."\" class='modal fade' role='dialog'>
                    <div class='modal-dialog'>
                        <div class='modal-content modal-md'>
                            <div class='modal-header'>
                                <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                <h4 class='modal-title'>Delete Location</h4>
                            </div>
                            <div class='modal-body'>
                                <p>Are you sure want to delete this Location?</p>
                            </div>
                            <div class='modal-footer'>
                                <button type='button' class='btn btn-default' data-dismiss='modal'>No</button>
                                <a href='{$delete}'  class='btn btn-danger' title='Delete'>Yes</a>
                            </div>
                        </div>
                    </div>
                </div>
                
             
                ";
                // }                          

                $data[] = $nestedData;

            }
        }
          
        $json_data = array(
                    "draw"            => intval($request->input('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered), 
                    "data"            => $data   
                    );
            
        return $json_data; 

    }

    public function destroy($id) {
        $stock              = Location::where('id', $id)->first();
        $stock->deleted_at   = Carbon::now()->format('Y-m-d H:i:s');
        $stock->updated_by   = Auth::user()->id;
        $stock->save();
       if ($stock) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('location');
        
    }
 
    public function autocomplete(Request $request)
    {
        $data = Location::select("building as name")->whereRaw("LOWER(building) LIKE '%".$request->input('query')."%'")->get();
        return response()->json($data);
        var_dump($data);
    }

    public function print_barcode()
    {
        $location = Location::whereNull('deleted_at')->get();
        return view('location.barcode', ['location' => $location]);
    }

    public function show_active_ajax(Request $request)
    {
        if($request->has('q')){
            $search = strtoupper($request->q);
            $data   = Location::with(['plantLocation'])
                    ->whereNull('deleted_at')
                    ->Where('name','ilike','%'.$search.'%')
                    ->limit(20)
                    ->get();
        }
        else
        {
            $data = Location::with(['plantLocation'])
            ->whereNull('deleted_at')
            ->limit(5)
            ->get();
        }

        return $data;
    }

    public function locationPlant()
    {
        $id = request()->input('plant_id');
        $location = Location::where('id_plant', $id)->get();
        $data = array();
        $seed = array();
        if ($location) {
            foreach ($location as $l) {
                $seed['id'] = $l->id;
                $seed['text'] = $l->name;
                array_push($data, $seed);
            }
            return $data;
        } else {
            return NULL;
        }
    }

    public function locationMap()
    {

        $id = request()->input('id');
        $map = Location::findOrFail($id);

        $address = explode(',', $map->address);
        $n = count($address);
        if ($n > 1) {
            $prop = implode(' ', array($address[$n-2]));
            $city = implode(' ', array($address[$n-3]));
        }

        $collection = collect($map);

        $collection->put('city', $city);
        $collection->put('province', preg_replace('/[0-9]+/', '', $prop));
        $location = $collection->all();

        return $location;
    }
}
