<?php

namespace App\Http\Controllers\Masters;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;
use Validator;
use DB;
use Excel;
use PDF;
use App\Http\Controllers\Controller;
use App\Models\Masters\MaterialGroup;
use App\Models\Masters\Material;
use App\Models\Masters\MaterialParameter;
use App\Models\Masters\Classification;
use App\Models\Masters\MaterialImage;
use App\Models\Masters\ClassificationParameter;
use App\Models\Transaction\Asset;
use Carbon\Carbon;

class MaterialController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
     {
        $material = material::with(['classification', 'materialGroup'])->whereNull('deleted_at');
        if(!empty($request->q))
            $material->where('name', 'ilike', '%'.$request->q.'%');
        if(!empty($request->mg) && $request->mg != 'null')
            $material->whereIn('id_material_group', explode(',', $request->mg));
        if(!empty($request->clsf) && $request->clsf != 'null')
            $material->whereIn('id_classification', explode(',', $request->clsf));
        $material->orderBy('id', 'DESC');

        $mat_group      = MaterialGroup::select('id','name','description')->whereNull('deleted_at')->get();
        $classification = Classification::select('id','name')->whereNull('deleted_at')->get();
        
        $list = $material->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);
        if(empty($request->mg))
            $list->appends(request()->mg);
        if(empty($request->clsf))
            $list->appends(request()->clsf);

        $params = array(
            'list' => $list,
            'mat_group' => $mat_group,
            'classification' => $classification,
            'input_mgroup' => $request->mg,
            'input_classification' => $request->clsf
        );

        // return $list;
        return view('material.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

 
    public function create()
    {
        $material_group     = MaterialGroup::All();
        $classification     = Classification::All();
        return view('material.create', compact('classification', 'material_group'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // return $request->all();
        $rules = array(
            'name'    => 'required|unique:materials',
        );
        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            //return $request->all();

            return redirect('material/create')->withErrors($validator);
            
        } else {
            $insert = Material::create([
                'id_classification'  => $request->id_classification,
                'id_material_group'  => $request->id_material_group,
                'name'               => $request->name,
                'description'        => $request->description,
                'created_by'         => Auth::user()->id,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

            // Save Parameter
            $parameter = $request->input('parameter');
            $id_material = $insert->id;
    if($parameter){
            foreach($parameter as $key => $value){
                // ID Parameter
                $id_parameter = explode("-",$key)[1];
                $value_parameter = $value;
                $insert_param = MaterialParameter::updateOrCreate(
                    [
                        'id_material'   => $id_material,
                        'id_parameter'  => $id_parameter,
                    ],
                    [
                        'value'         => $value_parameter,
                    ]
                );               
            }
        }
            if ($insert) {
                Session::flash('success', 'Material Created');
                return redirect('material');
            } else {
                Session::flash('error', 'Error save material');
            }

            return redirect('material');
        }
    }

    public function show($id)
    {
        
        $image = MaterialImage::where('id_material',$id)->get();
        $items  = Material::find($id);
        $classification = Classification::whereNull('deleted_at')->orderBy('id', 'asc')->get();
        $material_group = MaterialGroup::all();

        $material_parameter = MaterialParameter::where('id_material',$id)->get();
        $parameter = ClassificationParameter::where('id_classification',$items->id_classification)
            ->get()
            ->map(function($param) use($material_parameter){
                foreach($material_parameter as $value){
                    if($value->id_parameter == $param->id){
                        $param->existing = $value->value;
                    }
                }

                return $param;
            });


        return view('material.show', compact('items', 'classification', 'image', 'parameter','material_group','material_parameter'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $image = MaterialImage::where('id_material',$id)->get();
        $items  = Material::find($id);
        $classification = Classification::whereNull('deleted_at')->orderBy('id', 'asc')->get();
        $material_group = MaterialGroup::all();

        $material_parameter = MaterialParameter::where('id_material',$id)->get();
        $parameter = ClassificationParameter::where('id_classification',$items->id_classification)
            ->get()
            ->map(function($param) use($material_parameter) {
                foreach($material_parameter as $value){
                    if($value->id_parameter == $param->id){
                        $param->existing = $value->value;
                    }
                }
                return $param;
            });

        return view('material.edit', compact('items', 'classification', 'image', 'parameter','material_group','material_parameter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id_material)
    {
        $rules      = [
            'name' => 'required'
        ];
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $update                     = Material::find($id_material);
            $update->id_classification  = $request->id_classification;
            $update->name               = $request->name;
            $update->id_material_group  = $request->id_material_group;
            $update->description        = $request->description;
            $update->updated_by         = Auth::user()->id;
            $update->updated_at         = Carbon::now()->format('Y-m-d H:i:s');
            $update->save();

            if ($update) {
                Session::flash('success', 'Data has been changed');
            } else {
                Session::flash('error', 'Data failed to change');
            }
            return redirect('material');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_material)
    {
        $delete             = Material::find($id_material);
        $delete->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        $delete->updated_by = Auth::user()->id;
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('material');
    }

    public function images (Request $request, $id_material)
    {
        
        $rules = array(
            'image_material'   => 'required',
            'id_material'      => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $request->all();
        } else {
            $request->hasFile('image_material') ? $image_material = $request->image_material->store('image/') : $image_material = null ;
            $insert = MaterialImage::create([
                'id_material'       => $request->id_material,
                'image_material'    => $request->image_material,
                'created_by'        => Auth::user()->id,
                'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
             
            if ($insert) {
                Session::flash('success', 'Data Berhasil Ditambahkan');
            } else {
                Session::flash('error', 'Gagal Tambah Data');
            }
            // return $request->all();
           return redirect()->back()->withErrors($validator);
        }
    }

    public function parameter(Request $request){
        $parameter = $request->input('parameter');
        $id_material = $request->input('id_material');
        if($parameter){
            foreach($parameter as $key => $value){
                $id_parameter = explode("-",$key)[1];
                $value_parameter = $value;
                $insert = MaterialParameter::updateOrCreate(
                    [
                        'id_material'   => $id_material,
                        'id_parameter'  => $id_parameter,
                    ],
                    [
                        'value'         => $value_parameter,
                    ]
                );
        }
            if($insert){
                Session::flash('success','Parameter Saved!');
            }
            
        }

        return redirect('material');

    }

    public function ajax_parameter(Request $request, $id){
        $parameter = ClassificationParameter::where('id_classification',$id)->get();

        return view('material.partial_parameter')->withParameter($parameter);
    }

    public function upload_images(Request $request, $id){

        foreach($request->file('images') as $image){
            $saved       = $image->store('public/asset/material');
            $public_path = str_replace('public','storage',$saved);
            $insert      = MaterialImage::create([
                'id_material'       => $id,
                'image_material'    => $public_path,
                'created_by'        => Auth::user()->id,
            ]);
            echo $public_path.PHP_EOL;
        }

        return response()->json([
            'status' => 'ok',
            'request' => $request->all()
        ]);
    }

    public function get_images(Request $request, $id) {

        $images = MaterialImage::where('id_material',$id)->get();
        return view('material.partial_images')->withImages($images);

    }

    public function delete_image(Request $request,$id){
        $image  = MaterialImage::where('id',$id)->firstOrFail();
        $path   = str_replace('storage/','public/',$image->image_material);

        if(\Storage::delete($path)){
            $image->delete();
            return response()->json([
                'status' => 'success',
                'message' => 'success delete image'
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'error delete image'
            ],406);
        }
    }

    public function downloadTemplate() {

        $header = array(
            ['No', 'Material_Code', 'Description', 'Material_Group', 'Classification', 'C_Brand', 'C_Panjang', 'C_Lebar', 'C_Tahun'],
            ['1', '123456789', 'Aset Perusahaan', 'barang', 'APX', '0897827637', 'Cahaya', '8', '53', '2018']
        );
        $export = new \App\Exports\Excel\ExportFromArray($header);
        return Excel::download($export, 'Upload Material.xlsx');
    }

    public function upload(Request $request)
    {
        // Validate Form
        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        $array_excel = new \App\Imports\Excel\ImportToArray();
        $ts = \Maatwebsite\Excel\Facades\Excel::import($array_excel, $request->file('upload_file')->path(), NULL, \Maatwebsite\Excel\Excel::XLSX);
        
        $data = []; $dataOk = true;
        foreach($array_excel->sheetData[$array_excel->sheetNames[0]] as $row) {
            // Validate Data
            $isValid = true;
            $messages = [];            
            // EXCEL VALIDATION ------------------------------------------
            // check material
 
            //cek code_material
            if(empty($row['material_code'])) {
                $isValid = false;
                array_push($messages,"Material Code is required");
            }
            else {
                $material = Material::where('name',$row['material_code'])->first();
                if( $material ) {
                    $isValid = false;
                    array_push($messages,"Material Code ".$row['material_code']." already exists");
                }
                elseif(strlen($row['material_code']) > 30){
                    $isValid = false;
                    array_push($messages,"Material Code should be no more than 30 caracters");
                }
            }

            $classification = null;
            if(!empty($row['classification'])) {
                $classification = Classification::where('name',$row['classification'])->first();
                if( empty($classification) ){
                    $isValid = false;
                    array_push($messages,"classification ".$row['classification']." doesn't exists");
                }
            }
            else{
                $isValid = false;
                array_push($messages,"Classification is required");
            }

            if(!empty($row['material_group'])){
                $material_group = MaterialGroup::where('name',$row['material_group'])->first();
                if( !$material_group ){
                    $isValid = false;
                    array_push($messages,"Material Group ".$row['material_group']." doesn't exists");
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Material Group is required");
            }

            //fiter header c_
            $filtered = array();
            foreach($row as $k => $v){
                if(preg_match('/c_/',$v)){
                    $filtered[] = $v;
                }
            }
            // dump($filtered);
            //data parameter yang akan diinsert
            $params = [];
            if ($row['material_code'] != null){
                foreach($filtered as $keyparam){
                    $where = str_replace('c_','',$keyparam);
                    $wherespace = str_replace('_',' ',$where);
                    $param = ClassificationParameter::whereRaw("LOWER(name) = (?)", [$wherespace])
                            ->where('id_classification', isset($classification) ? $classification->id : 0)
                            ->get();

                    foreach($param as $coba){
                        $params[] = [
                            'id_parameter' => $coba->id,
                            'value'      => $row[$keyparam],
                        ];
                    }     
                }
            }
            // dump($params);
            

            // END OF EXCEL VALIDATION ---------------------------------------

            // Return New Data
            $data[] = [
                'no'                => $row['no'],
                'name'              => $row['material_code'],
                'description'       => $row['description'],
                'classification'    => $row['classification'],
                'id_classification' => isset($classification) ? $classification->id : null,
                'material_group'    => $row['material_group'],
                'id_material_group' => isset($material_group) ? $material_group->id : null,
                'created_by'       => Auth::user()->id,
                'message'           => implode(', ',$messages),
                'valid'             => $isValid,
                'params'            => $params, //untuk insert parameter
            ];

            if(!$isValid)
                $dataOk = false;
        }

        $validate = [];
        foreach($data as $d) {
            if($d['valid'])
                $validate[] = $d;
        }
        if(!$dataOk) {
            foreach($data as $d) {
                if(!$d['valid'])
                    $validate[] = $d;
            }
        }
        
        //where there is no error flash session
        if($dataOk){
            Session::put('dataToInsert', $validate);         
        }

        return view('material.validate', ['validate' => $validate, 'dataOk' => $dataOk]);
    }

    public function saveData(){
        if(Session::has('dataToInsert')){
            $data = Session::get('dataToInsert');

            foreach($data as $k => $v) {
                unset($v['no']);
                unset($v['classification']);
                unset($v['material_group']);
                unset($v['message']);
                unset($v['valid']);
                $data[$k] = $v;
            }

            try {
                \DB::beginTransaction();
                foreach($data as $datamaterial){
                    $insertedmaterial = Material::create($datamaterial);
                    $paramToInsert = [];
                    foreach($datamaterial['params'] as $dataparam){
                        $paramToInsert[] = [
                            'id_material'  => $insertedmaterial->id,
                            'id_parameter' => $dataparam['id_parameter'],
                            'value'        => $dataparam['value']
                        ];
                    }
                    // dump($paramToInsert);
                    MaterialParameter::insert($paramToInsert);
                }
                \DB::commit();
                Session::flash('success', 'Data saved');
            }
            catch(\Exception $ex) {
                \DB::rollBack();
                Session::flash('error', 'ERROR : Data cannot be saved. '.$ex->getMessage()); 
            }           
        }
        return redirect('material');        
    }

    public function getassetused($id){
        $asset = Asset::select('code_asset', 'description_asset')->where('id_material',$id)->where('status', 1)->get();
        return $asset->toJson();
    }

    public function getAssetAsedPdf($id)
    {
        $asset = Asset::where('id_material',$id)->where('status', 1)->get();
        $material = Material::findOrfail($id);

        $pdf = PDF::loadview('material.pdf', compact('asset', 'material'))->setPaper('a4', 'potrait');

        return $pdf->download('material.pdf');
    }

    public function show_active_ajax(Request $request)
    {
        $materials  = Material::whereNull('deleted_at')->get();
        return $materials;
    }
    
    public function show_ajax(Request $request){

        $data = [];

        if($request->has('q')){
            $search = strtolower($request->q);
            $data = Material::where(DB::raw("LOWER(name)"),"LIKE","%".$search."%")->orWhere(DB::raw("LOWER(description)"), "LIKE", "%".$search."%")->get();
        }else{
            $data = Material::all();
        }

        return response()->json($data);
    }
}
