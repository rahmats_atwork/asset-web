<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Auth;
use Session;
use Validator;
use App\Models\Masters\AssetType;
use App\Http\Controllers\Controller;

class AssetTypeController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $assettype = AssetType::whereNull('deleted_at');
            // ->select('*')
        if(!empty($request->q))
            $assettype->where('name', 'ilike', "%".$request->q."%")
            ->orWhere('code','ilike','%'.$request->q.'%');

        $assettype->orderBy('id', 'desc');
            // ->get();

        $list = $assettype->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);
        
        $params = array(
            'list' => $list,
        );
        return view('assettype.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('assettype.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $rules = array(
            'code'    => 'required|unique:asset_type',
            'name'    => 'required|unique:asset_type',
        );

        $this->validate(request(), [
            'code'    => 'required|unique:asset_type',
            'name'    => 'required|unique:asset_type',
        ]);
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //return $request->all();
            return redirect('assettype/create')->withErrors($validator);
        } else {
            $insert = AssetType::create([
                'code'     => $request->code,
                'name'     => $request->name,
                'created_by'         => Auth::user()->id,
            ]);
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Gagal Tambah Data');
            }
            return redirect('assettype');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $items  = AssetType::find($id);
        return view('assettype.show', compact('items'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $items  = AssetType::find($id);
        return view('assettype.edit', compact('items'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'code'    => 'required',
            'name'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('assettype/'.$id.'/edit')->withErrors($validator);
        } else {
            $update                     = AssetType::find($id);
            $update->code     = $request->code;
            $update->name     = $request->name;
            $update->updated_by     = Auth::user()->id;
            
            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Gagal rubah data');
            }
            return redirect('assettype');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $destroy = \App\AssetType::find($id)->delete();
    	// return back();

        $delete = AssetType::find($id);
        $delete->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        $delete->updated_by = Auth::user()->id;
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('assettype');
    }

    /**
     * Display the specified resource from storage.
     *
     * @param  \App\AssetType $AssetType
     * @return \Illuminate\Http\Response
     */
    public function show_ajax(Request $request)
    {
        if($request->has('q')){
            $search = strtoupper($request->q);
            $data = AssetType::where('name','like','%'.$search.'%')
                ->orwhere('code','like','%'.$search.'%')
                ->whereNull('deleted_at')
                ->limit(4)
                ->get();
        }
        else
        {
            $data = AssetType::whereNull('deleted_at')
                ->limit(4)
                ->get();
        }
        return $data;
    }
}
