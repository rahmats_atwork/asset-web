<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Masters\LocationType;
use Auth;
use DB;
use Session;
use Validator;
use Carbon\Carbon;


class LocationTypeController extends Controller
{
	 public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $locationtype = LocationType::whereNull('deleted_at');
        if (!empty($request->q)) {
            $locationtype->where('name', 'ilike', "%".$request->q."%");
            $locationtype->orderBy('created_at', 'desc');
        };
        $list = $locationtype->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

        $params = array(
            'list' => $list,
        );


        return view('locationtype.index', $params);
    }

    public function create()
    {
        return view('locationtype.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'location_type'  => 'required|alpha_num|unique:location_type,name',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('locationtype/create')->withInput()->withErrors($validator);
        } else { 
            $file                   = $request->file('icon');

            $insert                 = new LocationType();
            $insert->name           = $request['location_type'];
            $insert->created_by     = Auth::user()->id;
            $insert->created_at     = Carbon::now()->format('Y-m-d H:i:s');
            
            if($file==null) {
                $insert->zoom_level     = 1;
                $insert->zoom_level_end = 2;
                $insert->save();
            }
            else {
                $fileName   = time().'-'.$file->getClientOriginalName();
                $request->file('icon')->storeAs("public/images/", $fileName);
                $insert->icon           = $fileName;
                $insert->zoom_level     = 1;
                $insert->zoom_level_end = 2;
                $insert->save();
            }
            if ($insert) {
                    Session::flash('success', 'Add data successfully');
            } else {
                    Session::flash('error', 'Add data failed');
            }
            return redirect('locationtype');
        }
    }

    public function edit($id)
    {

        $locname = LocationType::find($id);

		return \View::make('locationtype.edit')
		->with(compact('locname'));
                   
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'location_type'  => 'required|required|alpha_num',
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {

            $update  = LocationType::find($id);
            $file    = $request->file('icon');
            if($file==null){
                $update->name           = $request->location_type;
                $update->zoom_level     = 1;
                $update->zoom_level_end = 2;
                $update->updated_by     = Auth::user()->id;
                $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');
                $update->save();

            }
            else{
                $fileName   = time().'-'.$file->getClientOriginalName();
                $request->file('icon')->storeAs("public/images/", $fileName);                
                $update->name           = $request->location_type;
                $update->icon           = $fileName;
                $update->zoom_level     = 1;
                $update->zoom_level_end = 2;
                $update->updated_by     = Auth::user()->id;
                $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');
                $update->save();
            }
            if ($update) {
                Session::flash('success', 'Update data successfully');
            } else {
                Session::flash('error', 'Update data failed');
            }
            return redirect('locationtype');
        }
    }

    public function show($id)
    {
        $loc = LocationType::find($id);

        return \View::make('locationtype.show')
        ->with(compact('loc'));
    }

    public function destroy($id)
    {
    	$delete               = LocationType::find($id);
        $delete->deleted_at   = Carbon::now()->format('Y-m-d H:i:s');
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Delete data successfully');
        } else {
            Session::flash('error', 'Delete data failed');
        }
        return redirect('locationtype');        
    }

    public function show_active_ajax(Request $request)
    {
        $data  = LocationType::whereNull('deleted_at')->get();
        return $data;
    }
}
