<?php

namespace App\Http\Controllers\Masters;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Session;
use Validator;
use DB;
use Carbon\Carbon;
use App\Models\Masters\Classification;
use App\Models\Masters\ClassificationParameter;
use App\Models\Masters\Material;
use App\Http\Controllers\Controller;

class ClassificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $master_material = Material::all();

        $master_classification = Classification::whereNull('deleted_at');
        if(!empty($request->q))
            $master_classification->where('name', 'ilike', '%'.$request->q.'%');
        $master_classification->orderBy('created_at', 'desc');
        $list = $master_classification->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

        $params = array(
            'master_classification' => $list,
            'list'                  => $list,
            'master_material'       => $master_material,
        );

    	return view('classification.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('classification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'    => 'required|unique:classification',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //return $request->all();
            return redirect('classification/create')->withErrors($validator);
            Session::flash('error', 'Data Is Duplicate');
        } else {
            $insert = Classification::create([
                'name'              => $request->name,
                'created_by'        => Auth::user()->id,
                'created_at'        => Carbon::Now()->format('Y-m-d H:i:s'),
            ]);
            if ($insert) {
                Session::flash('success', 'Data Successfully Added');
                return redirect ('classification/'.$insert->id.'/addparameter');
            } else {
                Session::flash('error', 'Data Failed to Add');
            }
            return redirect('classification');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 public function show($id)
    {

        $items          = Classification::with(['userUpdated','userCreated'])
                        ->find($id);
        $paramclassific = ClassificationParameter::with(['classificationParameter'])
                        ->where('id_classification','=',$id)
                        ->orderBy('id','asc')
                        ->get();

        return view('classification.show', compact('items','paramclassific'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $paramclassific = ClassificationParameter::where('id_classification',$id)
                         ->orderBy('id', 'ASC')
                         ->get();
        $items          = Classification::find($id);
        return view('classification.edit', compact('items','paramclassific'));
    }

    public function addparam($id)
    {
        $paramclassific = ClassificationParameter::where('id_classification',$id)
                        ->orderBy('id', 'ASC')
                        ->get();
        $items          = Classification::find($id);
        return view('classification.addparam', compact('items','paramclassific'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $update                    = Classification::find($id);
            $update->name               = $request->name;
            $update->updated_by         = Auth::user()->id;
            $update->updated_at         = Carbon::Now()->format('Y-m-d H:i:s');
            $update->save();
            
            if ($update) {
                Session::flash('success', 'Data has been changed');
            } else {
                Session::flash('error', 'Data failed to change');
            }
            return redirect('classification');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Classification::find($id)->delete();

    	return redirect()->back()->with('success');
    }

     public function editparam($id)
    {
        $items  = ClassificationParameter::find($id);
        return view('classification.editparam', compact('items'));
    }

     public function updateparam(Request $request,$id_classification, $id)
    {
        $rules = array(
            'name'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
             return redirect()->back()->withErrors($validator);
        } else {
            $update                 = ClassificationParameter::find($id);
            $update->name           = $request->name;
            $update->type           = $request->type;
            $update->length         = $request->length;
            $update->decimal        = $request->decimal;
            $update->value          = $request->value;
            $update->reading        = $request->reading;
            $update->updated_by     = Auth::user()->id;
            $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');

            $update->save();
            if ($update) {
                Session::flash('success', 'Data telah di rubah');
            } else {
                Session::flash('error', 'Gagal rubah data');
            }
           return redirect()->route('classification.edit',$id_classification);

        }
    }

     public function paramclassific(Request $request, $id)
    {
        $rules = array(
            'name'     => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $insert = ClassificationParameter::create([
                'name'                => $request->name,
                'type'                => $request->type,
                'length'              => $request->length,
                'decimal'             => $request->decimal,
                'value'               => $request->value,
                'reading'             => $request->reading,
                'id_classification'   => $request->id_classification,
                'created_by'          => Auth::user()->id,
                'created_at'          => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            if ($insert) {
                Session::flash('success', 'Data Successfully Added');
            } else {
                Session::flash('error', 'Data Failed to Add');
            }
            return back();
        }
    }
     public function paramclassificdestroy($id_parameter)
    {
        $destroy = ClassificationParameter::find($id_parameter)->delete();

    	return back();
    }
    
    public function getmaterialused($idClass){
        $m = Material::where('id_classification',$idClass)->get();
        return $m;
    }

    public function show_ajax(Request $request){
        
        if($request->has('q')){
            $search = strtolower($request->q);
            $data   = Classification::where('name','ilike','%'.$search.'%')
                    ->get();
        }else{
            $data = Classification::limit(20)->get();
        }

        return $data;
    }
}
