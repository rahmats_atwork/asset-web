<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use Validator;
use Session;
use Auth;
use App\Models\Masters\DepreciationType;
use App\Models\Masters\Depreciation;
use App\Models\Transaction\Asset;
use App\Models\Transaction\AssetDepreciationHistory;

class DepreciationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $depreciation = Depreciation::whereNull('deleted_at')
                           ->with('depreciationType')
                           ->orderBy('updated_at','desc');
        
        $list = $depreciation->paginate(empty($request->limit)?10:intval($request->limit));

        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

       $params = array(
           'list' => $list,
       );

       return view('depreciation.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('depreciation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'id_depreciation_type'  => 'required',
            'name'                  => 'required',
            'useful_life'           => 'required',
        );
    
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('depreciation/create')->withErrors($validator);
        }
        else {
            try {
                DB::beginTransaction();
                $insert = Depreciation::create([
                    'name'                  => $request->name,
                    'id_depreciation_type'  => $request->id_depreciation_type,
                    'rate'                  => NULL,
                    'useful_life'          => $request->useful_life,
                    'created_at'            => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
                    'created_by'           => Auth::user()->id,
                    'updated_by'           => Auth::user()->id,
                    'deleted_at'            => NULL
                    ]);
                DB::commit();
                Session::flash('success', 'Data Successfully Added');
            }
            catch(\Exception $e) {
                DB::rollback();
                Session::flash('error', 'Data Failed to Add');
                
            }
            return redirect('depreciation');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Controller\Masters\Depreciation $depreciation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $depreciation = Depreciation::where('id',$id)
                    ->with(['depreciationType','userCreated','userUpdated'])
                    ->orderBy('created_at', 'desc')
                    ->first();
        return view('depreciation.show', [
            "list" => $depreciation,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Controller\Masters\Depreciation $depreciation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $depreciation = Depreciation::where('id',$id)
        ->with(['depreciationType','userCreated','userUpdated'])
        ->first();
        return view('depreciation.edit',['list' => $depreciation]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Controller\Masters\Depreciation $depreciation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $check = Asset::where('id_depreciation','=',$request->id_depreciation)->count();
        if ($check == 0) {
            try {            
                $destroy                = Depreciation::find($request->id_depreciation);
                $destroy->name          = $request->name;
                $destroy->useful_life   = $request->useful_life;
                $destroy->updated_by    = Auth::user()->id;
                $destroy->updated_at    = Carbon::now()->format('Y-m-d H:i:s');
                $destroy->save();
                Session::flash('success', 'Data has been updated.');
            }
            catch (Exception $e) {
                Session::flash('error', 'Data Failed to update. Error :'.$e.'.');
            }
        }
        else {
            Session::flash('error', 'Depreciation has been used on Asset');
        }
        return redirect('depreciation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Controller\Masters\Depreciation $depreciation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {            
            $destroy = Depreciation::find($id);
            $destroy->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
            $destroy->save();
            Session::flash('success', 'Data has been deleted.');
        }
        catch (Exception $e) {
            Session::flash('error', 'Data Failed to Add. Error :'.$e.'.');
        }
        return back();
    }
    public function ajaxDepreciationActive(Request $request)
    {
        if($request->has('q')) {
            $data = Depreciation::whereNull('deleted_at')
                    ->where('name','ilike','%'.$request->q.'%')
                    ->limit(5)
                    ->get();
        }
        else {
            $data = Depreciation::whereNull('deleted_at')
                    ->limit(5)
                    ->orderBy('updated_at','desc')
                    ->orderBy('id','asc')
                    ->get();
        }
        return $data;
    }

    public function ajaxDepreciationtypeActive(Request $request)
    {
        if($request->has('q')) {
            $data = DepreciationType::whereNull('deleted_at')
                    ->where('name','ilike','%'.$request->q.'%')
                    ->where('id',1)
                    ->limit(5)
                    ->get();
        }
        else {
            $data = DepreciationType::whereNull('deleted_at')
                    ->where('id',1)
                    ->limit(5)
                    ->orderBy('updated_at','desc')
                    ->orderBy('id','asc')
                    ->get();
        }
        return $data;
    }

    public function setActivedepreciationToday()
    {
        $date_now = Carbon::now()->format('Y-m-d');
        $asset = AssetDepreciationHistory::where('is_active','=',0)
        ->where('depreciation_periode','=',$date_now)
        ->orWhere('depreciation_periode','<',$date_now)
        ->update(['is_active' => 1]);
    }

    public function calculateDepreciation($data)
    {
            if(isset($data['id_asset'])){

            }
            else {
                $asset = Asset::where('code_asset','=',$data['code_asset'])->first();
                $id_asset = $asset->id_asset;
            }
            if(!empty($data['is_asset'])){
            $date_now = Carbon::now()->format('Y-m-d H:i:s');
            $da = Depreciation::find($data['id_depreciation']);
            if ($da->id_depreciation_type == 1) {
                $x                      = 1;
                $status                 = $data['status'];
                if(!empty($data['retired_date'])){
                    $carbon              = Carbon::parse($data['retired_date']);
                    $retired_date       = $carbon->format('Y-m-d');
                }
                $sequence               = ($da->useful_life)*12;
                $amount_depreciation    = (float)($data['purchase_cost']/$sequence);
                $date_purchase          = $data['purchase_date'];
                $salvage                = $data['purchase_cost'];
                $date                   = Carbon::create(
                    Carbon::parse($date_purchase)->format('Y'), 
                    Carbon::parse($date_purchase)->format('m'),  
                    Carbon::parse($date_purchase)->format('d'), 
                    0, 
                    0, 
                    0
                );
                $carbon              = Carbon::parse($date);
                $date                = $carbon;
                for($i=1;$i<=$sequence;$i++) {
                    if($status == 2){
                        if($salvage > 0){
                            if($retired_date == $date || $retired_date < $date) {
                                $amount_depreciation = $salvage;
                                $salvage             = 0;
                                $carbon              = Carbon::parse($retired_date);
                                $periode             = $carbon->format('Y-m-d');
                                if ($periode <= $date_now) {
                                    $active = 1;
                                }
                            }
                            else {
                                $carbon         = Carbon::parse($date);
                                $periode        = $carbon->format('Y-m-d');
                                $salvage        = (float)$salvage - (float)$amount_depreciation;
                                if ($salvage < 0) {
                                    $salvage = 0;
                                }
                                $active         = 0;
                                if ($periode <= $date_now) {
                                    $active = 1;
                                }
                            }
                            $datainsert     = AssetDepreciationHistory::create([
                                'id_asset'              => $data['id_asset'] ?? $id_asset,
                                'id_depreciation'       => $data['id_depreciation'],
                                'is_active'             => $active,
                                'sequence'              => $x,
                                'depreciation_amount'   => $amount_depreciation,
                                'depreciation_periode'  => $periode,
                                'salvage_value'         => $salvage,
                                'created_at'            => $date_now,
                                'created_by'           => Auth::user()->id,
                                'updated_at'            => $date_now,
                                'updated_by'            => Auth::user()->id 
                            ]);
                            $date->addMonthsNoOverflow(1);
                            $x++;
                        }
                        else {
                            $x++;
                        }
                    }
                    elseif ($status == 1) {
                        $carbon         = Carbon::parse($date);
                        $periode        = $carbon->format('Y-m-d');
                        $salvage        = (float)$salvage - (float)$amount_depreciation;
                        if ($salvage < 0) {
                            $salvage = 0;
                        }
                        $active         = 0;
                        if ($periode <= $date_now) {
                            $active = 1;
                        }
                        $datainsert     = AssetDepreciationHistory::create([
                            'id_asset'              => $data['id_asset'] ?? $id_asset ,
                            'id_depreciation'       => $data['id_depreciation'],
                            'is_active'             => $active,
                            'sequence'              => $x,
                            'depreciation_amount'   => $amount_depreciation,
                            'depreciation_periode'  => $periode,
                            'salvage_value'         => $salvage,
                            'created_at'            => $date_now,
                            'created_by'           => Auth::user()->id,
                            'updated_at'            => $date_now,
                            'updated_by'            => Auth::user()->id 
                        ]);
                        $date->addMonthsNoOverflow(1);
                        $x++;
                    }
                }
        }
    }
        return true;
    }

    public function deleteCalculatedepreciation($data)
    {
        $deleteRows = AssetDepreciationHistory::where('id_asset','=',$data)
        ->delete();
        return true;
    }

}
