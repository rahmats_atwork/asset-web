<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Masters\DepreciationType;

class DepreciationTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request) {
        if($request->has('q')){

            $depreciationType = DepreciationType::whereNull('deleted_at')
                            ->where('name','ilike','%'.$request->q.'%')
                            ->orderBy('updated_at','desc');
            $list = $depreciationType->paginate(empty($request->limit)?10:intval($request->limit));
            if(empty($request->q))
                $list->appends(request()->q);
            if(empty($request->limit))
                $list->appends(request()->limit);

            $params = array(
                'list' => $list,
            );
        }
        else {
            $depreciationType = DepreciationType::whereNull('deleted_at')
                                ->orderBy('updated_at','desc');
            $list = $depreciationType->paginate(empty($request->limit)?10:intval($request->limit));
            if(empty($request->q))
            $list->appends(request()->q);
            if(empty($request->limit))
            $list->appends(request()->limit);

            $params = array(
            'list' => $list,);
        }
       return view('depreciation_type.index', $params);
    }
}
