<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use Session;
use Validator;
use PDF;
use Auth;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\Masters\Plant;
use App\Http\Controllers\Controller;

class PlantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $plant = Plant::whereNull('deleted_at');

        if(!empty($request->q))
        $plant->where('name','ilike',"%".$request->q."%")
              ->orWhere('description','ilike', "%".$request->q."%")
              ->orderBy('created_at', 'desc');

        $list = $plant->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

        $params = array(
            'plants' => $list
        );

        // return view('plant.index', ['plants' => $plant]);
        return view('plant.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('plant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'name' => 'required|unique:plant,name',
        ]);

        $data = [
            'name'          => $request->name,
            'description'   => $request->description,
            'longitude'     => $request->longitude,
            'latitude'      => $request->latitude,
            'created_by'    => Auth::user()->id,
            'updated_by'    => Auth::user()->id,
            'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        Plant::create($data);

        \Session::flash('message', 'Data Saved!');
        \Session::flash('alert-class', 'alert-success');
        return \Redirect::to('system/plant');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plant = Plant::where('id','=',$id)->first();
        // Redirect to state list if updating state wasn't existed
        if ($plant == null) {
            return \Redirect::to('group');
        }

        return view('plant.show', ['plants' => $plant]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $plant = Plant::where('id','=',$id)->first();
        // Redirect to state list if updating state wasn't existed
        
        if (empty($plant)) {
            return \Redirect::to('group');
        }
        
        return view('plant.edit', ['plants' => $plant]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data  = [
            'name'          => $request->name,
            'description'   => $request->description,
            'longitude'     => $request->longitude,
            'latitude'      => $request->latitude,
            'updated_by'    => Auth::user()->id,
            'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        $plant = Plant::findOrFail($id);
            Plant::where('id', $id)
            ->update($data);

        \Session::flash('message', 'Data Updated');
        \Session::flash('alert-class', 'alert-success');
        return \Redirect::to('system/plant');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $plant                  = Plant::find($id);
        $plant->deleted_at      = Carbon::now()->format('Y-m-d H:i:s');
        $plant->updated_by       = Auth::user()->id;
        
        $plant->save();

        \Session::flash('message', 'Data Deleted');
        \Session::flash('alert-class', 'alert-success');
        return \Redirect::to('system/plant');
    }


     private function validateInput($request) {
        $this->validate($request, [
            'name' => 'required',
        ]);
    }


    private function createQueryInput($keys, $request) {
        $queryInput = [];
        for($i = 0; $i < sizeof($keys); $i++) {
            $key = $keys[$i];
            $queryInput[$key] = $request[$key];
        }

        return $queryInput;
    }



    public function show_active_ajax(Request $request)
    {
        if($request->has('q')){
            $data  = Plant::where('name','ilike','%'.$request->q.'%')
                    ->whereNull('deleted_at')
                    ->limit(5)
                    ->get();

        }
        else {
            $data  = Plant::whereNull('deleted_at')
                    ->limit(5)
                    ->get();
        }
        return $data;
    }

}
