<?php

namespace App\Http\Controllers\Masters;

use App\Models\Masters\Supplier;
use App\Models\Masters\RetiredReason;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Auth;
use Session;
use Validator;


class RetiredReasonController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $retiredreason = RetiredReason::whereNull('deleted_at');
        if(!empty($request->q))
            $retiredreason->where('name','ilike', '%'.$request->q.'%');
            $retiredreason->orderBy('id', 'desc');

        $list = $retiredreason->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

        $params = array(
            'list' => $list,
        );
        return view('retiredreason.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('retiredreason.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code'    => 'required|unique:retired_reason',
            'name'    => 'required|unique:retired_reason',
        );

        $this->validate(request(), [
            'code'    => 'required|unique:retired_reason',
            'name'    => 'required|unique:retired_reason',
        ]);
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //return $request->all();
            return redirect('retiredreason/create')->withErrors($validator);
        } else {
            $insert = RetiredReason::create([
                'code'              => $request->code,
                'name'              => $request->name,
                'created_by'        => Auth::user()->id,
                'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('retiredreason');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $items  = RetiredReason::find($id);
        return view('retiredreason.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = RetiredReason::find($id);
        return view('retiredreason.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'code'    => 'required|unique:retired_reason,code,'. $id .'',
            'name'    => 'required|unique:retired_reason,name,'. $id .'',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('retiredreason/'.$id.'/edit')->withErrors($validator);
        } else {
            $update                 = RetiredReason::find($id);
            $update->code           = $request->code;
            $update->name           = $request->name;
            $update->updated_by     = Auth::user()->id;
            $delete->updated_at     = Carbon::now()->format('Y-m-d H:i:s');

            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('retiredreason');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = RetiredReason::find($id);
        $delete->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        $delete->updated_by = Auth::user()->id;
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('retiredreason');
    }

    public function ajaxretiredreason()
    {
        $reason = RetiredReason::whereNull('deleted_at')->get();
        return $reason;
    }
}
