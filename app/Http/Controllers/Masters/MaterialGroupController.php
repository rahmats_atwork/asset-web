<?php

namespace App\Http\Controllers\Masters;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Validator;
use Session;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Masters\MaterialGroup;
use Carbon\Carbon;

class MaterialGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $materialgroup = MaterialGroup::whereNull('deleted_at');
        if(!empty($request->q))
            $materialgroup->where('name', 'ilike', "%".$request->q."%");

        $materialgroup->orderBy('id', 'desc');
        $list = $materialgroup->paginate(empty($request->limit)?10:intval($request->limit));

        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);

       $params = array(
           'list' => $list,
       );
       return view('material_group.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('material_group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code'    => 'required|unique:material_group',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('materialgroup/create')->withErrors($validator);
        } else {
            $insert = MaterialGroup::create([
                'code'               => $request->code,
                'name'               => $request->name,
                'description'        => $request->description,
                'created_by'         => Auth::user()->id,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'deleted_at'         => null
            ]);
            if ($insert) {
                Session::flash('success', 'Data Savad');
            } else {
                Session::flash('error', 'Gagal Tambah Data');
            }
            return redirect('materialgroup');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items  = MaterialGroup::find($id);
        return view('material_group.show', compact('items'));
    }

    public function edit($id)
    {
        $items  = MaterialGroup::find($id);
        return view('material_group.edit', compact('items'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'code'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('materialgroup/'.$id.'/edit')->withErrors($validator);
        } else {
            $update                 = MaterialGroup::find($id);
            $update->code           = $request->code;
            $update->name           = $request->name;
            $update->description    = $request->description;
            $update->updated_by     = Auth::user()->id;
            $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');
            
            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Gagal rubah data');
            }
            return redirect('materialgroup');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = MaterialGroup::find($id);
        $delete->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        $delete->updated_by = Auth::user()->id;
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('materialgroup');
    }

    public function show_ajax(Request $request){
        if($request->has('q')){
            $search = strtolower($request->q);
            $data = MaterialGroup::where("material_group","ilike","%".$search."%")
                    ->whereNull('deleted_at')
                    ->limit(5)
                    ->get();
        }
        else {
            $search = strtolower($request->q);
            $data = MaterialGroup::whereNull('deleted_at')
                    ->limit(5)
                    ->get();
        }
        return $data;
    }
}
