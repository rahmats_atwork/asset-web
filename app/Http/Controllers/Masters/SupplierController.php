<?php

namespace App\Http\Controllers\Masters;

use App\Models\Masters\Supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;
use Session;
use Validator;
use DB;

class SupplierController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $supplier = Supplier::whereNull('deleted_at');
        if(!empty($request->q)){
            $supplier->where("name", 'ilike', "%".$request->q."%");
        }
        
        $supplier->orderBy('id', 'desc');
        $list = $supplier->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);
        if(empty($request->mg))
            $list->appends(request()->mg);
        if(empty($request->clsf))
            $list->appends(request()->clsf);

        $params = array(
            'list' => $list,
        );
        return view('supplier.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate(request(), [
            'name'              => 'required',
            'contact_person'    => 'required',
        ]); 

        $rules = array(
            'name'              => 'required',
            'contact_person'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('supplier/create')->withErrors($validator);
        } else {
            $insert = Supplier::create([
                'name'              => $request->name,
                'contact_person'    => $request->name,
                'phone'             => $request->phone,
                'email'             => $request->email,
                'website'           => $request->website,
                'address_location'  => $request->supplier_address,
                'building'          => $request->building,
                'unit'              => $request->unit,
                'city'              => $request->city,
                'province'          => $request->province,
                'latitude'          => $request->latitude,
                'longitude'         => $request->longitude,
                'created_by'        => Auth::user()->id,
                'created_at'        => Carbon::Now()->format('Y-m-d H:i:s'),
            ]);
            if ($insert) {
                Session::flash('success', 'Data Successfully Added');
            } else {
                Session::flash('error', 'Data Failed to Add');
            }
            return redirect('supplier');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $items = Supplier::where('id','=',$id)
         ->with([
             'userUpdated',
             'userCreated',
        ])
         ->first();
        return view('supplier.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = Supplier::find($id);
        return view('supplier.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'              => 'required',
            'contact_person'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('supplier/'.$id.'/edit')->withErrors($validator);
        } else {
            $update                     = Supplier::find($id);
            $update->name               = $request->name;
            $update->contact_person      = $request->contact_person;
            $update->phone              = $request->phone;
            $update->email              = $request->email;
            $update->address_location   = $request->address;
            $update->building           = $request->building;
            $update->unit               = $request->unit;
            $update->city               = $request->city;
            $update->province           = $request->province;
            $update->website            = $request->website;
            $update->latitude           = $request->latitude;
            $update->longitude          = $request->longitude;
            $update->updated_by         = Auth::user()->id;
            $update->updated_at         = Carbon::Now()->format('Y-m-d H:i:s');
            $update->save();

            if ($update) {
                Session::flash('success', 'Data has been changed');
            } else {
                Session::flash('error', 'Data failed to change');
            }
            return redirect('supplier');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Supplier::find($id);

        $delete->updated_by = Auth::user()->id;
        $delete->deleted_at = Carbon::Now()->format('Y-m-d H:i:s');
        
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data telah dihapus');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('supplier');
    }
    public function autocompletebuilding(Request $request)
    {
        $data = Supplier::select("building as name")->where("building","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
        var_dump($data);
    }
    public function autocompleteunit(Request $request)
    {
        $data = Supplier::select("unit as name")->where("unit","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
        var_dump($data);
    }

    public function show_ajax(Request $request)
    {
       if($request->has('q'))
       {
           $data = Supplier::where('name','ilike', '%'.$request->q.'%')
           ->whereNull('deleted_at')
           ->limit(5)
           ->get();
       }
       else
       {
           $data = Supplier::whereNull('deleted_at')
           ->limit(5)
           ->orderBy('created_at','desc')
           ->get();
       }
       return $data;
   }
}
