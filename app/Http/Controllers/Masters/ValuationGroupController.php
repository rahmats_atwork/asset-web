<?php

namespace App\Http\Controllers;

use App\Models\Masters\Supplier;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use App\Models\Masters\ValuationGroup;

class ValuationGroupController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $list = ValuationGroup::orderBy('id', 'asc')
            ->get();
        $params = array(
            'list' => $list,
        );
        return view('valuationgroup.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('valuationgroup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code'    => 'required|unique:valuation_group',
            'name'    => 'required|unique:valuation_group',
            // 'supplier_address'  => 'required',
            // 'latitude'          => 'required',
            // 'longitude'         => 'required',
        );
        $this->validate(request(), [
            'code'    => 'required|unique:valuation_group',
            'name'    => 'required|unique:valuation_group',
        ]);
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //return $request->all();
            return redirect('valuationgroup/create')->withErrors($validator);
        } else {
            $insert = ValuationGroup::create([
                'code'     => $request->code,
                'name'     => $request->name,
                'created_by'         => Auth::user()->id,
            ]);
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Gagal Tambah Data');
            }
            return redirect('valuationgroup');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $items  = ValuationGroup::find($id);
        return view('valuationgroup.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = ValuationGroup::find($id);
        return view('valuationgroup.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'code'    => 'required',
            'name'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('valuationgroupe/'.$id.'/edit')->withErrors($validator);
        } else {
            $update             = ValuationGroup::find($id);
            $update->code       = $request->code;
            $update->name       = $request->name;
            $update->updated_by = Auth::user()->id;

            $update->save();

            if ($update) {
                Session::flash('success', 'Data edited');
            } else {
                Session::flash('error', 'Gagal rubah data');
            }
            return redirect('valuationgroup');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = ValuationGroup::find($id)->delete();
    	return back();
    }

    public function show_ajax(Request $request)
    {
        if($request->has('q'))
            {
                $data = ValuationGroup::select('id','name')
                ->where('name','ilike','%'.$request->q.'%')
                ->orwhere('code','ilike','%'.$request->q.'%')
                ->limit(4)
                ->get();
            }
        else
            {
                $data = ValuationGroup::orderBy('created_at','desc')
                ->limit(4)
                ->get();
            }
        
        return $data;
    }
}
