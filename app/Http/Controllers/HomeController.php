<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AssetClassification;
use App\Models\Masters\Location;
use App\Models\Masters\LocationType;
use App\Models\Masters\Plant;
use App\Models\Masters\Classification;
use App\Models\Masters\ClassificationParameter;
use App\Models\Masters\Supplier;
use App\Models\Masters\Material;
use App\Models\Masters\AssetStatus;
use App\Models\Masters\AssetType;
use App\Models\Masters\MaterialGroup;
use App\Models\Masters\ValuationGroup;
use App\Models\Masters\RetiredReason;
use App\Models\Transaction\Asset;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        //Master Data Asset
        //tabel tim_serpo
        $countTimSerpo = Location::whereNull('deleted_at')->count();
        //tabel master_supplier
        $countMasterSupplier = Supplier::whereNull('deleted_at')->count();
        //Setting Master Data
        //tabel region
        $countRegion = Plant::whereNull('deleted_at')->count();
        //tabel location_type
        $countLocationType = LocationType::whereNull('deleted_at')->count();

        //Asset
        //tabel classification_asset
        $countClassification = Classification::count();
        //tabel master_material
        $countMasterMaterial = Material::count();
        //tabel master_asset
        $countMasterAsset = Asset::count();
        //Setting Asset
        //tabel material_group
        $countMaterialGroub = MaterialGroup::count();
        //tabel asset_type
        $countAssetType = AssetType::count();
        //tabel valuation_group
        $countValuationGroup = ValuationGroup::count();
        //tabel status_asset
        $countStatusAsset = AssetStatus::count();
        //tabel retired_reason
        $countRetiredReason = RetiredReason::count();

        $List = [
            'countTimSerpo'=>$countTimSerpo
            ,'countMasterSupplier'=>$countMasterSupplier

            ,'countRegion'=>$countRegion
            ,'countLocationType'=>$countLocationType

            ,'countClassification'=>$countClassification
            ,'countMasterMaterial'=>$countMasterMaterial
            ,'countMasterAsset'=>$countMasterAsset

            ,'countMaterialGroup'=>$countMaterialGroub
            ,'countAssetType'=>$countAssetType
            ,'countValuationGroup'=>$countValuationGroup
            ,'countStatusAsset'=>$countStatusAsset
            ,'countRetiredReason'=>$countRetiredReason
            
    ];

        return view('home', $List);
    }
}
