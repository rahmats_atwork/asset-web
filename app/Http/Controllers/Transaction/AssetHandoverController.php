<?php

namespace App\Http\Controllers\Transaction;

use Auth;
use DB;
use Exception;
use Session;
use Validator;
use TransactionHelpers;
use PDF;
use QRCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config\Roles;
use App\Models\Config\Users;
use App\Models\Masters\Material;
use App\Models\Transaction\Asset;
use App\Models\Transaction\AssetHandoverHeader;
use App\Models\Transaction\AssetHandoverLine;
use App\Models\Masters\TransactionSeriesLine;
use App\Models\Transaction\AssetResponsiblePersonHistory;
use App\Http\Controllers\Transaction\AssetController;

class AssetHandoverController extends Controller
{
    public function index(Request $request)
    {

        $query = AssetHandoverHeader::with(['responsible', 'userCreated'])
            ->with(array('handoverLine' => function ($query) {
                $query->whereNull('deleted_at')->get();
            }))
            ->whereNull('deleted_at');


        if (!empty($request->status)) {
            $query->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->status); $i++) {
                    $query->orWhere('printed', $request->status[$i]);
                }
            });
        }

        if (!empty($request->documents)) {
            $query->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->documents); $i++) {
                    $query->orWhere('document_no', $request->document_no[$i]);
                }
            });
        }

        if (!empty($request->responsible)) {
            $q = $request->responsible;
            $query->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->responsible); $i++) {
                    $query->orWhere('id_responsible_person', '=', $request->responsible[$i]);
                }
            });
        }

        if (!empty($request->start_date)) {
            if (empty($request->end_date)) {
                $to = Carbon::parse($from)->addDays(1)->format('Y-m-d h:i:s');
            }
            $from = Carbon::parse($request->start_date)->format('Y-m-d h:i:s');
            $to = Carbon::parse($request->end_date)->format('Y-m-d h:i:s');
            $query->whereBetween('date', [$from, $to]);
        }

        if (!empty($request->q)) {
            $query->where(function ($query) use ($request) {
                $q = strtolower($request->q);
                $query->orwhereHas('responsible', function ($query) use ($q) {
                    $query->where('name', 'ilike', '%' . $q . '%');
                });
            });
        };

        $document       = $query->paginate(empty($request->limit) ? 10 : intval($request->limit));

        $data1 = [
            'documents'         => $document,
            'input_status'      => $request->status,
            'input_document'    => $request->document,
            'input_responsible' => $request->responsible,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'limit'             => $request->limit
        ];

        return view('asset_handover.index', $data1);
    }

    public function create()
    {
        return view('asset_handover.create');
    }

    public function destroy($id)
    {
        $data = [
            'updated_by'      => Auth::user()->id,
            'deleted_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        DB::beginTransaction();
        $update = AssetHandoverHeader::find($id)
            ->update($data);
        DB::commit();

        return back();
    }

    public function edit($id)
    {
        $header = AssetHandoverHeader::with(['responsible', 'userCreated', 'handoverLine'])
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $line   = AssetHandoverLine::with(['handoverHeader'])
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        return view('asset_handover.edit', \compact(['line', 'header']));
    }

    public function show($id)
    {
        $header = AssetHandoverHeader::with(['responsible', 'userCreated', 'handoverLine'])
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $line   = AssetHandoverLine::with(['handoverHeader'])
            ->where('id_asset_handover', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        return view('asset_handover.show', \compact(['line', 'header']));
    }

    public function storeHeader(Request $request)
    {

        $rules = array(
            'date_handover'       => 'required',
            'responsible_person'   => 'required',
        );

        $messages = array(
            'date_handover.required' => 'Hand Over date is required !!',
            'responsible_person.required' => 'Responsible Person is required!!'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'result'    => false,
                'message'   => $validator->errors()->first()
            ], 200);
        } else {
            try {
                $doc    = [
                    'module'              => 'asset-handover',
                    'transaction_date'    => $request->date_handover,
                ];

                $doc_no = TransactionHelpers::generateNotransaction($doc);

                $data   = [
                    'document_no'               => $doc_no['docNo'],
                    'date'                      => $request->date_handover,
                    'id_responsible_person'     => $request->responsible_person,
                    'description'               => $request->description ?? null,
                    'status'                    => 0,
                    'created_by'                => Auth::user()->id,
                    'updated_by'                => Auth::user()->id,
                    'updated_at'                => Carbon::now()->format('Y-m-d H:i:s'),
                    'created_at'                => Carbon::now()->format('Y-m-d H:i:s'),
                    'deleted_at'                => null,
                ];
                $updateLine = [
                    'last_no_used' => $doc_no['docNo'],
                    'updated_at'   => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_by'   => Auth::user()->id,
                ];
                DB::beginTransaction();

                $insert     = AssetHandoverHeader::create($data)->id;
                $updatetrx  = TransactionSeriesLine::where('id_transaction_series', '=', $doc_no['result']->id_transaction_series)
                    ->where('starting_no', '=', $doc_no['result']->starting_no)
                    ->update($updateLine);
                DB::commit();

                return response()->json([
                    'result'        => true,
                    'message'       => $insert,
                    'documentNo'    => $doc_no['docNo'],
                ], 201);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json([
                    'result'    => false,
                    'message'   => $e->getMessage(),
                ], 200);
            }
        }
    }

    public function updateHeader(Request $request)
    {

        $rules = array(
            'date_handover'       => 'required',
            'responsible_person'  => 'required',
            'document_no'         => 'required',
        );

        $messages = array(
            'date_handover.required'        => 'Hand Over date is required !!',
            'responsible_person.required'   => 'Responsible Person is required!!',
            'document_no.required'          => 'Document No. not found !!'
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'result'    => false,
                'message'   => $validator->errors()->first()
            ], 200);
        } else {
            try {
                $data = [
                    'date'                      => $request->date_handover,
                    'id_responsible_person'     => $request->responsible_person,
                    'description'               => $request->description ?? null,
                    'updated_by'                => Auth::user()->id,
                    'updated_at'                => Carbon::now()->format('Y-m-d H:i:s'),
                ];
                DB::beginTransaction();
                $update = AssetHandoverHeader::where('id', '=', $request->document_no)
                    ->update($data);
                DB::commit();

                /* Save Document No to Session */
                $request->session()->put('id_asset_handover', $request->document_no);

                return response()->json([
                    'result'    => true,
                    'message'   => 'Update data is successfully.'
                ], 201);
            } catch (Exception $e) {
                DB::rollback();
                return response()->json([
                    'result'    => false,
                    'message'   => $e->getMessage()
                ], 200);
            }
        }
    }

    public function storeLine(Request $request)
    {
        try {
            $dataLine = [
                'id_asset'           => $request->asset,
                'id_asset_handover'  => $request->document,
                'created_at'         => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
            ];

            $dataHeader = [
                'updated_by'         => Auth::user()->id,
                'updated_at'         => Carbon::now()->format('Y-m-d H:i:s'),
            ];


            $data       = AssetHandoverLine::where('id_asset', '=', $request->asset)
                ->where('id_asset_handover', '=', $request->document)
                ->whereNull('deleted_at')
                ->count();

            if ($data == 0) {
                DB::beginTransaction();
                $insertLine     = AssetHandoverLine::create($dataLine)->id;
                $updateHeader   = AssetHandoverHeader::where('id', '=', $request->document)->update($dataHeader);
                DB::commit();

                return response()->json([
                    'result'    => true,
                    'message'   => $insertLine,
                ], 201);
            } else {
                return response()->json([
                    'result'    => false,
                    'message'   => "You can't insert duplicate asset in the same document.",
                ], 200);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response()->json([
                'result'    => false,
                'message'   => $e->getMessage()
            ], 200);
        }
    }

    public function destroyLine($id)
    {
        $data = [
            'deleted_at'      => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        DB::beginTransaction();
        $update = AssetHandoverLine::find($id)->update($data);
        if ($update == true) {
            DB::commit();
            return response()->json([
                'result'    => true,
                'message'   => $update,
            ], 200);
        } else {
            DB::rollback();
            return response()->json([
                'result'    => false,
                'message'   => $update,
            ], 200);
        }
    }

    public function documentList(Request $request)
    {

        if ($request->has('q')) {
            $data = AssetHandoverHeader::where('document_no', 'ilike', '%' . $request->q . '%')
                ->whereNull('deleted_at')
                ->limit(10)
                ->get();
        } else {
            $data = AssetHandoverHeader::whereNull('deleted_at')
                ->limit(10)
                ->get();
        }

        return $data;
    }

    public function assetHandover(Request $request)
    {

        $line   = AssetHandoverLine::with(['asset', 'handoverHeader'])
            ->where('id_asset_handover', '=', $request->id)
            ->whereNull('deleted_at')
            ->get();

        $count_false = 0;
        $asset_list  = [];

        DB::beginTransaction();
        foreach ($line as $l) {
            $last_maintenance = carbon::parse($l->handoverHeader->date)->format('Y-m-d');
            $resp             = Asset::where('id_asset', '=', $l->id_asset)->first();

            if (intval($resp->id_type_asset) == '11') {
                $dataAsset = [
                    'last_maintenance'      =>  $last_maintenance,
                    'next_maintenance'      =>  carbon::parse($last_maintenance)->addYears(1),
                    'id_responsible_person' =>  $l->handoverHeader->id_responsible_person,
                    'updated_by'            =>  Auth::user()->id,
                    'updated_at'            =>  Carbon::now()->format('Y-m-d H:i:s'),
                ];
            } else {
                $dataAsset = [
                    'last_maintenance'      =>  $last_maintenance,
                    'next_maintenance'      =>  carbon::parse($last_maintenance)->addYears(2),
                    'id_responsible_person' =>  $l->handoverHeader->id_responsible_person,
                    'updated_by'            =>  Auth::user()->id,
                    'updated_at'            =>  Carbon::now()->format('Y-m-d H:i:s'),
                ];
            }


            $dataHistory    = [
                'id_asset'      => $resp->id_responsible_person,
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                'deleted_at'    => null,
                'updated_at'    => Carbon::now()->format('Y-m-d H:i:s'),
                'created_by'    => Auth::user()->id,
                'updated_by'    => Auth::user()->id,
            ];

            $count       = Asset::where('id_asset', $l->id_asset)
                ->where('id_responsible_person', '=', '2')
                ->count();

            if ($count > 0) {
                $updateAsset = Asset::where('id_asset', '=', $l->id_asset)
                    ->where('id_responsible_person', '=', '2')
                    ->update($dataAsset);

                $createHistory = AssetResponsiblePersonHistory::create($dataHistory);
            } else {
                $dataFalse  = [
                    'id_asset' => $l->id_asset,
                    'code'     => $l->asset->code_asset,
                ];
                array_push($asset_list, $dataFalse);
                $count_false++;
            }
        }

        if ($count_false > 0) {
            DB::commit();
            return response()->json([
                'result'    => false,
                'message'   => $count_false,
                'asset'     => json_encode($asset_list),
            ], 200);
        } else {
            DB::commit();
            return response()->json([
                'result'    => true,
                'message'   => true,
            ], 200);
        }
    }

    public function pdfHandover($id)
    {

        $data = AssetHandoverHeader::with(['responsible', 'userCreated', 'handoverLine'])
            ->where('id', '=', $id)
            ->whereNull('deleted_at')
            ->first();
        $line = AssetHandoverLine::with(['asset'])
            ->where('id_asset_handover', '=', $id)
            ->whereNull('deleted_at')
            ->get();


        $pdf        = PDF::loadview('pdf.handover', compact(['data', 'line']))->setPaper('a5', 'potrait');
        $pdf->output();
        $dom_pdf    = $pdf->getDomPDF();
        $canvas     = $dom_pdf->get_canvas();
        $canvas->page_text(498, 800, "Page {PAGE_NUM} / {PAGE_COUNT}", null, 10, array(0, 0, 0));
        $dataUpdate = [
            'printed'       => 1,
            'count_printed' => $data->count_printed + 1,
        ];
        $update     = AssetHandoverHeader::where('id', '=', $id)->update($dataUpdate);
        $printedPDF = $pdf->download('[' . $data->responsible->name . ' - ' . $data->responsible->nik . ']' . $data->document_no . Carbon::now()->format('dmYhis') . '.pdf');
        return $printedPDF;
    }
}
