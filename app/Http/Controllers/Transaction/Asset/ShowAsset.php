<?php

namespace App\Http\Controllers\Transaction\Asset;

use App\Models\Transaction\Asset;
use App\Models\Transaction\AssetClassification;
use App\Models\Transaction\AssetImage;
use App\Models\Transaction\AssetImage360;
use App\Models\Transaction\AssetLocationHistory;
use App\Models\Transaction\AssetResponsiblePersonHistory;
use App\Models\Transaction\AssetDepreciationHistory;
use App\Models\Transaction\AssetHandoverHeader;
use App\Models\Transaction\AssetHandoverLine;

class ShowAsset
{
    public static function call($id) {
        
        $person_history         = AssetResponsiblePersonHistory::with(['user','userCreated'])
                                ->where('id_asset',$id)
                                ->orderBy('created_at', 'desc')
                                ->get();

        $location_history       = AssetLocationHistory::with(['location', 'userCreated'])
                                ->where('id_asset',$id)
                                ->orderBy('created_at', 'desc')
                                ->get();
        $depreciation_history   = AssetDepreciationHistory::where('id_asset',$id)
                                ->where('is_active','=','1')
                                ->orderBy('sequence', 'asc')
                                ->get();                    

        $list                   = Asset::with(['asset_status','depreciation','location','old_asset','parent_asset','plant_asset','responsible_person_asset','supplier','type','userCreated','userUpdated','valuation',])
                                ->where('id_asset','=',$id)
                                ->first();

        $items                  = Asset::find($id);

        $level_asset            = Asset::where('level_asset',1)->get();
        
        return view('asset.show', [ 'list' => $list, "level_asset" => $level_asset, "location_history" => $location_history, "person_history" => $person_history, 'items' => $items, 'depreciation_history' => $depreciation_history]);
    }
}
