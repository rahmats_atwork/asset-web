<?php

namespace App\Http\Controllers\Transaction\Asset;

use DB;
use App\Models\Transaction\Asset;

class ShowDataHandoverAsset
{
    public static function call($typeList, $start, $end) {

        $data = Asset::groupBy('id_type_asset')
                ->groupBy('id_responsible_person')
                ->with(['type', 'responsible_person_asset'])
                ->with(['historyPersonal' => function ($query) use($start, $end) {
                    $query->whereBetween('created_at',[$start,$end])->where('id_responsible_person','=',2);
                }])
                ->whereIn('id_type_asset',$typeList)
                ->whereNotIn('id_responsible_person',[2,3,4181])
                ->get();

        return $data;

    }
}
