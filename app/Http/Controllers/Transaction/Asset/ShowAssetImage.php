<?php

namespace App\Http\Controllers\Transaction\Asset;

use App\Models\Transaction\AssetImage;

class ShowAssetImage
{
    public static function call($id) {
        $images = AssetImage::where('id_asset',$id)->get();
        return view('asset.partial_images')->withImages($images);
    }
}
