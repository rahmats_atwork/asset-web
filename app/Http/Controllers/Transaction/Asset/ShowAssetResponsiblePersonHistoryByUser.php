<?php

namespace App\Http\Controllers\Transaction\Asset;

use App\Models\Transaction\AssetResponsiblePersonHistory;

use Auth;
use Carbon\Carbon;
use DB;


class ShowAssetResponsiblePersonHistoryByUser
{
    public static function call($id) {

        $data = AssetResponsiblePersonHistory::where('id_responsible_person', $id)
                ->get();

        return $data;
    }
}
