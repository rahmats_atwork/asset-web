<?php

namespace App\Http\Controllers\Transaction\Asset;

use Excel;

class CreateTemplateUploadAsset
{
    public static function call() {

        $header = array(['No', 'Material Code', 'Asset Number','Description','Is_asset','Depreciation','Serial Number', 'Superior Asset Number',
            'Old Asset Number', 'Vendor', 'Plant', 'Valuation Group', 'Asset Type', 'Responsible_Person_NIK', 
            'Location', 'Status','Purchase Date','Purchase Price', 'Currency', 'Warranty End Date', 'Maintenance Cycle', 'MC Unit', 'Last Maintenance Date',
            'Retired Remarks','Retired Date','Retired Remarks']
        );

        $export = new \App\Exports\Excel\ExportFromArray($header);
        return Excel::download($export, 'Upload Asset.xlsx');
    }

}
