<?php

namespace App\Http\Controllers\Transaction\Asset;

use Auth;
use DB;
use Carbon\Carbon;
use App\Models\Transaction\Asset;
use App\Models\Masters\AssetType;

class CountHandOverAsset
{
    public static function call($type, $period) {

        $startPeriod = carbon::parse($period)->startOfMonth();
        $endPeriod = carbon::parse($period)->endOfMonth();

        $result = Asset::select(DB::raw("to_char(asset_responsible_person_history.created_at,'YYYY-MM') as periode,count(*) as countdata"))
        ->leftJoin('asset_responsible_person_history','asset.id_asset','=','asset_responsible_person_history.id_asset')
        ->leftJoin('asset_type','asset.id_type_asset','=','asset_type.id')
        ->where('asset.id_type_asset','=',$type)
        ->whereBetween('asset_responsible_person_history.created_at',[$startPeriod,$endPeriod])
        ->where('asset_responsible_person_history.id_responsible_person','=',2)
        ->whereNotIn('asset.id_responsible_person',[2,3,4181])
        ->groupBy('asset_type.name','periode')
        ->orderBy('asset_type.name','ASC')  
        ->first();

        return $result['countdata'] ?? 0;

    } 

    /* Old Coding

    $result = Asset::select(DB::raw("asset_type.name as asset_type_name, to_char(asset_responsible_person_history.created_at,'YYYY-MM') as periode,count(*) as sum"))
        ->Join('asset_responsible_person_history','asset.id_asset','=','asset_responsible_person_history.id_asset')
        ->Join('asset_type','asset.id_type_asset','=','asset_type.id')
        ->whereIn('asset.id_type_asset',$assetType)
        ->whereBetween('asset_responsible_person_history.created_at',[$start,$end])
        ->where('asset_responsible_person_history.id_responsible_person','=',2)
        ->whereNotIn('asset.id_responsible_person',[2,3])
        ->groupBy('asset_type_name','periode')
        ->orderBy('asset_type_name','ASC')  
        ->get();

    */
}
