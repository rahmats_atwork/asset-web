<?php

namespace App\Http\Controllers\Transaction\Asset;

use App\Models\Transaction\AssetImage;

class DeleteAssetImage
{
    public static function call($id) {

        $image = AssetImage::where('id_image_asset',$id)->firstOrFail();

        $path = str_replace('storage/','public/',$image->image_asset);

        if(\Storage::delete($path)) {
            $image->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'Success delete image'
            ],200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'error delete image'
            ],406);
        }
    }
}
