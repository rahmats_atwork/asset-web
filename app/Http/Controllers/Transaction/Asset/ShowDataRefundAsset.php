<?php

namespace App\Http\Controllers\Transaction\Asset;

use DB;
use App\Models\Transaction\Asset;

class ShowDataRefundAsset
{
    public static function call($typeList, $start, $end) {

        $excel  = Asset::select(DB::raw("asset_type.name as asset_type,asset.retired_date as date_transaction,users.name as responsible,users.nik as nik_responsible,count(*) as sum"))
        ->leftJoin('asset_responsible_person_history','asset.id_asset','=','asset_responsible_person_history.id_asset')
        ->leftJoin('users','asset_responsible_person_history.id_responsible_person','=','users.id')
        ->leftJoin('asset_type','asset.id_type_asset','=','asset_type.id')
        ->whereIn('asset.id_type_asset',$typeList)
        ->whereBetween('asset.retired_date',[$start,$end])
        ->whereNotIn('asset_responsible_person_history.id_responsible_person',[2,3,4181])
        ->whereIn('asset.id_responsible_person',[4181])
        ->groupBy('asset_type.name','date_transaction','users.name','nik_responsible')
        ->orderBy('asset_type.name','ASC')
        ->get();

        return $excel;
    }



}
