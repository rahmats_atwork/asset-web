<?php

namespace App\Http\Controllers\Transaction\Asset;

use Auth;
use DB;
use Carbon\Carbon;
use App\Models\Transaction\Asset;
use App\Models\Masters\AssetType;

class CountRefundAsset
{
    public static function call($type, $period) {

        $startPeriod = carbon::parse($period)->startOfMonth();
        $endPeriod = carbon::parse($period)->endOfMonth();

        $result = Asset::select(DB::raw("to_char(asset.retired_date,'YYYY-MM') as periode,count(*) as countdata"))
        ->leftJoin('asset_responsible_person_history','asset.id_asset','=','asset_responsible_person_history.id_asset')
        ->leftJoin('asset_type','asset.id_type_asset','=','asset_type.id')
        ->where('asset.id_type_asset','=',$type)
        ->whereBetween('asset.retired_date',[$startPeriod,$endPeriod])
        ->whereNotIn('asset_responsible_person_history.id_responsible_person',[2,3,4181])
        ->where('asset.id_responsible_person','=',4181)
        ->groupBy('asset_type.name','periode')
        ->orderBy('asset_type.name','ASC')  
        ->first();
    
        return $result['countdata'] ?? 0;

    }

}
