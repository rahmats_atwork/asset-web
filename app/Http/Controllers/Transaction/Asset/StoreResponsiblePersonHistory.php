<?php

namespace App\Http\Controllers\Transaction\Asset;

use App\Models\Transaction\AssetResponsiblePersonHistory;

use Auth;
use Carbon\Carbon;
use DB;


class StoreResponsiblePersonHistory
{
    public static function store($request) {
        $data = [
            'id_asset'              => $request['id'],
            'id_responsible_person' => $request['responsible'],
            'deleted_at'            => Carbon::now()->format('Y-m-d H:i:s'),
            'created_by'            => Auth::user()->id,
            'created_at'            => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at'            => Carbon::now()->format('Y-m-d H:i:s'),
        ];

        AssetResponsiblePersonHistory::create($data);
        return true;
    }
}
