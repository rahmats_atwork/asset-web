<?php

namespace App\Http\Controllers\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\Transaction\Report\AnalystPeriodicReport;
use App\Http\Controllers\Transaction\Report\GraphPeriodicReport;
use App\Http\Controllers\Transaction\Report\StockCardReport;
use App\Http\Controllers\Transaction\Report\DueUsageReport;
use App\Http\Controllers\Transaction\Report\AssetHandoverReport;

use App\Models\Transaction\Asset;



class ReportController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function PeriodicMovement() {
        return view('report.periodic_movement');
    }

    public function AssetHandover() {
        return view('report.asset_handover');
    }

    public function StockCard(Request $request) {
        $stock  = StockCardReport::Execute($request);
        return view('report.stock_card',['stock' => $stock]);
    }

    public function ExpiryUsage() {
        return view('report.expiry_usage');
    }

    public function ShowDataExpiryUsage(Request $request) {
        return DueUsageReport::execute($request);
    }

    public function ShowDataAssetHandover(Request $request) {
        return AssetHandoverReport::execute($request);
    }

    public function DataExcelPeriodicAsset (Request $request) {
        return AnalystPeriodicReport::call($request);
    }

    public function DataGraphPeriodicAsset (Request $request) {
        return GraphPeriodicReport::call($request);
    }

}
