<?php

namespace App\Http\Controllers\Masters;

use App\Http\Controllers\Controller;
use App\Models\Masters\AssetStatus;
use App\Models\Masters\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Auth;
use Session;
use Validator;

class AssetStatusController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $assetstatus = AssetStatus::whereNull('deleted_at');
        if(!empty($request->q))
            $assetstatus->where('name','ilike', '%'.$request->q.'%');

        $assetstatus->orderBy('id', 'desc');
        $list = $assetstatus->paginate(empty($request->limit)?10:intval($request->limit));
        if(empty($request->q))
            $list->appends(request()->q);
        if(empty($request->limit))
            $list->appends(request()->limit);
        
        $params = array(
            'list' => $list,
        );
        return view('assetstatus.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assetstatus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code'    => 'required|unique:status_asset',
            'name'    => 'required|unique:status_asset',
        );
        $this->validate(request(), [
            'code'    => 'required|unique:asset_status',
            'name'    => 'required|unique:asset_status',
        ]);
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            //return $request->all();
            return redirect('assetstatus/create')->withErrors($validator);
        } else {
            $insert = AssetStatus::create([
                'code'     => $request->code,
                'name'     => $request->name,
                'created_by'         => Auth::user()->id,
            ]);
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('assetstatus');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  use App\Models\Masters\AssetStatus  $assetstatus
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $items  = AssetStatus::find($id);
        return view('assetstatus.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  use App\Models\Masters\AssetStatus  $assetstatus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = AssetStatus::find($id);
        return view('assetstatus.edit', compact('items'));
    }

    public function ajaxStatus()
    {
        $data = AssetStatus::whereNull('deleted_at')->get();
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  use App\Models\Masters\AssetStatus  $assetstatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'code'    => 'required|unique:asset_status,code,'. $id .'',
            'name'    => 'required|unique:asset_status,name,'. $id .'',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('assetstatus/'.$id.'/edit')->withErrors($validator);
        } else {
            $update           = AssetStatus::find($id);
            $update->code     = $request->code;
            $update->name     = $request->name;
            $update->updated_by     = Auth::user()->id;

            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('assetstatus');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  use App\Models\Masters\AssetStatus  $assetstatus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = AssetStatus::find($id);
        $delete->deleted_at = Carbon::now()->format('Y-m-d H:i:s');
        $delete->updated_by = Auth::user()->id;
        $delete->save();

        if ($delete) {
            Session::flash('success', 'Data Deleted');
        } else {
            Session::flash('error', 'Gagal Hapus Data');
        }
        return redirect('assetstatus');
    }
}
