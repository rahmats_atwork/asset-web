<?php

namespace App\Http\Controllers\Transaction;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Masters\DepreciationController;
use App\Http\Controllers\Transaction\Asset\StoreResponsiblePersonHistory;
use App\Http\Controllers\Transaction\Asset\ShowAsset as ShowAssetbyID;
use App\Http\Controllers\Transaction\Asset\ShowAssetImage as ShowImageAssetByID;
use App\Http\Controllers\Transaction\Asset\DeleteAssetImage as DeleteImageAssetByID;
use App\Http\Controllers\Transaction\Asset\CreateTemplateUploadAsset as CreateTemplateUpload;
use App\Http\Controllers\Transaction\Asset\ShowAssetResponsiblePersonHistoryByUser;



use DB;
use Validator;
use Auth;
use Session;
use Excel;
use Carbon\Carbon;
use App\Models\Config\Users;
use App\Models\Config\ConfigBarcode;
use App\Models\Config\Roles;
use App\Models\Masters\AssetStatus;
use App\Models\Masters\AssetType;
use App\Models\Masters\ClassificationParameter;
use App\Models\Masters\Depreciation;
use App\Models\Masters\Material;
use App\Models\Masters\MaterialGroup;
use App\Models\Masters\MaterialParameter;
use App\Models\Masters\Location;
use App\Models\Masters\LocationType;
use App\Models\Masters\ValuationGroup;
use App\Models\Masters\Plant;
use App\Models\Masters\Supplier;
use App\Models\Transaction\Asset;
use App\Models\Transaction\AssetClassification;
use App\Models\Transaction\AssetImage;
use App\Models\Transaction\AssetImage360;
use App\Models\Transaction\AssetLocationHistory;
use App\Models\Transaction\AssetResponsiblePersonHistory;
use App\Models\Transaction\AssetDepreciationHistory;
use App\Models\Transaction\AssetHandoverHeader;
use App\Models\Transaction\AssetHandoverLine;


use PDF;


class AssetController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request){
        if(Roles::find(Auth::user()->id_role)->special != 'all-access'){
            $query = Asset::with(['type','location','responsible_person_asset','plant_asset','supplier','material'])
                ->whereNull('deleted_at')
                ->where('valuation_group', Auth::user()->id_role)
                ->orderBy('id_asset', 'desc');
        }
        else
        {
            $query = Asset::whereNull('deleted_at')
            ->orderBy('id_asset', 'desc');
        }    

        if(!empty($request->status)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->status); $i++ ){
                    $query->orWhere('status', $request->status[$i]); 
                }
            });
        }

        if(!empty($request->plant)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->plant); $i++ ){
                    $query->orWhere('id_plant', $request->plant[$i]); 
                }
            });
        }

        if(!empty($request->responsible)){
            $q = $request->responsible;
            $query->where(function($query) use ($q) {
                $query->where("id_responsible_person",'=',$q);
            });
        }

        if(!empty($request->vendor)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->vendor); $i++ ){
                    $query->orWhere('id_supplier', $request->vendor[$i]); 
                }
            });
        }

        if(!empty($request->material)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->material); $i++ ){
                    $query->orWhere('id_material', $request->material[$i]); 
                }
            });
        }

        if(!empty($request->type)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->type); $i++ ){
                    $query->orWhere('id_type_asset', $request->type[$i]); 
                }
            });
        }

        if(!empty($request->valuation)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->valuation); $i++ ){
                    $query->orWhere('valuation_group', $request->valuation[$i]); 
                }
            });
        }

        if(!empty($request->superior)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->superior); $i++ ){
                    $query->orWhere('id_superior_asset', $request->superior[$i]); 
                }
            });
        }

        if(!empty($request->location)){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->location); $i++ ){
                    $query->orWhere('id_location', $request->location[$i]); 
                }
            });
        }

        if(!empty($request->start_date)){
            $from = Carbon::parse($request->start_date)->format('Y-m-d h:i:s');
            if (empty($request->end_date)) {
                $to = Carbon::parse($from)->addDays(1)->format('Y-m-d h:i:s');
            }
            $to = Carbon::parse($request->end_date)->format('Y-m-d h:i:s');
            $query->whereBetween('created_at',[$from,$to]);

        }

        if(!empty($request->q)){
            $query->where(function($query) use ($request){
                $q = strtolower($request->q);
                $query->orWhere(DB::raw("LOWER(code_asset)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(description_asset)"), 'LIKE', "%".$q."%");
                $query->orWhere(DB::raw("LOWER(serial_number)"), 'LIKE', "%".$q."%");
                $query->orwhereHas('plant_asset',function($query) use ($q) {
                    $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                });
                $query->orwhereHas('responsible_person_asset',function($query) use ($q) {
                    $query->where(DB::raw("LOWER(name)"), 'LIKE', "%".$q."%");
                });
            });

            $q = strtolower($request->q);
            if ($q === 'hr' | $q === 'ga') {
                $query->WhereHas('valuation', function($query) use ($q){                
                    $query->orWhere(DB::raw("LOWER(roles.name)"), 'LIKE', "%".$q."%");
                });
            }
        }

        $assets = $query->paginate(empty($request->limit)?10:intval($request->limit));
        $plants     =  Plant::whereNull('deleted_at')->get();
        $status      = AssetStatus::All();
        $responsible = Users::All();
        $supplier    = Supplier::whereNull('deleted_at')->get();
        $type        = AssetType::All();
        $valuation   = Roles::where('id', '!=', 1)->get();        
        $location    = Location::whereNull('deleted_at')->get();

        $data = [
            'assets'            => $assets,
            'valuation'         => $valuation,
            'plants'            => $plants,
            'status'            => $status,
            'supplier'          => $supplier,
            'type'              => $type,
            'valuation'         => $valuation,
            'location'          => $location,
            'responsible'       => $responsible,
            'input_plant'       => $request->plant,
            'input_status'      => $request->status,
            'input_vendor'      => $request->vendor,
            'input_type'        => $request->type,
            'input_valuation'   => $request->valuation,
            'input_material'    => $request->material,
            'input_superior'    => $request->superior,
            'input_location'    => $request->location,
            'input_responsible' => $request->responsible,
            'start_date'        => $request->start_date,
            'end_date'          => $request->end_date,
            'limit'             => $request->limit
        ];
        
        return view('asset.index_new', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('asset.create', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'code_asset'    => 'required|unique:asset',
            'id_material'   => 'required',
        );

        $this->validate(request(), [
            'code_asset'    => 'required|unique:asset',
            'id_material'   => 'required',
        ]);

        $validator = Validator::make($request->all(), $rules);
        $superior  = $request->id_superior_asset;
        $match = Asset::where('id_asset',$superior)->first();
        
        if (!$match){
            $level = 1;

           }else{
                $level = $match->level_asset + 1;
           }
        $time = $request->time_cycle;
        $unit = $request->unit_cycle;
        if ($unit==4){
            $time = $time;
        }elseif($unit==5){
             $time = $time*7;
        }elseif($unit==6){
             $time = $time*30;
        }else{
            $time = $time*365;
        }

        if(empty($request->depreciation)){
            $is_asset = NULL;
        }
        else {
            $is_asset = 1;
        }

        if ($validator->fails()) {
            return redirect('asset/create')->withErrors($validator);
        } else {
            try{
                $data = [
                    'code_asset'            => $request->code_asset,
                    'id_material'           => $request->id_material,
                    'description_asset'     => $request->desc_assets,
                    'purchase_date'         => $request->purchase_date,
                    'id_depreciation'       => $request->depreciation,
                    'is_asset'              => $is_asset,
                    'purchase_cost'         => $request->purchase_cost,
                    'waranty_finish'        => $request->warranty_finish,
                    'id_supplier'           => $request->supplier,
                    'serial_number'         => $request->serial_num,
                    'level_asset'           => $level,
                    'id_superior_asset'     => $request->id_superior_asset,
                    'count_duration'        => $time,
                    'id_location'           => $request->location,
                    'id_plant'              => $request->plant,
                    'id_type_asset'         => $request->type_assets,
                    'old_asset'             => $request->old_asset,
                    'status'                => $request->status,
                    'curency'               => $request->curency,
                    'valuation_group'       => $request->valuation_group, 
                    'id_responsible_person' => $request->responsible_person,
                    'id_location'           => $request->id_location,
                    'created_by'            => Auth::user()->id,
                ];
                DB::beginTransaction();
                $insert = Asset::create($data)->id_asset;
                $param = [
                    'is_asset' => $is_asset,
                    'id_depreciation'   => $request->depreciation,
                    'purchase_cost'     => $request->purchase_cost,
                    'purchase_date'     => $request->purchase_date,
                    'id_asset'          => $insert,
                ];
                $depreciate = new DepreciationController(); 
                $depreciate->calculateDepreciation($param);
                DB::commit();
                Session::flash('success', 'Data Successfully Added '.$insert);
                
            }
            catch(\Exception $e) {
                \DB::rollback();
                Session::flash('error', 'Data Failed to Add. Error :'.$e->getMessage() );
            }
            return redirect('asset');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        return ShowAssetbyID::call($id);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */



    public function show_loc_history($request)
    {
        $asset_location_history  = \DB::table('asset_location_history')
        ->select('asset_location_history.*', 'users.name as name_user','location.name')
        ->leftjoin('users', 'users.id', '=', 'asset_location_history.create_by')
        ->leftjoin('location', 'location.id', '=', 'asset_location_history.id_location')
        ->where('asset_location_history.id_asset',$request)
        ->orderBy('asset_location_history.created_at', 'desc')
        ->get();
    }

    public function edit($id_asset)
    {
        $items = Asset::whereNull('deleted_at')
                ->where('id_asset','=',$id_asset)
                ->with(
                    [
                        'userCreated',
                        'userUpdated',
                        'supplier',
                        'type',
                        'asset_status',
                        'material',
                        'old_asset',
                        'parent_asset',
                        'plant_asset',
                        'location',
                        'valuation',
                        'retiredReason',
                        'depreciation',
                    ]
                    )
                ->first();
        $person_history  = AssetResponsiblePersonHistory::where('id_asset',$id_asset)
                        ->with(array('user'=>function($query){
                            $query->select('name as name_user')->get();
                        }))
                        ->orderBy('created_at', 'desc')
                        ->get();
                      
        return view('asset.edit', ['items'=> $items,'person_history'=>$person_history]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id_asset)
    {
         $rules = array (
            'id_material'   => 'required',
            'code_asset'    => 'required',
        );
        
        $validator = Validator::make($request->all(), $rules);
        $superior  = $request->id_superior_asset;
        $match = Asset::where('id_asset',$superior)->first();
        
        if (!$match){
            $level = 1;

           }else{
                $level = $match->level_asset + 1;
           }

        $time = $request->time_cycle;
        $unit = $request->unit_cycle;
        if ($unit==4){
            $time = $time;
        }elseif($unit==5){
             $time = $time*7;
        }elseif($unit==6){
             $time = $time*30;
        }else{
            $time = $time*365;
        }

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            try {
                \DB::beginTransaction();
                $depreciate = new DepreciationController(); 
                $depreciate->deleteCalculatedepreciation($id_asset);

                $update                    = Asset::find($id_asset);
                $update->code_asset        = $request->code_asset;
                $update->id_material       = $request->id_material;
                $update->description_asset = $request->desc_assets;
                /*
                * Tidak bisa edit Purchase Date 
                
                */
                //$update->id_depreciation   = $request->depreciation;
                //$update->purchase_date     = $request->purchase_date;
                //$update->purchase_cost     = $request->purchase_cost;
                $update->waranty_start     = $request->warranty_start;
                $update->waranty_finish    = $request->warranty_finish;
                $update->id_supplier       = $request->supplier;
                $update->serial_number     = $request->serial_num;
                $update->level_asset       = $level;
                $update->id_superior_asset = $request->id_superior_asset;
                $update->count_duration    = $time;
                $update->unit_duration     = $request->unit_cycle;
                $update->cycle_schedule    = $request->unit_cycle;
                $update->id_location       = $request->location;
                $update->id_plant          = $request->plant;
                $update->id_type_asset     = $request->type_assets;
                $update->old_asset         = $request->old_asset;
                $update->status            = $request->status;
                $update->curency           = $request->curency;
                $update->valuation_group   = $request->valuation_group; 
                $update->retired_reason    = $request->retired_reason;
                $update->retired_date      = $request->retired_date;
                $update->retired_ramarks   = $request->retired_ramarks;
                $update->last_maintenance  = $request->last_maintenance;
                $update->updated_by        = Auth::user()->id;
                $update->save();

                if(empty($request->depreciation)){
                    $is_asset = NULL;
                }
                else {
                    $is_asset = 1;
                }
        
                $dataasset = [
                    'id_asset'          => $id_asset,
                    'retired_date'      => $request->retired_date,
                    'purchase_cost'     => $request->purchase_cost,
                    'purchase_date'     => $request->purchase_date,
                    'status'            => $request->status,
                    'id_depreciation'   => $request->depreciation,
                    'is_asset'          => $is_asset
                ];
                $depreciate = new DepreciationController(); 
                $depreciate->calculateDepreciation($dataasset); 
                \DB::commit(); 
                Session::flash('success', 'Data has been changed.');
            }
            catch(\Exception $e) {
                \DB::rollBack();
                return back()->withError($e->getMessage());
            }
            return redirect('asset');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_asset)
    {
        $destroy = Asset::find($id_asset)->delete();

    	return back();
    }

    /**
     * Show Asset with Criteria.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    
    public function show_ajax(Request $request)
    {
        $role_user = Auth::user()->id_role;
        if($request->has('q')){
            if($role_user === 1)
            {
                $search = strtoupper($request->q);
                $data = Asset::where('code_asset','ilike','%'.$search.'%')
                ->where('description_asset','ilike','%'.$search.'%')
                ->limit(20)
                ->get();
            }
            else
            {
                $search = strtoupper($request->q);
                $data = Asset::where('code_asset','ilike','%'.$search.'%')
                ->where('description_asset','ilike','%'.$search.'%')
                ->where('valuation_group','=',$role_user)
                ->limit(20)
                ->get();
            }
        }else{
            if($role_user === 1)
            {
                $search = strtolower($request->q);
                $data = Asset::limit(20)->orderBy('created_at','desc')->get();
            }
            else
            {
                $search = strtolower($request->q);
                $data = Asset::where('valuation_group','=',$role_user)
                ->orderBy('created_at','desc')
                ->limit(20)
                ->get();
            }
        }
        return $data;
    }

    public function assetStock(Request $request)
    {
        $role_user = Auth::user()->id_role;
        if($request->has('q')) {
            $search = strtoupper($request->q);
            $data = Asset::where('code_asset','=',$search)
                ->where('id_responsible_person','=', '2')
                ->where('status',1)
                ->whereNull('deleted_at')
                ->limit(1)
                ->get();
            return $data;
        }
    }


    /**
     * Add Asset Classification.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */

    public function classification($id_asset)
    {
        // var_dump 
        $items  = Asset::find($id_asset);
        $material = Material::where('id',$items->id_material)->first();
        $material_parameter = MaterialParameter::where('id_material',$items->id_material)->get();

        $parameter = ClassificationParameter::where('id_classification',$material->id_classification)->get()
        ->map(function($param) use($material_parameter){
                foreach($material_parameter as $value){
                    if($value->id_parameter == $param->id){
                        $param->existing = $value->value;
                    }
                }

                return $param;
            });

        return view('asset.classification', compact('items', 'parameter', 'material_parameter'));
    }

    public function storevalue(Request $request)
    {
        $parameter = $request->input('parameter');
        $code_asset = $request->input('code_asset');
        if($parameter){
            foreach($parameter as $key => $value){
                // ID Parameter
                $id_parameter = explode("-",$key)[1];
                $value_parameter = $value;
                $insert = AssetClassification::updateOrCreate(
                    [
                        'code_asset'   => $code_asset,
                        'id_parameter'  => $id_parameter,
                    ],
                    [
                        'value_classification'         => $value_parameter,
                    ]
                );
            }
        }
        return redirect('asset');
    }

    public function upload(Request $request)
    {
        // Validate Form
        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        $array_excel    = new \App\Imports\Excel\ImportToArray();
        $ts             = \Maatwebsite\Excel\Facades\Excel::import($array_excel, $request->file('upload_file')->path(), NULL, \Maatwebsite\Excel\Excel::XLSX);
        
        $data = []; $dataOk = true;
        foreach($array_excel->sheetData[$array_excel->sheetNames[0]] as $row) {

            $isValid = true;
            $messages = [];            
            // EXCEL VALIDATION ------------------------------------------
            // check material
            $material = Material::with(['classification'])->where('name',$row['material_code'])->first();
            if( empty($material) ){
                $isValid = false;
                array_push($messages,"Material Code doesn't exists");
            }

            //cek code_asset
            if(empty($row['asset_number'])) {
                $isValid = false;
                array_push($messages,"Asset Number is required");
            } 
            else {
                $asset = Asset::where('code_asset',$row['asset_number'])->first();
                if( !empty($asset) ){
                    $isValid = false;
                    array_push($messages,"Asset Number ".$row['asset_number']." already exists");
                }
            }

            if(!empty($row['location'])) {
                $location = Location::where('name',$row['location'])->first();
                if(empty($location)){
                    $isValid = false;
                    array_push($messages,"Location ".$row['location']." doesn't exists");
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Vendor is required");
            }

            //cek superior field
            $level = 1;
            if(!empty($row['superior_asset_number'])){
                $superior = Asset::where('code_asset',$row['superior_asset_number'])->first();
                if (!empty($superior)){
                    $level = $match->level_asset + 1;
                }else{
                    $isValid = false;
                    array_push($messages,"Superior Asset Code doesn't exists"); 
                }
            }

            //cek old asset number field
            if(!empty($row['old_asset_number'])){
                $old_asset = Asset::where('code_asset',$row['old_asset_number'])->first();
            }

            //cek vendor field
            if(!empty($row['vendor'])){
                $vendor = Supplier::where('name',$row['vendor'])->first();
                if(empty($vendor)){
                    $isValid = false;
                    array_push($messages,"Vendor ".$row['vendor']." doesn't exists");
                }
            }
            else{
                $isValid = false;
                array_push($messages,"Vendor is required");
            }

            //cek plant / plant field
            if(empty($row['plant'])) {
                $isValid = false;
                array_push($messages,"Plant is required");
            } 
            else {
                $plant = plant::where('name','=',$row['plant'])->first();
                if(empty($plant)){
                    $isValid = false;
                    array_push($messages,"Plant ".$row['plant']." doesn't exists");
                }
            }

            //cek valuation group
            if(empty($row['valuation_group'])){
                $isValid = false;
                array_push($messages,"Valuation Group is required");
            }else{
                $valuation_group = Roles::where('name',$row['valuation_group'])->first();
                if(empty($valuation_group)){
                    $isValid = false;
                    array_push($messages,"Valuation Group ".$row['valuation_group']." doesn't exists");
                }
            }

            //cek asset type
            if(!empty($row['asset_type'])){
                $asset_type = AssetType::where('name',$row['asset_type'])->first();
                if(empty($asset_type)){
                    $isValid = false;
                    array_push($messages,"Asset Type ".$row['asset_type']." doesn't exists");
                }
            }
            else{
                $isValid = false;
                array_push($messages,"Asset Type is required");
            }

            //cek status asset
            if(empty($row['status'])){
                $isValid = false;
                array_push($messages,"Status is required");
            }else{
                $status = AssetStatus::where('name',$row['status'])->first();
                if(empty($status)){
                    $isValid = false;
                    array_push($messages,"Status ".$row['status']." doesn't exists");
                }
                else {
                    if($status->id == 2) {
                        if(!empty($row['retired_date'])) {
                            $retired_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['retired_date']));
                            if($retired_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                                $isValid = false;
                                array_push($messages,"Retired date is not valid");
                                $retired_date = null;
                            }
                        }
                        else {
                            array_push($messages,"Retired Date doesn't exists");
                        }
                    }
                }
            }

            if(!empty($row['purchase_price'])){
                if(is_numeric($row['purchase_price'])==false){
                        $isValid = false;
                        array_push($messages,$row['purchase_price']." is not a number");
                }
            }

            if(!empty($row['currency'])) {
                if($row['currency']=="IDR") {
                    $currency = 1;
                } 
                elseif ($row['currency']=="USD") {
                    $currency = 2;
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Currency cod is required");
            }

            $filtered = array();
            foreach($row as $k => $v){
                if(preg_match('/c_/',$k)){
                    $filtered[] = $k;
                }
            }

            $params = [];
            if (!$material) {
                $isValid = false;
            }
            else {
                foreach($filtered as $keyparam){
                        $where = str_replace('c_','',$keyparam);
                        $wherespace = str_replace('_',' ',$where);
                        $param = ClassificationParameter::whereRaw("LOWER(name) = (?)", [$wherespace])
                                    ->where('id_classification',$material->classification->id)
                                    ->get();
                        foreach($param as $p){
                            $params[] = [
                            'id_parameter' => $p->id,
                            'value'      => $row[$keyparam],
                            ];
                        
                        }     
                }

            }
            //cek Depreciation
            if(!empty($row['is_asset'])){
                $is_asset = strtolower($row['is_asset']);
                switch ($is_asset) {
                    case 'asset' :
                        if(!empty($row['depreciation'])) {
                            $is_asset = 1;
                            $depreciation = Depreciation::where('name','=',$row['depreciation'])->first();
                            if(empty($depreciation)) {
                                $isValid = false;
                                array_push($messages,"Data Depreciation ".$row['depreciation']." doesn't exists");
                            }
                        }
                        else {
                            $isValid = false;
                            array_push($messages,"Depreciation is Required");
                        }
                        break;
                    case 'expense' :
                        $is_asset = NULL;
                        break;
                    
                    default :
                        $isValid = false;
                        array_push($messages,"This material is Asset or Expense ?");
                }
            }

            //cek Responsible Field
            if(!empty($row['responsible_person_nik'])){
            $user = Users::where('nik','=',$row['responsible_person_nik'])->first();
                if(empty($user)){
                    $isValid = false;
                    array_push($messages,"NIK Responsible User ".$row['responsible_person_nik']." doesn't exists");
                }
            }
            else{
                $isValid = false;
                array_push($messages,"NIK Responsible User is required");
            }

            //check maintenance cycle and mc unit
            if(!empty($row['maintenance_cycle']) && !empty($row['mc_unit'])){
                $cycle = $row['maintenance_cycle'];

                switch($row['mc_unit']){
                    case 1: //days
                        $count_duration = $cycle * 1;
                        break;
                    case 2: //month
                        $count_duration = $cycle * 30;
                        break;
                    case 3: //year
                        $count_duration = $cycle * 365;         
                        break;
                }
            }
            if(!empty($row['latitude']) && !empty($row['longitude'])){
                $lat=$row['latitude'];
                $long=$row['longitude'];
                // $url="http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=true";
                $url="https://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=true&key=AIzaSyA7nntc0ptu8sZjm4FggxTPaKWTXnq6CIg";
                //  Initiate curl
                $ch = curl_init();
                // Disable SSL verification
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                // Will return the response, if false it print the response
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // Set the url
                curl_setopt($ch, CURLOPT_URL,$url);
                // Execute
                $result=curl_exec($ch);
                // Closing
                curl_close($ch);


                $json_data=json_decode($result);
                if(count($json_data->results) > 0)
                {
                    $full_address=$json_data->results[0]->formatted_address;
                    // dump($full_address);
                    if(!$row['location']){
                        $row['location'] = $full_address;
                    }
                }
            }

            // Validate Purchase Date
            if(!empty($row['purchase_date'])) {
                $purchase_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['purchase_date']));
                if($purchase_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $isValid = false;
                    array_push($messages,"Purchase date is not valid");
                    $purchase_date = null;
                }
            }
            else {
                $isValid = false;
                array_push($messages,"Purchase date is required");
                $purchase_date = null;
            }

            // Validate Warranty End Date
            if(!empty($row['warranty_end_date'])) {
                $warranty_end_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['warranty_end_date']));
                if($warranty_end_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $warranty_end_date = null;
                }
            }
            else {
                $warranty_end_date = null;
            }
            
            // Validate Last Maintenance Date
            if(!empty($row['last_maintenance_date'])) {
                $last_maintenance_date = Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row['last_maintenance_date']));
                if($last_maintenance_date->lt(Carbon::create(1971, 1, 1, 0, 0, 0))) {
                    $last_maintenance_date = null;
                } else {
                    if($row['asset_type'] == 11) {
                        $next_maintenance_date = Carbon::parse($last_maintenance_date)->addYears(1);
                    } else {
                        $next_maintenance_date = Carbon::parse($last_maintenance_date)->addYears(2);
                    }
                }
            }
            else {
                if(isset($user)) {
                    if($user->id != 2 || $user->id != 3) {
                        $last_maintenance_date = carbon::now();
                        if($row['asset_type'] == 11) {
                            $next_maintenance_date = $last_maintenance_date->addYears(1);
                        } else {
                            $next_maintenance_date = $last_maintenance_date->addYears(2);
                        }
                    } else {
                        $last_maintenance_date = null;
                    }
                }
            }
            

            // END OF EXCEL VALIDATION ---------------------------------------

            $data[] = [
                'asset_type'            => $row['asset_type'] ?? null,
                'code_asset'            => $row['asset_number'] ?? null,
                'count_duration'        => isset($count_duration) ? $count_duration : null,
                'created_by'            => Auth::user()->id,
                'currency'              => $currency ?? null,
                'depreciation'          => $row['depreciation'] ?? null,
                'description_asset'     => $row['description'] ?? null,
                'id_depreciation'       => isset($depreciation) ? $depreciation->id : null,
                'id_location'           => isset($location) ? $location->id : null,
                'id_material'           => isset($material) ? $material->id : null,
                'id_plant'              => isset($plant) ? $plant->id : null,
                'id_responsible_person' => isset($user) ? $user->id : null,
                'id_superior_asset'     => isset($superior) ? $superior->id_asset : null,
                'id_supplier'           => isset($vendor) ? $vendor->id : null,
                'id_type_asset'         => $asset_type->id ?? null,
                'is_asset'              => isset($is_asset) ? $is_asset : null,
                'is_asset_name'         => $row['is_asset'] ?? null,
                'last_maintenance'      => $last_maintenance_date,
                'next_maintenance'      => $next_maintenance_date ?? null,
                'last_maintenance_date' => $row['last_maintenance_date'] ?? null,
                'level_asset'           => $level,
                'location'              => $row['location'] ?? null,
                'maintenance_cycle'     => $row['maintenance_cycle'] ?? null,
                'material_code'         => $row['material_code'] ?? null,
                'mc_unit'               => $row['mc_unit'] ?? null,
                'message'               => implode(', ',$messages),
                'no'                    => $row['no'],
                'old_asset'             => isset($old_asset) ? $old_asset->id_asset : null,
                'old_asset_number'      => $row['old_asset_number'] ?? null,
                'params'                => $params, //untuk insert parameter
                'plant'                 => $row['plant'] ?? null,
                'purchase_cost'         => intval($row['purchase_price'] ?? 0),
                'purchase_date'         => $purchase_date,//Carbon::parse($row['purchase_date']):null,//date('Y-m-d H:i:s',strtotime(str_replace('/', '-',$row['purchase_date']))) : null,
                'purchase_date_str'     => $row['purchase_date'] ?? null,
                'purchase_price'        => $row['purchase_price'] ?? null,
                'responsible_person_nik'=> $row['responsible_person_nik'] ?? null,
                'retired_ramarks'       => $row['retired_remarks'] ?? null,
                'retired_date'          => $retired_date ?? null,
                'serial_number'         => $row['serial_number'] ?? null,
                'status_name'           => $row['status'] ?? null,
                'status'                => isset($status->id) ? $status->id: null,  
                'superior_asset_number' => $row['superior_asset_number'] ?? null,
                'valid'                 => $isValid,
                'valuation_group'       => isset($valuation_group) ? $valuation_group->id : null,
                'valuation_group_name'  => $row['valuation_group'] ?? null,
                'vendor'                => $row['vendor'] ?? null,
                'waranty_finish'        => $warranty_end_date,//Carbon::parse($row['warranty_end_date']):null,//date('Y-m-d H:i:s',strtotime(str_replace('/', '-',$row['warranty_end_date']))) : null,
                'warranty_end_date'     => $row['warranty_end_date'] ?? null,                
            ];

            if(!$isValid)
                $dataOk = false;
        }

        $validate = [];
        foreach($data as $d) {
            if($d['valid'])
                $validate[] = $d;
        }
        if(!$dataOk) {
            foreach($data as $d) {
                if(!$d['valid'])
                    $validate[] = $d;
            }
        }
        
        //where there is no error flash session
        if($dataOk) {
            Session::flash('dataToInsert', $validate);         
        }
        return view('asset.validate', ['validate' => $validate, 'dataOk' => $dataOk]);
    }

    public function saveData(){
        if(Session::has('dataToInsert')){
            $data = Session::get('dataToInsert');
            try {
                \DB::beginTransaction();
                foreach($data as $dataasset){
                    $insertedasset = Asset::create($dataasset);  
                    $depreciate    = new DepreciationController(); 
                    $depreciate->calculateDepreciation($dataasset); 
                    
                    if ($dataasset['id_responsible_person'] != 2 || $dataasset['id_responsible_person'] != 3 ) {
                        $asset = Asset::where('code_asset','=',$dataasset['code_asset'])->first();
                        $responsible_person_old = 2;
                        if(Auth::user()->id_role = 3) {
                            $responsible_person_old = 2;
                        }
                        $insert = AssetResponsiblePersonHistory::create([
                            'id_asset'                     => $asset->id_asset,
                            'id_responsible_person'        => $responsible_person_old,
                            'created_by'                   => Auth::user()->id,
                            'created_at'                   => $dataasset['last_maintenance'],
                        ]);
                    }                   
                }
                Session::flash('success', 'Data saved');
                \DB::commit();
            }
            catch(\Exception $ex) {
                \DB::rollBack();
                Session::flash('error', 'ERROR : Data cannot be saved. '.$ex->getMessage()); 
            }
        }
        return redirect('asset');        
    }

    public function downloadTemplate(){

        return CreateTemplateUpload::call();

    }

    public function upload_images(Request $request, $id){
        foreach($request->file('images') as $image){
            $saved = $image->store('public/asset/material');

            $public_path = str_replace('public','storage',$saved);

            $insert = AssetImage::create([
                'code_asset'           => $id,
                'id_asset'           => $id,
                'image_asset'        => $public_path,
                'created_by'              => Auth::user()->id,
            ]);
            echo $public_path.PHP_EOL;
        }

        return response()->json([
            'status' => 'ok',
            'request' => $request->all()
        ]);
    }

    public function get_images($id){

        return ShowImageAssetByID::call($id);

    }

    public function delete_image($id) {

        return DeleteImageAssetByID::call($id);

    }

    public function upload_images360(Request $request, $id){
        // dump($request);
        foreach($request->file('images') as $image){
            $saved = $image->store('public/asset/material');

            $public_path = str_replace('public','storage',$saved);

            $insert = AssetImage360::create([
                
                'id_asset'           => $id,
                'image360_asset'        => $public_path,
                'created_by'              => Auth::user()->id,
                
            ]);
            echo $public_path.PHP_EOL;
        }

        return response()->json([
            'status' => 'ok',
            'request' => $request->all()
        ]);
    }

    public function get_images360(Request $request, $id){
        $images = AssetImage360::where('id_asset',$id)->get();

        return view('asset.partial_images360')->withImages($images);
    }

    public function delete_image360(Request $request,$id){
        $image = AssetImage360::where('id',$id)->firstOrFail();
        $path = str_replace('storage/','public/',$image->image360_asset);

        if(\Storage::delete($path)){
            $image->delete();

            return response()->json([
                'status' => 'success',
                'message' => 'success delete image'
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'error delete image'
            ],406);
        }
    }
    public function storehistoryloc(Request $request,$id){
       $rules = array(
            //'address'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules); 
         if ($validator->fails()) {
            return $request -> all();
            return redirect('asset/'.$id.'/edit')->withErrors($validator);
        } else {
            
            $insert = LocationHistory::create([
                'id_asset'          => $request->id_asset,
                'address'           => $request->address_old,
                'latitude'          => $request->latitude_old,
                'longitude'         => $request->longitude_old,
                'building'          => $request->building_old,
                'unit'              => $request->unit_old,
                'city'              => $request->city_old,
                //'province'          => $request->province_old,
                'id_location'       => $request->id_location_old,
                'created_by'        => Auth::user()->id,
            ]);
            
                $update                    = Asset::find($id);
                $update->building          = $request->building;
                $update->unit              = $request->unit;
                $update->city              = $request->city;
                $update->province          = $request->province;
                //$update->address_location  = $request->location;
                $update->latitude          = $request->latitude;
                $update->longitude         = $request->longitude;
                $update->id_location       = $request->new_location;
                $update->save();
            if ($insert) {
                Session::flash('success', 'Data Successfully Added');
                
            } else {
                return back()->withError($e->getMessage());
            }

            return redirect('asset/'.$id.'/edit');
        }         
    }



    public function ajax_description(Request $request, $id){

        $data = DB::table("master_material")
        ->where('master_material.id',$id)
        ->leftjoin('material_group', 'material_group.id', '=', 'master_material.id_material_group')
        ->select('master_material.description','material_group.name as group_name')
        ->first();
        return response()->json($data);

    }


    public function ajaxSuperiorAsset(Request $request){

        if(Auth::user()->user_role_id == 2 || Auth::user()->user_role_id == 5){
            $data = [];

            if($request->has('q')){
                $search = strtolower($request->q);
                $data = DB::table("asset")
                        ->select("id_asset","code_asset","description_asset")
                        ->where('status', 2)
                        ->where('plant', Auth::user()->plant_bismo)
                        ->whereRaw("LOWER(code_asset) LIKE '%".$search."%'")
                        ->get();
            }
            else
            {
                $data = Asset::select("id_asset","code_asset", "description_asset")
                ->where('status', 2)
                ->where('plant', Auth::user()->plant_bismo)->limit(20)->get();
            }
        }else{
            $data = [];

            if($request->has('q')){
                $search = strtolower($request->q);
                $data = DB::table("asset")
                        ->select("id_asset","code_asset","description_asset")
                        ->where("code_asset","ilike","%".$search."%")
                        ->where('status', 2)
                        ->get();
            }else{
                $data = Asset::select("id_asset","code_asset", "description_asset")
                ->where('status', 2)
                ->limit(20)->get();
            }
        }

        //var_dump($data);exit();

        return response()->json($data);
    }

    public function export(Request $request){

        $valuation    = ValuationGroup::All();
        $query = Asset::whereNull('deleted_at')
            ->with(['material', 'type', 'supplier', 'valuation', 'location', 'asset_status','responsible_person_asset'])
            ->orderBy('id_asset', 'asc');

        if($request->status){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->status); $i++ ){
                    $query->Where('status', $request->status[$i]); 
                }
            });
        }

        if($request->responsible){
            if (is_array($request->responsible)) {
                $query->where(function($query) use ($request){
                    for($i=0; $i<count($request->responsible); $i++ ){
                        $query->Where('id_responsible_person','=', $request->responsible[$i]); 
                    }
                });
            }
            else {
                $query->Where('id_responsible_person','=', $request->responsible); 
            }
        }
        
        if($request->plant){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->plant); $i++ ){
                    $query->Where('id_plant', $request->plant[$i]); 
                }
            });
        }
        if($request->vendor){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->vendor); $i++ ){
                    $query->orWhere('id_supplier', $request->vendor[$i]); 
                }
            });
        }


        if($request->material){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->material); $i++ ){
                    $query->orWhere('id_material', $request->material[$i]); 
                }
            });
        }

        if($request->type){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->type); $i++ ){
                    $query->orWhere('id_type_asset', $request->type[$i]); 
                }
            });
        }
      
        if($request->superior){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->superior); $i++ ){
                    $query->orWhere('id_superior_asset', $request->superior[$i]); 
                }
            });
        }

        if($request->location){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->location); $i++ ){
                    $query->Where('id_location', $request->location[$i]); 
                }
            });
        }

        if(!empty($request->start_date)){
            if (empty($request->end_date)) {
                $to = Carbon::parse($from)->addDays(1)->format('Y-m-d h:i:s');
            }
            $from = Carbon::parse($request->start_date)->format('Y-m-d h:i:s');
            $to = Carbon::parse($request->end_date)->format('Y-m-d h:i:s');
            $query->whereBetween('created_at',[$from,$to]);
        }
        
        $assets = $query->get();

        $header = array(
            [
            'No', 'Material Code', 'Asset Number','Description','Serial Number', 'Superior Asset Number',
            'Old Asset Number', 'Vendor', 'Plant', 'Group Owner', 'Asset Type', 'Responsible Person', 'Employee No.', 
            'Location Name', 'Location', 'Status', 'Purchase Date',
            'Purchase Price', 'Currency', 'Warranty End Date', 'Maintenance Cycle', 'MC Unit', 'Last Maintenance Date',
            'Retired Remarks'
            ]
        );

        $i=1;
        foreach($assets as $asset) {
            $header[] = [
                $i++,
                isset($asset->material) ? $asset->material->name : '',
                $asset->code_asset,
                $asset->description_asset,
                $asset->serial_number or '',
                '',
                '',
                $asset->supplier ? $asset->supplier->name_supplier : '',
                $asset->plant,
                $asset->valuation ? $asset->valuation->name : '',
                $asset->type ? $asset->type->name : '',
                //
                //$asset->nik." - ".$asset->responsible_person,,
                isset($asset->responsible_person_asset->name) ? $asset->responsible_person_asset->name : '',
                isset($asset->responsible_person_asset->nik) ? $asset->responsible_person_asset->nik : '',
                isset($asset->location->name) ? $asset->location->name : '',
                isset($asset->location->address) ? $asset->location->address : '',
                $asset->status_asset ? $asset->status_asset->name : '',
                $asset->purchase_date!=NULL ? Carbon::parse($asset->purchase_date)->format('d/m/Y') : '',
                $asset->purchase_cost,
                $asset->curency,
                $asset->waranty_finish!=NULL ? Carbon::parse($asset->waranty_finish)->format('d/m/Y') : '',
                $asset->count_duration,
                $asset->unit_duration,
                $asset->last_maintenance!=NULL ? Carbon::parse($asset->last_maintenance)->format('d/m/Y') : "",
                $asset->retired_ramarks
            ];
        }

        $export = new \App\Exports\Excel\ExportFromArray($header);
        return Excel::download($export, 'Asset.xlsx');

    }

    public function tes_pdf(Request $request)
    {
        $valuation    = ValuationGroup::All();
            if(Auth::user()->id_role != 1)
            {
                $query = Asset::whereNull('deleted_at')
                    // ->whereHas('material')
                    ->where('valuation_group', Auth::user()->id_role)//HR
                    ->with('material')
                    ->with('type')
                    ->orderBy('id_asset', 'asc');
            }
            else
            {
                $query = Asset::whereNull('deleted_at')
                ->with('material')
                ->with('type')
                ->orderBy('id_asset', 'asc');
            }
            
            $getfilter =\DB::table('filter')
            ->select('*')
            ->where('user_id', Auth::user()->id)
            ->where('page_id', 1) //id this page is 1
            ->first();
    
            $filter = json_decode($getfilter ? $getfilter->value : 0);
    
    
            if($filter && (count($request->location) != 0 || count($request->superior) != 0 ||count($request->valuation) != 0 ||count($request->type) != 0 ||count($request->vendor) != 0 || count($request->material) != 0 || count($request->status) != 0 || count($request->plant) != 0))
            {
    
            }
            elseif($filter)
            {
                $request->plant  = $filter->plant;
                $request->status  = $filter->status;
                $request->material  = $filter->material;
                $request->valuation  = $filter->valuation;
                $request->type  = $filter->type;
                $request->superior  = $filter->superior;
                $request->vendor  = $filter->vendor;
                $request->location  = $filter->location;
            }

        if($request->q){
            $query->where(function($query) use ($request){
                $query->orWhere('code_asset','like','%'.$request->q.'%');
                $query->orWhere('description_asset','like','%'.strtolower($request->q).'%');
            });
        }
        if($request->status){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->status); $i++ ){
                    $query->orWhere('status', $request->status[$i]); 
                }
            });
        }

        if($request->plant){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->plant); $i++ ){
                    $query->orWhere('id_plant', $request->plant[$i]); 
                }
            });
        }

        if($request->vendor){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->vendor); $i++ ){
                    $query->orWhere('id_supplier', $request->vendor[$i]); 
                }
            });
        }

        if($request->material){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->material); $i++ ){
                    $query->orWhere('id_material', $request->material[$i]); 
                }
            });
        }

        if($request->type){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->type); $i++ ){
                    $query->orWhere('id_type_asset', $request->type[$i]); 
                }
            });
        }

        if($request->valuation){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->valuation); $i++ ){
                    $query->orWhere('valuation_group', $request->valuation[$i]); 
                }
            });
        }

        if($request->superior){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->superior); $i++ ){
                    $query->orWhere('id_superior_asset', $request->superior[$i]); 
                }
            });
        }

        if($request->location){
            $query->where(function($query) use ($request){
                for($i=0; $i<count($request->location); $i++ ){
                    $query->orWhere('id_location', $request->location[$i]); 
                }
            });
        }
        
        $assets = $query->get();

        //On Printing HTML
        $config_barcode = ConfigBarcode::all();
        $config = array();
        foreach ($config_barcode as $cb) {
            $config[$cb->key] = $cb->value;
        }
        return view('asset.print_barcode', ['assets' => $assets, 'config'=>$config]);
    }

    public function one_pdf($id)
    {

        $assets = Asset::where('id_asset', $id)
                ->get();
        //On Printing HTML
        $config_barcode = ConfigBarcode::all();
        $config = array();
        foreach ($config_barcode as $cb) {
            $config[$cb->key] = $cb->value;
        }
        return view('asset.print_barcode', ['assets' => $assets, 'config'=>$config]);

    }

    public function area(Request $request)
    {
        // dd($request->all());
        $insert = AssetArea::updateOrCreate(
            [
                'id_asset'        => $request->id_asset,
            ],
            [
                'name'        => $request->name,
                'large'        => $request->large,
                'area'        => $request->area,
                'created_by'     => Auth::user()->id,
            ]
        );
        if ($insert) {
            Session::flash('success', 'Data Successfully Edited');
        } else {
            Session::flash('error', 'Data Failed to Add');
        }
        return redirect('asset/'.$request->id_asset.'/edit');

    }

    public function refund(Request $request)
    {        
        if ($request->get('user')) {
            $user = Users::findOrfail($request->user);
            if (Auth::user()->id_role === 1) {
                $data2 = Asset::where('id_responsible_person','=',$request->user)->get();
                $data = AssetResponsiblePersonHistory::with([
                    'assetAttr',
                    'user',
                ])
                ->where('id_responsible_person','=',$request->user)
                ->get();
            } 
            else 
            {
                
                $data2 = Asset::where('id_responsible_person','=', $request->user)
                        ->where('valuation_group', Auth::user()->id_role)
                        ->get();
                $data = AssetResponsiblePersonHistory::with(array('assetAttr'=>function($query){
                            $query->where('valuation_group','=',Auth::user()->id_role)->get();
                        }))
                        ->with('user')
                        ->where('id_responsible_person','=',$request->user)
                        ->get();                    
            }
        } else {
            $data = [];
            $data2 = [];
            $user = null;
        }
        return view('asset.refund', compact('data', 'data2', 'user'));
    }

    public function refundAjax(Request $request)
    {
        Return ShowAssetResponsiblePersonHistoryByUser::call($request->id);
    }

    public function refundAjaxNot(Request $request)
    {
        $data = Asset::where('responsible_person', $request->id)->get();

        return response()->json($data);
    }

    public function exportRefund($id)
    {
        $user = Users::findOrfail($id);
        $user->date = now();

        if (Auth::user()->id_role === 1) {
            $assets = Asset::where('responsible_person', $user->name)->get();
            
            $refund  = \DB::table('asset_responsible_person_history')
            ->select('asset_responsible_person_history.*', 'users.name as name_user', 'asset.*', 'asset_responsible_person_history.created_at as date')
            ->leftjoin('users', 'users.name', '=', 'asset_responsible_person_history.id_responsible_person')
            ->join('asset', 'asset.id_asset', '=', 'asset_responsible_person_history.id_asset')
            ->where('asset_responsible_person_history.id_responsible_person',$user->name)
            ->get();
        } else {
            $assets = Asset::where('responsible_person', $user->name)->where('valuation_group', Auth::user()->id_role)->get();

            $refund  = \DB::table('asset_responsible_person_history')
            ->select('asset_responsible_person_history.*', 'users.name as name_user', 'asset.*', 'asset_responsible_person_history.created_at as date')
            ->leftjoin('users', 'users.name', '=', 'asset_responsible_person_history.id_responsible_person')
            ->join('asset', 'asset.id_asset', '=', 'asset_responsible_person_history.id_asset')
            ->where('asset_responsible_person_history.id_responsible_person',$user->name)
            ->where('asset.valuation_group', Auth::user()->id_role)
            ->get();
        }

        $data = [['NIK',$user->nik],['NAMA',$user->name],['TANGGAL',$user->date]];
        $data[] = ['Detail Pengembalian'];
        $data[] = ['No', 'Asset Name', 'Asset Price', 'Date'];
        if($refund->count() > 0) {
            $i=1;
            foreach($refund as $d) {
                $data[] = [$i++, ('['.$d->code_asset.'] - '.$d->description_asset), number_format($d->purchase_cost), $d->date];
            }
        }
        $data[] = ['Detail Potong'];
        $data[] = ['No', 'Asset Name', 'Asset Price'];
        if($assets->count() > 0) {
            $sum = 0;
            $i=1;
            foreach($assets as $d) {
                $data[] = [$i++, ('['.$d->code_asset.'] - '.$d->description_asset), number_format($d->purchase_cost)];
                $sum += $d->purchase_cost;
            }
        }
        $data[] = ['Total Refund', number_format($sum)];

        $export = new \App\Exports\Excel\ExportFromArray($data);
        return Excel::download($export, 'Asset Refund.xlsx');

        // Excel::create('AssetRefund', function($excel) use ($assets, $refund, $user) {
        //     $excel->sheet('New sheet', function($sheet) use ($assets, $refund, $user) {
        //         $sheet->loadView('asset.export', compact('assets', 'refund', 'user'));
        //     });
        // })->export('xls');
    }

    public function assetRefundAjax(Request $request) 
    {
        $data = $request->json()->all();

        foreach ($data as $a)
        {
            $asset = Asset::select('valuation_group')
            ->where('id_asset','=',$a['id'])
            ->first();
            if (isset($asset->valuation_group))
                {
                    if ($asset->valuation_group === 5) 
                    {
                        $responsible_person = "4181";
                    }
                    else 
                    {
                        $responsible_person = "3";
                    }
                    
                    Asset::where('id_asset',$a['id'])
                    ->update(
                    [
                                'status'=>$a['status_retired'],
                                'retired_reason'=>$a['status_reason'],
                                'retired_date'=>$a['retired_date'],
                                'retired_ramarks'=>$a['retired_remarks'],
                                'id_responsible_person'=>$responsible_person,
                                'updated_by'=> Auth::user()->id,
                                'updated_at'=>date('Y-m-d H:i:s')
                                ]
                    );
                    
                    $insert = StoreResponsiblePersonHistory::store($a);

                    if ($insert == false)
                    {
                        return response()->make('Fail',406);
                    }
                }
        }

        return response()->json('Success',200);

    }

    /*
    * ********************************
    *  History Responsible User Asset 
    * ********************************
    */

    public function showhistoryperson_byid($request){
        $person_history  = \DB::table('asset_responsible_person_history')
        ->select('asset_responsible_person_history.*', 'users.name as name_user')
        ->leftjoin('users', 'users.id', '=', 'asset_responsible_person_history.created_by')
        ->where('asset_responsible_person_history.id_asset',$request)
        ->orderBy('asset_responsible_person_history.created_at', 'desc')
        ->get();
        return $person_history;
    }

    public function storehistoryperson(Request $request,$id){
        $rules = array(
             //'address'    => 'required',
         );

         $validator = Validator::make($request->all(), $rules); 
          if ($validator->fails()) {
             //return $request -> all();
             return redirect('asset/'.$id.'/edit')->withErrors($validator);
         } else {
             
             $insert = AssetResponsiblePersonHistory::create([
                 'id_asset'                     => $request->id_asset,
                 'id_responsible_person'        => $request->responsible_person_old,
                 'created_by'                   => Auth::user()->id,
             ]);
                 $update                        = Asset::find($id);
                 $update->id_responsible_person = $request->responsible_person;
                 $update->save();
             if ($insert) {
                 Session::flash('success', 'Data Successfully Added');
                 
             } else {
                 Session::flash('error', 'Data Failed to Add');
             }
             return redirect('asset/'.$id.'/edit');
         }         
     }

     public function savefilter(Request $request) {
         $input_plant  = $request->plant;
         $input_status  = $request->status;
         $input_vendor  = $request->vendor;
         $input_type  = $request->type;
         $input_valuation  = $request->valuation;
         $input_material  = $request->material;
         $input_superior  = $request->superior;
         $input_location  = $request->location;
         $input_responsible  = $request->responsible;
         $start_date  = $request->start_date;
         $end_date  = $request->end_date;
 
         $filter = [
             'responsible' => $input_responsible,
             'location' => $input_location,
             'superior' => $input_superior,
             'valuation' => $input_valuation,
             'type' => $input_type,
             'vendor' => $input_vendor,
             'status' => $input_status,
             'material'=> $input_material,
             'plant'=>$input_plant,
             'start_date' => $start_date,
             'end_date' => $end_date
         ];
 
         $insert_filter = \App\Filter::updateOrCreate(
             [
                 'user_id'   => Auth::user()->id,
                 'page_id'   => 1,
             ],
             [
                 'value'         => json_encode($filter),
             ]
         );
 
         return redirect('asset')->with('message', 'Filter Saved');
         
     }


}
