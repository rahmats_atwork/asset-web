<?php

namespace App\Http\Controllers\Transaction\Report;

use Illuminate\Http\Request;

use Carbon\Carbon;

use App\Models\Masters\AssetType;
use App\Models\Transaction\Asset;


class StockCardReport
{
    static function Execute (Request $request) {

        $list   = [];

        $type   =  Asset::with(['Type'])->select('id_type_asset')
                ->where('id_responsible_person','=',2)
                ->groupBy('id_type_asset')
                ->orderBy('id_type_asset','asc')
                ->get();

        foreach ($type as $t) {

            $data   = Asset::selectRaw('materials.name as material_name, asset.purchase_date, count(*)')
            ->leftJoin('asset_responsible_person_history','asset.id_asset','=','asset_responsible_person_history.id_asset')
            ->leftJoin('users','asset_responsible_person_history.id_responsible_person','=','users.id')
            ->leftJoin('asset_type','asset.id_type_asset','=','asset_type.id')
            ->leftJoin('materials','asset.id_material','=','materials.id')
            ->where('asset.id_responsible_person','=',2)
            ->where('asset_type.id','=', $t->type->id )
            ->groupBy('materials.name', 'asset.purchase_date')
            ->orderBy('material_name','asc')
            ->orderBy('asset.purchase_date','asc')
            ->get();

            $collection = collect($data)->sum('count');
            $data = collect($data)->groupBy('material_name')->all();

            $arr = [
                'material_type' => $t->type->name,
                'total_stock'   => $collection,
                'stock_card'    => $data,
                //'col'           => $coll,
            ];
            
            array_push($list, $arr);
        }

        return $list;
        
    }
}
