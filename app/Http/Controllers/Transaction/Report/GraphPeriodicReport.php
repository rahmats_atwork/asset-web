<?php

namespace App\Http\Controllers\Transaction\Report;

use Auth;
use DB;
use Carbon\Carbon;

use App\Models\Transaction\Asset;
use App\Http\Controllers\Transaction\Asset\CountHandOverAsset;
use App\Http\Controllers\Transaction\Asset\CountRefundAsset;
use App\Http\Controllers\Transaction\Asset\ShowDataHandOverAsset;
use App\Http\Controllers\Transaction\Asset\ShowDataRefundAsset;

use App\Models\Masters\AssetType;
use App\Helpers\PeriodRange;

class GraphPeriodicReport
{
    public static function call ($request) {

        $start      = null;
        $end        = null;
        $asset_type = null;
        
        if (($request->type_asset) == 'null' || ($request->type_asset) == '' || ($request->type_asset) == null) {
            
            $list      = AssetType::orderBy('id','asc')->get();
            $asset_type = collect($list)->pluck('id')->all();

        } else {

            $asset_type = $request->type_asset;

        }

        if ($request->startPeriode  == null) {

            $start =  date('Y')."-01-01 00:00:00";


        } else {

            $start =  date('Y-m-d H:i:s', strtotime($request->startPeriode));

        }

        if ($request->endPeriode == null) {
            
            $end =  carbon::Now()->endOfMonth();

        } else {

            $end =  carbon::parse($request->endPeriode);
            $end =  $end->endOfMonth();
            
        }

        
        $period = PeriodRange::call($start, $end);


        $data = [];
        
        switch($request->choose) {
            
            //Penyerahan Asset
            case 1 :
 
                foreach ($asset_type as $a) {
                    
                    $resultPeriode = [];
                    $nameType      = AssetType::where('id','=',$a)->first();
                    
                    foreach ($period as $p) {
                        $dataResult = CountHandOverAsset::call($a, $p);
                        $arr = [
                            /*
                            'x_y'    => date('Y',strtotime($p)),
                            'x_m'    => date('m',strtotime($p)),
                            'x_d'    => date('d',strtotime($p)),
                            */
                            'x'    => date('Y-m-d',strtotime($p)),
                            'y'    => $dataResult
                        ];
                        array_push($resultPeriode, $arr);
                    }
                    $asset = [
                        'assettype'      => $nameType->name,
                        'assetresult'    => $resultPeriode
                    ];
                    array_push($data, $asset);
                }

                return $data;

            // Pengembalian Asset  
            case 2 :

                foreach ($asset_type as $a) {
                    $resultPeriode = [];
                    $nameType      = AssetType::where('id','=',$a)->first();
                    
                    foreach ($period as $p) {
                        $dataResult = CountRefundAsset::call($a, $p);
                        $arr = [
                            'x'    => date('Y-m-d',strtotime($p)),
                            'y'    => $dataResult
                        ];
                        array_push($resultPeriode, $arr);
                    }
                    $asset = [
                        'assettype'      => $nameType->name,
                        'assetresult'    => $resultPeriode
                    ];
                    array_push($data, $asset);
                }

                return $data;

            // Mix Asset 
            case 3 :

                return true;

            case 4 :

                return $asset_type ;
        }
        
    }
}
