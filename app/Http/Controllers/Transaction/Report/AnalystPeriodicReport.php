<?php

namespace App\Http\Controllers\Transaction\Report;

use Auth;
use DB;
use Carbon\Carbon;

use App\Models\Transaction\Asset;
use App\Http\Controllers\Transaction\Asset\CountHandOverAsset;
use App\Http\Controllers\Transaction\Asset\CountRefundAsset;
use App\Http\Controllers\Transaction\Asset\ShowDataHandOverAsset;
use App\Http\Controllers\Transaction\Asset\ShowDataRefundAsset;

use App\Models\Masters\AssetType;
use App\Helpers\PeriodRange;

class AnalystPeriodicReport
{
    public static function call ($request) {

        $start      = null;
        $end        = null;
        $asset_type = null;
        
        if (($request->type_asset) == 'null' || ($request->type_asset) == '' || ($request->type_asset) == null) {
            
            $list      = AssetType::orderBy('id','asc')->get();
            $asset_type = collect($list)->pluck('id')->all();

        } else {

            $asset_type = $request->type_asset;

        }

        if ($request->startPeriode  == null) {

            $start =  date('Y')."-01-01 00:00:00";


        } else {

            $start =  date('Y-m-d H:i:s', strtotime($request->startPeriode));

        }

        if ($request->endPeriode == null) {
            
            $end =  carbon::Now()->endOfMonth();

        } else {

            $end =  carbon::parse($request->endPeriode);
            $end =  $end->endOfMonth();
            
        }
        
        $data = null;

        switch($request->choose) {
            
            //Penyerahan Asset
            case 1 :

                $data = ShowDataHandOverAsset::call($asset_type, $start, $end); //$data['graph'];
                return $data;

            // Pengembalian Asset 
            case 2 :

                $data = ShowDataRefundAsset::call($asset_type, $start, $end);
                return $data;

            // Mix Asset 
            case 3 :

                return true;

            case 4 :
                return $asset_type ;
        }
        
    }
}
