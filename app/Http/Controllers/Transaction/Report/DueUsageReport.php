<?php

namespace App\Http\Controllers\Transaction\Report;

use Illuminate\Http\Request;

use Carbon\Carbon;
use DB;
use App\Models\Transaction\Asset;

class DueUsageReport
{
    static function execute(Request $request) {
        /*
        $data   = Asset::with(['historyPersonal','historyPersonal.user'])
                ->whereNotIn('id_responsible_person',[2,3,4181])
                ->where('status','=',1)
                ->orwhereHas('historyPersonal',function($query) {
                    $query->whereNotIn('id_responsible_person',[2]);
                })
                ->get();
        */

        $start = carbon::parse($request->from_date)->format('Y-m-d');
        $end   = carbon::parse($request->to_date)->format('Y-m-d');

        $data   = Asset::groupBy('id_material')
        ->groupBy('id_responsible_person')
        ->groupBy('next_maintenance')
        ->selectRaw('id_material,next_maintenance,id_responsible_person, count(*)')
        ->with(['material', 'responsible_person_asset'])
        ->whereNotIn('id_responsible_person',[2,3,4181])
        ->where('status','=',1)
        ->orderBy('id_responsible_person','asc')
        ->orderBy('next_maintenance','asc');

        if ( $start === $end) {
            $data->whereDate('next_maintenance',date($start));
        } else {
            $data->whereBetween('next_maintenance',[$start,$end]);
        }
        
        $collection = collect($data->get());
        $dataNew = $collection->groupBy('responsible_person_asset.name')->toArray();
        /*
        $groupCount = $dataNew->map(function ($item, $key) {
            return collect($item)->count();
        });
        */
        //$datafinal = $dataNew->union($groupCount);

        return $dataNew;

    }
}
