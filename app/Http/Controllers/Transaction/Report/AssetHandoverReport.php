<?php

namespace App\Http\Controllers\Transaction\Report;
use Illuminate\Http\Request;


use Auth;
use DB;
use Carbon\Carbon;

use App\Models\Transaction\Asset;
use App\Http\Controllers\Transaction\Asset\ShowDataHandOverAsset;
use App\Models\Masters\AssetType;
use App\Helpers\PeriodRange;

class AssetHandoverReport
{
    public static function execute(Request $request) {

        $start      = null;
        $end        = null;
        $asset_type = null;
        
        if (($request->type_asset) == 'null' || ($request->type_asset) == '' || ($request->type_asset) == null) {
            
            $list      = AssetType::orderBy('id','asc')->get();
            $asset_type = collect($list)->pluck('id')->all();

        } else {

            $asset_type = $request->type_asset;

        }

        if ($request->startPeriode  == null) {

            $start =  date('Y')."-01-01 00:00:00";


        } else {

            $start =  date('Y-m-d H:i:s', strtotime($request->startPeriode));

        }

        if ($request->endPeriode == null) {
            
            $end =  carbon::Now()->endOfMonth();

        } else {

            $end =  carbon::parse($request->endPeriode);
            $end =  $end->endOfMonth();
            
        }

        $period = PeriodRange::call($start, $end);


        //$data = [];

        $data = Asset::groupBy('id_type_asset')
                ->groupBy('id_responsible_person')
                ->groupBy('last_maintenance')
                ->with(['type', 'responsible_person_asset'])
                ->with(['historyPersonal' => function ($query) use($start, $end) {
                    $query->whereBetween('created_at',[$start,$end])->where('id_responsible_person','=',2);
                }])
                ->selectRaw('id_type_asset, id_responsible_person, last_maintenance, count(*)')
                ->whereIn('id_type_asset',$asset_type)
                ->whereNotIn('id_responsible_person',[2,3,4181])
                ->orderBy('id_responsible_person','asc')
                ->get();
        
        $collection = collect($data)->groupBy('id_responsible_person')->toArray();

        return $collection;

        //return ShowDataHandOverAsset::execute($asset_type, $start, $end);
        
    }
}
