<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Config\Users;
use App\Models\Config\Roles;
use App\Models\Masters\Plant;
use Auth;
use Session;
use Validator;
use Hash;
use Excel;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        $list  = Users::orderBy('created_at', 'desc');
        $users = $list->paginate(empty($request->limit)?100:intval($request->limit));
        $params = array(
            'list' => $users,
        );
        return view('user.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $regions  = Plant::whereNull('deleted_at')
                       ->get();
        $roles  = Roles::All();
        $data = [
            'regions' => $regions,
            'roles'   => $roles
        ];
        return view('user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $rules      = array();
        $validator  = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return redirect('user/create')->withErrors($validator);
        } else {
            $file    = $request->file('icon');
            if($file==null){
                $insert = Users::create([
                    'email'             => $request->email,
                    'password'          => bcrypt('123456'),
                    'name'              => $request->name,
                    'mobile'            => $request->mobile,
                    'plant_id'          => $request->region,
                    'id_role'           => $request->role,
                    'nik'               => $request->nik,
                    'isactive'          => 1,
                    'created_by'        => Auth::user()->id,
                    'updated_by'        => Auth::user()->id,
                    'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }else {
                $fileName   = time().'-'.$file->getClientOriginalName();
                $request->file('icon')->storeAs("public/images/user", $fileName);  
                $insert = Users::create([
                    'email'             => $request->email,
                    'password'          => bcrypt('123456'),
                    'name'              => $request->name,
                    'image'             => $fileName,
                    'mobile'            => $request->mobile,
                    'plant_id'          => $request->region,
                    'id_role'           => $request->role,
                    'nik'               => $request->nik,
                    'isactive'          => 1,
                    'created_by'        => Auth::user()->id,
                    'updated_by'        => Auth::user()->id,
                    'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
                
            }
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items  = Users::find($id);
        return view('user.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = Users::find($id);
        $regions  = Plant::whereNull('deleted_at')
                       ->get();
        $roles  = Roles::All();
        $data = [
            'regions' => $regions,
            'roles'   => $roles,
            'items'     =>$items
        ];
        return view('user.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'email'    => 'required',
            'name'    => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('user/'.$id.'/edit')->withErrors($validator);
        } else {
            $file    = $request->file('icon');
            if($file==null){
                $update                 = Users::find($id);
                $update->email          = $request->email;
                $update->name           = $request->name;
                $update->plant_id       = $request->region;
                $update->id_role        = $request->role;
                $update->mobile         = $request->mobile;
                $update->nik            = $request->nik;
                $update->updated_by     = Auth::user()->id;
                $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');
            }else{
                $fileName   = time().'-'.$file->getClientOriginalName();
                $request->file('icon')->storeAs("public/images/user", $fileName);  
                $update                 = Users::find($id);
                $update->email          = $request->email;
                $update->password       = bcrypt('123456');
                $update->name           = $request->name;
                $update->image          = $fileName;
                $update->plant_id       = $request->region;
                $update->id_role        = $request->role;
                $update->mobile         = $request->mobile;
                $update->nik            = $request->nik;
                $update->updated_by     = Auth::user()->id;
                $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');         
            }
            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Error');
            }
            if(Auth::user()->id==1){
                return redirect('user');
            }else{
                return redirect('home');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Users::find($id)->delete();
    	return back();
    }

    public function list()
    {
        return \Role::all();
    }

    /**
     * Show the form for change password the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function changePassword($id)
    {
        $items  = Users::find($id);
        $data = ['items'=>$items];
         return view('user.changePassword', $data);
    }

    /**
     * Show the form for update password the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, $id)
    {
        $items  = Users::find($id);
        $request->validate(
            [
                'oldPassword' => [
                    'required',
                    function($attribute, $value, $fail) use ($items, $request) {
                        if(!Hash::check($request->oldPassword, $items->password)) {
                            return $fail($attribute.' not valid.');
                        }
                    },
                ],
                'newPassword' => 'required',
                'confirmNewPassword' => 'required|same:newPassword'
            ]
        );
        $items->password = Hash::make($request->newPassword);
        $items->save();
        return redirect('user');
    }

    public function downloadTemplate()
    {
        $header = array(
            ['No', 'Name', 'Email', 'Nik', 'Plant', 'Mobile', 'Role'],
            ['1', 'User name', 'user@mail.co.id', '123456', 'bekasi', '0897827637', 'GA']
        );

        $export = new \App\Exports\Excel\ExportFromArray($header);
        return Excel::download($export, 'Upload User.xlsx');
    }

    public function upload(Request $request)
    {
        // Validate Form
        $this->validate($request, [
            'upload_file' => 'required'
        ]);

        $array_excel = new \App\Imports\Excel\ImportToArray();
        $ts = \Maatwebsite\Excel\Facades\Excel::import($array_excel, $request->file('upload_file')->path(), NULL, \Maatwebsite\Excel\Excel::XLSX);
        
        $data = []; $dataOk = true;
        foreach($array_excel->sheetData[$array_excel->sheetNames[0]] as $row) {
            // Validate Data
            $isValid = true;
            $messages = [];            
            // EXCEL VALIDATION ------------------------------------------
            // check User
 
            //cek name
            if(empty($row['name'])){
                $isValid = false;
                array_push($messages,"Name is required");
            }

            //cek email
            if(empty($row['email'])){
                $isValid = false;
                array_push($messages,"Email is required");
            }else{
                $user = Users::where('email', $row['email'])->first();
                if( $user ){
                    $isValid = false;
                    array_push($messages,"Email ".$row['email']." already exists");
                }
            }

            //cek nik
            if(empty($row['nik'])){
                $isValid = false;
                array_push($messages,"NIK is required");
            }else{
                $user = Users::where('nik', $row['nik'])->first();
                if( $user ){
                    $isValid = false;
                    array_push($messages,"NIK ".$row['nik']." already exists");
                }
            }

            //cek plant
            
            if(empty($row['plant'])){
                //$isValid = false;
                //array_push($messages,"Plant is required");
            }else{
                $region = Plant::where('name',$row['plant'])->first();
                if(empty($region)){
                    $isValid = false;
                    array_push($messages,"Plant/region ".$row['plant']." doesn't exists");
                }
            }
            
            // cek mobile
            if(empty($row['mobile'])){
                $isValid = false;
                array_push($messages,"Mobile is required");
            }

            // Role
            if(empty($row['role'])){
                $isValid = false;
                array_push($messages,"Role is required");
            }else{
                $role = Roles::where('name',$row['role'])->first();
                if(empty($role)){
                    $isValid = false;
                    array_push($messages,"Role ".$row['role']." doesn't exists");
                }
            }
            // END OF EXCEL VALIDATION ---------------------------------------

            $data[] = [
                'no'                => $row['no'],
                'name'              => $row['name'],
                'email'             => $row['email'],
                'nik'               => $row['nik'],
                'mobile'            => $row['mobile'],
                'region'            => $region->name ?? Null,
                'plant_id'          => $row['plant'] ?? Null,
                'id_role'           => isset($role) ? $role->id : null,
                'role'              => $row['role'],
                'created_by'        => Auth::user()->id,
                'password'          => bcrypt('123456'),
                'isactive'          => 1,
                'message'           => implode(', ',$messages),
                'valid'             => $isValid,
            ];
            if(!$isValid)
                $dataOk = false;
        }

        $validate = [];
        foreach($data as $d) {
            if($d['valid'])
                $validate[] = $d;
        }
        if(!$dataOk) {
            foreach($data as $d) {
                if(!$d['valid'])
                    $validate[] = $d;
            }
        }
        
        //where there is no error flash session
        if($dataOk){
            Session::flash('dataToInsert', $validate);         
        }
        
        return view('user.validate', ['validate' => $validate, 'dataOk' => $dataOk]);
    }

    public function saveUpload()
    {
        if(Session::has('dataToInsert')){
            $data = Session::get('dataToInsert');

            foreach($data as $k => $v) {
                unset($v['no']);
                unset($v['role']);
                unset($v['message']);
                unset($v['valid']);
                unset($v['region']);
                $data[$k] = $v;
            }

            try {
                \DB::beginTransaction();
                $users = Users::insert($data);
                Session::flash('success', 'Data saved');
                \DB::commit();
            }
            catch(\Exception $ex) {
                \DB::rollBack();
                Session::flash('error', 'ERROR : Data cannot be saved. '.$ex->getMessage()); 
            }           
        }
        return redirect('user');
    }

    public function show_ajax(Request $request){

        if($request->has('q')){
            $search = strtolower($request->q);
            $data = Users::where('name','ilike','%'.$search.'%')
                    ->orWhere('nik','ilike','%'.$search.'%')
                    ->limit(10)
                    ->where('id','!=', 1)
                    ->get();
        }else{
            $data = Users::where('id','!=', 1)->limit(10)->get();
        }
        return $data;
    }
}
