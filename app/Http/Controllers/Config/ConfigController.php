<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Models\Config\ConfigBarcode;
use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;

class ConfigController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }

    public function barcode(Request $request) {
        $config_barcode = ConfigBarcode::all();
        $config = array();
        foreach ($config_barcode as $cb) {
            $config[$cb->key] = $cb->value;
        }

        $view['config'] = $config;

        return view('config.barcode', $view);
    }

    public function barcode_input(Request $request) {
        $config_barcode = ConfigBarcode::all();
        foreach ($config_barcode as $cb) {
            if($cb->key==='height')
                $cb->value = $request->height;
            elseif($cb->key==='width')
                $cb->value = $request->width;
            elseif($cb->key==='margin-top')
                $cb->value = $request->margin_top;
            elseif($cb->key==='margin-left')
                $cb->value = $request->margin_left;
            elseif($cb->key==='margin-bottom')
                $cb->value = $request->margin_bottom;
            elseif($cb->key==='text-below')
                $cb->value = $request->text_below;
            elseif($cb->key==='text-over')
                $cb->value = $request->text_over;
            elseif($cb->key==='measurement')
                $cb->value = $request->measurement;
            elseif($cb->key==='margin-right')
                $cb->value = $request->margin_right;
            elseif($cb->key==='font-size')
                $cb->value = $request->font_size;
            $cb->save();
        }
        return redirect('config/barcode');
    }
}
?>