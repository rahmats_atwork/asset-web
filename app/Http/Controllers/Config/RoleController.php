<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use Auth;
use Session;
use Validator;
use DB;
use App\Http\Requests;
use App\Models\Config\Roles;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class RoleController extends Controller
{
    public function index()
    {
        $list  = Roles::orderBy('created_at', 'desc')->get();
        $params = array(
            'list' => $list,
        );
        return view('role.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('role.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'name'    => 'required|unique:roles,name'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('role/create')->withErrors($validator)->withInput();
        } else {
            $insert = Roles::create([
                'description'       => $request->description,
                'slug'              => $request->name,
                'name'              => $request->name,
                'created_at'        => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'        => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
            if ($insert) {
                Session::flash('success', 'Data Saved');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('role');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $items  = Roles::find($id);
        return view('role.show', compact('items'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $items  = Roles::find($id);
        return view('role.edit', compact('items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'description'    => 'required',
            'name'    => 'required|unique:roles,name,'. $id .'',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('role/'.$id.'/edit')->withErrors($validator);
        } else {
            $update                 = Roles::find($id);
            $update->description    = $request->description;
            $update->slug           = $request->name;
            $update->name           = $request->name;
            $update->updated_at     = Carbon::now()->format('Y-m-d H:i:s');

            $update->save();

            if ($update) {
                Session::flash('success', 'Data Edited');
            } else {
                Session::flash('error', 'Error');
            }
            return redirect('role');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $destroy = Roles::find($id)->delete();
    	return back();
    }

    public function list()
    {
        return \Role::all();
    }
    
    public function show_ajax(Request $request)
    {
        $role_user = Auth::user()->id_role;
        if($request->has('q'))
        {
            if($role_user === 1)
            {
                $search = strtolower($request->q);
                $data = Roles::where('name','ilike','%'.$search.'%')
                ->limit(5)
                ->get();
            }
            else 
            {
                $search = strtolower($request->q);
                $data = Roles::where('id','=',$role_user)
                ->where('name','ilike','%'.$search.'%')
                ->limit(5)
                ->get();
            }
        }
        else
        {
            if($role_user === 1)
            {
                $data = Roles::limit(5)->get();
            }
            else 
            {
                $data = Roles::where('id','=',$role_user)
                ->limit(5)
                ->get();
            }
        }
        return $data;
    }
}
