<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Models\Transaction\Asset;
use App\Models\Masters\Location;
use App\Models\Masters\AssetStatus;
use App\Models\Config\Users;
use App\Models\Transaction\AssetResponsiblePersonHistory;
use App\Models\Transaction\AssetLocationHistory;
use App\Models\Masters\Plant;


class UserModuleController extends Controller
{
    /**
     * Login user
     *
     * @return HttpResponse
     */
    public function loginUser(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'password' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            if (Auth::attempt([
                'email' => $request->email,
                'password' => $request->password,
                'isactive' => 1,
            ])) {
                $user = Auth::user();
                return response()->json([
                    'user' => $user], 200);
            } else {
                return response()->json([
                    'error' => 'Data Not Found !'], 404);
            }
        }
    }
    
    /**
     * Get data asset
     *
     * @return HttpResponse
     */
    public function getDataAsset(Request $request)
    {
        $rules = array(
            'asset_code' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $getData = Asset::whereNull('deleted_at')
                ->with('material')
                ->with('type')
                ->with('supplier')
                ->with('asset_status')
                ->with('childrens')
                ->with('parent_asset')
                ->with('valuation')
                ->with('location')
                ->where('code_asset', $request->asset_code)
                ->first();

            if ($getData) {
                return response()->json([
                    'data' => $getData], 200);
            } else {
                return response()->json([
                    'error' => 'Data Not Found !',
                ], 404);
            }
        }
    }
    /**
     * Get data status asset
     *
     * @return HttpResponse
     */
    public function getDataStatus(Request $request)
    {
        $rules = array(
            'user_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $getData = AssetStatus::Where('code','ilike','%'. $request->search .'%')
                    ->orWhere('name','ilike','%'.$request->search .'%')
                    ->orderBy('code', 'asc')
                    ->get();
            if ($getData) {
                return response()->json([
                    'data' => $getData], 200);
            } else {
                return response()->json([
                    'error' => 'Data Not Found !',
                ], 404);
            }
        }
    }
    /**
     * Get data location warehouse
     *
     * @return HttpResponse
     */
    public function getDataLocation(Request $request)
    {
        $rules = array(
            'user_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {

            $getData = Location::select('id','name','code  as region')
                        ->with('plant_location')
                        ->where('name','ilike','%'.$request->search.'%')
                        ->orderBy('name','asc')
                        ->get();

            if ($getData) {
                return response()->json([
                    'data' => $getData], 200);
            } else {
                return response()->json([
                    'error' => 'Data Not Found !',
                ], 404);
            }
        }
    }
    /**
     * Get data responsible person
     *
     * @return HttpResponse
     */
    public function getDataResponsiblePerson(Request $request)
    {
        $rules = array(
            'user_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $getData = Users::where('name','ilike','%'. $request->search . '%')
                    ->where('id','!=', 1)
                    ->orderBy('id', 'asc')
                    ->get();
            if ($getData) {
                return response()->json([
                    'data' => $getData], 200);
            } else {
                return response()->json([
                    'error' => 'Data Not Found !',
                ], 404);
            }
        }
    }
    /**
     * Set data status
     *
     * @return HttpResponse
     */
    public function setDataStatus(Request $request)
    {
        $rules = array(
            'user_id' => 'required',
            'asset_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $update            = Asset::find($request->asset_id);
            $update->status    = $request->newVal;
            $update->save();
            if ($update->save()) {
                return response()->json([
                    'success' => 'Success update status asset !'], 200);
            } else {
                return response()->json([
                    'error' => 'Failed update status asset !',
                ], 404);
            }
        }
    }
    /**
     * Set data location
     *
     * @return HttpResponse
     */
    public function setDataLocation(Request $request)
    {
        $rules = array(
            'user_id' => 'required',
            'asset_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $insert = AssetLocationHistory::create([
                'id_asset'      => $request->asset_id,
                'address'       => $request->oldVal["address"],
                'latitude'      => $request->oldVal["lat"],
                'longitude'     => $request->oldVal["lng"],
                'building'      => $request->oldVal["building"],
                'unit'          => $request->oldVal["unit"],
                'city'          => $request->oldVal["city"],
                'province'      => $request->oldVal["province"],
                'id_location'   => $request->oldVal["lc_id"],
                'created_by'      => $request->user_id,
            ]);
            if($insert){
                $update                    = Asset::find($request->asset_id);
                $update->building          = $request->newVal["building"];
                $update->unit              = $request->newVal["unit"];
                $update->city              = $request->newVal["city"];
                $update->province          = $request->newVal["province"];
                $update->address_location  = $request->newVal["address"];
                $update->latitude          = $request->newVal["lat"];
                $update->longitude         = $request->newVal["lng"];
                $update->id_location       = $request->newVal["lc_id"];
                $update->save();
                if ($update->save()) {
                    return response()->json([
                        'success' => 'Success update location asset !'], 200);
                } else {
                    return response()->json([
                        'error' => 'Failed update location asset !',
                    ], 404);
                }
            } else {
                return response()->json([
                    'error' => 'Failed update location asset !',
                ], 404);
            }
        }
    }
    /**
     * Set data responsible person
     *
     * @return HttpResponse
     */
    public function setDataResponsiblePerson(Request $request)
    {
        $rules = array(
            'user_id' => 'required',
            'asset_id' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'error' => 'Parameter not completed !',
            ], 404);
        } else {
            $insert = AssetResponsiblePersonHistory::create([
                'id_asset'              => $request->asset_id,
                'responsible_person'    => $request->oldVal,
                'created_by'              => $request->user_id,
            ]);
            if($insert){
                $update                        = Asset::find($request->asset_id);
                $update->responsible_person    = $request->newVal;
                $update->save();
                if ($update->save()) {
                    return response()->json([
                        'success' => 'Success update responsible person !'], 200);
                } else {
                    return response()->json([
                        'error' => 'Failed update responsible person !',
                    ], 404);
                }
            }else{
                return response()->json([
                    'error' => 'Failed update responsible person !',
                ], 404);
            }
        }
    }
}
