<?php
namespace App\Helpers;

use Carbon\Carbon;

class PeriodRange {
    
    public static function call ($start, $end) {

        $a        = Carbon::parse(date('Y-m-d', strtotime($start)));
        $aYM      = date('Y-m', strtotime($start));
        $endfirst = date('Y-m-d H:i:s',strtotime($end));

        

        $range    = [$aYM];

        $now      = Carbon::now()->endOfMonth()->format('Y-m-d');

        $b        = Carbon::parse(date('Y-m-d',strtotime($endfirst)))->subMonth();

        /*
        if($now == $endfirst)  {

            $b    = date('Y-m-d H:i:s', strtotime($endfirst));

        } else {

            $b    = date('Y-m-d',strtotime("+1 months", strtotime($endfirst)));
            
        } 
        */
       
        while ($a < $b) {

            $addMonth  = $a->addMonth();
            $result    = date('Y-m', strtotime($addMonth));
            array_push($range, $result);
            
        }
        
        return $range;
        
    } 
}
?>