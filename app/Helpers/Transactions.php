<?php
namespace App\Helpers;

use App\Models\Masters\TransactionSeries;
use App\Models\Masters\TransactionSeriesLine;
use Carbon\Carbon;

class Transactions {
    
    public static function generateNotransaction($data) {
        $no = '';
        $date = $data['transaction_date'];
        $result =   TransactionSeriesLine::with(['seriesHeader'])
                    ->where('module','=',$data['module'])
                    ->where('ending_date','>=', $date)
                    ->where('starting_date','<=', $date)
                    ->first();

        if ($result->last_no_used == null) {
            $no = $result->starting_no;
        }
        else {
            $year       = date('y');
            $last_code  = $result->last_no_used;
            $docNo      = intval(substr($last_code, strpos($last_code,$year) + 2)) + 1;
            switch (strlen($docNo)) {
                case '1' :
                    $increment  = sprintf("%08d",$docNo);
                break;
                case '2' :
                    $increment  = sprintf("%07d",$docNo);
                break;
                case '3' :
                    $increment  = sprintf("%06d",$docNo);
                break;
                case '4' :
                    $increment  = sprintf("%05d",$docNo);
                break;
                case '5' :
                    $increment  = sprintf("%04d",$docNo);
                break;
                case '6' :
                    $increment  = sprintf("%03d",$docNo);
                break;
                case '7' :
                    $increment  = sprintf("%02d",$docNo);
                break;
                case '8' :
                    $increment  = sprintf("%01d",$docNo);
                break;
            }
            $increment  = sprintf("%08d",$docNo);
            $no         = $result->seriesHeader->code.$year.$increment;
        }
        $results = [
            'docNo'     => $no,
            'result'    => $result,
        ];
        return $results;
    } 
}
?>