<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{
    protected $table = 'filter';
    public $primaryKey = self::ATTRIBUTE_ID;
    protected $fillable = [self::ATTRIBUTE_ID,'user_id','value','page_id'];
}