<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/template', function() {
    return view('template.master');
});

Route::group(['prefix' => 'dashboard','middleware' => 'auth'], function(){
    Route::get('/', function() {
        return view('template.master');
    });
    // Permission
    Route::resource('/role/permissions', 'PermissionController');
    // Role
    Route::get('/role', 'Config\RoleController@index')->name('role.index');
    Route::get('/role/list', 'Config\RoleController@list')->name('role.list');

    
});

Auth::routes();


//role
Route::resource('/role', 'Config\RoleController')->middleware('auth');
Route::resource('/user', 'Config\UserController')->middleware('auth');
Route::get('/changepassword/{id}', 'Config\UserController@changePassword');
Route::put('/changepassword/store/{id}', 'Config\UserController@updatePassword');

// User
Route::get('user/download/template', 'Config\UserController@downloadTemplate');
Route::post('user/upload', 'Config\UserController@upload');
Route::get('user/upload/save', 'Config\UserController@saveUpload');
Route::get('user_ajax', 'Config\UserController@show_ajax')->name('user_ajax');

// Material Group
Route::resource('/materialgroup', 'Masters\MaterialGroupController');
Route::get('material_group_ajax', 'Masters\MaterialGroupController@show_ajax')->name('material_group_ajax');
// Route::get('/materialgroup', 'Masters\MaterialGroupController@index')->name('material_group.index');
// Route::post('/materialgroup/{id}', 'Masters\MaterialGroupController@update')->name('material_group.update');

//classification
Route::get('classification/materialused/{idClass}', 'Masters\ClassificationController@getmaterialused');
Route::post('classification/{id}/parameter', 'Masters\ClassificationController@paramclassific')->name('save.paramclassific');
Route::get('classification/editparam/{id_parameter}', 'Masters\ClassificationController@editparam');
Route::get('classification/{id_clasification}/addparameter', 'Masters\ClassificationController@addparam');
Route::put('editparam/{id_clasification}/{id_parameter}', 'Masters\ClassificationController@updateparam');
Route::delete('classification/paramclassificdestroy/{id_parameter}', 'Masters\ClassificationController@paramclassificdestroy');
Route::resource('classification', 'Masters\ClassificationController');
Route::get('classification_ajax', 'Masters\ClassificationController@show_ajax')->name('classification_ajax');

//material
Route::resource('material', 'Masters\MaterialController');
Route::get('material/assetused/{id}', 'Masters\MaterialController@getassetused');
Route::get('material/assetusedpdf/{id}', 'Masters\MaterialController@getAssetAsedPdf');
Route::delete('material/image/{id}', 'Masters\MaterialController@delete_image');
Route::get('material/classification/{id}.ajax', 'Masters\MaterialController@ajax_parameter')->name('material.classification.ajax');
Route::post('material/{id}/parameter', 'Masters\MaterialController@parameter');
Route::post('material/{id}/image', 'Masters\MaterialController@upload_images')->name('material.images.upload');
Route::get('material/{id}/image', 'Masters\MaterialController@get_images')->name('material.images.list');
Route::get('material/download/template', 'Masters\MaterialController@downloadTemplate');
Route::post('material/upload', 'Masters\MaterialController@upload');
Route::get('material/upload', 'Masters\MaterialController@index');
Route::get('material/upload/save', 'Masters\MaterialController@saveData');
Route::get('material-active', 'Masters\MaterialController@show_active_ajax')->name('material_active');
Route::get('material_ajax', 'Masters\MaterialController@show_ajax')->name('material_ajax');

//master supplier
Route::resource('supplier', 'Masters\SupplierController');
Route::get('autocomplete_supplier_building', 'Masters\SupplierController@autocompletebuilding')->name('autocomplete.supplier.building.ajax');
Route::get('autocomplete_supplier_unit', 'Masters\SupplierController@autocompleteunit')->name('autocomplete.supplier.unit.ajax');
Route::get('supplier_ajax', 'Masters\SupplierController@show_ajax')->name('supplier_ajax');

//setting (Asset Status - Type - Material Group - Retired Reason - ValuationGroup)
Route::resource('assettype', 'Masters\AssetTypeController');
Route::resource('valuationgroup', 'Masters\ValuationGroupController');
Route::resource('statusasset', 'Masters\AssetStatusController');
Route::resource('retiredreason', 'Masters\RetiredReasonController');
Route::get('status_ajax', 'Masters\AssetStatusController@ajaxStatus')->name('status_ajax');
Route::get('retired_reason_ajax', 'Masters\RetiredReasonController@ajaxretiredreason')->name('retired_reason_ajax');
Route::get('assettype-ajax', 'Masters\AssetTypeController@show_ajax')->name('assettype_ajax');
Route::get('groupowner-ajax', 'Config\RoleController@show_ajax')->name('groupowner_ajax');
Route::get('valuationgroup-ajax', 'Masters\ValuationGroupController@show_ajax')->name('valuationgroup_ajax');

//Plant
Route::resource('system/plant', 'Masters\PlantController');

Route::get('plant-active-ajax', 'Masters\PlantController@show_active_ajax')->name('plant_active_ajax');


//asset
//Route::get('update_responsible', 'Transaction\AssetController@update_responsible')->name('update_responsible');
Route::post('asset/{id_asset}/location', 'Transaction\AssetController@storehistoryloc');
Route::post('asset/{id_asset}/person', 'Transaction\AssetController@storehistoryperson');
Route::resource('asset', 'Transaction\AssetController');
Route::get('asset/{id_asset}/classification', 'Transaction\AssetController@classification');
Route::post('asset/{code_asset}/value', 'Transaction\AssetController@storevalue')->name('save.value');
Route::post('asset/upload', 'Transaction\AssetController@upload');
// Route::post('asset/upload/store', 'Transaction\AssetController@uploadStore');
// Route::get('asset/upload/success', 'Transaction\AssetController@uploadStoreSuccess');
Route::get('asset/upload/save', 'Transaction\AssetController@saveData');
Route::get('asset/download/template', 'Transaction\AssetController@downloadTemplate');
Route::post('asset/{id}/value', 'Transaction\AssetController@storevalue');
Route::post('asset/{id}/image', 'Transaction\AssetController@upload_images')->name('asset.images.upload');
Route::get('asset/{id}/image', 'Transaction\AssetController@get_images')->name('asset.images.list');
Route::delete('asset/image/{id}', 'Transaction\AssetController@delete_image');
Route::post('asset/{id}/image360', 'Transaction\AssetController@upload_images360')->name('asset.images360.upload');
Route::get('asset/{id}/image360', 'Transaction\AssetController@get_images360')->name('asset.images360.list');
Route::delete('asset/image360/{id}', 'Transaction\AssetController@delete_image360');
Route::get('asset/material/{id}.ajax', 'Transaction\AssetController@ajax_description')->name('asset.material.ajax');
Route::get('autocomplete', 'Transaction\AssetController@autocomplete')->name('autocomplete.ajax');
Route::get('superior_asset_ajax', 'Transaction\AssetController@ajaxSuperiorAsset')->name('superior_asset_ajax');
Route::get('export/asset', 'Transaction\AssetController@export');
Route::get('savefilter/asset', 'Transaction\AssetController@savefilter');
Route::get('barcode/asset', 'Transaction\AssetController@tes_pdf');
Route::get('print_barcode/asset/{id}', 'Transaction\AssetController@one_pdf');
Route::post('asset/{id_asset}/area', 'Transaction\AssetController@area');
Route::get('asset_ajax', 'Transaction\AssetController@show_ajax')->name('asset_ajax');
Route::get('asset-stock', 'Transaction\AssetController@assetStock')->name('asset-stock');

// asset-refund
Route::get('asset-refund', 'Transaction\AssetController@refund');
Route::post('refund-ajax', 'Transaction\AssetController@refundAjax')->name('refund_ajax');
Route::post('refund-ajax-not', 'Transaction\AssetController@refundAjaxNot')->name('refund_ajax_not');
Route::get('export-refund/{id}', 'Transaction\AssetController@exportRefund')->name('export_refund');
Route::post('asset-refund-ajax', 'Transaction\AssetController@assetRefundAjax')->name('asset-refund-ajax');


// asset-handover
Route::resource('asset-handover', 'Transaction\AssetHandoverController');
//Route::get('asset-handover-create', 'Transaction\AssetController@assetHandovercreate');
Route::post('asset-handover-store-header', 'Transaction\AssetHandoverController@storeHeader')->name('asset-handover-store-header');
Route::post('asset-handover-store-line', 'Transaction\AssetHandoverController@storeLine')->name('asset-handover-store-line');
Route::delete('asset-handover-remove-line/{id}', 'Transaction\AssetHandoverController@destroyLine')->name('asset-handover-remove-line');
Route::post('asset-handover-update-header', 'Transaction\AssetHandoverController@updateHeader')->name('asset-handover-update-header');
Route::get('asset-handover-printed', 'Transaction\AssetHandoverController@assetHandover')->name('asset-handover-printed');
Route::get('asset-handover-pdf/{id}', 'Transaction\AssetHandoverController@pdfHandover')->name('asset-handover-pdf');
Route::get('asset-handover-document', 'Transaction\AssetHandoverController@documentList')->name('asset-handover-document');
Route::get('test2', 'Transaction\AssetHandoverController@test2');
/*
Route::post('refund-ajax', 'Transaction\AssetController@refundAjax')->name('refund_ajax');
Route::post('refund-ajax-not', 'Transaction\AssetController@refundAjaxNot')->name('refund_ajax_not');
Route::get('export-refund/{id}', 'Transaction\AssetController@exportRefund')->name('export_refund');
Route::post('asset-refund-ajax', 'Transaction\AssetController@assetRefundAjax')->name('asset-refund-ajax');
*/
# Location Type
Route::resource('locationtype', 'Masters\LocationTypeController');
Route::get('location_type_ajax', 'Masters\LocationTypeController@show_active_ajax')->name('location_type_ajax');

# Location
Route::resource('location', 'Masters\LocationController');
Route::get('locationbuild', 'Masters\LocationController@autocomplete')->name('completebuilding.ajax');
Route::post('location_ajax', 'Masters\LocationController@show_ajax')->name('location_ajax');
Route::get('/location/delete/{id}', 'Masters\LocationController@destroy');
Route::get('barcode/location', 'Masters\LocationController@print_barcode');
Route::get('location-active-ajax', 'Masters\LocationController@show_active_ajax')->name('location_active_ajax');
Route::get('location/savefilterlocation','Masters\LocationController@saveFilter');
Route::post('/get_location', 'Masters\LocationController@locationPlant')->name('get_location');
Route::post('/get_map', 'Masters\LocationController@locationMap')->name('get_map');

//home
Route::get('/home', 'HomeController@index')->name('home');


// Select 2 Ajax Material
Route::get('config/barcode', 'Config\ConfigController@barcode');
Route::post('config/barcode/input', 'Config\ConfigController@barcode_input');
Route::get('test_depre', 'Masters\DepreciationController@calculateDepreciation2');
Route::resource('/depreciationtype', 'Masters\DepreciationTypeController');
Route::resource('/depreciation', 'Masters\DepreciationController');
Route::get('depreciation-type-ajax', 'Masters\DepreciationController@ajaxDepreciationtypeActive')->name('depreciation_type_ajax');
Route::get('depreciation-ajax', 'Masters\DepreciationController@ajaxDepreciationActive')->name('depreciation_ajax');

# Report


//Sentry Test
Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});



#Report Asset
Route::get('report/periodic-movement', 'Transaction\ReportController@PeriodicMovement');
Route::get('report/asset-handover', 'Transaction\ReportController@AssetHandover');
Route::get('report/stock-card', 'Transaction\ReportController@StockCard');
Route::get('report/expiry-usage', 'Transaction\ReportController@ExpiryUsage');
Route::get('data-graph-periodic', 'Transaction\ReportController@DataGraphPeriodicAsset')->name('data-graph-periodic');
Route::get('data-excel-periodic', 'Transaction\ReportController@DataExcelPeriodicAsset')->name('data-excel-periodic');
Route::get('data-asset-handover', 'Transaction\ReportController@ShowDataAssetHandover')->name('data-asset-handover');
Route::get('report/data-expiry-usage', 'Transaction\ReportController@ShowDataExpiryUsage')->name('data-expiry-usage');
