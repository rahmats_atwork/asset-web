<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Login User API
Route::group(['prefix' => 'auth', 'namespace' => 'Api'], function () {
    Route::post('/loginUser', 'UserModuleController@loginUser');
    Route::post('/getDataAssets', 'UserModuleController@getDataAssets');
    Route::post('/getDataAsset', 'UserModuleController@getDataAsset');
    Route::post('/getDataStatus', 'UserModuleController@getDataStatus');
    Route::post('/getDataLocation', 'UserModuleController@getDataLocation');
    Route::post('/getDataResponsiblePerson', 'UserModuleController@getDataResponsiblePerson');
    Route::post('/setDataStatus', 'UserModuleController@setDataStatus');
    Route::post('/setDataLocation', 'UserModuleController@setDataLocation');
    Route::post('/setDataResponsiblePerson', 'UserModuleController@setDataResponsiblePerson');
    // Route::post('/logoutUser', 'UserModuleController@logoutUser');
});

Route::get('report-analyst-asset', 'Transaction\AssetController@ReportPeriodicAsset')->name('report-analyst-asset');