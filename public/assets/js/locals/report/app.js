function showGraph(baseurl,start,end,transaction,assettype) {
    var dataSeries  = [];
    var colorChart  = ['#F08080','#F05082','#4C8BF5','#F08080','#48B164','#DAC73D','#1D2B36','#593A05','#F38630','#86F330','#8630F3','#F03085','#791843','#F05082','#F03085','#F08080','#F05082','#F03085','#F08080','#F05082','#4C8BF5','#F08080','#48B164','#DAC73D','#1D2B36','#593A05','#F38630','#86F330','#8630F3','#F03085','#791843','#F05082','#F03085','#F08080','#F05082','#F03085'];
    
    var json   = {startPeriode : start,endPeriode : end, choose : transaction, type_asset: assettype };
   
    $.ajax({
        dataType: 'JSON',
        type:'GET',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseurl,
        data:json,
        success : function (query){
            var dataRes = null;

            for (i = 0;i < query.length; i++ ) {
                var dataPointer = [];
                for (a = 0;a < query[i].assetresult.length; a++) {
                    dataPointer.push({ x: new Date (Date.parse(query[i].assetresult[a].x))
                        , y: query[i].assetresult[a].y })
                }
                
                dataRes =  {
                    type: "line",
                    showInLegend: true,
                    name: query[i].assettype,
                    markerType: "circle",
                    xValueFormatString: "MMM- YYYY",
                    color: colorChart[i],
                    dataPoints: dataPointer
                }

                dataSeries.push(dataRes);
                
            }

            //console.log(dataSeries);
            showChart(dataSeries)
        },
        error : function(e){
            window.alert("error show chart !!. Message error :" + e );
        }
    })
}    

function showChart(resultSearch) {


    var course      = [];
    var uniqueLabels= {};
    var distinct    = [];

    const options = {
        animationEnabled: true,
        theme: "light2",
        title:{
            text: "",
            display: false,
        },
        axisX:{
            valueFormatString: "MMM YYYY"
        },
        axisY: {
            title: "Total",
            minimum: 0
        },
        toolTip:{
            shared:true
        },  
        legend:{
            cursor:"pointer",
            verticalAlign: "bottom",
            horizontalAlign: "left",
            dockInsidePlotArea: true,
            itemclick: toogleDataSeries
        },
        data: resultSearch
    };

    $("#chartAsset").CanvasJSChart(options);
    
    
    $("#graphAsset").show();

}

function toogleDataSeries(e){
    if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    } else{
        e.dataSeries.visible = true;
    }
    e.chart.render();
}

function showData(baseurl,start,end,transaction,assettype) {

    var json   = {startPeriode : start,endPeriode : end, choose : transaction, type_asset: assettype };
   
    $.ajax({
        dataType: 'JSON',
        type:'GET',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseurl,
        data:json,
        success : function (query){
            var row = null;
            
            for (i = 0;i < query.length; i++ ) {
                row = "<tr><td>" + query[i].asset_type + "</td><td>" + query[i].responsible + " - " + query[i].nik_responsible + "</td><td>" + query[i].sum + "</td><td>" + query[i].date_transaction + "</td></tr>"
                $("#tableasset").append(row);
            }
        },
        error : function(e){
            window.alert("error show excel !!. Message error :" + e );
        }
    })
}

function showDataExpiry(baseurl,start,end) {

    var json   = {from_date : start, to_date : end};
   
    $.ajax({
        dataType: 'JSON',
        type:'GET',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseurl,
        data:json,
        success : function (query){
            
            var result = Object.keys(query).map(function (key) { 
                return [query[key]]; 
            }); 
            $("#expiryTable").empty();
            if (result.length === 0) {
               var row = "<tr><td colspan = '4' style='text-align:center;'> Data not found. </td></tr>";
               $("#expiryTable").append(row);
            } else {
                for (i = 0;i < result.length; i++ ) {
                    var rows = result[i][0].length;
                    for (b = 0; b < rows; b++) {
                        var row = null;
                        //console.log(result[i][0][b].responsible_person_asset.nik);
                        
                        if (b == 0) {
                            row = "<tr><td rowspan='"+ rows +"'>" + 
                            result[i][0][b].responsible_person_asset.nik + " - " + 
                            result[i][0][b].responsible_person_asset.name + "</td><td>" + 
                            result[i][0][b].next_maintenance + "</td><td>" + 
                            result[i][0][b].material.description + "</td><td>" + 
                            result[i][0][b].count + " Unit </td></tr>";
                        } else {
                            row = "<tr><td>" + 
                                result[i][0][b].next_maintenance + "</td><td>" + 
                                result[i][0][b].material.description + "</td><td>" + 
                                result[i][0][b].count + " Unit </td></tr>";
                        }
                        //console.log(row);
                        $("#expiryTable").append(row);
                        
                    }
                }
            }
        },
        error : function(e){
            console.log(e);
            window.alert("error show data !!. Message error :" + e );
        }
    })
}

function showDataHandover(baseurl, start, end, assettype) {

    var json   = {from_date : start, to_date : end, type_asset : assettype};
   
    $.ajax({
        dataType: 'JSON',
        type:'GET',
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        url: baseurl,
        data:json,
        success : function (query){
            $("#tableasset").empty();
            
            var result = Object.keys(query).map(function (key) { 
                return [query[key]]; 
            }); 
            $('#collapsedbutton').click();
            if (result.length === 0) {
               var row = "<tr><td colspan = '4' style='text-align:center;'> Data not found. </td></tr>";
               $("#expiryTable").append(row);
            } else {

                for (i = 0;i < result.length; i++ ) {
                    var rows = result[i][0].length;

                    for (b = 0; b < rows; b++) {
                        if (b == 0) {
                         
                            row = "<tr><td rowspan='"+ rows +"'> <b>" + 
                                result[i][0][b].responsible_person_asset.nik + " - " + 
                                result[i][0][b].responsible_person_asset.name + "</b></td><td>" + 
                                result[i][0][b].type.name + "</td><td>" + 
                                result[i][0][b].last_maintenance + "</td><td>" + 
                                result[i][0][b].count + " Unit </td></tr>";
                        
                        } else {
                            row = "<tr><td>" + 
                                result[i][0][b].type.name + "</td><td>" + 
                                result[i][0][b].last_maintenance + "</td><td>" + 
                                result[i][0][b].count + " Unit </td></tr>";
                        }
                        $("#tableasset").append(row);
                    }
                    
                }
            
            }

        },
        error : function(e){
            console.log(e);
            window.alert("error show data !!. Message error :" + e );
        }
    })
}
/*
[{
    type: "line",
    showInLegend: true,
    name: "Projected Asset Trend Analyst",
    markerType: "square",
    xValueFormatString: "DD MMM, YYYY",
    color: "#F08080",
    yValueFormatString: "#,##0K",
    dataPoints: [
        { x: new Date(2017, 10, 1), y: 63 },
        { x: new Date(2017, 10, 2), y: 69 },
        { x: new Date(2017, 10, 3), y: 65 },
        { x: new Date(2017, 10, 4), y: 70 },
        { x: new Date(2017, 10, 5), y: 71 },
        { x: new Date(2017, 10, 6), y: 65 },
        { x: new Date(2017, 10, 7), y: 73 },
        { x: new Date(2017, 10, 8), y: 96 },
        { x: new Date(2017, 10, 9), y: 84 },
        { x: new Date(2017, 10, 10), y: 85 },
        { x: new Date(2017, 10, 11), y: 86 },
        { x: new Date(2017, 10, 12), y: 94 },
        { x: new Date(2017, 10, 13), y: 97 },
        { x: new Date(2017, 10, 14), y: 86 },
        { x: new Date(2017, 10, 15), y: 89 }
    ]
},
{
    type: "line",
    showInLegend: true,
    name: "Actual Sales",
    lineDashType: "dash",
    yValueFormatString: "#,##0K",
    dataPoints: [
        { x: new Date(2017, 10, 1), y: 60 },
        { x: new Date(2017, 10, 2), y: 57 },
        { x: new Date(2017, 10, 3), y: 51 },
        { x: new Date(2017, 10, 4), y: 56 },
        { x: new Date(2017, 10, 5), y: 54 },
        { x: new Date(2017, 10, 6), y: 55 },
        { x: new Date(2017, 10, 7), y: 54 },
        { x: new Date(2017, 10, 8), y: 69 },
        { x: new Date(2017, 10, 9), y: 65 },
        { x: new Date(2017, 10, 10), y: 66 },
        { x: new Date(2017, 10, 11), y: 63 },
        { x: new Date(2017, 10, 12), y: 67 },
        { x: new Date(2017, 10, 13), y: 66 },
        { x: new Date(2017, 10, 14), y: 56 },
        { x: new Date(2017, 10, 15), y: 64 }
    ]
}]
*/