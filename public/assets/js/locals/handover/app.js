$('#tabs_2').on('click',function() {
    var doc = $('input[name="document_no"]').val();
    if(doc == null || doc == '') {
        return false;
    }
    else {
        return true;
    }
})

/* Create / Store Data Asset Handover */
function saveOrupdateHeader() {
    var base_url    = window.location.origin;
    var person      = $('select[name="responsible_person"]').val();
    var date        = $('input[name="date_handover"]').val();
    var description = $('input[name="description"]').val();
    var document    = $('input[name="document_no"]').val();
    var documentID  = $('input[name="id"]').val();

    var json = {
            responsible_person:person,
            date_handover:date,
            description:description,
            document_no:document,
    };

    if (person === null||date === null){
            window.alert("Hand Over Date / Responsible Person is Required");
            return false;
    }

    if (document != '') {
        $.ajax({
            dataType : 'JSON',
            type:'POST',
            url: base_url + '/asset-handover-update-header',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : json,
            success : function (data){
                if (data.result == true){
                    $('#tabs_2').click();                   
                }
                else if (data.result == false){
                    window.alert('Failed to get data');
                    console.log(data.message);
                }
            },
            error : function(e){
                window.alert("Fail save data. Message error :" + e );
            }
        })
    }
    else {
        $.ajax({
            dataType : 'JSON',
            type:'POST',
            url: base_url + '/asset-handover-store-header',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : json,
            success : function (data){
                if (data.result == true){
                    var resp = $('select[name="responsible_person"]').text();
                    var idx     = resp.indexOf(" - ") + 2;
                    var tex = resp.substring(idx);
                    $('select[name="responsible_person"]').attr('disabled', true);
                    $('input[name="date_handover"]').attr('disabled', true);
                    $('input[name="document_no"]').val(data.message);
                    $('input[name="id"]').val(data.message);
                    var markup =    "<tr><td><h3 style='font-size: 18px;'> Document No. : " + data.documentNo + " - ( " + tex + " ) </h3></td></tr>";
                    $("#header_document").append(markup);
                    $("#document_no_txt").text("Document Handover No. = " + data.documentNo);
                    $("#responsible").text("Responsible Name = " + tex );
                    $('#tabs_2').click();
                }
                else if (data.result == false){
                    window.alert('Failed to get data');
                    console.log(data.message);
                }
            },
            error : function(e){
                window.alert("Fail save data. Message error :" + e );
            }
        })
    }
}

/* Create / Store Data Line Asset Handover */
function storeLine() {
    var base_url  = window.location.origin;
    var documentNo  = $('input[name="id"]').val();
    var assetID     = $('select[name="asset"]').val();
    var assetName   = $('select[name="asset"] option:selected').text();

    var jsonLine  = { asset:assetID,document:documentNo };

    if (assetID === '' || assetID == null) {
        window.alert("Choose Asset !!");
        return false;
    }

    if (assetID === '' || assetID == null) {
        window.alert("Choose Asset !!");
        return false;
    }
    else {
        $.ajax({
            dataType : 'JSON',
            type:'POST',
            url: base_url + '/asset-handover-store-line',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data : jsonLine,
            success : function (data){
                if (data.result == true){
                    //console.log(data.message);
                    var markup = "<tr id='" + data.message + "'><td>" + assetName + "</td>" + "<td><button type='button' class='btn btn-danger' onclick='deleteLine("+ data.message +")'><i class='mdi mdi-delete'></i></button></td></tr>"
                    $("#data-asset").append(markup);
                    $('select[name="asset"]').empty();
                }
                else if (data.result == false){
                    window.alert("Error Message : " + data.message );
                }
            },
            error : function(e){
                window.alert("Save Data Fail. Error Message : " + e );
            }
        })
    }
}

/* Delete Data Line Asset Handover */
function deleteLine(line) {
    var token     = {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')};
    var base_url  = window.location.origin;
    $.ajax({
        dataType : 'JSON',
        type:'delete',
        url: base_url + '/asset-handover-remove-line/' + line ,
        data : {_method:'delete', _token:$('meta[name="csrf-token"]').attr('content')},
        success : function (data){
            $('tr[id="' + line +'"]').remove();
            //console.log(data.message);
        },
        error : function(e){
            window.alert("Save Data Fail. Error Message : " + e );
        }
    })
}

/* Delete Data Line Asset Handover */
function printDocument() {
    var token       = {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')};
    var document    = $('input[name="id"]').val();
    var base_url    = window.location.origin;
    $.ajax({
        dataType : 'JSON',
        type:'get',
        url: base_url + '/asset-handover-printed',
        data : {id:document},
        success : function (data){
            if (data.result == false){
                alert(data.asset);                  
            }
            else {
                window.open(base_url + "/asset-handover-pdf/" + document, "_blank");
                setInterval(function(){ window.location.href = base_url + "/asset-handover"; }, 1500);
            }

        },
        error : function(e){
            window.alert("Save Data Fail. Error Message : " + e );
        }
    })
}
