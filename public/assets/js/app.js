$(document).ready(function(){

  var base_url = '../..'

  $("#detail_info_images").hide();
  $("#show_hide_detail_info_images").click(function(){
        $("#detail_info_images").toggle(1000);
  });

  $( ".datepicker" ).datepicker({
    autoclose: true,
    format: "dd-mm-yyyy",
    todayHighlight: true,
    orientation: "top auto",
    todayBtn: true,
    todayHighlight: true,
  });

  $(".textarea_editor").wysihtml5({
    toolbar: {

    "image": false, // Button to insert an image.

  }
  });

});